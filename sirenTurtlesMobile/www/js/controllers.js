angular.module('starter.controllers', ['starter.services'])

.controller('AppCtrl', function($scope,$cordovaSQLite, $ionicLoading,$timeout,$state,$ionicPopup,$translate,$rootScope,UserDataService,$ionicHistory,$http) {
  $scope.$on('$ionicView.enter', function(e) {
        var t = new Date();
        var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
       // console.log(t.toLocaleString("fr-FR",options));
        var segment = {nom:"d453-c221",dateAdd:t};
       // UserDataService.createUser(user);
        
         //console.log(data);
        // console.log(new Date());
          //$scope.user= data[0];

        //})

        });
    
  $scope.language = {item:""};
  $scope.showPopup = function() {
                            
                            var titre=$translate.instant('choix de la langue');
                            var annu=$translate.instant('annuler');
                            var sav=$translate.instant('enregistrer');
                            var myPopup = $ionicPopup.show({
                              templateUrl: 'templates/choix_langue.html',
                              title: titre,
                              scope: $scope,
                              buttons: [
                                { text: annu },
                                {
                                  text: sav,
                                  type: $scope.utilisateur.mode,
                                  onTap: function(e) {
                                    if (!$scope.language.item) {
                                      
                                      e.preventDefault();
                                    } else {
                                      $translate.use($scope.language.item);
                                      $rootScope.utilisateur.lang=$scope.language.item;
                                      $state.reload();
                                      return $scope.language.item;
                                    }
                                  }
                                }
                              ]
                            });
                              $timeout(function() {
                                myPopup.close();
                              }, 20000);
  };
  $scope.profil = function(){
        $state.go('app.profil');   
    };
  $scope.logout = function(){
        $ionicHistory.clearCache().then(function(){ $state.go('login') });  
    };
  $scope.theme=function(){
    if($scope.utilisateur.mode=="jour"){
      $scope.utilisateur.mode="nuit";
      $scope.utilisateur.arriere="arrierenuit";
    }
    else{
      $scope.utilisateur.mode="jour";
      $scope.utilisateur.arriere="arrierejour";
    }
  }
  $scope.synchro=function(){
    $ionicLoading.show({
      template: 'Loading...',
      duration: 3000
    }).then(function(){
      //console.log("requette effectuer");
    });
    UserDataService.getAllPatrouilleSynchro(function(data){
    var i = 0;
    console.log(data);
    var patrouille={};
    var tableau=[];
    var id=-1;
    var numpat = -1;
    var numobser = 0;
    var result = {};
    while(i < data.length){
    var etat ={etat:1,id:data[i].patrouille_id};
    //data[i].patrouilleur_id= 2;
   // data[i].segment_nom=data[i].nom;
    //data[i].type_id=1;
    var cmpt= data[i].patrouille_id;

    if(cmpt!=id){
      numpat++;
      result[numpat]={};
      result[numpat]["id"]=cmpt;
      result[numpat]["patrouilleur_id"]=2;
      result[numpat]["etat"]=1;
      result[numpat]["segment_nom"]=data[i].nom;
      result[numpat]["nomSite"]=data[i].nomSite;
      result[numpat]["dateDebut"]=data[i].dateDebut;
      result[numpat]["dateFin"]=data[i].dateFin;
      result[numpat]["coordXDebut"]=data[i].coordXDebut;
      result[numpat]["coordYDebut"]=data[i].coordYDebut;
      result[numpat]["coordXFin"]=data[i].coordXFin;
      result[numpat]["coordYFin"]=data[i].coordYFin;
      result[numpat]["type_id"]=1;
      
      id=cmpt;
      numobser =0;
      result[numpat]["observations"] = {};
      //console.log(result);
    }
    
    result[numpat]["observations"][numobser] = {};
    result[numpat]["observations"][numobser]["id"]=data[i].id;
    result[numpat]["observations"][numobser]["etat"]=1;
    result[numpat]["observations"][numobser]["coordX"]=data[i].coordX;
    result[numpat]["observations"][numobser]["coordY"]=data[i].coordY;
    result[numpat]["observations"][numobser]["dateAdd"]=data[i].dateAdd;
    result[numpat]["observations"][numobser]["categorie_id"]=data[i].categorie_id;
    
    numobser++;
    UserDataService.updateEtatPatrouille(etat);
    i++;
    }
    //console.log(JSON.stringify(result));
    var data1 = {patrouilles: JSON.parse(JSON.stringify(result))};
   // var observations=[];
    //console.log(data1);
    $http.post('http://localhost/siren/web/fr/mobile/synchro/patrouille',
            data1
        ).success(function(result) {
          console.log("Auth.signin.success!")
          console.log(result);
      }).error(function(data, status, headers, config) {
          console.log("Auth.signin.error!")
          console.log(data);
          console.log(status);
          console.log(headers);
          console.log(config);
      });
    
    $state.reload();
  });
  }
  $scope.about=function(){
    $state.go('app.about');
  }
})

.controller('voir_patrouilleCtrl', function($scope,$state,UserDataService,$stateParams) {

  UserDataService.getAllPatrouille(function(data){
    var id = $stateParams.id
    $scope.affi_patrouille = data[id];
  });

})
.controller('PlaylistsCtrl', function($scope,$state,$timeout,$ionicPopup,$ionicPlatform,$ionicHistory,$translate,$cordovaGeolocation,$rootScope,UserDataService) {
  var coord={coordx:'',coordy:''}
  //console.log($rootScope.utilisateur.lang);
  $ionicPlatform.ready(function () {
      var posOptions = {timeout: 20000, enableHighAccuracy: false};
      
      $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
        coord.coordx  = position.coords.latitude;
        coord.coordy = position.coords.longitude;
        $scope.lat=coord.coordx;
        $scope.long=coord.coordy;
        var point1 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); 
        var point2 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      //$scope.long = google.maps.geometry.spherical.computeDistanceBetween(point1,point2);
      }, function(err) {
        //console.log(err);
        //if(err.code==1){ 
          $scope.showAlert();
        //}
      });      

  });
$scope.$on('$ionicView.enter', function(e) {
  $scope.patrouillef();
});
$scope.voir_patrouille=function(id_p){
  var t = id_p - 1;
  $state.go('app.voir_patrouille',{id:t});
    //$state.reload();

}
//#################"START AFFICHE PLAYLISTS"####################
$scope.patrouillef= function (){
  $scope.boucle={};
  $scope.patrouille={};
  $rootScope.utilisateur.t =1;
  UserDataService.getAllPatrouille(function(data){
    var options = {weekday: "long", year: "numeric", month: "long", day: "numeric"};
    for (i = 0, max = data.length; i < max; i++) { 
      var dd = new Date();
      dd.setTime(data[i].dateDebut);
      data[i].dateAffiche=dd.toLocaleDateString($rootScope.utilisateur.lang,options);
      }
      $scope.pat_affiche_observ(data);
  });
  //
  //console.log($rootScope.utilisateur.t);
  
}
$scope.pat_affiche_observ =function(data){
  for(i=0,max = data.length;i < max; i++){
    $scope.recherche_obser(i,data[i].id);
  }
  $scope.patrouille=data;
}

$scope.recherche_obser=function(i,id){
  UserDataService.getAllObservation(id,function(obs){
    $scope.patrouille[i].taille= obs.length;
   // console.log($scope.result);
              });
  
};
//######################END AFFICHE##########################3

$scope.showAlert = function() {
        var alertPopup = $ionicPopup.alert({
        title: 'Attention',
       template: $translate.instant('Veillez activer la geolocalisation !')
        });
         alertPopup.then(function(res) {
          $ionicPlatform.ready(function () {
             var posOptions = {timeout: 20000, enableHighAccuracy: false};
             $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
                $scope.lat  = position.coords.latitude;
                $scope.long = position.coords.longitude;
                var point1 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); 
                var point2 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              //$scope.long = google.maps.geometry.spherical.computeDistanceBetween(point1,point2);
              }, function(err) {
                //$scope.showAlert();
              });      
          });
        });
};
  
  $scope.data={seg:'',tron:'',nseg:"",id:''};

  $scope.commencer=function (){
      $state.go('app.detail');
  };
  $scope.showPopup = function() {
              console.log($translate.proposedLanguage())

                            UserDataService.getAllSegment(function(data){
                                $scope.segAll=data;
                                //console.log(data);
                            });
                            var titre=$translate.instant('choix du lieu');
                            var annu=$translate.instant('annuler');
                            var sav=$translate.instant('depart');
                            var myPopup = $ionicPopup.show({
                              templateUrl: 'templates/choix_troncon.html',
                              title: titre,
                              scope: $scope,
                              buttons: [
                                { text: annu },
                                {
                                  text: sav,
                                  type: $scope.utilisateur.mode,
                                  onTap: function(e) {
                                    if ((!$scope.data.tron)||((!$scope.data.seg)&&($scope.data.nseg==""))) {
                                      //don't allow the user to close unless he enters wifi password
                                      e.preventDefault();
                                    } else {
                                      if($scope.data.nseg!=""){
                                        $scope.data.seg=$scope.data.nseg;
                                        $scope.data.nseg='';
                                      }
                                      
                                      $rootScope.pat_tmp.segment=$scope.data.seg;
                                      
                                      $rootScope.pat_tmp.patrouilleur=$rootScope.utilisateur.id;
                                      $rootScope.pat_tmp.nomSite=$scope.data.tron;
                                      var dd = new Date()
                                      $rootScope.pat_tmp.date= dd.getTime();
                                      $rootScope.duree.h=0;
                                      $rootScope.duree.m=0;
                                      $rootScope.pat_tmp.coordx=coord.coordx;
                                      $rootScope.pat_tmp.coordy=coord.coordy;
                                      $scope.data.seg='';
                                      $scope.data.tron='';
                                      $state.go('app.detail');
                                    }
                                  }
                                }
                              ]
                            });
                              $timeout(function() {
                                myPopup.close();
                              }, 60000);
  };

  
})

.controller('RecapitulatifCtrl', function($scope,$state, $ionicHistory,UserDataService) {
      $scope.patrouille={};
      $scope.$on('$ionicView.enter', function(e) {
          UserDataService.getAllPatrouille(function(data){
            $scope.patrouille=data.pop();
            return $scope.patrouille;
          })
      });   
           $scope.terminer = function () {
                $ionicHistory.clearHistory();
                $ionicHistory.nextViewOptions({
                    disableBack: true,
                    historyRoot: true
                });
                $state.go('app.profil');
      
  };
})

.controller('DetailCtrl', function($scope,$state,UserDataService,$ionicModal,$cordovaCamera,$rootScope,$cordovaGeolocation,$ionicPlatform,$timeout) {
  $scope.question={test:"t",texte:''};
  var trace;
  var saut;
  var saut_id;
  $scope.i;
  $scope.donne='';
  $scope.reponse='';
  $scope.result="";
  $scope.result1={};
  $scope.result1.reponseText='';
  var i;
  var tmp;
  var reponse={};

  var tab_reponse=[];
  var observation={};
  var patrouille=[];
  $scope.list_observation=[];           
  $scope.solu=function(id){
     UserDataService.getReponseId(id,function(data){
          $scope.result=data[0];
          //console.log($scope.result.question_suivante_id);
        });   
  }

  function reset(id){
  $scope.question={test:"t",texte:'',taille:''};
  $scope.i=0;
  $scope.donne='';
  $scope.reponse='';
  saut=true;
  tab_reponse=[];
  observation={};
  }
  $scope.trace=function(){
    saut=true;
    i=0;
    ques(1);
  }
  $scope.carcasse=function(){
    saut=true;
    i=0;
    ques(2);
  }
  $scope.nid=function(){
    i=0;
    saut=true;
    ques(3);
  }
  $scope.individu=function(){
    i=0;
    saut=true;
    ques(4);
  }
  function ques(id){
    UserDataService.getQuestion(id,function(data){
        $scope.question.test=data[0].type;
        if($rootScope.utilisateur.lang == 'fr'){
          $scope.question.texte=data[0].titre_fr;         
        }
        else{
          $scope.question.texte=data[0].titre_en;
        }
        $scope.donne=data;
        //$scope.i=1;
        observation.categorie =id;
        observation.coordX=1;
        observation.coordY=1;
        var dd = new Date();
        observation.dateAdd= dd.getTime();
        rep(data[0].id);
        
        });   
  }

  function ques_id(id){
    UserDataService.getQuestionId(id,function(data){
        $scope.question.test=data[0].type;
        if($rootScope.utilisateur.lang == 'fr'){
          $scope.question.texte=data[0].titre_fr;         
        }
        else{
          $scope.question.texte=data[0].titre_en;
        }
        $scope.donne=data;
        //$scope.i=1;
        rep(data[0].id);
        
        });   
  }

  function rep(id){
    UserDataService.getReponse(id,function(data){

      if(angular.isDefined(data[0])==true){
        if(data[0].valeur_fr=="Photo"){
        $scope.question.test="pho";
        }
      }
      else{
        $scope.question.test="ter";

        //reset(1);
      }
      
      //console.log(id);
      if($rootScope.utilisateur.lang == 'en'){   
         for (var i = 0; i < data.length; i++) {
            data[i].valeur_fr=data[i].valeur_en;         
           }
        }
      $scope.reponse=data;

      if(data.length==1){
        $scope.result=data[0];
      }
    });
  }

  $scope.suivant=function(){
    
    if($scope.result!=""){
      reponse.question=$scope.result.question_id;
      reponse.reponse=$scope.result.id;
      reponse.reponseText=$scope.result1.reponseText;
      var dd =new Date();
      reponse.dateAdd= dd.getTime();
      //console.log(reponse);
      tab_reponse.push(reponse);
      reponse={};
      //console.log(tab_reponse);
      $scope.result1.reponseText=''

      if((angular.isNumber($scope.result.question_suivante_id))&&saut){
        ques_id($scope.result.question_suivante_id);  
        $scope.result=""; 
       // console.log(saut);      
      }
      else{
        //console.log("saut");          
        fn_saut(i,$scope.result.saut_id);
      }
    }
  }

  function fn_saut(numero,id_saut){
    if(numero==0){
      tmp=id_saut;
    }
//console.log(i);
    UserDataService.getQuestionBySaut(tmp,function(data){
        if(numero == (data.length -1)){
          ques_id(data[i].question_id);
          //console.log("dernier element");
          saut=true;
          i=0;
        }
        else{
          //console.log("element");
          ques_id(data[i].question_id);
          i=i+1;
          saut=false;
        }
        });   
  }
$scope.imgURI=[];
  $scope.takePhoto = function () {
                  var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 300,
                    targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
   
                    $cordovaCamera.getPicture(options).then(function (imageData) {
                        $scope.imgURI.push(imageData);
                    }, function (err) {
                        // An error occured. Show a message to the user
                    });
                }

  $scope.choosePhoto = function () {
                  var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 300,
                    targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
   
                    $cordovaCamera.getPicture(options).then(function (imageData) {
                        //$scope.imgURI = "data:image/jpeg;base64," + imageData;
                        $scope.imgURI.push(imageData);
                    }, function (err) {
                        // An error occured. Show a message to the user
                    });
                }
  $ionicModal.fromTemplateUrl('templates/question.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.openModal = function() {
    reset(1);
    $scope.modal.show();
  };
  $scope.closeModal = function() {
    var dd = new Date();
    reponse={};
    reponse.question=$scope.result.question_id;
    reponse.reponse=$scope.result.id;
    reponse.reponseText=$scope.result1.reponseText;
    console.log(reponse);
    reponse.dateAdd= dd.getTime();
    reponse.etat= 0;

    var posOptions = {timeout: 20000, enableHighAccuracy: false};
    $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
      observation.coordX = position.coords.latitude;
      observation.coordY = position.coords.longitude;
      observation.list=tab_reponse;
      $scope.list_observation.push(observation);
      var point1 = new google.maps.LatLng($rootScope.pat_tmp.coordx, $rootScope.pat_tmp.coordy); 
      var point2 = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      $scope.pat_tmp.distance = google.maps.geometry.spherical.computeDistanceBetween(point1,point2);
    }, function(err) {
      console.log(err);
    });  
    
    $scope.modal.hide();
    var de = new Date();
    de.setTime($rootScope.pat_tmp.date);
    $rootScope.duree.h=dd.getHours() - de.getHours();
    $rootScope.duree.m=dd.getMinutes() - de.getMinutes();

  };

  $scope.position =function(){
    $scope.lat = {};
    $scope.long = {};
    var coord1={coordx:'',coordy:''};
    var posOptions = {timeout: 20000, enableHighAccuracy: false};
    $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
        $scope.lat = position.coords.latitude;
        $scope.long = position.coords.longitude;
      }, function(err) {
        console.log(err);
      });
      console.log($scope.long);
      return coord1;
  }
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    //$scope.list_observation=[];
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $scope.stop=function (){

  var essaie=new Date();

  UserDataService.findSegment($rootScope.pat_tmp.segment,function(data){
    if(data.length>0){
     $rootScope.pat_tmp.segment_id=data[0].id;
     return $rootScope.pat_tmp.segment_id;
    }
    else{
      var dd = new Date();
      var segment={};
      segment.dateAdd= dd.getTime();
      segment.nom= $rootScope.pat_tmp.segment;
      UserDataService.createSegment(segment);
    }
  });
  var coord={};
  var posOptions = {timeout: 20000, enableHighAccuracy: false};
    
  function save_observation(observation){
    //console.log(observation);
    UserDataService.createObservation(observation);
  }
  if($scope.list_observation.length>0){
    //console.log("observation");
  if(!angular.isUndefined($rootScope.pat_tmp.segment_id)){
    $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
    coord.coordx = position.coords.latitude;
    coord.coordy = position.coords.longitude;
    var patrouille={segment_id:$rootScope.pat_tmp.segment_id,type_id:1,patrouilleur_id:'',nomSite:$rootScope.pat_tmp.nomSite,etat:0,dateDebut:$rootScope.pat_tmp.date,dateFin:essaie.getTime(),coordXDebut:$rootScope.pat_tmp.coordx,coordYDebut:$rootScope.pat_tmp.coordy,coordXFin:coord.coordx,coordYFin:coord.coordy};
    return(patrouille);
    }, function(err) {
 
  })
  .then(function(patrouille){
        UserDataService.createPatrouille(patrouille);
      return 1
  })
  .then(function(){
    UserDataService.getAllPatrouille(function(data){
      data = data.pop();
      //console.log(data);
      for (var i = 0; i < $scope.list_observation.length; i++) {
        $scope.list_observation[i].patrouille_id=data.id;
      }
      //console.log($scope.list_observation);
      for (var i = 0; i < $scope.list_observation.length; i++) {
        save_observation($scope.list_observation[i]);
      }
      
    })
    
  });
      //$scope.list_observation=[];
      $state.go('app.recapitulatif');
  }
  } 
  };
})


.controller('LoginCtrl', function($scope,$state,UserDataService,$timeout,$ionicPopup,$translate,$ionicLoading) {
    $scope.login={pseudo:'',pwd:''}; 
    $scope.connexion=function(){
     if($scope.login.pseudo&&$scope.login.pwd){
      UserDataService.getByEmail($scope.login.pseudo,function(data){
              if(data&&($scope.login.pwd==data.password)){
              $scope.utilisateur.username= data.username;
              $state.go('app.profil');
            }
            else{
              $scope.login_error=$translate.instant('Erreur lors de l identification, ressayer !!');
            }
      });
    }
    else{
      $scope.login_error=$translate.instant('Erreur lors de l identification, ressayer !!');
      //
    }
    //$state.go('app.profil');
    };
    $scope.inscrire={username:'',email:'',password:'',etat:''};
    $scope.showPopup = function() {
                            
                            var titre=$translate.instant('inscription');
                            var annu=$translate.instant('annuler');
                            var sav=$translate.instant('valider');
                            var myPopup = $ionicPopup.show({
                              templateUrl: 'templates/inscription.html',
                              title: titre,
                              scope: $scope,
                              buttons: [
                                { text: annu },
                                {
                                  text: sav,
                                  type: $scope.utilisateur.mode,
                                  onTap: function(e) {
                                    if (!$scope.inscrire.username) {
                                      
                                      e.preventDefault();
                                    } else {
                                      $scope.inscrire.etat=1;
                                     // console.log($scope.inscrire);
                                      UserDataService.createUser($scope.inscrire)
                                      $scope.login_error=$translate.instant('Votre compte a ete creer avec succes!!');
                                      //return $scope.inscrire;
                                    }
                                  }
                                }
                              ]
                            });
                              $timeout(function() {
                                myPopup.close();
                              }, 2000000);
  };
  $scope.showPopup1 = function() {
                            
                            var titre=$translate.instant('mot de passe oublie');
                            var annu=$translate.instant('annuler');
                            var sav=$translate.instant('valider');
                            var myPopup = $ionicPopup.show({
                              templateUrl: 'templates/forget.html',
                              title: titre,
                              scope: $scope,
                              buttons: [
                                { text: annu },
                                {
                                  text: sav,
                                  type: $scope.utilisateur.mode,
                                  onTap: function(e) {
                                    if (!$scope.inscrire.email) {
                                      
                                      e.preventDefault();
                                    } else {
                                      $scope.login_error=$translate.instant('un mail a ete envoyer !!');
                                      //return $scope.inscrire;
                                    }
                                  }
                                }
                              ]
                            });
                              $timeout(function() {
                                myPopup.close();
                              }, 20000);
  };
  $scope.testconnec=function(){
    $ionicLoading.show({
      template: 'Loading...',
      duration: 7000
    }).then(function(){
      $state.go('app.profil');
    });
    
  }
});

