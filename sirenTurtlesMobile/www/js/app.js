// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers','starter.services','ngCordova', 'ion-floating-menu','pascalprecht.translate'])

.run(function($ionicPlatform,$rootScope,$translate) {
  $rootScope.utilisateur={username:'',mode:"jour",arriere:"arrierejour",lang:''};
  $rootScope.utilisateur.lang=$translate.preferredLanguage();
  $rootScope.pat_tmp={segment:'',patrouilleur:'',nomSite:'',date:'',coordx:'',coordy:'',duree:''};
  $rootScope.duree={m:'',h:''};
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,$translateProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.detail', {
      url: '/detail',
      views: {
        'menuContent': {
          templateUrl: 'templates/detail.html',
          controller: 'DetailCtrl'

        }
      }
    })
    .state('app.profil', {
      url: '/profil',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
          controller: 'AppCtrl'
        }
      }
    })

    .state('app.voir_patrouille', {
      url: '/voir_patrouille',
      views: {
        'menuContent': {
          templateUrl: 'templates/voir_patrouille.html',
          controller: 'voir_patrouilleCtrl'
        }
      
      },
      params: {
        'id':''
        }
    })
    
    .state('login', {
     url: '/login',
     templateUrl: 'templates/login.html',
     controller: 'LoginCtrl'
    })
    
    .state('app.recapitulatif', {
     url: '/recapitulatif',
     views: {
        'menuContent': {
          templateUrl: 'templates/recapitulatif.html',
          controller: 'RecapitulatifCtrl'
        }
      }
     
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('login');
  $translateProvider.useSanitizeValueStrategy('escape'); // gestion des caractères d’échappement
  $translateProvider.useStaticFilesLoader({prefix: 'languages/', suffix: '.json'}); // chargement des fichiers de langues
  $translateProvider.registerAvailableLanguageKeys(['en','fr'], {'en_US': 'en', 'en_UK': 'en', 'fr_FR': 'fr', 'fr_BE': 'fr'}) // définition des langues disponibles
  .determinePreferredLanguage(); // sélection de la langue du système
  $translateProvider.fallbackLanguage(['fr','en']);
  //$translateProvider.use();
});
