<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DatetimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use AppBundle\Entity\Especes;
use AppBundle\Entity\Sous_groupes;
use AppBundle\Entity\Groupes;
use AppBundle\Repository\EspecesRepository;
use AppBundle\Repository\Sous_groupesRepository;
use AppBundle\Repository\GroupesRepository;


class Observations3Type extends AbstractType
{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        
        $builder
            ->add('collecteur',null, array(
    'label'  => 'Nom du collecteur',
    'required' => true,
))->add('alpha',null, array(
    'label'  => 'Alpha-Numérique observation',
    'required' => true,
))->add('date')
            ->add('projet')
            ->add('coordX', NumberType::class, array(
    'label'  => 'Latitude X (en degré décimal)',
))
            ->add('coordY', NumberType::class, array(
    'label'  => 'Longitude Y (en degré décimal)',
))
            ->add('typeObservations', null, array(
    'label'  => 'Type d\'observation',
))
->add('groupes', EntityType::class, [
                'class' => Groupes::class,

            'choice_label' => 'nom_fr',
            'label' => 'Groupe',
            'query_builder' => function (GroupesRepository $g) {
                return $g->createQueryBuilder('g')
                    ->where('g.projet = :projet')
                    ->setParameter('projet', 62153)
                    ->orderBy('g.nom_fr', 'ASC');
            }
            ])
->add('sous_groupes', EntityType::class, [
                'class' => Sous_groupes::class,

            'choice_label' => 'nom_fr',
            'label' => 'Sous groupe',
            'query_builder' => function (Sous_groupesRepository $s) {
                return $s->createQueryBuilder('s')
                    ->where('s.projet = :projet')
                    ->setParameter('projet', 62153)
                    ->orderBy('s.nom_fr', 'ASC');
            }
            ])
->add('especes', EntityType::class, [
                'class' => Especes::class,

            'choice_label' => 'nom_fr',
            'label' => 'Espèces',
            'query_builder' => function (EspecesRepository $es) {
                return $es->createQueryBuilder('e')
                    ->where('e.projet = :projet')
                    ->setParameter('projet', 62153)
                    ->orderBy('e.nom_fr', 'ASC');
            }
            ])
                
            ->add('img1File', FileType::class, array(
    'label'  => 'Image 1 (Ajouter/Modifier)',
    'required' => false
))
            ->add('img2File', FileType::class, array(
    'label'  => 'Image 2 (Ajouter/Modifier)',
    'required' => false
))
            ->add('img3File', FileType::class, array(
    'label'  => 'Image 3 (Ajouter/Modifier)',
    'required' => false
))
            ->add('img4File', FileType::class, array(
    'label'  => 'Image 4 (Ajouter/Modifier)',
    'required' => false
))
            ->add('note',TextareaType::class, array(
    'label'  => 'Note d\'observation',
    'required' => false,
));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Observations',
            'entityManager' => null,
            'projet' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_observations';
    }


}
