<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DatetimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;


class QuestionsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titreFr')->add('titreEn')->add('typeDebut', ChoiceType::class, array(
        'choices'  => array(
        'Personnel' => 'personnel',
        'Commune' => 'commune',
    ),
    // *this line is important*
    'choices_as_values' => true,
))->add('typeReponse', ChoiceType::class, array(
        'choices'  => array(
        'Text' => 'text',
        'Select' => 'select',
        'Entier' => 'entier',
        'Annuler' => 'annuler',
    ),
    // *this line is important*
    'choices_as_values' => true,
))->add('intervale')->add('imageFile', FileType::class, array(
    'label'  => 'Image (Ajouter/Modifier)',
    'required' => false
))->add('questions', null, array(
    'label'  => 'Question suivante',
    'required' => false
));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Questions'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_questions';
    }


}
