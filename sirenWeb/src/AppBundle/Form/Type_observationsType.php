<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use AppBundle\Entity\Questions;

class Type_observationsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomFr', TextType::class)
                ->add('nomEn', TextType::class)
                ->add('niveau')
                ->add('typeQuestion', ChoiceType::class, array(
        'choices'  => array(
        'Sélectionner une espèces' => 'groupe',
        'repondre aux questions (Précisez juste en bas)' => 'question',
    ),'label'  =>'Commencer par :',
    // *this line is important*
    'choices_as_values' => true,
))
               ->add('questions') ->add('imageFile', FileType::class,array('required' => false))
            ;
    }
    
    /**
     * {@inheritdoc
     * }
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\type_observations'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_type_observations';
    }


}
