<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DatetimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class ObservationsType extends AbstractType
{
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        
        $builder
            ->add('collecteur',null, array(
    'label'  => 'Nom du collecteur',
    'required' => true,
))->add('alpha',null, array(
    'label'  => 'Alpha-Numérique observation',
    'required' => true,
))->add('date')
            ->add('projet')
            ->add('coordX', NumberType::class, array(
    'label'  => 'Latitude X (en degré décimal)',
))
            ->add('coordY', NumberType::class, array(
    'label'  => 'Longitude Y (en degré décimal)',
))
            ->add('typeObservations', null, array(
    'label'  => 'Type d\'observation',
))->add('groupes')->add('sous_groupes')->add('especes')
                
            ->add('img1File', FileType::class, array(
    'label'  => 'Image 1 (Ajouter/Modifier)',
    'required' => false
))
            ->add('img2File', FileType::class, array(
    'label'  => 'Image 2 (Ajouter/Modifier)',
    'required' => false
))
            ->add('img3File', FileType::class, array(
    'label'  => 'Image 3 (Ajouter/Modifier)',
    'required' => false
))
            ->add('img4File', FileType::class, array(
    'label'  => 'Image 4 (Ajouter/Modifier)',
    'required' => false
))
            ->add('note',TextareaType::class, array(
    'label'  => 'Note d\'observation',
    'required' => false,
));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Observations',
            'entityManager' => null,
            'projet' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_observations';
    }


}
