<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class EspecesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nomFr')
                ->add('nomEn')
                ->add('nomInaturalist')
                ->add('descriptionFr')
                ->add('descriptionEn')
                ->add('imageFile', FileType::class,array('required' => false))
                ->add('questions_animal',null, array( 'label'  =>'Première question animal (Par défaut 9)'))
                ->add('questions_menaces',null, array( 'label'  =>'Première question menaces (Par défaut 18)'))
                ->add('questions_signe',null, array( 'label'  =>'Première question signe de présence (Par défaut 28)'))
                ->add('questions_alimentation',null, array( 'label'  =>'Première question alimentation (Par défaut 29)'))
                ->add('defaut', ChoiceType::class, array(
        'label'  =>'Espèce Commune','choices'  => array(
        'Oui' => '1',
        'Non' => '0',
    ),
    // *this line is important*
    'choices_as_values' => true,
))
->add('projet');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Especes'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_especes';
    }


}
