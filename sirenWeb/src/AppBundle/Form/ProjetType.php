<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DatetimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;


class ProjetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nom')
        ->add('lieu')
        ->add('couleur',null, array(
            'label'  => 'Couleur en Hexadecimal (21A2B7)',
            'required' => false,
        ))
        ->add('pays')
        ->add('public', ChoiceType::class, array(
            'choices'  => array(
                'Oui' => '1',
                'Non' => '0',       
            ),
            // *this line is important*
            'choices_as_values' => true,
        ))
        ->add('maplink', null, array(
            'required' => false
        ))
        ->add('site', null, array(
            'required' => false
        ))
        ->add('Organization', null, array(
            'required' => false
        ))
        ->add('imageFile', FileType::class, array(
            'label'  => 'Image (Ajouter/Modifier)',
            'required' => false
        ))
        ->add('mention',TextareaType::class, array(
            'label'  => 'Mention légale du projet',
            'required' => false,
        ))
        ->add('mention_en',TextareaType::class, array(
            'label'  => 'Legal notice of the project',
            'required' => false,
        ))
        ->add('note',TextareaType::class, array(
            'label'  => 'Texte à afficher en fin d\'observation',
            'required' => false,
        ))
        ->add('note_en',TextareaType::class, array(
            'label'  => 'Text to display at the end of the observation',
            'required' => false,
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Projet'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_projet';
    }


}
