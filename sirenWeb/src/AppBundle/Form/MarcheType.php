<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DatetimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class MarcheType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date')->add('typeDePoisson')->add('description',TextareaType::class)->add('debarcadaire')->add('siteDePeche')->add('quantite')->add('prix')->add('telephone')->add('img1File', FileType::class, array(
    'label'  => 'Image 1 (Ajouter/Modifier)',
    'required' => false
))->add('img2File', FileType::class, array(
    'label'  => 'Image 2 (Ajouter/Modifier)',
    'required' => false
))->add('img3File', FileType::class, array(
    'label'  => 'Image 3 (Ajouter/Modifier)',
    'required' => false
))->add('img4File', FileType::class, array(
    'label'  => 'Image 4 (Ajouter/Modifier)',
    'required' => false
))->add('etat')->add('utilisateurs');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Marche'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_marche';
    }


}
