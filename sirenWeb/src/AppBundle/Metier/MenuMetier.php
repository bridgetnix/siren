<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Menu;
use Doctrine\ORM\EntityManager;

class MenuMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Menu $menu) {
        $this->em->persist($menu);
        $this->em->flush();
    }
    
    public function update(Menu $menu) {
        $this->em->merge($menu);
        $this->em->flush();
    }
    
    public function delete($id) {
        $menu = $this->getRepository()->find($id);
        if ($menu) {
            $this->em->remove($menu);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Menu");
    }
}
