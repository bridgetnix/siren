<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Groupes;
use Doctrine\ORM\EntityManager;

class GroupesMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Groupes $groupes) {
        $this->em->persist($groupes);
        $this->em->flush();
    }
    
    public function update(Groupes $groupes) {
        $this->em->merge($groupes);
        $this->em->flush();
    }
    
    public function delete($id) {
        $groupes = $this->getRepository()->find($id);
        if ($groupes) {
            $this->em->remove($groupes);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Groupes");
    }
}
