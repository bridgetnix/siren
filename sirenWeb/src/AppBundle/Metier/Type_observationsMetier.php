<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Type_observations;
use Doctrine\ORM\EntityManager;

class Type_observationsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Type_observations $tob) {
        $this->em->persist($tob);
        $this->em->flush();
    }
    
    public function update(Type_observations $tob) {
        $this->em->merge($tob);
        $this->em->flush();
    }
    
    public function delete($id) {
        $tob = $this->getRepository()->find($id);
        if ($tob) {
            $this->em->remove($tob);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    public function niveau() {
        return $this->getRepository()->findBy(array(),array('niveau'=>"ASC"));
           
    }
    public function niveauProjet($projet) {
        return $this->getRepository()->findBy(array('projet'=> $projet),array('niveau'=>"ASC"));
           
    }
    
    
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Type_observations");
    }
}
