<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Questions;
use Doctrine\ORM\EntityManager;

class QuestionsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Questions $e) {
        $this->em->persist($e);
        $this->em->flush();
    }
    
    public function update(Questions $e) {
        $this->em->merge($e);
        $this->em->flush();
    }
    
    public function delete($id) {
        $e = $this->getRepository()->find($id);
        if ($e) {
            $this->em->remove($e);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Questions");
    }
}
