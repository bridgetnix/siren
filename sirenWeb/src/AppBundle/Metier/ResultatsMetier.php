<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Resultats;
use Doctrine\ORM\EntityManager;

class ResultatsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Resultats $res) {
        $this->em->persist($res);
        $this->em->flush();
    }
    
    public function update(Resultats $res) {
        $this->em->merge($res);
        $this->em->flush();
    }
    
    public function delete($id) {
        $res = $this->getRepository()->find($id);
        if ($res) {
            $this->em->remove($res);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Resultats");
    }
}
