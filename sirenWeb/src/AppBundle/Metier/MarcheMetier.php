<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Marche;
use Doctrine\ORM\EntityManager;

class MarcheMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Marche $marche) {
        $this->em->persist($marche);
        $this->em->flush();
    }
    
    public function update(Marche $marche) {
        $this->em->merge($marche);
        $this->em->flush();
    }
    
    public function delete($id) {
        $marche = $this->getRepository()->find($id);
        if ($marche) {
            $this->em->remove($marche);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function findByUtilisateurs($utilisateurs) {
        return $this->getRepository()->findByUtilisateurs($utilisateurs);
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Marche");
    }
}
