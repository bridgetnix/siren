<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Feedback;
use Doctrine\ORM\EntityManager;

class FeedbackMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Feedback $feedback) {
        $this->em->persist($feedback);
        $this->em->flush();
    }
    
    public function update(Feedback $feedback) {
        $this->em->merge($feedback);
        $this->em->flush();
    }
    
    public function delete($id) {
        $feedback = $this->getRepository()->find($id);
        if ($feedback) {
            $this->em->remove($feedback);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Feedback");
    }
}
