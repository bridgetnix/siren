<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Position;
use Doctrine\ORM\EntityManager;

class PositionMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Position $position) {
        $this->em->persist($position);
        $this->em->flush();
    }
    
    public function update(Position $position) {
        $this->em->merge($position);
        $this->em->flush();
    }
    
    public function delete($id) {
        $position = $this->getRepository()->find($id);
        if ($position) {
            $this->em->remove($position);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Position");
    }
}
