<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Utilisateurs;
use Doctrine\ORM\EntityManager;

class UtilisateursMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Utilisateurs $user) {
        $this->em->persist($user);
        $this->em->flush();
    }
    
    public function update(Utilisateurs $user) {
        $this->em->merge($user);
        $this->em->flush();
    }
    
    public function delete($id) {
        $user = $this->getRepository()->find($id);
        if ($user) {
            $this->em->remove($user);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Utilisateurs");
    }
}
