<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Projet_typeObservations;
use Doctrine\ORM\EntityManager;

class Projet_typeObservationsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Projet_typeObservations $projet_typeObservations) {
        $this->em->persist($projet_typeObservations);
        $this->em->flush();
    }
    
    public function update(Projet_typeObservations $projet_typeObservations) {
        $this->em->merge($projet_typeObservations);
        $this->em->flush();
    }
    
    public function delete($id) {
        $projet_typeObservations = $this->getRepository()->find($id);
        if ($projet_typeObservations) {
            $this->em->remove($projet_typeObservations);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function findByProjet($projet) {
        return $this->getRepository()->findByProjet($projet);
    }
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Projet_typeObservations");
    }
}
