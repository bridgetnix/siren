<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Projet_menu;
use Doctrine\ORM\EntityManager;

class Projet_menuMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Projet_menu $projet_menu) {
        $this->em->persist($projet_menu);
        $this->em->flush();
    }
    
    public function update(Projet_menu $projet_menu) {
        $this->em->merge($projet_menu);
        $this->em->flush();
    }
    
    public function delete($id) {
        $projet_menu = $this->getRepository()->find($id);
        if ($projet_menu) {
            $this->em->remove($projet_menu);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function findByProjet($projet) {
        return $this->getRepository()->findByProjet($projet);
    }
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Projet_menu");
    }
}
