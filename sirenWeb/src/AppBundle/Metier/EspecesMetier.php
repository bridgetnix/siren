<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Especes;
use Doctrine\ORM\EntityManager;

class EspecesMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Especes $e) {
        $this->em->persist($e);
        $this->em->flush();
    }
    
    public function update(Especes $e) {
        $this->em->merge($e);
        $this->em->flush();
    }
    
    public function delete($id) {
        $e = $this->getRepository()->find($id);
        if ($e) {
            $this->em->remove($e);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function findNameFr($name) {
        return $this->getRepository()->findOneByNomFr($name);
    }
    public function findNameEn($name) {
        return $this->getRepository()->findOneByNomEn($name);
    }
    
     public function findByDefaut($id) {
        return $this->getRepository()->findByDefaut($id);
    }
    
     public function sousgroupes($id) {
        return $this->getRepository()->findBy(array('sous_groupes'=> $id));
           
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Especes");
    }
}
