<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Commentaire;
use Doctrine\ORM\EntityManager;

class CommentaireMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Commentaire $commentaire) {
        $this->em->persist($commentaire);
        $this->em->flush();
    }
    
    public function update(Commentaire $commentaire) {
        $this->em->merge($commentaire);
        $this->em->flush();
    }
    
    public function delete($id) {
        $commentaire = $this->getRepository()->find($id);
        if ($commentaire) {
            $this->em->remove($commentaire);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function findByMarche($marche) {
        return $this->getRepository()->findByMarche($marche);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Commentaire");
    }
}
