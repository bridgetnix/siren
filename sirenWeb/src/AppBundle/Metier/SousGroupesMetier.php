<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Sous_groupes;
use Doctrine\ORM\EntityManager;

class SousGroupesMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Sous_groupes $groupes) {
        $this->em->persist($groupes);
        $this->em->flush();
    }
    
    public function update(Sous_groupes $groupes) {
        $this->em->merge($groupes);
        $this->em->flush();
    }
    
    public function delete($id) {
        $groupes = $this->getRepository()->find($id);
        if ($groupes) {
            $this->em->remove($groupes);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    public function groupes($id) {
        return $this->getRepository()->findByGroupes($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Sous_groupes");
    }
}
