<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Notifications;
use Doctrine\ORM\EntityManager;

class NotificationsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Notifications $fon) {
        $this->em->persist($fon);
        $this->em->flush();
    }
    
    public function update(Notifications $fon) {
        $this->em->merge($fon);
        $this->em->flush();
    }
    
    public function delete($id) {
        $fon = $this->getRepository()->find($id);
        if ($fon) {
            $this->em->remove($fon);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }

    
    
    public function getnew($date) {
        $qb= $this->em->createQueryBuilder();
        
        $qb->select('m')
               ->from('AppBundle:Notifications','m')               
               ->where('m.date >= :date')
               ->setParameter('date',$date);
        $notifications =$qb->getQuery()->getResult();
        return $notifications;
        
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Notifications");
    }
}
