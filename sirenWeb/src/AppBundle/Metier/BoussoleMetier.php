<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Boussole;
use Doctrine\ORM\EntityManager;

class BoussoleMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Boussole $boussole) {
        $this->em->persist($boussole);
        $this->em->flush();
    }
    
    public function update(Boussole $boussole) {
        $this->em->merge($boussole);
        $this->em->flush();
    }
    
    public function delete($id) {
        $boussole = $this->getRepository()->find($id);
        if ($boussole) {
            $this->em->remove($boussole);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Boussole");
    }
}
