<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Reponses;
use Doctrine\ORM\EntityManager;

class ReponsesMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Reponses $reponse) {
        $this->em->persist($reponse);
        $this->em->flush();
    }
    
    public function update(Reponses $reponse) {
        $this->em->merge($reponse);
        $this->em->flush();
    }
    
    public function delete($id) {
        $reponse = $this->getRepository()->find($id);
        if ($reponse) {
            $this->em->remove($reponse);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    public function findName($name) {
        return $this->getRepository()->findOneByTitreFr($name);
    }
    
    public function findquestions($id) {
        return $this->getRepository()->findByQuestions($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Reponses");
    }
}
