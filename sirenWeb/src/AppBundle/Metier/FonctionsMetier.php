<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Fonctions;
use Doctrine\ORM\EntityManager;

class FonctionsMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Fonctions $fon) {
        $this->em->persist($fon);
        $this->em->flush();
    }
    
    public function update(Fonctions $fon) {
        $this->em->merge($fon);
        $this->em->flush();
    }
    
    public function delete($id) {
        $fon = $this->getRepository()->find($id);
        if ($fon) {
            $this->em->remove($fon);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }

    public function findAllFr() {
        return $this->getRepository()->findAllFr();
    }

    public function findAllEn() {
        return $this->getRepository()->findAllEn();
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Fonctions");
    }
}
