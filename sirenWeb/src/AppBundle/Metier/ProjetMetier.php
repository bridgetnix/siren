<?php

namespace AppBundle\Metier;

use AppBundle\Entity\Projet;
use Doctrine\ORM\EntityManager;

class ProjetMetier {

    private $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function create(Projet $projet) {
        $this->em->persist($projet);
        $this->em->flush();
    }
    
    public function update(Projet $projet) {
        $this->em->merge($projet);
        $this->em->flush();
    }
    
    public function delete($id) {
        $projet = $this->getRepository()->find($id);
        if ($projet) {
            $this->em->remove($projet);
            $this->em->flush();
        }
    }
    
    public function findAll() {
        return $this->getRepository()->findAll();
    }
    
    public function findByPublic() {
        return $this->getRepository()->findByPublic(true);
    }
    
    public function find($id) {
        return $this->getRepository()->find($id);
    }
    
    private function getRepository() {
        return $this->em->getRepository("AppBundle:Projet");
    }
}
