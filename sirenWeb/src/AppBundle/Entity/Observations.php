<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Observations
 *
 * @ORM\Table(name="observations")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ObservationsRepository")
 * @Vich\Uploadable
 */
class Observations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="Id_inaturalist", type="integer", nullable=True)
     */
    private $id_inaturalist;
    
    /**
     * @var int
     *
     * @ORM\Column(name="CoordX", type="float")
     */
    private $coordX;

    /**
     * @var int
     *
     * @ORM\Column(name="CoordY", type="float")
     */
    private $coordY;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Type_observations")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $typeObservations;

    /**
     * @var string
     *
     * @ORM\Column(name="Note", type="string", length=1024 , nullable=True)
     */
    private $note = null;

    /**
     * @var string
     *
     * @ORM\Column(name="alpha", type="string", length=1024 , nullable=false)
     */
    private $alpha;
/**
     * @var string
     *
     * @ORM\Column(name="collecteur", type="string", length=1024 , nullable=true)
     */
    private $collecteur="admin";

    
    /**
     * @var text
     *
     * @ORM\Column(name="Img1", type="text", nullable=True)
     */
    private $img1 = "observation.png";
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="observations_image1", fileNameProperty="img1")
     */
    private $img1File;
    
    /**
     * @var text
     *
     * @ORM\Column(name="Img2", type="text", nullable=True)
     */
    private $img2;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="observations_image2", fileNameProperty="img2")
     */
    private $img2File;
    
    /**
     * @var text
     *
     * @ORM\Column(name="Img3", type="text", nullable=True)
     */
    private $img3;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="observations_image3", fileNameProperty="img3")
     */
    private $img3File;
    
    /**
     * @var text
     *
     * @ORM\Column(name="Img4", type="text", nullable=True)
     */
    private $img4;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="observations_image4", fileNameProperty="img4")
     */
    private $img4File;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Especes")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE") 
    */
    private $especes; 
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sous_groupes")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE") 
    */
    private $sous_groupes; 
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Groupes")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE") 
    */
    private $groupes; 
    
 /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Utilisateurs")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $utilisateurs; 
    
    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat = 0;
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE")
    */
    private $projet;
    
    /**
     * @var int
     *
     * @ORM\Column(name="mort", type="integer")
     */
    private $mort = 0;

    public function __construct() {
        $this->date = new \DateTime('now');
    }
   
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Observations
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set idInaturalist
     *
     * @param int $idInaturalist
     *
     * @return Observations
     */
    public function setIdInaturalist($idInaturalist = null)
    {
        $this->id_inaturalist = $idInaturalist;

        return $this;
    }

    /**
     * Get idInaturalist
     *
     * @return int
     */
    public function getIdInaturalist()
    {
        return $this->id_inaturalist;
    }


    /**
     * Set coordX
     *
     * @param double $coordX
     *
     * @return Observations
     */
    public function setCoordX($coordX)
    {
        $this->coordX = $coordX;

        return $this;
    }

    /**
     * Get coordX
     *
     * @return double
     */
    public function getCoordX()
    {
        return $this->coordX;
    }

    /**
     * Set coordY
     *
     * @param double $coordY
     *
     * @return Observations
     */
    public function setCoordY($coordY)
    {
        $this->coordY = $coordY;

        return $this;
    }

    /**
     * Get coordY
     *
     * @return double
     */
    public function getCoordY()
    {
        return $this->coordY;
    }

    /**
     * Set typeObservations
     *
     * @param string $typeObservations
     *
     * @return Observations
     */
    public function setTypeObservations($typeObservations)
    {
        $this->typeObservations = $typeObservations;

        return $this;
    }

    /**
     * Get typeObservations
     *
     * @return Type_observations
     */
    public function getTypeObservations()
    {
        return $this->typeObservations;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Observations
     */
    public function setNote($note = null)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set img1
     *
     * @param string $img1
     *
     * @return Observations
     */
    public function setImg1($img1)
    {
        $this->img1 = $img1;

        return $this;
    }

    /**
     * Get img1
     *
     * @return string
     */
    public function getImg1()
    {
        return $this->img1;
    }
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img1File
     *
     * @return Observations
    */
    public function setImg1File(File $img1File = null)
    {
        $this->img1File = $img1File;
        if ($img1File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg1File()
    {
        return $this->img1File;
    }
    
    /**
     * Set img2
     *
     * @param string $img2
     *
     * @return Observations
     */
    public function setImg2($img2)
    {
        $this->img2 = $img2;

        return $this;
    }

    /**
     * Get img2
     *
     * @return string
     */
    public function getImg2()
    {
        return $this->img2;
    }
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img2File
     *
     * @return Observations
    */
    public function setImg2File(File $img2File = null)
    {
        $this->img2File = $img2File;
        if ($img2File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg2File()
    {
        return $this->img2File;
    }
    
    /**
     * Set img3
     *
     * @param string $img3
     *
     * @return Observations
     */
    public function setImg3($img3)
    {
        $this->img3 = $img3;

        return $this;
    }

    /**
     * Get img3
     *
     * @return string
     */
    public function getImg3()
    {
        return $this->img3;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img3File
     *
     * @return Observations
    */
    public function setImg3File(File $img3File = null)
    {
        $this->img3File = $img3File;
        if ($img3File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg3File()
    {
        return $this->img3File;
    }
    
    /**
     * Set img4
     *
     * @param string $img4
     *
     * @return Observations
     */
    public function setImg4($img4)
    {
        $this->img4 = $img4;

        return $this;
    }

    /**
     * Get img4
     *
     * @return string
     */
    public function getImg4()
    {
        return $this->img4;
    }

    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $img4File
     *
     * @return Observations
    */
    public function setImg4File(File $img4File = null)
    {
        $this->img4File = $img4File;
        if ($img4File) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImg4File()
    {
        return $this->img4File;
    }

    /**
     * Set especes
     *
     * @param \AppBundle\Entity\Especes $especes
     *
     * @return Observations
     */
    public function setEspeces(\AppBundle\Entity\Especes $especes)
    {
        $this->especes = $especes;

        return $this;
    }

    /**
     * Get especes
     *
     * @return Especes
     */
    public function getEspeces()
    {
        return $this->especes;
    }

    /**
     * Set sousGroupes
     *
     * @param \AppBundle\Entity\Sous_groupes $sousGroupes
     *
     * @return Observations
     */
    public function setSousGroupes(\AppBundle\Entity\Sous_groupes $sousGroupes)
    {
        $this->sous_groupes = $sousGroupes;

        return $this;
    }

    /**
     * Get sousGroupes
     *
     * @return \AppBundle\Entity\Sous_groupes
     */
    public function getSousGroupes()
    {
        return $this->sous_groupes;
    }

    /**
     * Set groupes
     *
     * @param \AppBundle\Entity\Groupes $groupes
     *
     * @return Observations
     */
    public function setGroupes(\AppBundle\Entity\Groupes $groupes)
    {
        $this->groupes = $groupes;

        return $this;
    }

    /**
     * Get groupes
     *
     * @return \AppBundle\Entity\Groupes
     */
    public function getGroupes()
    {
        return $this->groupes;
    }
    
    /**
     * Set utilisateurs
     *
     * @param \AppBundle\Entity\Utilisateur $utilisateurs
     *
     * @return Observations
     */
    public function setUtilisateurs(\AppBundle\Entity\Utilisateurs $utilisateurs)
    {
        $this->utilisateurs = $utilisateurs;

        return $this;
    }

    /**
     * Get utilisateurs
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }
    
     /**
     * Get etat
     *
     * @return int
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     *
     * @return Observations
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return int
     */
    public function getMort()
    {
        return $this->mort;
    }

    /**
     * @param int $mort
     */
    public function setMort($mort)
    {
        $this->mort = $mort;
    }



     public function __toString() {
        return $this->id;
    }

    /**
     * Set alpha
     *
     * @param string $alpha
     *
     * @return Observations
     */
    public function setAlpha($alpha)
    {
        $this->alpha = $alpha;

        return $this;
    }

    /**
     * Get alpha
     *
     * @return string
     */
    public function getAlpha()
    {
        return $this->alpha;
    }
    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Notifications_projet
     */
    public function setProjet(\AppBundle\Entity\Projet $projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set collecteur
     *
     * @param string $collecteur
     *
     * @return Observations
     */
    public function setCollecteur($collecteur)
    {
        $this->collecteur = $collecteur;

        return $this;
    }

    /**
     * Get collecteur
     *
     * @return string
     */
    public function getCollecteur()
    {
        return $this->collecteur;
    }
}
