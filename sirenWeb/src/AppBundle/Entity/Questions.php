<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Questions
 *
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionsRepository")
 * @Vich\Uploadable
 */
class Questions
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre_fr", type="string", length=255)
     */
    private $titre_fr;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre_en", type="string", length=255)
     */
    private $titre_en;

    /**
     * @var string
     *
     * @ORM\Column(name="Type_reponse", type="string", length=255)
     */
    private $typeReponse;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Type_debut", type="string", length=255)
     */
    private $typeDebut;

    /**
     * @var int
     *
     * @ORM\Column(name="Intervale", type="integer")
     */
    private $intervale;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE")
    */
    private $questions;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Projet")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE")
    */
    private $projet;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_picture", type="boolean")
     */
    private $isPicture = false;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=255, nullable=True)
     */
    private $image;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="question_image", fileNameProperty="image")
     */
    private $imageFile;
    
    
    public function __construct() {
        $this->updatedAt = new \DateTime('now');
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeReponse
     *
     * @param string $typeReponse
     *
     * @return Questions
     */
    public function setTypeReponse($typeReponse)
    {
        $this->typeReponse = $typeReponse;

        return $this;
    }

    /**
     * Get typeReponse
     *
     * @return string
     */
    public function getTypeReponse()
    {
        return $this->typeReponse;
    }
    
    /**
     * Set typeDebut
     *
     * @param string $typedebut
     *
     * @return Questions
     */
    public function setTypeDebut($typeDebut)
    {
        $this->typeDebut = $typeDebut;

        return $this;
    }

    /**
     * Get typeDebut
     *
     * @return string
     */
    public function getTypeDebut()
    {
        return $this->typeDebut;
    }

    /**
     * Set intervale
     *
     * @param integer $intervale
     *
     * @return Questions
     */
    public function setIntervale($intervale)
    {
        $this->intervale = $intervale;

        return $this;
    }

    /**
     * Get intervale
     *
     * @return int
     */
    public function getIntervale()
    {
        return $this->intervale;
    }

    /**
     * Set questions
     *
     * @param \AppBundle\Entity\Questions $questions
     *
     * @return Questions
     */
    public function setQuestions(\AppBundle\Entity\Questions $questions = null)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set titreFr
     *
     * @param string $titreFr
     *
     * @return Questions
     */
    public function setTitreFr($titreFr)
    {
        $this->titre_fr = $titreFr;

        return $this;
    }

    /**
     * Get titreFr
     *
     * @return string
     */
    public function getTitreFr()
    {
        return $this->titre_fr;
    }

    /**
     * Set titreEn
     *
     * @param string $titreEn
     *
     * @return Questions
     */
    public function setTitreEn($titreEn)
    {
        $this->titre_en = $titreEn;

        return $this;
    }

    /**
     * Get titreEn
     *
     * @return string
     */
    public function getTitreEn()
    {
        return $this->titre_en;
    }
    /**
     * Set projet
     *
     * @param \AppBundle\Entity\Projet $projet
     *
     * @return Projet_utilisateur
     */
    public function setProjet(\AppBundle\Entity\Projet $projet)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }
    public function __toString() {
        return $this->titre_fr.' ('.$this->id.')';
    }
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Questions
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Set isPicture
     *
     * @param boolean $isPicture
     *
     * @return Questions
     */
    public function setIsPicture($isPicture)
    {
        $this->isPicture = $isPicture;

        return $this;
    }

    /**
     * Get isPicture
     *
     * @return boolean
     */
    public function getIsPicture()
    {
        return $this->isPicture;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Questions
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
    
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     *
     * @return Questions
    */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
    
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }
}
