<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Pays
 *
 * @ORM\Table(name="pays")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaysRepository")
 */
class Pays
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_fr", type="string", length=255)
     */
    private $nom_fr;

    /**
     * @var int
     *
     * @ORM\Column(name="Code_Tel", type="integer")
     */
    private $codeTel;

    /**
     * @var string
     *
     * @ORM\Column(name="Sigle", type="string", length=255)
     */
    private $sigle;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_en", type="string", length=255)
     */
    private $nom_en;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeTel
     *
     * @param integer $codeTel
     *
     * @return Pays
     */
    public function setCodeTel($codeTel)
    {
        $this->codeTel = $codeTel;

        return $this;
    }

    /**
     * Get codeTel
     *
     * @return int
     */
    public function getCodeTel()
    {
        return $this->codeTel;
    }

    /**
     * Set sigle
     *
     * @param string $sigle
     *
     * @return Pays
     */
    public function setSigle($sigle)
    {
        $this->sigle = $sigle;

        return $this;
    }

    /**
     * Get sigle
     *
     * @return string
     */
    public function getSigle()
    {
        return $this->sigle;
    }


    /**
     * Set nomFr
     *
     * @param string $nomFr
     *
     * @return Pays
     */
    public function setNomFr($nomFr)
    {
        $this->nom_fr = $nomFr;

        return $this;
    }

    /**
     * Get nomFr
     *
     * @return string
     */
    public function getNomFr()
    {
        return $this->nom_fr;
    }

    /**
     * Set nomEn
     *
     * @param string $nomEn
     *
     * @return Pays
     */
    public function setNomEn($nomEn)
    {
        $this->nom_en = $nomEn;

        return $this;
    }

    /**
     * Get nomEn
     *
     * @return string
     */
    public function getNomEn()
    {
        return $this->nom_en;
    }
    
    public function __toString() {
        return '(+'.$this->codeTel.') '.$this->nom_fr;
    }
}
