<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Boussole
 *
 * @ORM\Table(name="boussole")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BoussoleRepository")
 * @Vich\Uploadable
 */
class Boussole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime")
     */
    private $date;

    
    /**
     * @var int
     *
     * @ORM\Column(name="CoordX", type="float")
     */
    private $coordX;

    /**
     * @var int
     *
     * @ORM\Column(name="CoordY", type="float")
     */
    private $coordY;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=1024)
     */
    private $titre;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Utilisateurs")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $utilisateurs; 
    
    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat = 1;
    
    
    public function __construct() {
        $this->date = new \DateTime('now');
    }
   
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }
    
     public function __toString() {
        return $this->titre;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Boussole
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set coordX
     *
     * @param float $coordX
     *
     * @return Boussole
     */
    public function setCoordX($coordX)
    {
        $this->coordX = $coordX;

        return $this;
    }

    /**
     * Get coordX
     *
     * @return float
     */
    public function getCoordX()
    {
        return $this->coordX;
    }

    /**
     * Set coordY
     *
     * @param float $coordY
     *
     * @return Boussole
     */
    public function setCoordY($coordY)
    {
        $this->coordY = $coordY;

        return $this;
    }

    /**
     * Get coordY
     *
     * @return float
     */
    public function getCoordY()
    {
        return $this->coordY;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Boussole
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     *
     * @return Boussole
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set utilisateurs
     *
     * @param \AppBundle\Entity\Utilisateurs $utilisateurs
     *
     * @return Boussole
     */
    public function setUtilisateurs(\AppBundle\Entity\Utilisateurs $utilisateurs)
    {
        $this->utilisateurs = $utilisateurs;

        return $this;
    }

    /**
     * Get utilisateurs
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }
}
