<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pays_sousgroupes
 *
 * @ORM\Table(name="pays_sousgroupes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Pays_sousgroupesRepository")
 */
class Pays_sousgroupes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $pays;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Sous_groupes")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $sous_groupes;

    /**
     * Set pays
     *
     * @param \AppBundle\Entity\Pays $pays
     *
     * @return Pays_sousgroupes
     */
    public function setPays(\AppBundle\Entity\Pays $pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \AppBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set sousGroupes
     *
     * @param \AppBundle\Entity\Sous_groupes $sousGroupes
     *
     * @return Pays_sousgroupes
     */
    public function setSousGroupes(\AppBundle\Entity\Sous_groupes $sousGroupes)
    {
        $this->sous_groupes = $sousGroupes;

        return $this;
    }

    /**
     * Get sousGroupes
     *
     * @return \AppBundle\Entity\Sous_groupes
     */
    public function getSousGroupes()
    {
        return $this->sous_groupes;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
