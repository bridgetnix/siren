<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Resultats
 *
 * @ORM\Table(name="resultats")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ResultatsRepository")
 */
class Resultats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Contenu", type="string", length=255)
     */
    private $contenu;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
     * @ORM\JoinColumn(nullable=false,onDelete="CASCADE")
    */
    private $questions; 

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Observations")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $observations; 
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Resultats
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set questions
     *
     * @param \AppBundle\Entity\Questions $questions
     *
     * @return Resultats
     */
    public function setQuestions(\AppBundle\Entity\Questions $questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set observations
     *
     * @param \AppBundle\Entity\Observations $observations
     *
     * @return Resultats
     */
    public function setObservations(\AppBundle\Entity\Observations $observations)
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * Get observations
     *
     * @return \AppBundle\Entity\Observations
     */
    public function getObservations()
    {
        return $this->observations;
    }
    
    public function __toString() {
        return $this->contenu;
    }
}
