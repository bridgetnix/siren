<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Notifications
 *
 * @ORM\Table(name="notifications")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotificationsRepository")
 */
class Notifications
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_fr", type="string", length=255)
     */
    private $nom_fr;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom_en", type="string", length=255)
     */
    private $nom_en;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime",nullable=True)
     */
    private $date;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set nomFr
     *
     * @param string $nomFr
     *
     * @return Notifications
     */
    public function setNomFr($nomFr)
    {
        $this->nom_fr = $nomFr;

        return $this;
    }

    /**
     * Get nomFr
     *
     * @return string
     */
    public function getNomFr()
    {
        return $this->nom_fr;
    }

    /**
     * Set nomEn
     *
     * @param string $nomEn
     *
     * @return Notifications
     */
    public function setNomEn($nomEn)
    {
        $this->nom_en = $nomEn;

        return $this;
    }

    /**
     * Get nomEn
     *
     * @return string
     */
    public function getNomEn()
    {
        return $this->nom_en;
    }
    
    public function __toString() {
        return $this->nom_fr;
    }
     public function __construct() {
        $this->date = new \DateTime('now');
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Notifications
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
