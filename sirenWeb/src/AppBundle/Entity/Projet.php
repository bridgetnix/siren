<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Cocur\Slugify\Slugify;

/**
 * Projet
 *
 * @ORM\Table(name="Projet")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjetRepository")
 * @Vich\Uploadable
 */
class Projet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Note_en", type="string", length=255, nullable=True)
     */
    private $note_en = null;

    /**
     * @var string
     *
     * @ORM\Column(name="Lieu", type="string", length=255)
     */
    private $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="Site", type="string", length=255, nullable=true)
     */
    private $site = null;

     /**
     * @var string
     *
     * @ORM\Column(name="Organization", type="string", length=255, nullable=true)
     */
    private $organization = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Maplink", type="string", length=255, nullable=true)
     */
    private $maplink = null;

    /**
     * @var int
     *
     * @ORM\Column(name="Public", type="integer")
     */
    private $public;

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $pays;
     /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Utilisateurs")
    * @ORM\JoinColumn(nullable=true,onDelete="CASCADE") 
    */
    private $utilisateurs=null;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime")
     */
    private $date;
/**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions")
    */
    private $questions;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Couleur", type="string", length=255, nullable=True)
     */
    private $couleur = null;
    
    /**
     * @var string
     *
     * @ORM\Column(name="Note", type="string", length=1024 , nullable=True)
     */
    private $note = null;

    /**
     * @var string
     *
     * @ORM\Column(name="Mention", type="string", length=1024 , nullable=True)
     */
    private $mention = null;

    /**
     * @var string
     *
     * @ORM\Column(name="Mention_en", type="string", length=1024 , nullable=True)
     */
    private $mention_en = null;

    /**
     * @var string
     *
     * @ORM\Column(name="Image", type="string", length=255, nullable=True)
     */
    private $image;
    
    /**
     * @Assert\File(
     *     maxSizeMessage = "L'image ne doit pas dépasser 10Mb.",
     *     maxSize = "10024k",
     *     mimeTypes = {"image/jpg", "image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Les images doivent être au format JPG, GIF ou PNG."
     * )
     * @Vich\UploadableField(mapping="projet_image", fileNameProperty="image")
     */
    private $imageFile;
    
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;
    
    
    public function __construct() {
        $this->date = new \DateTime('now');
        $this->updatedAt = new \DateTime('now');
    }
       
    public function __toString() {
        return $this->nom.'('.$this->utilisateurs.')';
    }
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Projet
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return Projet
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return Projet
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Set maplink
     *
     * @param string $maplink
     *
     * @return Projet
     */
    public function setMaplink($maplink)
    {
        $this->maplink = $maplink;
        
        return $this;
    }

    /**
     * Get maplink
     *
     * @return string
     */
    public function getMaplink()
    {
        return $this->maplink;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set organization
     *
     * @param string $organization
     *
     * @return Projet
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set public
     *
     * @param integer $public
     *
     * @return Projet
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return integer
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Projet
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set pays
     *
     * @param \AppBundle\Entity\Pays $pays
     *
     * @return Projet
     */
    public function setPays(\AppBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \AppBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }
    
    /**
     * Set utilisateurs
     *
     * @param \AppBundle\Entity\Utilisateurs $utilisateurs
     *
     * @return Projet_utilisateur
     */
    public function setUtilisateurs(\AppBundle\Entity\Utilisateurs $utilisateurs)
    {
        $this->utilisateurs = $utilisateurs;

        return $this;
    }

    /**
     * Get utilisateurs
     *
     * @return \AppBundle\Entity\Utilisateurs
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }
    
    
    /**
     * Set questions
     *
     * @param \AppBundle\Entity\Questions $questions
     *
     * @return Reponses
     */
    public function setQuestions(\AppBundle\Entity\Questions $questions)
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * Get questions
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }
    
    
    /**
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $imageFile
     *
     * @return Projet
    */
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
    
    /**
     * Set image
     *
     * @param string $image
     *
     * @return Projet
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * Set couleur
     *
     * @param string $couleur
     *
     * @return Projet
     */
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }

    /**
     * Get couleur
     *
     * @return string
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Projet
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note_en
     *
     * @param string $note_en
     *
     * @return Projet
     */
    public function setNoteEn($note_en)
    {
        $this->note_en = $note_en;

        return $this;
    }

    /**
     * Get note_en
     *
     * @return string
     */
    public function getNoteEn()
    {
        return $this->note_en;
    }

    /**
     * Set mention
     *
     * @param string $mention
     *
     * @return Projet
     */
    public function setMention($mention)
    {
        $this->mention = $mention;

        return $this;
    }

    /**
     * Get mention
     *
     * @return string
     */
    public function getMention()
    {
        return $this->mention;
    }

    /**
     * Set mention_en
     *
     * @param string $mention_en
     *
     * @return Projet
     */
    public function setMentionEn($mention_en)
    {
        $this->mention_en = $mention_en;

        return $this;
    }

    /**
     * Get mention_en
     *
     * @return string
     */
    public function getMentionEn()
    {
        return $this->mention_en;
    }
    
    
    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Projet
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    
    function getSlug() {
        $slugify = new Slugify();
        return $slugify->slugify(microtime());
    }

}
