<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pays_especes
 *
 * @ORM\Table(name="pays_especes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Pays_especesRepository")
 */
class Pays_especes
{
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;   

    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $pays;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Especes")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $especes;
    

    /**
     * Set pays
     *
     * @param \AppBundle\Entity\Pays $pays
     *
     * @return Pays_especes
     */
    public function setPays(\AppBundle\Entity\Pays $pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \AppBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set especes
     *
     * @param \AppBundle\Entity\Especes $especes
     *
     * @return Pays_especes
     */
    public function setEspeces(\AppBundle\Entity\Especes $especes)
    {
        $this->especes = $especes;

        return $this;
    }

    /**
     * Get especes
     *
     * @return \AppBundle\Entity\Especes
     */
    public function getEspeces()
    {
        return $this->especes;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
