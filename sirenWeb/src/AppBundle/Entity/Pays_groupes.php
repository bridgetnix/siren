<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pays_groupes
 *
 * @ORM\Table(name="pays_groupes")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Pays_groupesRepository")
 */
class Pays_groupes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $pays;
    
    /**
    * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Groupes")
    * @ORM\JoinColumn(nullable=false,onDelete="CASCADE") 
    */
    private $groupes;


    /**
     * Set pays
     *
     * @param \AppBundle\Entity\Pays $pays
     *
     * @return Pays_groupes
     */
    public function setPays(\AppBundle\Entity\Pays $pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \AppBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set groupes
     *
     * @param \AppBundle\Entity\Groupes $groupes
     *
     * @return Pays_groupes
     */
    public function setGroupes(\AppBundle\Entity\Groupes $groupes)
    {
        $this->groupes = $groupes;

        return $this;
    }

    /**
     * Get groupes
     *
     * @return \AppBundle\Entity\Groupes
     */
    public function getGroupes()
    {
        return $this->groupes;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
