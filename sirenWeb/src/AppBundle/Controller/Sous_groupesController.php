<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sous_groupes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Sous_groupe controller.
 *
 */
class Sous_groupesController extends Controller
{
    /**
     * Lists all sous_groupe entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sous_groupes = $em->getRepository('AppBundle:Sous_groupes')->findAll();

        return $this->render('sous_groupes/index.html.twig', array(
            'sous_groupes' => $sous_groupes,
        ));
    }

    /**
     * Creates a new sous_groupe entity.
     *
     */
    public function newAction(Request $request)
    {
        $sous_groupe = new Sous_groupes();
        $form = $this->createForm('AppBundle\Form\Sous_groupesType', $sous_groupe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sous_groupe);
            $em->flush();

            return $this->redirectToRoute('sous_groupes_show', array('id' => $sous_groupe->getId()));
        }

        return $this->render('sous_groupes/new.html.twig', array(
            'sous_groupe' => $sous_groupe,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sous_groupe entity.
     *
     */
    public function showAction(Sous_groupes $sous_groupe)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($sous_groupe);
        $especes = $em->getRepository('AppBundle:Especes')->findBy(array('sous_groupes' => $sous_groupe));

        return $this->render('sous_groupes/show.html.twig', array(
            'sous_groupe' => $sous_groupe,
            'especes' => $especes,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sous_groupe entity.
     *
     */
    public function editAction(Request $request, Sous_groupes $sous_groupe)
    {
        $sous_groupe2=$sous_groupe;
        $deleteForm = $this->createDeleteForm($sous_groupe);
        $editForm = $this->createForm('AppBundle\Form\Sous_groupesType', $sous_groupe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($sous_groupe->getImage() == NULL)
            {
                $sous_groupe->setImage($sous_groupe2->getImage()); 
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sous_groupes_edit', array('id' => $sous_groupe->getId()));
        }

        return $this->render('sous_groupes/edit.html.twig', array(
            'sous_groupe' => $sous_groupe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sous_groupe entity.
     *
     */
    public function deleteAction(Request $request, Sous_groupes $sous_groupe)
    {
        $form = $this->createDeleteForm($sous_groupe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sous_groupe);
            $em->flush();
        }

        return $this->redirectToRoute('sous_groupes_index');
    }

    /**
     * Creates a form to delete a sous_groupe entity.
     *
     * @param Sous_groupes $sous_groupe The sous_groupe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Sous_groupes $sous_groupe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sous_groupes_delete', array('id' => $sous_groupe->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    public function deleteImageAction(Request $request, Sous_groupes $sous_groupe)
    {
        $sous_groupe->setImage(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('sous_groupes_edit', array('id' => $sous_groupe->getId()));
        
    }
}
