<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Projet_typeObservations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Projet_typeobservation controller.
 *
 */
class Projet_typeObservationsController extends Controller
{
    /**
     * Lists all projet_typeObservation entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projet_typeObservations = $em->getRepository('AppBundle:Projet_typeObservations')->findAll();

        return $this->render('projet_typeobservations/index.html.twig', array(
            'projet_typeObservations' => $projet_typeObservations,
        ));
    }

    /**
     * Creates a new projet_typeObservation entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $projet_typeObservation = new Projet_typeObservations();
        $form = $this->createForm('AppBundle\Form\Projet_typeObservationsType', $projet_typeObservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $projet_typeObservation->setProjet($projet);
            $em->persist($projet_typeObservation);
            $em->flush();

            return $this->redirectToRoute('projet_typeobservations_show', array('id' => $projet_typeObservation->getId()));
        }

        return $this->render('projet_typeobservations/new.html.twig', array(
            'projet_typeObservation' => $projet_typeObservation,
            'form' => $form->createView(),
            'projet' => $projet,
        ));
    }

    /**
     * Finds and displays a projet_typeObservation entity.
     *
     */
    public function showAction(Request $request, Projet_typeObservations $projet_typeObservation)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
        
        $deleteForm = $this->createDeleteForm($projet_typeObservation);

        return $this->render('projet_typeobservations/show.html.twig', array(
            'projet_typeObservation' => $projet_typeObservation,
            'delete_form' => $deleteForm->createView(),
            'projet' => $projet,
        ));
    }

    /**
     * Displays a form to edit an existing projet_typeObservation entity.
     *
     */
    public function editAction(Request $request, Projet_typeObservations $projet_typeObservation)
    {
        $deleteForm = $this->createDeleteForm($projet_typeObservation);
        $editForm = $this->createForm('AppBundle\Form\Projet_typeObservationsType', $projet_typeObservation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projet_typeobservations_edit', array('id' => $projet_typeObservation->getId()));
        }

        return $this->render('projet_typeobservations/edit.html.twig', array(
            'projet_typeObservation' => $projet_typeObservation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a projet_typeObservation entity.
     *
     */
    public function deleteAction(Request $request, Projet_typeObservations $projet_typeObservation)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
        
        $form = $this->createDeleteForm($projet_typeObservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projet_typeObservation);
            $em->flush();
        }

        return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
    }

    /**
     * Creates a form to delete a projet_typeObservation entity.
     *
     * @param Projet_typeObservations $projet_typeObservation The projet_typeObservation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Projet_typeObservations $projet_typeObservation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projet_typeobservations_delete', array('id' => $projet_typeObservation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
