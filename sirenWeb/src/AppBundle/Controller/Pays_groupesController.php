<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pays_groupes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Pays_groupe controller.
 *
 */
class Pays_groupesController extends Controller
{
    /**
     * Lists all pays_groupe entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pays_groupes = $em->getRepository('AppBundle:Pays_groupes')->findAll();

        return $this->render('pays_groupes/index.html.twig', array(
            'pays_groupes' => $pays_groupes,
        ));
    }

    /**
     * Creates a new pays_groupe entity.
     *
     */
    public function newAction(Request $request)
    {
        $pays_groupe = new Pays_groupes();
        $form = $this->createForm('AppBundle\Form\Pays_groupesType', $pays_groupe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pays_groupe);
            $em->flush();

            return $this->redirectToRoute('pays_groupes_show', array('id' => $pays_groupe->getId()));
        }

        return $this->render('pays_groupes/new.html.twig', array(
            'pays_groupe' => $pays_groupe,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pays_groupe entity.
     *
     */
    public function showAction(Pays_groupes $pays_groupe)
    {
        $deleteForm = $this->createDeleteForm($pays_groupe);

        return $this->render('pays_groupes/show.html.twig', array(
            'pays_groupe' => $pays_groupe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pays_groupe entity.
     *
     */
    public function editAction(Request $request, Pays_groupes $pays_groupe)
    {
        $deleteForm = $this->createDeleteForm($pays_groupe);
        $editForm = $this->createForm('AppBundle\Form\Pays_groupesType', $pays_groupe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pays_groupes_edit', array('id' => $pays_groupe->getId()));
        }

        return $this->render('pays_groupes/edit.html.twig', array(
            'pays_groupe' => $pays_groupe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pays_groupe entity.
     *
     */
    public function deleteAction(Request $request, Pays_groupes $pays_groupe)
    {
        $form = $this->createDeleteForm($pays_groupe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pays_groupe);
            $em->flush();
        }

        return $this->redirectToRoute('pays_groupes_index');
    }

    /**
     * Creates a form to delete a pays_groupe entity.
     *
     * @param Pays_groupes $pays_groupe The pays_groupe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pays_groupes $pays_groupe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pays_groupes_delete', array('id' => $pays_groupe->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
