<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pays_especes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Pays_espece controller.
 *
 */
class Pays_especesController extends Controller
{
    /**
     * Lists all pays_espece entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pays_especes = $em->getRepository('AppBundle:Pays_especes')->findAll();

        return $this->render('pays_especes/index.html.twig', array(
            'pays_especes' => $pays_especes,
        ));
    }

    /**
     * Creates a new pays_espece entity.
     *
     */
    public function newAction(Request $request)
    {
        $pays_espece = new Pays_especes();
        $form = $this->createForm('AppBundle\Form\Pays_especesType', $pays_espece);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pays_espece);
            $em->flush();

            return $this->redirectToRoute('pays_especes_new');
        }

        return $this->render('pays_especes/new.html.twig', array(
            'pays_espece' => $pays_espece,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pays_espece entity.
     *
     */
    public function showAction(Pays_especes $pays_espece)
    {
        $deleteForm = $this->createDeleteForm($pays_espece);

        return $this->render('pays_especes/show.html.twig', array(
            'pays_espece' => $pays_espece,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pays_espece entity.
     *
     */
    public function editAction(Request $request, Pays_especes $pays_espece)
    {
        $deleteForm = $this->createDeleteForm($pays_espece);
        $editForm = $this->createForm('AppBundle\Form\Pays_especesType', $pays_espece);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pays_especes_edit', array('id' => $pays_espece->getId()));
        }

        return $this->render('pays_especes/edit.html.twig', array(
            'pays_espece' => $pays_espece,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pays_espece entity.
     *
     */
    public function deleteAction(Request $request, Pays_especes $pays_espece)
    {
        $form = $this->createDeleteForm($pays_espece);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pays_espece);
            $em->flush();
        }

        return $this->redirectToRoute('pays_especes_index');
    }

    /**
     * Creates a form to delete a pays_espece entity.
     *
     * @param Pays_especes $pays_espece The pays_espece entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pays_especes $pays_espece)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pays_especes_delete', array('id' => $pays_espece->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
