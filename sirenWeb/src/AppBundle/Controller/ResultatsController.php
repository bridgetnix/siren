<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Resultats;
use AppBundle\Entity\Reponses;
use AppBundle\Entity\Questions;
use AppBundle\Entity\Observations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Resultat controller.
 *
 */
class ResultatsController extends Controller
{
    /**
     * Lists all resultat entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $resultats = $em->getRepository('AppBundle:Resultats')->findAll();

        return $this->render('resultats/index.html.twig', array(
            'resultats' => $resultats,
        ));
    }

    /**
     * Creates a new resultat entity.
     *
     */
    public function newAction(Request $request)
    {
        $resultat = new Resultats();
        $form = $this->createForm('AppBundle\Form\ResultatsType', $resultat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($resultat);
            $em->flush();

            return $this->redirectToRoute('resultats_show', array('id' => $resultat->getId()));
        }

        return $this->render('resultats/new.html.twig', array(
            'resultat' => $resultat,
            'form' => $form->createView(),
        ));
    }
    
    /**
     * Creates a new resultat entity.
     *
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
           
        if ($request->getMethod() == 'POST') {
           $res=$_POST['contenu'];
           $questions=$em->getRepository('AppBundle:Questions')->find($_POST['question']);
           $observations=$em->getRepository('AppBundle:Observations')->find($_POST['observation']);
           $type=$_POST['type'];
           
           $resultat= new Resultats();
            if ($type==1)
            {
                $resultat->setContenu($res);
            }
            else
            {
                $reponses = new Reponses();
                $reponses=$em->getRepository('AppBundle:Reponses')->find($res);
                $resultat->setContenu($reponses->getTitreFr());
            }
           $resultat->setObservations($observations);
           $resultat->setQuestions($questions);
           $em->persist($resultat);
           $em->flush();
           
           if (!isset($_POST['rep']))
           {
               if($questions->getQuestions() != null)
               {
                   $question=$questions->getQuestions();
               
                    if($question->getTypeReponse()== "text")
                    {
                       return $this->render('resultats/new.html.twig', array('observation' => $observations->getId(),'question' => $question,'type' => 1));

                    }
                    else {
                        $resultats = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
                        return $this->render('resultats/new.html.twig', array('observation' => $observations->getId(),'question' => $question,'type' => 2,'reponses' => $resultats));

                     }
                   
               }
               else 
               {
                      return $this->render('resultats/fin.html.twig', array('observation' => $observations->getId()));

               }
           }
           else
           {
               if($reponses->getQuestionsnext() != null)
               {
                   $question=$reponses->getQuestionsnext();
               
                    if($question->getTypeReponse()== "text")
                    {
                       return $this->render('resultats/new.html.twig', array('observation' => $observations->getId(),'question' => $question,'type' => 1));

                    }
                    else {
                        $resultats = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
                        return $this->render('resultats/new.html.twig', array('observation' => $observations->getId(),'question' => $question,'type' => 2,'reponses' => $resultats));

                     }
                   
               }
               else 
               {
                      return $this->render('resultats/fin.html.twig', array('observation' => $observations->getId()));

               }
           }
               
           
          
        }
        
    }

    /**
     * Finds and displays a resultat entity.
     *
     */
    public function showAction(Resultats $resultat)
    {
        $deleteForm = $this->createDeleteForm($resultat);

        return $this->render('resultats/show.html.twig', array(
            'resultat' => $resultat,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing resultat entity.
     *
     */
    public function editAction(Request $request, Resultats $resultat)
    {
        $deleteForm = $this->createDeleteForm($resultat);
        $editForm = $this->createForm('AppBundle\Form\ResultatsType', $resultat);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('observations_show', array('id' => $resultat->getObservations()->getId()));
        }

        return $this->render('resultats/edit.html.twig', array(
            'resultat' => $resultat,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a resultat entity.
     *
     */
    public function deleteAction(Request $request, Resultats $resultat)
    {
        $observation = $resultat->getObservations();
        
        $em = $this->getDoctrine()->getManager();
            $em->remove($resultat);
            $em->flush();
        return $this->redirectToRoute('observations_show', array('id' => $observation->getId()));
    }

    /**
     * Creates a form to delete a resultat entity.
     *
     * @param Resultats $resultat The resultat entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Resultats $resultat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('resultats_delete', array('id' => $resultat->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
