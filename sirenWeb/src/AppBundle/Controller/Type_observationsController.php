<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Type_observations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * type_observations controller.
 *
 */
class Type_observationsController extends Controller
{
    /**
     * Lists all type_observations entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $type_observations = $em->getRepository('AppBundle:Type_observations')->findAll();

        return $this->render('type_observations/index.html.twig', array(
            'type_observations' => $type_observations,
        ));
    }

    /**
     * Creates a new type_observations entity.
     *
     */
    public function newAction(Request $request)
    {
        $type_observations = new Type_observations();
        $form = $this->createForm('AppBundle\Form\Type_observationsType', $type_observations);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($type_observations);
            $em->flush();

            return $this->redirectToRoute('type_observations_show', array('id' => $type_observations->getId()));
        }

        return $this->render('type_observations/new.html.twig', array(
            'type_observations' => $type_observations,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a type_observations entity.
     *
     */
    public function showAction(Type_observations $type_observations)
    {
        $deleteForm = $this->createDeleteForm($type_observations);

        return $this->render('type_observations/show.html.twig', array(
            'type_observations' => $type_observations,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing type_observations entity.
     *
     */
    public function editAction(Request $request, Type_observations $type_observations)
    {
        $type_observations2=$type_observations;
        $deleteForm = $this->createDeleteForm($type_observations);
        $editForm = $this->createForm('AppBundle\Form\Type_observationsType', $type_observations);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
             if($type_observations->getImage() == NULL)
            {
                $type_observations->setImage($type_observations2->getImage()); 
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_observations_edit', array('id' => $type_observations->getId()));
        }

        return $this->render('type_observations/edit.html.twig', array(
            'type_observations' => $type_observations,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a type_observations entity.
     *
     */
    public function deleteAction(Request $request, Type_observations $type_observations)
    {
        $form = $this->createDeleteForm($type_observations);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($type_observations);
            $em->flush();
        }

        return $this->redirectToRoute('type_observations_index');
    }

    /**
     * Creates a form to delete a type_observations entity.
     *
     * @param Type_observations $type_observations The type_observations entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Type_observations $type_observations)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('type_observations_delete', array('id' => $type_observations->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    public function deleteImageAction(Request $request, Type_observations $type_observations)
    {
        $type_observations->setImage(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('type_observations_edit', array('id' => $type_observations->getId()));
        
    }
}
