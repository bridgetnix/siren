<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Boussole;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Boussole controller.
 *
 */
class BoussoleController extends Controller
{
    /**
     * Lists all boussole entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $boussoles = $em->getRepository('AppBundle:Boussole')->findAll();

        return $this->render('boussole/index.html.twig', array(
            'boussoles' => $boussoles,
        ));
    }

    /**
     * Creates a new boussole entity.
     *
     */
    public function newAction(Request $request)
    {
        $boussole = new Boussole();
        $form = $this->createForm('AppBundle\Form\BoussoleType', $boussole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($boussole);
            $em->flush();

            return $this->redirectToRoute('boussole_show', array('id' => $boussole->getId()));
        }

        return $this->render('boussole/new.html.twig', array(
            'boussole' => $boussole,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a boussole entity.
     *
     */
    public function showAction(Boussole $boussole)
    {
        $deleteForm = $this->createDeleteForm($boussole);

        return $this->render('boussole/show.html.twig', array(
            'boussole' => $boussole,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing boussole entity.
     *
     */
    public function editAction(Request $request, Boussole $boussole)
    {
        $deleteForm = $this->createDeleteForm($boussole);
        $editForm = $this->createForm('AppBundle\Form\BoussoleType', $boussole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('boussole_edit', array('id' => $boussole->getId()));
        }

        return $this->render('boussole/edit.html.twig', array(
            'boussole' => $boussole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a boussole entity.
     *
     */
    public function deleteAction(Request $request, Boussole $boussole)
    {
        $form = $this->createDeleteForm($boussole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($boussole);
            $em->flush();
        }

        return $this->redirectToRoute('boussole_index');
    }

    /**
     * Creates a form to delete a boussole entity.
     *
     * @param Boussole $boussole The boussole entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Boussole $boussole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('boussole_delete', array('id' => $boussole->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
