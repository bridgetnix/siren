<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Notifications_projet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Notifications_projet controller.
 *
 */
class Notifications_projetController extends Controller
{
    /**
     * Lists all notifications_projet entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $notifications_projets = $em->getRepository('AppBundle:Notifications_projet')->findAll();

        return $this->render('notifications_projet/index.html.twig', array(
            'notifications_projets' => $notifications_projets,
        ));
    }

    /**
     * Creates a new notifications_projet entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $notifications_projet = new Notifications_projet();
        $form = $this->createForm('AppBundle\Form\Notifications_projetType', $notifications_projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $notifications_projet->setProjet($projet);
            $em->persist($notifications_projet);
            $em->flush();

            return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
        }
        
        return $this->render('notifications_projet/new.html.twig', array(
            'notifications_projet' => $notifications_projet,
            'form' => $form->createView(),
            'projet' => $projet,
            
        ));
    }

    /**
     * Finds and displays a notifications_projet entity.
     *
     */
    public function showAction(Notifications_projet $notifications_projet,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $deleteForm = $this->createDeleteForm($notifications_projet);

        return $this->render('notifications_projet/show.html.twig', array(
            'notifications_projet' => $notifications_projet,
            'delete_form' => $deleteForm->createView(),
            'projet' => $projet,
        ));
    }

    /**
     * Displays a form to edit an existing notifications_projet entity.
     *
     */
    public function editAction(Request $request, Notifications_projet $notifications_projet)
    {
        $deleteForm = $this->createDeleteForm($notifications_projet);
        $editForm = $this->createForm('AppBundle\Form\Notifications_projetType', $notifications_projet);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('notifications_projet_edit', array('id' => $notifications_projet->getId()));
        }

        return $this->render('notifications_projet/edit.html.twig', array(
            'notifications_projet' => $notifications_projet,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a notifications_projet entity.
     *
     */
    public function deleteAction(Request $request, Notifications_projet $notifications_projet)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $form = $this->createDeleteForm($notifications_projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($notifications_projet);
            $em->flush();
        }

        return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
    }

    /**
     * Creates a form to delete a notifications_projet entity.
     *
     * @param Notifications_projet $notifications_projet The notifications_projet entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Notifications_projet $notifications_projet)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('notifications_projet_delete', array('id' => $notifications_projet->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
