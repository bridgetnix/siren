<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Questions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Question controller.
 *
 */
class QuestionsController extends Controller
{
    /**
     * Lists all question entities.
     *
     */
    public function indexAction(Request $request)
    {
         $session = $request->getSession();
        $session->remove('question');
         $session->remove('projet');
        
        $em = $this->getDoctrine()->getManager();

        $questions = $em->getRepository('AppBundle:Questions')->findBy(
        array('projet' => NULL),
        array('id' => 'ASC')
    );

        return $this->render('questions/index.html.twig', array(
            'questions' => $questions,
        ));
    }

    /**
     * Creates a new question entity.
     *
     */
    public function newAction(Request $request)
    {
        $question = new Questions();
        $form = $this->createForm('AppBundle\Form\QuestionsType', $question);
        $form->handleRequest($request);
        
        $session = $request->getSession();
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
         
            if($session->get("projet") !=NULL)
            {
                $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
        
                $question->setProjet($projet);
            }
        
            $em->persist($question);
            $em->flush();
            
            if($session->get("projet") !=NULL)
            {
                $oldquestion=$projet->getQuestions();
                if ($question->getTypeReponse()=='text')
                {
                    $question->setQuestions($oldquestion);
                }
                else{
                    $reponses = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
                    
                    foreach($reponses as $rep)
                    {
                        $rep->setQuestionsNext($oldquestion);
                        $em->flush();
                        
                    }
        
                }
                $projet->setQuestions($question);
                $em->flush();
            }
            

            return $this->redirectToRoute('questions_show', array('id' => $question->getId()));
        }
        
        $retour=0;
        if($session->get("projet") !=NULL)
            {
                $retour=$session->get("projet");
            }
        
        
        return $this->render('questions/new.html.twig', array(
            'question' => $question,
            'retour' => $retour,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a question entity.
     *
     */
    public function showAction(Questions $question, Request $request)
    {
        $session = $request->getSession();
        $deleteForm = $this->createDeleteForm($question);
        
        $em = $this->getDoctrine()->getManager();
        if ($question->getTypeReponse()=='text')
{
    $reponses='text';
}
else
{
        $reponses = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
}
$session->set('question', $question->getId());
        
        $retour=0;
        if($session->get("projet") !=NULL)
            {
                $retour=$session->get("projet");
            }
        
        
        return $this->render('questions/show.html.twig', array(
            'question' => $question,
            'reponses' => $reponses,
            'retour' => $retour,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing question entity.
     *
     */
    public function editAction(Request $request, Questions $question)
    {
        $question2=$question;
        $session = $request->getSession();
        
        $deleteForm = $this->createDeleteForm($question);
        $editForm = $this->createForm('AppBundle\Form\QuestionsType', $question);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($question->getImage() == NULL)
            {
                $question->setImage($question2->getImage()); 
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('questions_edit', array('id' => $question->getId()));
        }
        
        $retour=0;
        if($session->get("projet") !=NULL)
            {
                $retour=$session->get("projet");
            }
        

        return $this->render('questions/edit.html.twig', array(
            'question' => $question,
            'retour' => $retour,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a question entity.
     *
     */
    public function deleteAction(Request $request, Questions $question)
    {
        $form = $this->createDeleteForm($question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($question);
            $em->flush();
        }
        
        if($session->get("projet") !=NULL)
            {
                return $this->redirectToRoute('projet_show', array('id' => $session->get("projet")));
            }
        

        return $this->redirectToRoute('questions_index');
    }

    /**
     * Creates a form to delete a question entity.
     *
     * @param Questions $question The question entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Questions $question)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('questions_delete', array('id' => $question->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    public function deleteImageAction(Request $request, Questions $question)
    {
        $question->setImage(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('questions_edit', array('id' => $question->getId()));
        
    }
}
