<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Projet;
use AppBundle\Entity\Menu;
use AppBundle\Entity\Projet_menu;
use AppBundle\Entity\Projet_typeObservations;
use AppBundle\Entity\Type_observations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Projet controller.
 *
 */
class ProjetController extends Controller
{
    /**
     * Lists all projet entities.
     *
     */
    public function indexAction(Request $request)
    {
        $session = $request->getSession();
        
        $em = $this->getDoctrine()->getManager();
        
        /*
        $pos = $em->getRepository('AppBundle:Projet')->findAll();
        $nb = count($pos);
            
        for($i = 0; $i < $nb; $i++){
            $po = $pos[$i];
            
            $me1 = $em->getRepository('AppBundle:Menu')->find(13);
            $me2 = $em->getRepository('AppBundle:Menu')->find(14);
            
            $projet_menu1 = new Projet_menu();
            $projet_menu1->setMenu($me1);
            $projet_menu1->setProjet($po);
            $em->persist($projet_menu1);
            
            $projet_menu2 = new Projet_menu();
            $projet_menu2->setMenu($me2);
            $projet_menu2->setProjet($po);
            $em->persist($projet_menu2);
            
            $em->flush();
            
            
            
            $tes = $em->getRepository('AppBundle:Type_observations')->findAll();
            $nb2 = count($tes);
                
            for($k = 0; $k < $nb2; $k++){
                $te = $tes[$k];
                $projet_typeObservation = new Projet_typeObservations();
                $projet_typeObservation->setTypeObservations($te);
                $projet_typeObservation->setProjet($po);
                
                $em->persist($projet_typeObservation);
                $em->flush();
                
            }
            
            
        }
        */
        
        $result= NULL;
        if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
             $result = $em->getRepository('AppBundle:Projet')->findBy(
                array(),
                array('nom' => 'ASC')
            );
                
        }
        else{
            $liste_projet = $em->getRepository('AppBundle:Projet_utilisateur')->findByUtilisateurs($this->getUser());
            
            $nb = count($liste_projet);
            
            for($i = 0; $i < $nb; $i++){
                $projet = $liste_projet[$i];
                $result[$i]= $projet->getProjet();
            }   
        }


        
        
        $session->remove('projet');

        return $this->render('projet/index.html.twig', array(
            'projets' => $result,
        ));
    }

    /**
     * Creates a new projet entity.
     *
     */
    public function newAction(Request $request)
    {
        $projet = new Projet();
        $form = $this->createForm('AppBundle\Form\ProjetType', $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $currentUser = $this->getUser();
            $projet->setUtilisateurs($currentUser);
            $em->persist($projet);
            $em->flush();
            
            
            
            $notification_projet=  new \AppBundle\Entity\Notifications_projet();
            $notification_projet->setProjet($projet);
            $notification_projet->setNomFr('Bienvenue sur le projet');
            $notification_projet->setNomEn('Welcome to the project');
            $em->persist($notification_projet);
            $em->flush();
            
            $liste_especes = $em->getRepository('AppBundle:Especes')->findByDefaut("1");
            
            $nb = count($liste_especes);
            
            for($i = 0; $i < $nb; $i++){
                $especes = $liste_especes[$i];
                $projet_especes=  new \AppBundle\Entity\Projet_especes();
                $projet_especes->setProjet($projet);
                $projet_especes->setEspeces($especes);
                $em->persist($projet_especes);
                $em->flush();
            }
            
            if($projet->getPublic()) {
                $liste_utilisateurs = $em->getRepository('AppBundle:Utilisateurs')->findAll();
                $nb_util = count($liste_utilisateurs);
            
                for($i = 0; $i < $nb_util; $i++){
                    $util = $liste_utilisateurs[$i];
                    
                    $projet_utilisateur=  new \AppBundle\Entity\Projet_utilisateur();
                    $projet_utilisateur->setProjet($projet);
                    $projet_utilisateur->setUtilisateurs($util);
                    $em->persist($projet_utilisateur);
                    $em->flush();
                }
            }
            
            
            
            return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
        }

        return $this->render('projet/new.html.twig', array(
            'projet' => $projet,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a projet entity.
     *
     */
    public function showAction(Projet $projet,Request $request)
    {
        $session = $request->getSession();
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($projet);

         $observations = $em->getRepository('AppBundle:Observations')->findBy(
            array('projet' => $projet),
            array('id' => 'DESC')
        );
        
        $projet_especes = $em->getRepository('AppBundle:Projet_especes')->findBy(
            array('projet' => $projet),
            array('id' => 'ASC')
        );
        
        $projet_utilisateurs = $em->getRepository('AppBundle:Projet_utilisateur')->findBy(
            array('projet' => $projet),
            array('id' => 'ASC')
        );
        
        $projet_menus = $em->getRepository('AppBundle:Projet_menu')->findBy(
            array('projet' => $projet),
            array('id' => 'ASC')
        );
        
        $projet_type_observations = $em->getRepository('AppBundle:Projet_typeObservations')->findBy(
            array('projet' => $projet),
            array('id' => 'ASC')
        );
        
        $notifications_projets = $em->getRepository('AppBundle:Notifications_projet')->findBy(
            array('projet' => $projet),
            array('id' => 'DESC')
        );
        
        $questions = $em->getRepository('AppBundle:Questions')->findBy(
            array('projet' => $projet),
            array('id' => 'DESC')
        );
        
        $session->set('projet', $projet->getId());
        
        return $this->render('projet/show.html.twig', array(
            'projet' => $projet,
            'observations' => $observations,
            'projet_utilisateurs' => $projet_utilisateurs,
            'projet_menus' => $projet_menus,
            'projet_type_observations' => $projet_type_observations,
            'projet_especes' => $projet_especes,
            'notifications_projets' => $notifications_projets,
            'questions' => $questions,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing projet entity.
     *
     */
    public function editAction(Request $request, Projet $projet)
    {
        $projet2=$projet;
        $deleteForm = $this->createDeleteForm($projet);
        $editForm = $this->createForm('AppBundle\Form\ProjetType', $projet);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            
            if($projet->getImage() == NULL)
            {
                $projet->setImage($projet2->getImage()); 
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projet_edit', array('id' => $projet->getId()));
        }

        return $this->render('projet/edit.html.twig', array(
            'projet' => $projet,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a projet entity.
     *
     */
    public function deleteAction(Request $request, Projet $projet)
    {
        $form = $this->createDeleteForm($projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projet);
            $em->flush();
        }

        return $this->redirectToRoute('projet_index');
    }

    /**
     * Creates a form to delete a projet entity.
     *
     * @param Projet $projet The projet entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Projet $projet)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projet_delete', array('id' => $projet->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    public function deleteImageAction(Request $request, Projet $projet)
    {
        $projet->setImage(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('projet_edit', array('id' => $projet->getId()));
        
    }
}
