<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Observations;
use AppBundle\Entity\Projet_menu;
use AppBundle\Entity\Projet_typeObservations;
use AppBundle\Entity\Type_observations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpClient\HttpClient;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();

        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array(),
        array('id' => 'DESC',),20
    );
        $obs = $em->getRepository('AppBundle:Observations')->findAll();
        $prs = $em->getRepository('AppBundle:Projet')->findAll();
        $ess = $em->getRepository('AppBundle:Especes')->findAll();
        $uts = $em->getRepository('AppBundle:Utilisateurs')->findAll();
        
        
        if ($this->getUser())
        {
            if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            $projet = $em->getRepository('AppBundle:Projet')->findAll();
            }
            else {
                $projet = $em->getRepository('AppBundle:Projet')->findByUtilisateurs($this->getUser()); 
            }
        
        }
        else {
            $projet=NULL;
        }
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'observations' => $observations,
            'projet' => $projet,
            'ob' => count($obs),
            'pr' => count($prs),
            'es' => count($ess),
            'ut' => count($uts),
            'page' => 1
        ));
    }
    
    /**
     * @Route("/plus/{page}", name="homepage2")
     */
    public function index2Action(Request $request, $page)
    {
        $em = $this->getDoctrine()->getManager();

        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array(),
        array('id' => 'DESC',),($page *20),(($page *20)-20)
    );
         $obs = $em->getRepository('AppBundle:Observations')->findAll();
        $prs = $em->getRepository('AppBundle:Projet')->findAll();
        $ess = $em->getRepository('AppBundle:Especes')->findAll();
        $uts = $em->getRepository('AppBundle:Utilisateurs')->findAll();
        
        if ($this->getUser())
        {
            if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            $projet = $em->getRepository('AppBundle:Projet')->findAll();
            }
            else {
                $projet = $em->getRepository('AppBundle:Projet')->findByUtilisateurs($this->getUser()); 
            }
        
        }
        else {
            $projet=NULL;
        }
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'observations' => $observations,
            'projet' => $projet,
            'ob' => count($obs),
            'pr' => count($prs),
            'es' => count($ess),
            'ut' => count($uts),
            'page' => $page,
        ));
    }
    
    /**
     * @Route("/renatura", name="homepageRenatura")
     */
    public function indexRenaturaAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        
        $proj = $em->getRepository('AppBundle:Projet')->find('62153');

        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj),
        array('id' => 'DESC',),20
        );
        $obs = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj)
        );
        $prs = $em->getRepository('AppBundle:Projet')->findAll();
        
        $ess = array();
        foreach($obs as $ob) {
            $espece = $ob->getEspeces();
            if ($espece != null) {
                if (!isset($ess[$espece->getId()])) {
                $ess[$espece->getId()] = 1;
                } else {
                    $ess[$espece->getId()]++;
                }
            }
        }
        $uts= array();
        foreach($obs as $ob) {
            $username = $ob->getUtilisateurs()->getUsername();
            
            if (!isset($uts[$username])) {
            $uts[$username] = 1;
            } else {
                $uts[$username]++;
            }
            
        }
        
        if ($this->getUser())
        {
            if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            $projet = $em->getRepository('AppBundle:Projet')->findAll();
            }
            else {
                $projet = $em->getRepository('AppBundle:Projet')->findByUtilisateurs($this->getUser()); 
            }
        
        }
        else {
            $projet=NULL;
        }
        // replace this example code with whatever you need
        return $this->render('default/indexRenatura.html.twig', array(
            'observations' => $observations,
            'projet' => $projet,
            'ob' => count($obs),
            'pr' => count($prs),
            'es' => count($ess),
            'ut' => count($uts),
            'page' => 1
        ));
    }
    
    /**
     * @Route("/renatura/plus/{page}", name="homepage2Renatura")
     */
    public function index2RenaturaAction(Request $request, $page)
    {
        $em = $this->getDoctrine()->getManager();

        $proj = $em->getRepository('AppBundle:Projet')->find('62153');
        
        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj),
        array('id' => 'DESC',),($page *20),(($page *20)-20)
    );
        $obs = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj)
        );
        $prs = $em->getRepository('AppBundle:Projet')->findAll();
        $ess = array();
        foreach($obs as $ob) {
            $espece = $ob->getEspeces();
            if ($espece != null) {
                if (!isset($ess[$espece->getId()])) {
                $ess[$espece->getId()] = 1;
                } else {
                    $ess[$espece->getId()]++;
                }
            }
        }
        $uts= array();
        foreach($obs as $ob) {
            $username = $ob->getUtilisateurs()->getUsername();
            
            if (!isset($uts[$username])) {
            $uts[$username] = 1;
            } else {
                $uts[$username]++;
            }
            
        }
        
        if ($this->getUser())
        {
            if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            $projet = $em->getRepository('AppBundle:Projet')->findAll();
            }
            else {
                $projet = $em->getRepository('AppBundle:Projet')->findByUtilisateurs($this->getUser()); 
            }
        
        }
        else {
            $projet=NULL;
        }
        // replace this example code with whatever you need
        return $this->render('default/indexRenatura.html.twig', array(
            'observations' => $observations,
            'projet' => $projet,
            'ob' => count($obs),
            'pr' => count($prs),
            'es' => count($ess),
            'ut' => count($uts),
            'page' => $page,
        ));
    }
    
    
    /**
     * @Route("/admin", name="admin")
     */
    public function adminAction(Request $request)
    {
        if( NULL == $this->getUser() ){
            
            return $this->redirectToRoute('fos_user_security_login');
        }
        
        $em = $this->getDoctrine()->getManager();

        if ($this->getUser())
        {
            if(in_array('ROLE_ADMIN', $this->getUser()->getRoles()) && $this->getUser()->getProjet()->getId() == 62153){
                $observations1 = $em->getRepository('AppBundle:Observations')->findBy(
                    array('projet' => $this->getUser()->getProjet(), 'etat' => 1),
                    array('id' => 'DESC',));
                $observations2 = $em->getRepository('AppBundle:Observations')->findBy(
                    array('projet' => $this->getUser()->getProjet(), 'etat' => 0),
                    array('id' => 'DESC',));
            }
            else {
                $observations1 = $em->getRepository('AppBundle:Observations')->findBy(
                    array('etat' => 1),
                    array('id' => 'DESC',));
                $observations2 = $em->getRepository('AppBundle:Observations')->findBy(
                    array('etat' => 0),
                    array('id' => 'DESC',));
            }
        
        }
        
        $projet = $em->getRepository('AppBundle:Projet')->findAll();
        
        $especes = $em->getRepository('AppBundle:Especes')->findAll();
        
        $sous_groupes = $em->getRepository('AppBundle:Sous_groupes')->findAll();
        
        $groupes = $em->getRepository('AppBundle:Groupes')->findAll();
        
        $notifications = $em->getRepository('AppBundle:Notifications')->findAll();
        
        $pays = $em->getRepository('AppBundle:Pays')->findAll();
        
        $type_observations = $em->getRepository('AppBundle:Type_observations')->findAll();
        
        $questions = $em->getRepository('AppBundle:Questions')->findAll();
        
        $utilisateurs = $em->getRepository('AppBundle:Utilisateurs')->findAll();
        
        $boussoles = $em->getRepository('AppBundle:Boussole')->findAll();
        $marches = $em->getRepository('AppBundle:Marche')->findAll();
        $menus = $em->getRepository('AppBundle:Menu')->findAll();
        
        $resultats = $em->getRepository('AppBundle:Resultats')->findBy(array(), array('id'=>'ASC'), 0, 1000);
        $nb = count($resultats);
        for($i = 0; $i < $nb; $i++){
        
            $res = $resultats[$i];
            if($res->getContenu()=="Mort" or $res->getContenu()=="Death" or $res->getContenu()=="Fraichement mort" or $res->getContenu()=="Freshly dead" or $res->getContenu()=="Mort récemment/intact mais en début de décomposition" or $res->getContenu()=="Death recently / intact but early decomposition")
            {
                $obs=$res->getObservations();
                $obs->setMort(1);
                
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                
            }
            
        }
        
    
        // replace this example code with whatever you need
        return $this->render('default/admin.html.twig', array(
            'observations1' => count($observations1),
            'observations2' => count($observations2),
            'especes' => count($especes),
            'sous_groupes' => count($sous_groupes),
            'groupes' => count($groupes),
            'notifications' => count($notifications),
            'pays' => count($pays),
            'type_observations' => count($type_observations),
            'questions' => count($questions),
            'utilisateurs' => count($utilisateurs),
            'projet' => count($projet),
            'boussole' => count($boussoles),
            'marche' => count($marches),
            'menu' => count($menus),
        ));
    }
    
    /**
     * @Route("/statistique", name="statistique")
     */
    public function statistiqueAction(Request $request)
    {
        return $this->render('default/statistique.html.twig');
    }
    
    /**
     * @Route("/statistiqueRenatura", name="statistiqueRenatura")
     */
    public function statistiqueRenaturaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $proj = $em->getRepository('AppBundle:Projet')->find('62153');
        
        $obs1 = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj)
        );
        
        $t11 = $em->getRepository('AppBundle:Type_observations')->find(11);
        $t12 = $em->getRepository('AppBundle:Type_observations')->find(12);
        $t13 = $em->getRepository('AppBundle:Type_observations')->find(13);
        $t14 = $em->getRepository('AppBundle:Type_observations')->find(14);
        $t7 = $em->getRepository('AppBundle:Type_observations')->find(7);
        
        $obs2 = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'typeObservations' => $t11)
        );
        $obs3 = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'typeObservations' => $t12)
        );
        $obs4 = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'typeObservations' => $t13)
        );
        $obs5 = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'typeObservations' => $t14)
        );
        $obs6 = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'typeObservations' => $t7)
        );
        
        $obs7 = array();
        foreach($obs1 as $ob) {
            $espece = $ob->getEspeces();
            if ($espece != null) {
                if (!isset($ess[$espece->getId()])) {
                $ess[$espece->getId()] = 1;
                } else {
                    $ess[$espece->getId()]++;
                }
            }
        }
        
        $obs8 = array();
        foreach($obs1 as $ob) {
            $username = $ob->getUtilisateurs()->getUsername();
            
            if (!isset($obs8[$username])) {
            $obs8[$username] = 1;
            } else {
                $obs8[$username]++;
            }
            
        }
        
        
        return $this->render('default/statistiqueRenatura.html.twig', array(
            'obs1' => count($obs1),
            'obs2' => count($obs2),
            'obs3' => count($obs3),
            'obs4' => count($obs4),
            'obs5' => count($obs5),
            'obs6' => count($obs6),
            'obs7' => count($obs7),
            'obs8' => count($obs8),
            'obs9' => $obs8,
            
        ));
    }
    
     /**
     * @Route("/exportation", name="exportation")
     */
    public function exportationAction(Request $request)
    {
        if( NULL == $this->getUser() ){
            
            return $this->redirectToRoute('fos_user_security_login');
        }
        
        $em = $this->getDoctrine()->getManager();

        if(in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
            $projet = $em->getRepository('AppBundle:Projet')->findAll();
        }
        else {
            $projet = $em->getRepository('AppBundle:Projet')->findByUtilisateurs($this->getUser()); 
        }
        
        // replace this example code with whatever you need
        return $this->render('default/exportation.html.twig', array(
            'projet' => $projet,
        ));
    }
    
     public function CallAPI($method, $url, $data = false)
    {
        $site = "https://www.inaturalist.org";
        
        $curl = curl_init();
    
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
    
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $site.$url, http_build_query($data));
        }
    
        curl_setopt($curl, CURLOPT_URL, $site.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    
        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return $result;
    }
    
    
}