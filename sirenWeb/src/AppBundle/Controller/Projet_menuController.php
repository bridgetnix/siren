<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Projet_menu;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Projet_menu controller.
 *
 */
class Projet_menuController extends Controller
{
    /**
     * Lists all projet_menu entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projet_menus = $em->getRepository('AppBundle:Projet_menu')->findAll();

        return $this->render('projet_menu/index.html.twig', array(
            'projet_menus' => $projet_menus,
        ));
    }

    /**
     * Creates a new projet_menu entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $projet_menu = new Projet_menu();
        $form = $this->createForm('AppBundle\Form\Projet_menuType', $projet_menu);
        $form->handleRequest($request);
        

        if ($form->isSubmitted() && $form->isValid()) {
            
            $projet_menu->setProjet($projet);
            $em->persist($projet_menu);
            $em->flush();

            return $this->redirectToRoute('projet_menu_show', array('id' => $projet_menu->getId()));
        }

        return $this->render('projet_menu/new.html.twig', array(
            'projet_menu' => $projet_menu,
            'form' => $form->createView(),
            'projet' => $projet,
        ));
    }

    /**
     * Finds and displays a projet_menu entity.
     *
     */
    public function showAction(Request $request, Projet_menu $projet_menu)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
        $deleteForm = $this->createDeleteForm($projet_menu);

        return $this->render('projet_menu/show.html.twig', array(
            'projet_menu' => $projet_menu,
            'delete_form' => $deleteForm->createView(),
            'projet' => $projet,
        ));
    }

    /**
     * Displays a form to edit an existing projet_menu entity.
     *
     */
    public function editAction(Request $request, Projet_menu $projet_menu)
    {
        $deleteForm = $this->createDeleteForm($projet_menu);
        $editForm = $this->createForm('AppBundle\Form\Projet_menuType', $projet_menu);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projet_menu_edit', array('id' => $projet_menu->getId()));
        }

        return $this->render('projet_menu/edit.html.twig', array(
            'projet_menu' => $projet_menu,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a projet_menu entity.
     *
     */
    public function deleteAction(Request $request, Projet_menu $projet_menu)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
        
        $form = $this->createDeleteForm($projet_menu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projet_menu);
            $em->flush();
        }

        return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
    }

    /**
     * Creates a form to delete a projet_menu entity.
     *
     * @param Projet_menu $projet_menu The projet_menu entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Projet_menu $projet_menu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projet_menu_delete', array('id' => $projet_menu->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
