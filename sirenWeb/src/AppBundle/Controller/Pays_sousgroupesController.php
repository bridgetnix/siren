<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pays_sousgroupes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Pays_sousgroupe controller.
 *
 */
class Pays_sousgroupesController extends Controller
{
    /**
     * Lists all pays_sousgroupe entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $pays_sousgroupes = $em->getRepository('AppBundle:Pays_sousgroupes')->findAll();

        return $this->render('pays_sousgroupes/index.html.twig', array(
            'pays_sousgroupes' => $pays_sousgroupes,
        ));
    }

    /**
     * Creates a new pays_sousgroupe entity.
     *
     */
    public function newAction(Request $request)
    {
        $pays_sousgroupe = new Pays_sousgroupes();
        $form = $this->createForm('AppBundle\Form\Pays_sousgroupesType', $pays_sousgroupe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pays_sousgroupe);
            $em->flush();

            return $this->redirectToRoute('pays_sousgroupes_new');
        }

        return $this->render('pays_sousgroupes/new.html.twig', array(
            'pays_sousgroupe' => $pays_sousgroupe,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a pays_sousgroupe entity.
     *
     */
    public function showAction(Pays_sousgroupes $pays_sousgroupe)
    {
        $deleteForm = $this->createDeleteForm($pays_sousgroupe);

        return $this->render('pays_sousgroupes/show.html.twig', array(
            'pays_sousgroupe' => $pays_sousgroupe,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing pays_sousgroupe entity.
     *
     */
    public function editAction(Request $request, Pays_sousgroupes $pays_sousgroupe)
    {
        $deleteForm = $this->createDeleteForm($pays_sousgroupe);
        $editForm = $this->createForm('AppBundle\Form\Pays_sousgroupesType', $pays_sousgroupe);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pays_sousgroupes_edit', array('id' => $pays_sousgroupe->getId()));
        }

        return $this->render('pays_sousgroupes/edit.html.twig', array(
            'pays_sousgroupe' => $pays_sousgroupe,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pays_sousgroupe entity.
     *
     */
    public function deleteAction(Request $request, Pays_sousgroupes $pays_sousgroupe)
    {
        $form = $this->createDeleteForm($pays_sousgroupe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pays_sousgroupe);
            $em->flush();
        }

        return $this->redirectToRoute('pays_sousgroupes_index');
    }

    /**
     * Creates a form to delete a pays_sousgroupe entity.
     *
     * @param Pays_sousgroupes $pays_sousgroupe The pays_sousgroupe entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Pays_sousgroupes $pays_sousgroupe)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pays_sousgroupes_delete', array('id' => $pays_sousgroupe->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
