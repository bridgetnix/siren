<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Observations;
use AppBundle\Form\ObservationsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class ObservationsController extends Controller
{
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/observation")
     */
    public function ObservationAction(Request $request) {
		$projet=urldecode($request->request->get('projet'));
		$dateo=urldecode($request->request->get('dateo'));
		$utilisateur=urldecode($request->request->get('utilisateur'));
		$coordX=urldecode($request->request->get('coordX'));
		$coordY=urldecode($request->request->get('coordY'));
		$typeObservations=urldecode($request->request->get('typeObservations'));
		$groupe=urldecode($request->request->get('groupe'));
		$sousgroupe=urldecode($request->request->get('sousgroupe'));
		$espece=urldecode($request->request->get('espece'));
		$img1File=urldecode($request->request->get('img1File'));
		$img2File=urldecode($request->request->get('img2File'));
		$img3File=urldecode($request->request->get('img3File'));
		$img4File=urldecode($request->request->get('img4File'));
		$note=urldecode($request->request->get('note'));
		
        $this->metier = $this->get("app.observations.metier");
        $obs = new Observations();
        
        $date = new \DateTime();
        $date->setTimestamp($dateo);
        
        $pro= $this->get("app.projet.metier");
        $newprojet=$pro->find($projet);
        if($newprojet==null)
        {
            $newprojet=$pro->find(39);
        }
        
        $util= $this->get("app.utilisateurs.metier");
        $newutil=$util->find($utilisateur);
        
        $type= $this->get("app.typeobsevations.metier");
        $newtype=$type->find($typeObservations);
        
        $groupes= $this->get("app.groupes.metier");
        $newgroupes=$groupes->find($groupe);
        if($newgroupes==null)
        {
            $newgroupes=$groupes->find(1);
        }
        $sousgroupes= $this->get("app.sousgroupes.metier");
        $newsousgroupes=$sousgroupes->find($sousgroupe);
        if($newsousgroupes==null)
        {
            $newsousgroupes=$sousgroupes->find(1);
        }
        $especes= $this->get("app.especes.metier");
        $newespeces=$especes->find($espece);
        
        if($newespeces==null)
        {
            $newespeces=$especes->find(1);
        }
        $idtype = $newtype->getId();
        $alpha = $this->metier->findBy($idtype);
        $nbe=count($alpha)+1;
        $obs->setAlpha("".chr(832 +$idtype).$nbe);
        
        $obs->setUtilisateurs($newutil);
        $obs->setProjet($newprojet);
        $obs->setDate($date);
        $obs->setCoordX($coordX);
        $obs->setCoordY($coordY);
        $obs->setTypeObservations($newtype);
        $obs->setGroupes($newgroupes);
        $obs->setSousGroupes($newsousgroupes);
        $obs->setEspeces($newespeces);
        $obs->setCollecteur($newutil->getUsername());
        
        if ($img1File)
        {
           
                            $photo = $img1File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/observations/tof1/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $obs->setImg1($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $obs->setImg1('observation.png');
        }
        if ($img2File)
        {
            
                            $photo = $img2File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/observations/tof2/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $obs->setImg2($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img3File)
        {
                           $photo = $img3File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/observations/tof3/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $obs->setImg3($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img4File)
        {
           
                            $photo = $img4File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/observations/tof4/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $obs->setImg4($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($note)
        {
            $obs->setNote($note);
        }
        
        $this->metier->create($obs);
        return $obs->getId();
    }   
	 
    /**
     * @Rest\View()
     * @Rest\Get("/observationsuser/{utilisateurs}/{projet}")
     */
    public function getObservationsUserAction($utilisateurs,$projet) {
        $this->metier = $this->get("app.observations.metier");
        return $this->metier->user($utilisateurs,$projet);
    }

    /**
     * @Rest\View()
     * @Rest\Get("/observations/{projet}")
     */
    public function getObservationsAction($projet, Request $request) {
        $this->metier = $this->get("app.observations.metier");

        $pro= $this->get("app.projet.metier");
        $newprojet=$pro->find($projet);
        if($newprojet==null)
        {
            return null;
        }
        if(!$newprojet->getPublic()) {
            return null;
        }

        $observations =  $this->metier->findByProjet($newprojet);

        $obs= array();
        foreach($observations as $ob)
        {
            $ob->setImg1((null !== $ob->getImg1()) ? $request->getSchemeAndHttpHost().'/web/images/observations/tof1/'.$ob->getImg1(): null);
            $ob->setImg2((null !== $ob->getImg2()) ? $request->getSchemeAndHttpHost().'/web/images/observations/tof2/'.$ob->getImg2(): null);
            $ob->setImg3((null !== $ob->getImg3()) ? $request->getSchemeAndHttpHost().'/web/images/observations/tof3/'.$ob->getImg3(): null);
            $ob->setImg4((null !== $ob->getImg4()) ? $request->getSchemeAndHttpHost().'/web/images/observations/tof4/'.$ob->getImg4(): null);
            $obs[]=$ob;
        }
        return $obs;
    }
}
