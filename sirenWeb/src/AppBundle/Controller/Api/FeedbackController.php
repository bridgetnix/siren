<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Feedback;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class FeedbackController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/feedback/{nom}/{email}/{contenu}")
     */
    public function getFeedbackAction($nom,$email,$contenu) {
        
            $feedback = new Feedback();
            $feedback->setContenu($contenu);
            
            $date = new \DateTime('now');
            $feedback->setDate($date);

            $feedback->setEmail($email);
            $feedback->setNom($nom);
            

            $this->metier = $this->get("app.feedback.metier");
            $this->metier->create($feedback);
            
        return 1;
    }
}
