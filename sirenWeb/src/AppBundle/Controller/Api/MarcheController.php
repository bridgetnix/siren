<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Marche;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class MarcheController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/getMarches")
     */
    public function getMarchesAction(Request $request) {
        $this->metier = $this->get("app.marche.metier");
        
        $gg=$this->metier->findAll();
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["date"]=$g->getDate();
            $groupe[$i]["typeDePoisson"]=$g->getTypeDePoisson();
            $groupe[$i]["description"]=$g->getDescription();
            $groupe[$i]["debarcadaire"]=$g->getDebarcadaire();
            $groupe[$i]["siteDePeche"]=$g->getSiteDePeche();
            $groupe[$i]["quantite"]=$g->getQuantite();
            $groupe[$i]["prix"]=$g->getPrix();
            $groupe[$i]["telephone"]=$g->getTelephone();
            $groupe[$i]["utilisateur"]=$g->getUtilisateurs();
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof1/'.$g->getImg1();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img1"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof2/'.$g->getImg2();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img2"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof3/'.$g->getImg3();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img3"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof4/'.$g->getImg4();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img4"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $i++;
        }
        return $groupe;
    }
    
    /**
     * @Rest\View()
     * @Rest\Get("/getMarche/{marche}")
     */
    public function getMarcheAction($marche) {
        
        $this->metier = $this->get("app.marche.metier");
        
        $g=$this->metier->find($marche);
        
        $groupe= array();
        $groupe["id"]=$g->getId();
        $groupe["date"]=$g->getDate();
        $groupe["typeDePoisson"]=$g->getTypeDePoisson();
        $groupe["description"]=$g->getDescription();
        $groupe["debarcadaire"]=$g->getDebarcadaire();
        $groupe["siteDePeche"]=$g->getSiteDePeche();
        $groupe["quantite"]=$g->getQuantite();
        $groupe["prix"]=$g->getPrix();
        $groupe["telephone"]=$g->getTelephone();
        $groupe["utilisateur"]=$g->getUtilisateurs();
        
        $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof1/'.$g->getImg1();
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);            
        $groupe["img1"]='data:image/' . $type . ';base64,' . base64_encode($data);
        
        $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof2/'.$g->getImg2();
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);            
        $groupe["img2"]='data:image/' . $type . ';base64,' . base64_encode($data);
        
        $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof3/'.$g->getImg3();
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);            
        $groupe["img3"]='data:image/' . $type . ';base64,' . base64_encode($data);
        
        $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof4/'.$g->getImg4();
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);            
        $groupe["img4"]='data:image/' . $type . ';base64,' . base64_encode($data);
        
        
        return $groupe;
        
    }
    
    
    /**
     * @Rest\View()
     * @Rest\Get("/getMarchesUtilisateur/{utilisateur}")
     */
    public function getMarchesUtilisateurAction($utilisateur) {
        $this->metier = $this->get("app.marche.metier");
        
        $gg=$this->metier->findByUtilisateurs($utilisateur);
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["date"]=$g->getDate();
            $groupe[$i]["typeDePoisson"]=$g->getTypeDePoisson();
            $groupe[$i]["description"]=$g->getDescription();
            $groupe[$i]["debarcadaire"]=$g->getDebarcadaire();
            $groupe[$i]["siteDePeche"]=$g->getSiteDePeche();
            $groupe[$i]["quantite"]=$g->getQuantite();
            $groupe[$i]["prix"]=$g->getPrix();
            $groupe[$i]["telephone"]=$g->getTelephone();
            $groupe[$i]["utilisateur"]=$g->getUtilisateurs();
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof1/'.$g->getImg1();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img1"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof2/'.$g->getImg2();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img2"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof3/'.$g->getImg3();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img3"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof4/'.$g->getImg4();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["img4"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $i++;
        }
        return $groupe;
    }
    
    
    
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/postMarche")
     */
    public function postMarcheAction(Request $request) {
		$typeDePoisson=urldecode($request->request->get('typeDePoisson'));
		$dateo=urldecode($request->request->get('dateo'));
		$utilisateur=urldecode($request->request->get('utilisateur'));
		$description=urldecode($request->request->get('description'));
		$debarcadaire=urldecode($request->request->get('debarcadaire'));
		$siteDePeche=urldecode($request->request->get('siteDePeche'));
		$quantite=urldecode($request->request->get('quantite'));
		$prix=urldecode($request->request->get('prix'));
		$telephone=urldecode($request->request->get('telephone'));
		$img1File=urldecode($request->request->get('img1File'));
		$img2File=urldecode($request->request->get('img2File'));
		$img3File=urldecode($request->request->get('img3File'));
		$img4File=urldecode($request->request->get('img4File'));
		
        $this->metier = $this->get("app.marche.metier");
        $marche = new Marche();
        
        $date = new \DateTime();
        $date->setTimestamp($dateo);
        
        $util= $this->get("app.utilisateurs.metier");
        $newutil=$util->find($utilisateur);
        
        $marche->setDate($date);
        $marche->setUtilisateurs($newutil);
        $marche->setTypeDePoisson($typeDePoisson);
        $marche->setDescription($description);
        $marche->setDebarcadaire($debarcadaire);
        $marche->setSiteDePeche($siteDePeche);
        $marche->setQuantite($quantite);
        $marche->setPrix($prix);
        $marche->setTelephone($telephone);
        $marche->setEtat(1);
        
        if ($img1File)
        {
           
                            $photo = $img1File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof1/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $marche->setImg1($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        else
        {
            $marche->setImg1('marche.png');
        }
        if ($img2File)
        {
            
                            $photo = $img2File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof2/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $marche->setImg2($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img3File)
        {
                           $photo = $img3File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof3/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $marche->setImg3($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        if ($img4File)
        {
           
                            $photo = $img4File;
                                    

                            try {
                                if ($photo != NULL) {
                                    $photo = explode(',', $photo);
                                    $extension = str_replace('data:image/', '', $photo[0]);
                                    $extension = str_replace(';base64', '', $extension);
                                    $webPath =  $this->get('kernel')->getRootDir().'/../web/images/marche/tof4/';
                                    $sourcename = $webPath . uniqid() . '.jpg';

                                    $file = fopen($sourcename, 'w+');
                                    fwrite($file, base64_decode(str_replace(' ', '+', $photo[1])));
                                    fclose($file);

                                    $photoUrl = str_replace($webPath, '', $sourcename);
                                    $marche->setImg4($photoUrl);
                                }
                            } catch (\Exception $e) {
                                $json["success"] = false;
                                $json["error"] = "image";
                                $json["message"] = $e->getMessage();
                            }
        }
        
        $this->metier->create($marche);
        return $marche->getId();
    }   
    
}
