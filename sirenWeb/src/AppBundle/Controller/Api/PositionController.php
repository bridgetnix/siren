<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Position;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class PositionController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/position/{coordx}/{coordy}/{utilisateur}/{projet}")
     */
    public function getPositionAction($coordx,$coordy,$utilisateur,$projet) {
        
            $position = new Position();
            $position->setCoordX($coordx);
            $position->setCoordY($coordy);
            
            $date=new \DateTime('now');
            $position->setDate($date);

            $this->metier = $this->get("app.utilisateurs.metier");
            $position->setUtilisateurs($this->metier->find(intval($utilisateur)));
            
            $this->metier = $this->get("app.projet.metier");
            $position->setProjet($this->metier->find(intval($projet)));

            

            $this->metier = $this->get("app.position.metier");
            $this->metier->create($position);
            
        return 1;
    }
}
