<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Projet_especes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class Projet_especesController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/especessousgroupes/{projet}/{sousgroupes}")
     */
    public function getprojetespecesAction($projet,$sousgroupes) {
        $this->metier = $this->get("app.projet_especes.metier");
        $newprojet= new \AppBundle\Entity\Projet();
        
        $util= $this->get("app.projet.metier");
        $newprojet=$util->find($projet);
        if($newprojet==null)
        {
            $newprojet=$util->find(39);
        }
        $sous= $this->get("app.sousgroupes.metier");
        $newsous=$sous->find($sousgroupes);
        
        $liste_projet=$this->metier->findByProjet($newprojet);
        
        $nb = count($liste_projet);
        
        $especes= new \AppBundle\Entity\Especes();
        $j=0;
        $result= NULL;
        for($i = 0; $i < $nb; $i++){
            $projet = $liste_projet[$i];
            
            $especes=$projet->getEspeces();
            if ($especes->getSousGroupes()== $newsous)
            {
               $result[$j]=  $especes;
               $j++;
            }
            
        }
        
        $gg=$result;
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nom_fr"]=$g->getNomFr();
            $groupe[$i]["nom_en"]=$g->getNomEn();
            $groupe[$i]["description_fr"]=$g->getDescriptionFr();
            $groupe[$i]["description_en"]=$g->getDescriptionEn();
            $groupe[$i]["sous_groupes"]=$g->getSousGroupes();
            $groupe[$i]["questions_animal"]=$g->getQuestionsAnimal();
            $groupe[$i]["questions_menaces"]=$g->getQuestionsMenaces();
            $groupe[$i]["questions_signe"]=$g->getQuestionsSigne();
            $groupe[$i]["questions_alimentation"]=$g->getQuestionsAlimentation();
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/especes/'.$g->getImage();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $i++;
        }
        return $groupe;
        
    }
    
    /**
     * @Rest\View()
     * @Rest\Get("/especes/{projet}")
     */
    public function getespecesAction($projet) {
        $this->metier = $this->get("app.projet_especes.metier");
        $newprojet= new \AppBundle\Entity\Projet();
        
        $util= $this->get("app.projet.metier");
        $newprojet=$util->find($projet);
        if($newprojet==null)
        {
            $newprojet=$util->find(39);
        }
        
        $liste_projet=$this->metier->findByProjet($newprojet);
        
        $nb = count($liste_projet);
        
        $j=0;
        $result= NULL;
        for($i = 0; $i < $nb; $i++){
            $projet = $liste_projet[$i];
            
            $result[$j]= $projet->getEspeces();
            $j++;
            
        }
        
        $gg=$result;
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nom_fr"]=$g->getNomFr();
            $groupe[$i]["nom_en"]=$g->getNomEn();
            $groupe[$i]["description_fr"]=$g->getDescriptionFr();
            $groupe[$i]["description_en"]=$g->getDescriptionEn();
            $groupe[$i]["sous_groupes"]=$g->getSousGroupes();
            $groupe[$i]["questions_animal"]=$g->getQuestionsAnimal();
            $groupe[$i]["questions_menances"]=$g->getQuestionsMenaces();
            $groupe[$i]["questions_signe"]=$g->getQuestionsSigne();
            $groupe[$i]["questions_alimentation"]=$g->getQuestionsAlimentation();
            
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/especes/'.$g->getImage();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $i++;
        }
        return $groupe;
        
    }
}
