<?php

namespace AppBundle\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use AppBundle\Entity\Projet_utilisateur;

header('Access-Control-Allow-Origin: *');
class UtilisateursController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/inscription/{username}/{email}/{telephone}/{password}/{ville}/{pays}/{fonction}")
     */
    public function inscriptionAction($username,$email,$telephone,$password,$ville,$pays,$fonction) {

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setPlainPassword($password);
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setTelephone($telephone);
        $user->setVille($ville);

        $this->metier = $this->get("app.pays.metier");
        $user->setPays($this->metier->find($pays));

        $this->metier = $this->get("app.fonctions.metier");
        $user->setFonctions($this->metier->find($fonction));
        
        $pro= $this->get("app.projet.metier");

        $projet=$pro->find(39);

        $user->setProjet($projet);
        
        $userManager->updateUser($user);
        
        $pro_util_metier = $this->get("app.projet_utilisateur.metier");
        
        $liste_projet_public = $pro->findByPublic();
        
        foreach($liste_projet_public as $p)
        {
            $pro_util = new Projet_utilisateur();
            $pro_util->setProjet($p);
            $pro_util->setUtilisateurs($user);
            $pro_util_metier->create($pro_util);
        
        }
        
        return 1;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/connexion/{email}/{password}")
     */
    public function connexionAction($email,$password) {

        $email = urldecode($email);  
        
        if ($email != null) {
            $user = $this->getDoctrine()
            ->getRepository('AppBundle:Utilisateurs')
            ->findOneBy(['email' => $email]);
        }

         
 
        if (!$user) {
            
             $user = $this->getDoctrine()
            ->getRepository('AppBundle:Utilisateurs')
            ->findOneBy(['telephone' => $email]);
            
            if (!$user) {
            
                return 0;
            }
        }
        
       
        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $password);
 
        if (!$isValid) {
            return 0;
        }
 
        $response = new Response(Response::HTTP_OK);
        $uu= array();
        
        $uu["id"]= $user->getId();
        $uu["username"]= $user->getUsernameCanonical();
        $uu["email"]= $user->getEmailCanonical();
        $uu["password"]= $password;
        $uu["ville"]= $user->getVille();
        $uu["telephone"]= $user->getTelephone();
        $uu["pays"]= $user->getPays();
        $uu["fonctions"]= $user->getFonctions();
        $g=$user->getProjet();
        
        $projet["id"]=$g->getId();
        $projet["nom"]=$g->getNom();
        $projet["lieu"]=$g->getLieu();
        $projet["maplink"]=$g->getMaplink();
        $projet["site"]=$g->getSite();
        $projet["organization"]=$g->getOrganization();
        $projet["public"]=$g->getPublic();
        $projet["pays"]=$g->getPays();
        $projet["utilisateurs"]=$g->getUtilisateurs();
        $projet["mention"]=$g->getMention();
        $projet["mention_en"]=$g->getMentionEn();
        $projet["note"]=$g->getNote();
        $projet["note_en"]=$g->getNoteEn();
        $projet["couleur"]=$g->getCouleur();
        if ($g->getQuestions() != NULL)
        {
            $projet["questions"]=$g->getQuestions();
        }
        else
        {
            $projet["questions"]=null;
        }
        
        if ($g->getImage() != NULL)
        {
            $path =  $this->get('kernel')->getRootDir().'/../web/images/projet/'.$g->getImage();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $projet["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
        }
        
        $uu["projet"]= $projet;
        return $uu;
    }   
    
     /**
     * @Rest\View()
     * @Rest\Get("/changerprojet/{utilisateur}/{projet}")
     */
    public function changerprojetAction($utilisateur,$projet) {

        $userManager = $this->get('fos_user.user_manager');
        $pro= $this->get("app.projet.metier");
        if($projet==null) {
            $projet=62153;
        }
        $newprojet=$pro->find($projet);
        
        $newutil= new \AppBundle\Entity\Utilisateurs();
        
        $util= $this->get("app.utilisateurs.metier");
        $newutil=$util->find($utilisateur);
        $newutil->setProjet($newprojet);
        $userManager->updateUser($newutil);
        return $newutil;
    }   
     
}
