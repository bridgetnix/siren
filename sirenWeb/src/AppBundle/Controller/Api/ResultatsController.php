<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Resultats;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class ResultatsController extends Controller
{
    
    
    /**
     * @Rest\View()
     * @Rest\Get("/resultats/{contenu}/{question}/{observation}", requirements={"contenu"=".+","question"=".+","observation"=".+"})
     */
    public function ResulatsAction($contenu=null,$question=null,$observation=null) {
        
            if($contenu==null || $question==null || $observation==null) {
                return 1;
            }
        
            $res = new Resultats();
            $res->setContenu($contenu);

            $this->metier = $this->get("app.questions.metier");
            $res->setQuestions($this->metier->find(intval($question)));
            
            $this->metier = $this->get("app.observations.metier");
            $observ = $this->metier->find(intval($observation));
            $res->setObservations($observ);
            if($contenu=="Mort")
            {
                $observ->setMort(1);
                $this->metier->update($observ);
            }
            
            $this->metier = $this->get("app.resultats.metier");
            $this->metier->create($res);
        
        return 1;
    }
    
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/resultats")
     */
    public function postResultatAction(Request $request) {
		$contenu=urldecode($request->request->get('contenu'));
		$question=urldecode($request->request->get('question'));
		$observation=urldecode($request->request->get('observation'));
		
		if($contenu==null || $question==null || $observation==null) {
                return 1;
            }
        
            $res = new Resultats();
            $res->setContenu($contenu);

            $this->metier = $this->get("app.questions.metier");
            $res->setQuestions($this->metier->find(intval($question)));
            
            $this->metier = $this->get("app.observations.metier");
            $observ = $this->metier->find(intval($observation));
            $res->setObservations($observ);
            if($contenu=="Mort")
            {
                $observ->setMort(1);
                $this->metier->update($observ);
            }
            
            $this->metier = $this->get("app.resultats.metier");
            $this->metier->create($res);
        
        return 1;
    }   
}
