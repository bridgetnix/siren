<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Projet_typeObservations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class Projet_typeObservationsController extends Controller
{
    private $metier;

    
    /**
     * @Rest\View()
     * @Rest\Get("/typeobservations/{projet}")
     */
    public function getTypeobservationsAction($projet) {
        $this->metier = $this->get("app.projet_typeObservations.metier");
        
        $pro= $this->get("app.projet.metier");
        $newpro=$pro->find($projet);
        if($newpro==null)
        {
            $newpro=$pro->find(39);
        }
        
        $liste_type_observations=$this->metier->findByProjet($newpro);
        
        $nb = count($liste_type_observations);
        $result= NULL;
        for($i = 0; $i < $nb; $i++){
            $projet = $liste_type_observations[$i];
            $result[$i]= $projet->getTypeObservations();
        }
        
        $gg= $result;
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nom_fr"]=$g->getNomFr();
            $groupe[$i]["nom_en"]=$g->getNomEn();
            $groupe[$i]["type_question"]=$g->getTypeQuestion();
            if ($g->getQuestions() != NULL)
            {
                $groupe[$i]["questions"]=$g->getQuestions();
            }
            else
            {
                $groupe[$i]["questions"]=0;
            }
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/typeObservations/'.$g->getImage();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            
            $i++;
        }
        return $groupe;

    }
}
