<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Notifications_projet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class Notifications_projetController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/notificationsprojet/{utilisateur}")
     */
    public function getnotificationsprojetAction($utilisateur) {
        $this->metier = $this->get("app.notifications_projet.metier");
        $newpro= new \AppBundle\Entity\Projet();
        
        $pro= $this->get("app.projet.metier");
        $newpro=$pro->find($utilisateur);
        
            return $this->metier->getnew($newpro);
        
    }
}
