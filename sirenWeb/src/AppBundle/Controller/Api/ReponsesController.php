<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Reponses;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class ReponsesController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/reponses")
     */
    public function getReponsesAction(Request $request) {
        $this->metier = $this->get("app.reponses.metier");
        $gg= $this->metier->findAll();
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["titre_fr"]=$g->getTitreFr();
            $groupe[$i]["titre_en"]=$g->getTitreEn();
            (strlen($g->getTitreFr()) > 34) ? $groupe[$i]["taille"]= true : $groupe[$i]["taille"]=false ;
            if ($g->getQuestions() != NULL)
            {
                //debut
                $ques =$g->getQuestions();
                
                $question= array();
                
                $question["id"]=$ques->getId();
                $question["titre_fr"]=$ques->getTitreFr();
                $question["titre_en"]=$ques->getTitreEn();
                $question["type_reponse"]=$ques->getTypeReponse();
                $question["type_debut"]=$ques->getTypeDebut();
                $question["isPicture"]=$ques->getIsPicture();
                if ($ques->getQuestions() != NULL)
                {
                    $question["questions"]=$ques->getQuestions();
                }
                else
                {
                    $question["questions"]=null;
                }
                if ($ques->getImage() != NULL)
                {
                    $path =  $this->get('kernel')->getRootDir().'/../web/images/question/'.$ques->getImage();
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);            
                    $question["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
                }
                
                $groupe[$i]["questions"]=$question;
                //fin
            }
            else
            {
                $groupe[$i]["questions"]=null;
            }
            if ($g->getQuestionsNext() != NULL)
            {
                //debut
                $ques =$g->getQuestionsNext();
                
                $question= array();
                
                $question["id"]=$ques->getId();
                $question["titre_fr"]=$ques->getTitreFr();
                $question["titre_en"]=$ques->getTitreEn();
                $question["type_reponse"]=$ques->getTypeReponse();
                $question["type_debut"]=$ques->getTypeDebut();
                $question["isPicture"]=$ques->getIsPicture();
                if ($ques->getQuestions() != NULL)
                {
                    $question["questions"]=$ques->getQuestions();
                }
                else
                {
                    $question["questions"]=null;
                }
                if ($ques->getImage() != NULL)
                {
                    $path =  $this->get('kernel')->getRootDir().'/../web/images/question/'.$ques->getImage();
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);            
                    $question["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
                }
                
                $groupe[$i]["questions_next"]=$question;
                //fin
            }
            else
            {
                $groupe[$i]["questions_next"]=null;
            }
            
            if ($g->getImage() != NULL)
            {
                $path =  $this->get('kernel')->getRootDir().'/../web/images/reponse/'.$g->getImage();
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);            
                $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            }
            
            $i++;
        }
        return $groupe;
    }
    
    
    /**
     * @Rest\View()
     * @Rest\Get("/reponsesquestions/{id}")
     */
    public function getReponsesQuestionsAction($id) {
        $this->metier = $this->get("app.reponses.metier");
        $gg= $this->metier->findquestions($id);
        
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["titre_fr"]=$g->getTitreFr();
            $groupe[$i]["titre_en"]=$g->getTitreEn();
            (strlen($g->getTitreFr()) > 34) ? $groupe[$i]["taille"]= true : $groupe[$i]["taille"]=false ;
            if ($g->getQuestions() != NULL)
            {
                //debut
                $ques =$g->getQuestions();
                
                $question= array();
                
                $question["id"]=$ques->getId();
                $question["titre_fr"]=$ques->getTitreFr();
                $question["titre_en"]=$ques->getTitreEn();
                $question["type_reponse"]=$ques->getTypeReponse();
                $question["type_debut"]=$ques->getTypeDebut();
                $question["isPicture"]=$ques->getIsPicture();
                if ($ques->getQuestions() != NULL)
                {
                    $question["questions"]=$ques->getQuestions();
                }
                else
                {
                    $question["questions"]=null;
                }
                if ($ques->getImage() != NULL)
                {
                    $path =  $this->get('kernel')->getRootDir().'/../web/images/question/'.$ques->getImage();
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);            
                    $question["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
                }
                
                $groupe[$i]["questions"]=$question;
                //fin
            }
            else
            {
                $groupe[$i]["questions"]=null;
            }
            if ($g->getQuestionsNext() != NULL)
            {
               //debut
                $ques =$g->getQuestionsNext();
                
                $question= array();
                
                $question["id"]=$ques->getId();
                $question["titre_fr"]=$ques->getTitreFr();
                $question["titre_en"]=$ques->getTitreEn();
                $question["type_reponse"]=$ques->getTypeReponse();
                $question["type_debut"]=$ques->getTypeDebut();
                $question["isPicture"]=$ques->getIsPicture();
                if ($ques->getQuestions() != NULL)
                {
                    $question["questions"]=$ques->getQuestions();
                }
                else
                {
                    $question["questions"]=null;
                }
                if ($ques->getImage() != NULL)
                {
                    $path =  $this->get('kernel')->getRootDir().'/../web/images/question/'.$ques->getImage();
                    $type = pathinfo($path, PATHINFO_EXTENSION);
                    $data = file_get_contents($path);            
                    $question["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
                }
                
                $groupe[$i]["questions_next"]=$question;
                //fin
            }
            else
            {
                $groupe[$i]["questions_next"]=null;
            }
            
            if ($g->getImage() != NULL)
            {
                $path =  $this->get('kernel')->getRootDir().'/../web/images/reponse/'.$g->getImage();
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);            
                $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            }
            
            $i++;
        }
        return $groupe;
    }
}
