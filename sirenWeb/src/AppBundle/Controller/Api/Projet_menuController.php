<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Projet_menu;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class Projet_menuController extends Controller
{
    private $metier;

    
    /**
     * @Rest\View()
     * @Rest\Get("/menu_projet/{projet}")
     */
    public function getMenuAction($projet) {
        $this->metier = $this->get("app.projet_menu.metier");
        
        $pro= $this->get("app.projet.metier");
        $newpro=$pro->find($projet);
        if($newpro==null)
        {
            $newpro=$pro->find(39);
        }
        
        $liste_menu=$this->metier->findByProjet($newpro);
        
        $nb = count($liste_menu);
        $result= NULL;
        for($i = 0; $i < $nb; $i++){
            $projet = $liste_menu[$i];
            $result[$i]= $projet->getMenu();
        }
        
        $gg= $result;
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nomFr"]=$g->getNomFr();
            $groupe[$i]["nomEn"]=$g->getNomEn();
            $groupe[$i]["title"]=$g->getTitre();
            
            $i++;
        }
        return $groupe;

    }
}
