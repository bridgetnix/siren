<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Commentaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class CommentaireController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/getCommentaire/{marche}")
     */
    public function getCommentaireAction($marche) {
        $this->metier = $this->get("app.commentaire.metier");
        
        $mar= $this->get("app.marche.metier");
        $newmar=$mar->find($marche);
        
            return $this->metier->findByMarche($newmar);
        
    }
    
    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/postCommentaire")
     */
    public function postCommentaireAction(Request $request) {
        
		$message=urldecode($request->request->get('message'));
		$marche=urldecode($request->request->get('marche'));
		$dateo=urldecode($request->request->get('dateo'));
		$utilisateur=urldecode($request->request->get('utilisateur'));
		
		$commentaire = new Commentaire();
        $commentaire->setMessage($message);
        
        $date = new \DateTime();
        $date->setTimestamp((int)$dateo);
        $commentaire->setDate($date);
        
        $util= $this->get("app.utilisateurs.metier");
        $newutil=$util->find($utilisateur);
        $commentaire->setUtilisateurs($newutil);
        
        $mar= $this->get("app.marche.metier");
        $newmar=$mar->find($marche);
        $commentaire->setMarche($newmar);
        
        
        $this->metier = $this->get("app.commentaire.metier");
        $this->metier->create($commentaire);
            
        return 1;
    }   
}
