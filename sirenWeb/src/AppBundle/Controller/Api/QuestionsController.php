<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Questions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class QuestionsController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/questions")
     */
    public function getQuestionsAction(Request $request) {
        $this->metier = $this->get("app.questions.metier");
        
        $gg= $this->metier->findAll();
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["titre_fr"]=$g->getTitreFr();
            $groupe[$i]["titre_en"]=$g->getTitreEn();
            $groupe[$i]["type_reponse"]=$g->getTypeReponse();
            $groupe[$i]["type_debut"]=$g->getTypeDebut();
            $groupe[$i]["isPicture"]=$g->getIsPicture();
            if ($g->getQuestions() != NULL)
            {
                $groupe[$i]["questions"]=$g->getQuestions();
            }
            else
            {
                $groupe[$i]["questions"]=null;
            }
            if ($g->getImage() != NULL)
            {
                $path =  $this->get('kernel')->getRootDir().'/../web/images/question/'.$g->getImage();
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);            
                $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            }
            
            $i++;
        }
        return $groupe;
    }
    
     /**
     * @Rest\View()
     * @Rest\Get("/questionsid/{id}")
     */
    public function getQuestionsIdAction($id) {
        $this->metier = $this->get("app.questions.metier");
        $g = $this->metier->find($id);
        
        $groupe["id"]=$g->getId();
        $groupe["titre_fr"]=$g->getTitreFr();
        $groupe["titre_en"]=$g->getTitreEn();
        $groupe["type_reponse"]=$g->getTypeReponse();
        $groupe["type_debut"]=$g->getTypeDebut();
        $groupe["isPicture"]=$g->getIsPicture();
        if ($g->getQuestions() != NULL)
        {
            $groupe["questions"]=$g->getQuestions();
        }
        else
        {
            $groupe["questions"]=null;
        }
        if ($g->getImage() != NULL)
        {
            $path =  $this->get('kernel')->getRootDir().'/../web/images/question/'.$g->getImage();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
        }
        return $groupe;
    }
}
