<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Fonctions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class FonctionsController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/fonctions")
     */
    public function getFonctionsAction(Request $request) {
        $this->metier = $this->get("app.fonctions.metier");
        
            return $this->metier->findAll();
        
    }
}
