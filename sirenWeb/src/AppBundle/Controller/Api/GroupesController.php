<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Groupes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

header('Access-Control-Allow-Origin: *');
class GroupesController extends Controller
{
    private $metier;

    /**
     * @Rest\View()
     * @Rest\Get("/groupes")
     */
    public function getGroupesAction(Request $request) {
        $this->metier = $this->get("app.groupes.metier");
        
        
        $gg=$this->metier->findAll();
        $i=0;
        $groupe= array();
        foreach($gg as $g)
        {
            $groupe[$i]["id"]=$g->getId();
            $groupe[$i]["nom_fr"]=$g->getNomFr();
            $groupe[$i]["nom_en"]=$g->getNomEn();
            $groupe[$i]["description_fr"]=$g->getDescriptionFr();
            $groupe[$i]["description_en"]=$g->getDescriptionEn();
            
            $path =  $this->get('kernel')->getRootDir().'/../web/images/groupes/'.$g->getImage();
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $data = file_get_contents($path);            
            $groupe[$i]["image"]='data:image/' . $type . ';base64,' . base64_encode($data);
            
            $i++;
        }
        return $groupe;
    }
}
