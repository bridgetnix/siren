<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Marche;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Marche controller.
 *
 */
class MarcheController extends Controller
{
    /**
     * Lists all marche entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $marches = $em->getRepository('AppBundle:Marche')->findAll();

        return $this->render('marche/index.html.twig', array(
            'marches' => $marches,
        ));
    }

    /**
     * Creates a new marche entity.
     *
     */
    public function newAction(Request $request)
    {
        $marche = new Marche();
        $form = $this->createForm('AppBundle\Form\MarcheType', $marche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($marche);
            $em->flush();

            return $this->redirectToRoute('marche_show', array('id' => $marche->getId()));
        }

        return $this->render('marche/new.html.twig', array(
            'marche' => $marche,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a marche entity.
     *
     */
    public function showAction(Marche $marche)
    {
        $deleteForm = $this->createDeleteForm($marche);

        return $this->render('marche/show.html.twig', array(
            'marche' => $marche,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing marche entity.
     *
     */
    public function editAction(Request $request, Marche $marche)
    {
        $marche2=$marche;
        $deleteForm = $this->createDeleteForm($marche);
        $editForm = $this->createForm('AppBundle\Form\MarcheType', $marche);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($marche->getImg1() == NULL)
            {
                $marche->setImg1($marche2->getImg1()); 
            }
            if($marche->getImg2() == NULL)
            {
                $marche->setImg2($marche2->getImg2()); 
            }
            if($marche->getImg3() == NULL)
            {
                $marche->setImg3($marche2->getImg3()); 
            }
            if($marche->getImg4() == NULL)
            {
                $marche->setImg4($marche2->getImg4()); 
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('marche_edit', array('id' => $marche->getId()));
        }

        return $this->render('marche/edit.html.twig', array(
            'marche' => $marche,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a marche entity.
     *
     */
    public function deleteAction(Request $request, Marche $marche)
    {
        $form = $this->createDeleteForm($marche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($marche);
            $em->flush();
        }

        return $this->redirectToRoute('marche_index');
    }

    /**
     * Creates a form to delete a marche entity.
     *
     * @param Marche $marche The marche entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Marche $marche)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('marche_delete', array('id' => $marche->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    
    public function deleteImg1Action(Request $request, Marche $marche)
    {
        $marche->setImg1(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('marche_edit', array('id' => $marche->getId()));
        
    }
    
    public function deleteImg2Action(Request $request, Marche $marche)
    {
        $marche->setImg2(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('marche_edit', array('id' => $marche->getId()));
        
    }
    
    public function deleteImg3Action(Request $request, Marche $marche)
    {
        $marche->setImg3(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('marche_edit', array('id' => $marche->getId()));
        
    }
    
    public function deleteImg4Action(Request $request, Marche $marche)
    {
        $marche->setImg4(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('marche_edit', array('id' => $marche->getId()));
        
    }
}
