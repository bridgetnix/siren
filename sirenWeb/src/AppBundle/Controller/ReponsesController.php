<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Reponses;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Reponse controller.
 *
 */
class ReponsesController extends Controller
{
    /**
     * Lists all reponse entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $reponses = $em->getRepository('AppBundle:Reponses')->findAll();

        return $this->render('reponses/index.html.twig', array(
            'reponses' => $reponses,
        ));
    }

    /**
     * Creates a new reponse entity.
     *
     */
    public function newAction(Request $request)
    {
        $reponse = new Reponses();
        $form = $this->createForm('AppBundle\Form\ReponsesType', $reponse);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
            
        $session = $request->getSession();
        $question=$em->getRepository('AppBundle:Questions')->find($session->get("question"));
        

        if ($form->isSubmitted() && $form->isValid()) {
            $reponse->setQuestions($question);
            $em->persist($reponse);
            $em->flush();
            
            if($reponse->getImage() != NULL)
            {
                $question->setIsPicture(true);
                $em->flush();
            }

            return $this->redirectToRoute('questions_show', array('id' => $question->getId()));
        }

        return $this->render('reponses/new.html.twig', array(
            'reponse' => $reponse,
            'question' => $session->get("question"),
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a reponse entity.
     *
     */
    public function showAction(Reponses $reponse)
    {
        $deleteForm = $this->createDeleteForm($reponse);
        

        return $this->render('reponses/show.html.twig', array(
            'reponse' => $reponse,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing reponse entity.
     *
     */
    public function editAction(Request $request, Reponses $reponse)
    {
        $reponse2=$reponse;
        $deleteForm = $this->createDeleteForm($reponse);
        $editForm = $this->createForm('AppBundle\Form\ReponsesType', $reponse);
        $editForm->handleRequest($request);
        
        $question= $reponse->getQuestions();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($reponse->getImage() == NULL)
            {
                $reponse->setImage($reponse2->getImage()); 
            }
            $this->getDoctrine()->getManager()->flush();
            
            if($reponse->getImage() != NULL)
            {
                
                $question->setIsPicture(true);
                $this->getDoctrine()->getManager()->flush();
            }
             return $this->redirectToRoute('questions_show', array('id' => $question->getId()));
        }

        return $this->render('reponses/edit.html.twig', array(
            'reponse' => $reponse,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a reponse entity.
     *
     */
    public function deleteAction(Request $request, Reponses $reponse)
    {
        $form = $this->createDeleteForm($reponse);
        $form->handleRequest($request);
        $question= $reponse->getQuestions();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($reponse);
            $em->flush();
        }
        
        $reponses = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
        
        if (count($reponses) == 0) {
            $question->setIsPicture(false);
        } else {
            $res= false;
            foreach($reponses as $reponse)
            {
                $valeur = ($reponse->getImage() != null)? true : false;
                $res = $res || $valeur;
            }
            $question->setIsPicture($res);
            
        }
        $em->flush();

        return $this->redirectToRoute('questions_show', array('id' => $question->getId()));
    }

    /**
     * Creates a form to delete a reponse entity.
     *
     * @param Reponses $reponse The reponse entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Reponses $reponse)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('reponses_delete', array('id' => $reponse->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
