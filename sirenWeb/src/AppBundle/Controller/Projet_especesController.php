<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Projet_especes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Projet_espece controller.
 *
 */
class Projet_especesController extends Controller
{
    /**
     * Lists all projet_espece entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projet_especes = $em->getRepository('AppBundle:Projet_especes')->findAll();

        return $this->render('projet_especes/index.html.twig', array(
            'projet_especes' => $projet_especes,
        ));
    }

    /**
     * Creates a new projet_espece entity.
     *
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $projet_especes = new Projet_especes();
        $form = $this->createForm('AppBundle\Form\Projet_especesType', $projet_especes);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $projet_especes->setProjet($projet);
            $em->persist($projet_especes);
            $em->flush();

            return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
        }
        
        return $this->render('projet_especes/new.html.twig', array(
            'projet_especes' => $projet_especes,
            'form' => $form->createView(),
            'projet' => $projet,
            
        ));
    }

    /**
     * Finds and displays a projet_espece entity.
     *
     */
    public function showAction(Projet_especes $projet_espece,Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $deleteForm = $this->createDeleteForm($projet_espece);

        return $this->render('projet_especes/show.html.twig', array(
            'projet_espece' => $projet_espece,
            'delete_form' => $deleteForm->createView(),
            'projet' => $projet,
        ));
    }

    /**
     * Displays a form to edit an existing projet_espece entity.
     *
     */
    public function editAction(Request $request, Projet_especes $projet_espece)
    {
        $deleteForm = $this->createDeleteForm($projet_espece);
        $editForm = $this->createForm('AppBundle\Form\Projet_especesType', $projet_espece);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projet_especes_edit', array('id' => $projet_espece->getId()));
        }

        return $this->render('projet_especes/edit.html.twig', array(
            'projet_espece' => $projet_espece,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a projet_espece entity.
     *
     */
    public function deleteAction(Request $request, Projet_especes $projet_espece)
    {
         $em = $this->getDoctrine()->getManager();
        
        $session = $request->getSession();
        $projet=$em->getRepository('AppBundle:Projet')->find($session->get("projet"));
         
        $form = $this->createDeleteForm($projet_espece);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($projet_espece);
            $em->flush();
        }

        return $this->redirectToRoute('projet_show', array('id' => $projet->getId()));
    }

    /**
     * Creates a form to delete a projet_espece entity.
     *
     * @param Projet_especes $projet_espece The projet_espece entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Projet_especes $projet_espece)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projet_especes_delete', array('id' => $projet_espece->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
