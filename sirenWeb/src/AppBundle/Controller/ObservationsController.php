<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Observations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * Observation controller.
 *
 */
class ObservationsController extends Controller
{
    /**
     * Lists all observation entities not validated.
     *
     */
    public function index0Action($page)
    {
        if( NULL == $this->getUser() ){
            
            return $this->redirectToRoute('fos_user_security_login');
        }
        if(!is_numeric($page)){
            $page = 1;
        }
        
        $em = $this->getDoctrine()->getManager();
        $pp = ($page-1)*100;

        $proj = $em->getRepository('AppBundle:Projet')->find('62153');
        
        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array('etat' => 0),
        array('id' => 'DESC'), 100,(int)$pp
        );
        $observations_utilisateurs = $em->getRepository('AppBundle:Observations')->findBy(
        array('utilisateurs' => $this->getUser(), 'etat' => 0),
        array('id' => 'DESC'), 100,(int)$pp
        );
        $observations_renatura = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'etat' => 0),
        array('id' => 'DESC'), 100,(int)$pp
        );
        
        $obs = $em->getRepository('AppBundle:Observations')->findBy(
            array('etat' => 0),
            array('id' => 'DESC',)
            );
        
        
        $obs_utilisateurs = $em->getRepository('AppBundle:Observations')->findBy(
        array('utilisateurs' => $this->getUser(), 'etat' => 0)
        );
        
        $obs_renatura = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'etat' => 0)
        );
        
        return $this->render('observations/index0.html.twig', array(
            'observations' => $observations,
            'observations_utilisateurs' => $observations_utilisateurs,
            'observations_renatura' => $observations_renatura,
            'nbobs' => count($obs),
            'nbobs_utilisateurs' => count($obs_utilisateurs),
            'nbobs_renatura' => count($obs_renatura),
            'page' => $page
        ));
    }

    /**
     * Lists all observation entities validated.
     *
     */
    public function index1Action($page)
    {
        if( NULL == $this->getUser() ){
            
            return $this->redirectToRoute('fos_user_security_login');
        }
        if(!is_numeric($page)){
            $page = 1;
        }
        
        $em = $this->getDoctrine()->getManager();
        $pp = ($page-1)*100;

        $proj = $em->getRepository('AppBundle:Projet')->find('62153');
        
        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array('etat' => 1),
        array('id' => 'DESC'), 100,(int)$pp
        );
        $observations_utilisateurs = $em->getRepository('AppBundle:Observations')->findBy(
        array('utilisateurs' => $this->getUser(), 'etat' => 1),
        array('id' => 'DESC'), 100,(int)$pp
        );
        $observations_renatura = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'etat' => 1),
        array('id' => 'DESC'), 100,(int)$pp
        );
        
        $obs = $em->getRepository('AppBundle:Observations')->findBy(
            array('etat' => 1),
            array('id' => 'DESC',)
            );
        
        
        $obs_utilisateurs = $em->getRepository('AppBundle:Observations')->findBy(
        array('utilisateurs' => $this->getUser(), 'etat' => 1)
        );
        
        $obs_renatura = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj, 'etat' => 1)
        );
        
        return $this->render('observations/index1.html.twig', array(
            'observations' => $observations,
            'observations_utilisateurs' => $observations_utilisateurs,
            'observations_renatura' => $observations_renatura,
            'nbobs' => count($obs),
            'nbobs_utilisateurs' => count($obs_utilisateurs),
            'nbobs_renatura' => count($obs_renatura),
            'page' => $page
        ));
    }

    /**
     * Creates a new observation entity.
     *
     */
    public function newAction(Request $request)
    {
        if( NULL == $this->getUser() ){
            
            return $this->redirectToRoute('fos_user_security_login');
        }
        
        $observation = new Observations();
        
        $form = $this->createForm('AppBundle\Form\Observations2Type', $observation);
        $form->handleRequest($request);
        

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $currentUser = $this->getUser();
            $observation->setUtilisateurs($currentUser);
            if ($observation->getTypeObservations()->getId()==1)
            {
                $alpha = $em->getRepository('AppBundle:Observations')->findBy(
                    array('typeObservations' => 1 )
                );
               $nbe=count($alpha)+1;
                $observation->setAlpha("A".$nbe);
            }
            if ($observation->getTypeObservations()->getId()==2)
            {
                $alpha = $em->getRepository('AppBundle:Observations')->findBy(
                    array('typeObservations' => 2 )
                );
                $nbe=count($alpha)+1;
                $observation->setAlpha("B".$nbe);
            }
            if ($observation->getTypeObservations()->getId()==3)
            {
                $alpha = $em->getRepository('AppBundle:Observations')->findBy(
                    array('typeObservations' => 3 )
                );
                $nbe=count($alpha)+1;
                $observation->setAlpha("C".$nbe);
            }
            if ($observation->getTypeObservations()->getId()==4)
            {
                $alpha = $em->getRepository('AppBundle:Observations')->findBy(
                    array('typeObservations' => 4 )
                );
                $nbe=count($alpha)+1;
                $observation->setAlpha("D".$nbe);
            }
            
            $em->persist($observation);
            $em->flush();
            
            $groupes=$em->getRepository('AppBundle:Groupes')->findAll();
            
            return $this->render('resultats/groupe.html.twig', array('observation' => $observation->getId(),'groupes' => $groupes));
          
            
        }

        return $this->render('observations/new.html.twig', array(
            'observation' => $observation,
            'form' => $form->createView(),
        ));
    }
    
    public function groupeAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
       if ($request->getMethod() == 'POST') {
           $observations=new Observations();
           $observations=$em->getRepository('AppBundle:Observations')->find($_POST['observation']);
            if(!isset($_POST['groupe']))
           {
                $groupes=$em->getRepository('AppBundle:Groupes')->find(1);
           
           }
           else{
               $groupes=$em->getRepository('AppBundle:Groupes')->find($_POST['groupe']);
           
           }
           
           
           $observations->setGroupes($groupes);
           $em->persist($observations);
           $em->flush();
           
           $sousgroupes=$em->getRepository('AppBundle:Sous_groupes')->findByGroupes($groupes);
           
           return $this->render('resultats/sous_groupe.html.twig', array('observation' => $observations->getId(),'sousgroupes' => $sousgroupes));
          
       }

    }
    
    public function sousgroupeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       if ($request->getMethod() == 'POST') {
           $observations=new Observations();
           $observations=$em->getRepository('AppBundle:Observations')->find($_POST['observation']);
           $sousgroupes=new \AppBundle\Entity\Sous_groupes();
           if(!isset($_POST['sousgroupe']))
           {
                $sousgroupes=$em->getRepository('AppBundle:Sous_groupes')->find(1);
           
           }
           else{
               $sousgroupes=$em->getRepository('AppBundle:Sous_groupes')->find($_POST['sousgroupe']);
           
           }
           
           $observations->setSousGroupes($sousgroupes);
           $em->persist($observations);
           $em->flush();
           
           $especes=$em->getRepository('AppBundle:Especes')->findBy(array('sous_groupes'=> $sousgroupes->getId()));
           
           return $this->render('resultats/espece.html.twig', array('observation' => $observations->getId(),'especes' => $especes));
          
       }

    }
    
     public function especeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
       if ($request->getMethod() == 'POST') {
           $observation=new Observations();
           $observation=$em->getRepository('AppBundle:Observations')->find($_POST['observation']);
           if(!isset($_POST['espece']))
           {
               $especes=$em->getRepository('AppBundle:Especes')->find(1);
           
           }
           else{
               $especes=$em->getRepository('AppBundle:Especes')->find($_POST['espece']);
           
           }
           $observation->setEspeces($especes);
           $em->persist($observation);
           $em->flush();
           $espece= $especes;
            $typeobservation= $observation->getTypeObservations();
            
            if ($typeobservation->getId() == 1)
            {
               $question=$espece->getQuestionsAnimal();
               
               if($question->getTypeReponse()== "text")
               {
                  return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 1));
          
               }
               else {
                   $resultats = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
                   return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 2,'reponses' => $resultats));
          
                }
               
            }
            if ($typeobservation->getId() == 2)
            {
               $question=$espece->getQuestionsMenaces();
               
               if($question->getTypeReponse()== "text")
               {
                  return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 1));
          
               }
               else {
                   $resultats = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
                   return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 2,'reponses' => $resultats));
          
                }
            }
            if ($typeobservation->getId() == 3)
            {
               $question=$espece->getQuestionsSigne();
               
               if($question->getTypeReponse()== "text")
               {
                  return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 1));
          
               }
               else {
                   $resultats = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
                   return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 2,'reponses' => $resultats));
          
                }
            }
            if ($typeobservation->getId() == 4)
            {
               $question=$espece->getQuestionsAlimentation();
               
               if($question->getTypeReponse()== "text")
               {
                  return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 1));
          
               }
               else {
                   $resultats = $em->getRepository('AppBundle:Reponses')->findByQuestions($question);
                   return $this->render('resultats/new.html.twig', array('observation' => $observation->getId(),'question' => $question,'type' => 2,'reponses' => $resultats));
          
                }
            }
       }

    }

    /**
     * Finds and displays a observation entity.
     *
     */
    public function showAction(Observations $observation)
    {
        if( NULL == $this->getUser() ){
            
            return $this->redirectToRoute('fos_user_security_login');
        }
        
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($observation);
        $resultats = $em->getRepository('AppBundle:Resultats')->findByObservations($observation);
                   
        return $this->render('observations/show.html.twig', array(
            'observation' => $observation,
            'resultats' => $resultats,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function show2Action(Observations $observation)
    {
        $em = $this->getDoctrine()->getManager();
        $resultats = $em->getRepository('AppBundle:Resultats')->findByObservations($observation);
                   
        return $this->render('observations/show2.html.twig', array(
            'observation' => $observation,
            'resultats' => $resultats,
            
        ));
    }
    
    /**
     * Displays a form to edit an existing observation entity.
     *
     */
    public function editAction(Request $request, Observations $observation)
    {
        if( NULL == $this->getUser() ){
            
            return $this->redirectToRoute('fos_user_security_login');
        }
        
        $observation2=$observation;
        $deleteForm = $this->createDeleteForm($observation);
        
        if ($observation->getProjet()->getId() == 62153) {
           $editForm = $this->createForm('AppBundle\Form\Observations3Type', $observation);
        } else {
           $editForm = $this->createForm('AppBundle\Form\Observations2Type', $observation);
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if($observation->getImg1() == NULL)
            {
                $observation->setImg1($observation2->getImg1()); 
            }
            if($observation->getImg2() == NULL)
            {
                $observation->setImg2($observation2->getImg2()); 
            }
            if($observation->getImg3() == NULL)
            {
                $observation->setImg3($observation2->getImg3()); 
            }
            if($observation->getImg4() == NULL)
            {
                $observation->setImg4($observation2->getImg4()); 
            }
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('observations_show', array('id' => $observation->getId()));
        }

        return $this->render('observations/edit.html.twig', array(
            'observation' => $observation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Valider une observation entity.
     *
     */
    public function validerAction(Request $request, Observations $observation)
    {
            $observation->setEtat(1);
            $this->getDoctrine()->getManager()->flush();
            
            return $this->redirectToRoute('observations_show', array('id' => $observation->getId()));
        
    }
    /**
     * devalider une observation entity.
     *
     */
    public function devaliderAction(Request $request, Observations $observation)
    {
            $observation->setEtat(0);
            $this->getDoctrine()->getManager()->flush();
            
            return $this->redirectToRoute('observations_show', array('id' => $observation->getId()));
        
    }
    
	/**
     * Deletes a observation entity.
     *
     */
    public function deleteAction(Request $request, Observations $observation)
    {
        $form = $this->createDeleteForm($observation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($observation);
            $em->flush();
        }

        return $this->redirectToRoute('observations_index0', array('page' => 1));
    }
    public function supAction(Request $request, Observations $observation)
    {
        try{
            $em = $this->getDoctrine()->getManager();
            $em->remove($observation);
            $em->flush();
            return $this->redirectToRoute('0', array('page' => 1));
            
        } catch (ErrorException $e) {
            return $this->redirectToRoute('observations_index0', array('page' => 1));
        }
         
    }

    /**
     * Creates a form to delete a observation entity.
     *
     * @param Observations $observation The observation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Observations $observation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('observations_delete', array('id' => $observation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
     public function exportAction(Request $request){
            $user = $this->getUser();
            $locale = $request->getLocale();
            $em = $this->getDoctrine()->getManager();
        
            $dateDebut = $request->request->get("datedebut");
            $dateFin = $request->request->get("datefin");
            
            $dateDebut = ($dateDebut == "") ? NULL : $dateDebut;
            $dateFin = ($dateFin == "") ? NULL : $dateFin;

            if($dateDebut != NULL){
                $debut = $dateDebut;
                $dateDebut = new \DateTime();
                $dateDebut->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }

            if($dateFin != NULL){
                $debut = $dateFin;
                $dateFin = new \DateTime();
                $dateFin->setDate(substr($debut, strrpos($debut, "-")+1), substr($debut, strpos($debut, "-")+1, (strrpos($debut, "-")-strpos($debut, "-")-1)), substr($debut, 0, strpos($debut, "-")));
            }
            $projet = $request->request->get("projet");
            $pro = $em->getRepository('AppBundle:Projet')->find($projet);
            $observations = array();   
            if($pro==NULL)
            {
                $projets = $em->getRepository('AppBundle:Projet')->findAll();
                
                foreach ($projets as $prot) {
                    $obsver = $em->getRepository("AppBundle:Observations")->getexport($prot->getId(), $dateDebut, $dateFin);
                    $observations = array_merge($observations, $obsver);
                }
                
            }
            else
            {
                
                $observations = $em->getRepository("AppBundle:Observations")->getexport($projet, $dateDebut, $dateFin);
                
            }
            
            // ask the service for a Excel5
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            // Pour traduire dans la locale de l'utilisateur :
            $titre = 'Exportation de données du';
            $description = "Ce fichier Excel 2007 a été généré par l'application Siren et contient des données qui y ont été sauvegardées";

            $dateExport = date("d-m-Y H:i:s");
            $phpExcelObject->getProperties()->setCreator($user->getUsername())
                ->setLastModifiedBy("Siren")
                ->setTitle($titre.' '.$dateExport)
                ->setSubject($titre)
                ->setDescription($description);
                

            
            
                
                $phpExcelObject->setActiveSheetIndex(0);

                
                $sheet = $phpExcelObject->getActiveSheet();
                $sheet->setTitle("Observation");
                $texte = 'Observation';
                $sheet->setCellValue('A1', $texte);
                $texte = 'Utilisateur';
                $sheet->setCellValue('B1', $texte);
                $texte = 'Mort';
                $sheet->setCellValue('C1', $texte);
                $texte = "Type d'observation";
                $sheet->setCellValue('D1', $texte);
                $texte = 'Groupe';
                $sheet->setCellValue('E1', $texte);
                $texte = 'Sous Groupe';
                $sheet->setCellValue('F1', $texte);
                $texte = 'Espece';
                $sheet->setCellValue('G1', $texte);
                $texte = 'Date';
                $sheet->setCellValue('H1', $texte);
                $texte = 'Latitude (x)';
                $sheet->setCellValue('I1', $texte);
                $texte = 'Longitude (y)';
                $sheet->setCellValue('J1', $texte);
                $texte = 'Note';
                $sheet->setCellValue('K1', $texte);
                $texte = 'Image 1';
                $sheet->setCellValue('L1', $texte);
                $texte = 'Image 2';
                $sheet->setCellValue('M1', $texte);
                $texte = 'Image 3';
                $sheet->setCellValue('N1', $texte);
                $texte = 'Image 4';
                $sheet->setCellValue('O1', $texte);
                $texte = 'Question 1';
                $sheet->setCellValue('P1', $texte);
                $texte = 'Reponse 1';
                $sheet->setCellValue('Q1', $texte);
                $texte = 'Question 2';
                $sheet->setCellValue('R1', $texte);
                $texte = 'Reponse 2';
                $sheet->setCellValue('S1', $texte);
                $texte = 'Question 3';
                $sheet->setCellValue('T1', $texte);
                $texte = 'Reponse 3';
                $sheet->setCellValue('U1', $texte);
                $texte = 'Question 4';
                $sheet->setCellValue('V1', $texte);
                $texte = 'Reponse 4';
                $sheet->setCellValue('W1', $texte);
                $texte = 'Question 5';
                $sheet->setCellValue('X1', $texte);
                $texte = 'Reponse 5';
                $sheet->setCellValue('Y1', $texte);
                $texte = 'Question 6';
                $sheet->setCellValue('Z1', $texte);
                $texte = 'Reponse 6';
                $sheet->setCellValue('AA1', $texte);
                $texte = 'Question 7';
                $sheet->setCellValue('AB1', $texte);
                $texte = 'Reponse 7';
                $sheet->setCellValue('AC1', $texte);
                $texte = 'Question 8';
                $sheet->setCellValue('AD1', $texte);
                $texte = 'Reponse 8';
                $sheet->setCellValue('AE1', $texte);
                $texte = 'Question 9';
                $sheet->setCellValue('AF1', $texte);
                $texte = 'Reponse 9';
                $sheet->setCellValue('AG1', $texte);
                $texte = 'Question 10';
                $sheet->setCellValue('AH1', $texte);
                $texte = 'Reponse 10';
                $sheet->setCellValue('AI1', $texte);
                
               
                $i = 2;
                foreach ($observations as $observation){
                    $sheet->setCellValue('A'.$i, ($i - 1));
                    $sheet->setCellValue('B'.$i, $observation->getUtilisateurs());
                    $sheet->setCellValue('C'.$i, $observation->getMort());
                    $sheet->setCellValue('D'.$i, ($locale=="fr")?$observation->getTypeObservations()->getNomFr():$observation->getTypeObservations()->getNomEn());
                    $sheet->setCellValue('E'.$i, ($observation->getGroupes())?($locale=="fr")?$observation->getGroupes()->getNomFr():$observation->getGroupes()->getNomEn():"null");
                    $sheet->setCellValue('F'.$i, ($observation->getSousGroupes())?($locale=="fr")?$observation->getSousGroupes()->getNomFr():$observation->getSousGroupes()->getNomEn():"null");
                    $sheet->setCellValue('G'.$i, ($observation->getEspeces())?($locale=="fr")?$observation->getEspeces()->getNomFr():$observation->getEspeces()->getNomEn():"null");
                    $sheet->setCellValue('H'.$i, $observation->getDate()->format("d/m/Y H:i:s"));
                    $sheet->setCellValue('I'.$i, $observation->getCoordX());
                    $sheet->setCellValue('J'.$i, $observation->getCoordY());
                    $sheet->setCellValue('K'.$i, $observation->getNote());
                    $nompro=$observation->getProjet()->getNom();
                    $sheet->setCellValue('L'.$i, ($observation->getImg1()!=null)?$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof1/".$observation->getImg1():"");
                    $sheet->setCellValue('M'.$i, ($observation->getImg2()!=null)?$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof2/".$observation->getImg2():"");
                    $sheet->setCellValue('N'.$i, ($observation->getImg3()!=null)?$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof3/".$observation->getImg3():"");
                    $sheet->setCellValue('O'.$i, ($observation->getImg4()!=null)?$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof4/".$observation->getImg4():"");
                    
                    $car = "P";
                    $rep= new \AppBundle\Entity\Resultats();
                    $repondres = $em->getRepository("AppBundle:Resultats")->findByObservations($observation);
                        
                     if(count($repondres) > 0)
                    {
                        foreach ($repondres as $repondre) {
                            $rep=$repondre;
                            $sheet->setCellValue($car . $i, ($locale=="fr")?$rep->getQuestions()->getTitreFr():$rep->getQuestions()->getTitreEn());

                            $sheet->setCellValue(++$car . $i, $rep->getContenu());

                            ++$car;
                        }
                    }
                    
                    $i++;
                }
            
            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers

            if($pro!=NULL)
            {
                $clearstring=filter_var($pro->getNom(), FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'export observations '.$clearstring.' du '.$dateExport.'.xls'
            );
            }
            else
            {
             $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'export observations total du '.$dateExport.'.xls'
            );   
            }
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        
    }
    public function geocodeAction($addressx,$addressy){
        $apiKey = "AIzaSyAynDbAhqx8Xj7aTyADvr6br02-R4Clkz0";

        try {
        $address = "None";
        $googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';   
        $options = array("key"=>$apiKey,"latlng"=>$addressx.",".$addressy);
        $googleApiUrl .= http_build_query($options,'','&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($json);
            if (count($obj->results) > 0) {
                $address = $obj->results[0]->formatted_address;
            }

        } catch (ErrorException $e) {
            $address = "error";
        }
        return $this->render('default/local.html.twig', array(
            'local' => $address,
            //'local' => "Inconu",
        ));
        
    }
    
    public function geocode2($addressx,$addressy){
        $apiKey = "AIzaSyAynDbAhqx8Xj7aTyADvr6br02-R4Clkz0";

        try {
        $address = "None";
        $googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?';   
        $options = array("key"=>$apiKey,"latlng"=>$addressx.",".$addressy);
        $googleApiUrl .= http_build_query($options,'','&');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $googleApiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);

        $obj = json_decode($json);
            if (count($obj->results) > 0) {
                $address = $obj->results[0]->formatted_address;
            }

        } catch (ErrorException $e) {
            $address = "error";
        }
        return  $address;
        
    }
    
    function getCurlValue($filename, $contentType, $postname)
{
    // PHP 5.5 introduced a CurlFile object that deprecates the old @filename syntax
    // See: https://wiki.php.net/rfc/curl-file-upload
    if (function_exists('curl_file_create')) {
        return curl_file_create($filename, $contentType, $postname);
    }

    // Use the old style if using an older version of PHP
    $value = "@{$filename};filename=" . $postname;
    if ($contentType) {
        $value .= ';type=' . $contentType;
    }

    return $value;
}

function build_data_files($boundary, $fields, $files){
    $data = '';
    $eol = "\r\n";

    $delimiter = '-------------' . $boundary;

    foreach ($fields as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
            . $content . $eol;
    }


    foreach ($files as $name => $content) {
        $data .= "--" . $delimiter . $eol
            . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
            //. 'Content-Type: image/png'.$eol
            . 'Content-Transfer-Encoding: binary'.$eol
            ;

        $data .= $eol;
        $data .= $content . $eol;
    }
    $data .= "--" . $delimiter . "--".$eol;


    return $data;
}

    /**
     * Envoyer l'observation sur inaturalist.
     *
     */
    public function sendAction(Request $request, Observations $observation)
    {
        if ($observation->getIdInaturalist() != null) {
            return $this->redirectToRoute('observations_show', array('id' => $observation->getId()));
        }

        $app_id = "ceb09035af7c21cd73bf157e73ad8fb03df24a3e6bc766c097d6dcf19cf82b20";
        $app_secret = "64d649a40dd464d2f1a65afa29c73b6e24b7b43afa56349152f7f6f6bc8bb647";
        $redirect_uri = 'http://siren.ammco.org/web/en/';
        $username = 'akamla@ammco.org';
        $password = 'LcU.@a7T&985]daU';
        
        $payload = [
          "client_id" => $app_id,
          "client_secret" => $app_secret,
          "grant_type" => "password",
          "username" => $username,
          "password" => $password
        ];
        
        $url = "/oauth/token";
        
        $value = $this->CallAPI("POST", $url, $payload);
        $value = json_decode($value);
        $token = $value->access_token;
        
        $em = $this->getDoctrine()->getManager();
        $repondres = $em->getRepository("AppBundle:Resultats")->findByObservations($observation);
        
        
        $description = "";
        if(!empty($observation->getNote()))
        {
            $description .= "Description: ".$observation->getNote();
        }
        if(count($repondres) > 0)
        {
            foreach ($repondres as $repondre) {
                $rep=$repondre;
                $description .= "~ ".$rep->getQuestions()->getTitreEn(). ": ".$rep->getContenu();
                
            }
        }
                    
        $payload = [
            "observation[species_guess]" => $observation->getEspeces()->getNomInaturalist(),
            "observation[observed_on_string]" => $observation->getDate()->format("Y/m/d"),
            "observation[time_zone]" => "West Central Africa",
            "observation[description]" => $description,
            "observation[tag_list]" => $observation->getTypeObservations()->getNomEn().",".$observation->getSousGroupes()->getNomEn().",".$observation->getGroupes()->getNomEn(),
            "observation[place_guess]" => $this->geocode2($observation->getCoordX(),$observation->getCoordY()),
            "observation[latitude]"=> $observation->getCoordX(),
            "observation[longitude]"=> $observation->getCoordY(),
            "observation[map_scale]" => 16,
            "observation[location_is_exact]" => true,
            "observation[positional_accuracy]" => 10,
            "observation[geoprivacy]" => "open"
        ];
        
        $url = "/observations.json";
        
        $obs = $this->CallAPIPOST("POST", $url, $token, $payload);
        $obs2 = json_decode($obs);
        $obsid = $obs2[0]->id;
        
        if (!empty($observation->getImg1()))
        {
           $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof1/".$observation->getImg1();
           
           $fields = array("observation_photo[observation_id]"=> $obsid);
        
        
            $files = array();
            $files["file"] = file_get_contents($photo);
            
            
            $boundary = uniqid();
            $delimiter = '-------------' . $boundary;
            
            $post_data = $this->build_data_files($boundary, $fields, $files);
            
    
            
            $url = "/observation_photos.json";
            
            $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
         
        }
        
        if (!empty($observation->getImg2()))
        {
           $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof2/".$observation->getImg2();
           
           $fields = array("observation_photo[observation_id]"=> $obsid);
        
        
            $files = array();
            $files["file"] = file_get_contents($photo);
            
            
            $boundary = uniqid();
            $delimiter = '-------------' . $boundary;
            
            $post_data = $this->build_data_files($boundary, $fields, $files);
            
    
            
            $url = "/observation_photos.json";
            
            $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
         
        }
        
        if (!empty($observation->getImg3()))
        {
           $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof3/".$observation->getImg3();
           
           $fields = array("observation_photo[observation_id]"=> $obsid);
        
        
            $files = array();
            $files["file"] = file_get_contents($photo);
            
            
            $boundary = uniqid();
            $delimiter = '-------------' . $boundary;
            
            $post_data = $this->build_data_files($boundary, $fields, $files);
            
    
            
            $url = "/observation_photos.json";
            
            $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
         
        }
        
        if (!empty($observation->getImg4()))
        {
           $photo = "http://".$request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/images/observations/tof4/".$observation->getImg4();
           
           $fields = array("observation_photo[observation_id]"=> $obsid);
        
        
            $files = array();
            $files["file"] = file_get_contents($photo);
            
            
            $boundary = uniqid();
            $delimiter = '-------------' . $boundary;
            
            $post_data = $this->build_data_files($boundary, $fields, $files);
            
    
            
            $url = "/observation_photos.json";
            
            $file = $this->CallAPIFILE("POST", $url, $token, $post_data, $delimiter);
         
        }
        
        $observation->setIdInaturalist($obsid);
        $this->getDoctrine()->getManager()->flush();
        
        
        return $this->redirectToRoute('observations_show', array('id' => $observation->getId()));
        
    }
    
    public function CallAPI($method, $url, $data = false)
    {
        $site = "https://www.inaturalist.org";
        
        $curl = curl_init();
    
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
    
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $site.$url, http_build_query($data));
        }
    
        curl_setopt($curl, CURLOPT_URL, $site.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    
        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return $result;
    }
    
    public function CallAPIPOST($method, $url, $token, $data = false)
    {
        $site = "https://www.inaturalist.org";
        
        $curl = curl_init();
    
        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
    
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $site.$url, http_build_query($data));
        }
        
    
        curl_setopt($curl, CURLOPT_URL, $site.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //Set your auth headers
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $token));
    
        $result = curl_exec($curl);
    
        curl_close($curl);
    
        return $result;
    }
    
    public function CallAPIFILE($method, $url, $token, $data = false, $delimiter)
    {
        $site = "https://www.inaturalist.org";
        
        
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $site.$url,
          CURLOPT_RETURNTRANSFER => 1,
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POST => 1,
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer $token",
            "Content-Type: multipart/form-data; boundary=" . $delimiter,
            "Content-Length: " . strlen($data)
        
          ),
        
        
        ));
        
        
        //
        $response = curl_exec($curl);

        return $response;
    }
    
    public function deleteImg1Action(Request $request, Observations $observation)
    {
        $observation->setImg1(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('observations_edit', array('id' => $observation->getId()));
        
    }
    
    public function deleteImg2Action(Request $request, Observations $observation)
    {
        $observation->setImg2(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('observations_edit', array('id' => $observation->getId()));
        
    }
    
    public function deleteImg3Action(Request $request, Observations $observation)
    {
        $observation->setImg3(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('observations_edit', array('id' => $observation->getId()));
        
    }
    
    public function deleteImg4Action(Request $request, Observations $observation)
    {
        $observation->setImg4(null); 
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('observations_edit', array('id' => $observation->getId()));
        
    }
    
}