<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of MapController
 *
 * @author Dorian
 */
class MapController extends Controller
{
    public function getAllAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $result = array();
        
        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array(),array('id' => 'DESC',)
    );
        $i = 0;
        //var_dump($request);
        $nb = count($observations);
        for($i = 0; $i < $nb; $i++){
            $observation = $observations[$i];
            $result[$i]["id"] = $observation->getId();
            $result[$i]["alpha"] = $observation->getAlpha();
            $result[$i]["date"] = $observation->getDate()->format("d/m/Y");
            $result[$i]["heure"] = $observation->getDate()->format("H:i");
            $result[$i]["mort"] = $observation->getMort();
            $result[$i]["coordX"] = $observation->getCoordX();
            $result[$i]["coordY"] = $observation->getCoordY();
            $result[$i]["collecteur"] = $observation->getCollecteur();
            $result[$i]["typeid"] = $observation->getTypeObservations()->getId();
            $result[$i]["typequestion"] = $observation->getTypeObservations()->getTypeQuestion();
            
            $result[$i]["userAdd"] =  $observation->getUtilisateurs()->getUsername();
            if($request->getLocale() == "fr") {
                $result[$i]["type"] = $observation->getTypeObservations()->getNomFr();
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["espece"] = $observation->getEspeces()->getNomFr();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomFr();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomFr();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }
            else {
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["type"] = $observation->getTypeObservations()->getNomEn();
                    $result[$i]["espece"] = $observation->getEspeces()->getNomEn();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomEn();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomEn();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }

            if($observation->getImg1() != NULL){
                $result[$i]["image"] = "/images/observations/tof1/".$observation->getImg1();
            }
        }

        return new JsonResponse($result);
    }

    public function getMarkersAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $result = array();

        $debut = $request->request->get("debut");
        $fin = $request->request->get("fin");

        $presence = filter_var($request->request->get("presence"), FILTER_VALIDATE_BOOLEAN);
        $menace = filter_var($request->request->get("menace"), FILTER_VALIDATE_BOOLEAN);
        $alimentation = filter_var($request->request->get("alimentation"), FILTER_VALIDATE_BOOLEAN);
        $vivant = filter_var($request->request->get("vivant"), FILTER_VALIDATE_BOOLEAN);
        $mort = filter_var($request->request->get("mort"), FILTER_VALIDATE_BOOLEAN);
        $suivi = filter_var($request->request->get("suivi"), FILTER_VALIDATE_BOOLEAN);
        $rapide = filter_var($request->request->get("rapide"), FILTER_VALIDATE_BOOLEAN);


        //Transformation des dates en DateTime si nécessaire;

        $dateDebut = new \DateTime();
        $dateFin = new \DateTime();

        $dateFin->setDate(substr($fin, strrpos($fin, "-") + 1), substr($fin, strpos($fin, "-") + 1, (strrpos($fin, "-") - strpos($fin, "-") - 1)), substr($fin, 0, strpos($fin, "-")));


        if ($debut != "") {
            $dateDebut->setDate(substr($debut, strrpos($debut, "-") + 1), substr($debut, strpos($debut, "-") + 1, (strrpos($debut, "-") - strpos($debut, "-") - 1)), substr($debut, 0, strpos($debut, "-")));
        } else {
            $dateDebut = NULL;
        }

        if ($fin != "") {
            $dateFin->setDate(substr($fin, strrpos($fin, "-") + 1), substr($fin, strpos($fin, "-") + 1, (strrpos($fin, "-") - strpos($fin, "-") - 1)), substr($fin, 0, strpos($fin, "-")));
        } else {
            $dateFin = NULL;
        }


        if (($presence && $mort && $menace && $vivant && $alimentation) || (!$presence && !$mort && !$menace && !$vivant && !$alimentation)) {
            $observations = $em->getRepository("AppBundle:Observations")->getBetween($dateDebut, $dateFin);
        } else {
            $param["presence"] = $presence;
            $param["mort"] = $mort;
            $param["menace"] = $menace;
            $param["vivant"] = $vivant;
            $param["alimentation"] = $alimentation;
            $param["suivi"] = $suivi;
            $param["rapide"] = $rapide;

            $observations = $em->getRepository("AppBundle:Observations")->getBetweenWithParam($dateDebut, $dateFin, $param);
        }

        $nb = count($observations);
        for($i = 0; $i < $nb; $i++){
            $observation = $observations[$i];
            $result[$i]["id"] = $observation->getId();
            $result[$i]["alpha"] = $observation->getAlpha();
            $result[$i]["date"] = $observation->getDate()->format("d/m/Y");
            $result[$i]["heure"] = $observation->getDate()->format("H:i");
            $result[$i]["mort"] = $observation->getMort();
            $result[$i]["coordX"] = $observation->getCoordX();
            $result[$i]["coordY"] = $observation->getCoordY();
            $result[$i]["collecteur"] = $observation->getCollecteur();
            $result[$i]["typeid"] = $observation->getTypeObservations()->getId();
            $result[$i]["typequestion"] = $observation->getTypeObservations()->getTypeQuestion();
            
            $result[$i]["userAdd"] =  $observation->getUtilisateurs()->getUsername();
            if($request->getLocale() == "fr") {
                $result[$i]["type"] = $observation->getTypeObservations()->getNomFr();
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["espece"] = $observation->getEspeces()->getNomFr();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomFr();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomFr();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }
            else {
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["type"] = $observation->getTypeObservations()->getNomEn();
                    $result[$i]["espece"] = $observation->getEspeces()->getNomEn();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomEn();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomEn();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }

            if($observation->getImg1() != NULL){
                $result[$i]["image"] = "/images/observations/tof1/".$observation->getImg1();
            }
        }

        return new JsonResponse($result);
    }
    
    public function getAllRenaturaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $result = array();
        
        $proj = $em->getRepository('AppBundle:Projet')->find('62153');
        
        $observations = $em->getRepository('AppBundle:Observations')->findBy(
        array('projet' => $proj),array('id' => 'DESC',)
    );
        $i = 0;
        //var_dump($request);
        $nb = count($observations);
        for($i = 0; $i < $nb; $i++){
            $observation = $observations[$i];
            $result[$i]["id"] = $observation->getId();
            $result[$i]["alpha"] = $observation->getAlpha();
            $result[$i]["date"] = $observation->getDate()->format("d/m/Y");
            $result[$i]["heure"] = $observation->getDate()->format("H:i");
            $result[$i]["mort"] = $observation->getMort();
            $result[$i]["coordX"] = $observation->getCoordX();
            $result[$i]["coordY"] = $observation->getCoordY();
            $result[$i]["collecteur"] = $observation->getCollecteur();
            $result[$i]["typeid"] = $observation->getTypeObservations()->getId();
            $result[$i]["typequestion"] = $observation->getTypeObservations()->getTypeQuestion();
            
            $result[$i]["userAdd"] =  $observation->getUtilisateurs()->getUsername();
            if($request->getLocale() == "fr") {
                $result[$i]["type"] = $observation->getTypeObservations()->getNomFr();
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["espece"] = $observation->getEspeces()->getNomFr();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomFr();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomFr();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }
            else {
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["type"] = $observation->getTypeObservations()->getNomEn();
                    $result[$i]["espece"] = $observation->getEspeces()->getNomEn();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomEn();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomEn();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }

            if($observation->getImg1() != NULL){
                $result[$i]["image"] = "/images/observations/tof1/".$observation->getImg1();
            }
        }

        return new JsonResponse($result);
    }
    
    
    public function getMarkersRenaturaAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $result = array();

        $debut = $request->request->get("debut");
        $fin = $request->request->get("fin");

        $tortues_marines = filter_var($request->request->get("tortues_marines"), FILTER_VALIDATE_BOOLEAN);
        $autre_espece_marine = filter_var($request->request->get("autre_espece_marine"), FILTER_VALIDATE_BOOLEAN);
        $production_de_peche = filter_var($request->request->get("production_de_peche"), FILTER_VALIDATE_BOOLEAN);
        $menace_renatura = filter_var($request->request->get("menace_renatura"), FILTER_VALIDATE_BOOLEAN);
        $rapide = filter_var($request->request->get("rapide"), FILTER_VALIDATE_BOOLEAN);
        
        $projet = $request->request->get("projet");

        // recuperation du projet
        $proj = $em->getRepository('AppBundle:Projet')->find($projet);
        
        //Transformation des dates en DateTime si nécessaire;

        $dateDebut = new \DateTime();
        $dateFin = new \DateTime();

        $dateFin->setDate(substr($fin, strrpos($fin, "-") + 1), substr($fin, strpos($fin, "-") + 1, (strrpos($fin, "-") - strpos($fin, "-") - 1)), substr($fin, 0, strpos($fin, "-")));


        if ($debut != "") {
            $dateDebut->setDate(substr($debut, strrpos($debut, "-") + 1), substr($debut, strpos($debut, "-") + 1, (strrpos($debut, "-") - strpos($debut, "-") - 1)), substr($debut, 0, strpos($debut, "-")));
        } else {
            $dateDebut = NULL;
        }

        if ($fin != "") {
            $dateFin->setDate(substr($fin, strrpos($fin, "-") + 1), substr($fin, strpos($fin, "-") + 1, (strrpos($fin, "-") - strpos($fin, "-") - 1)), substr($fin, 0, strpos($fin, "-")));
        } else {
            $dateFin = NULL;
        }


        if (($tortues_marines && $autre_espece_marine && $production_de_peche && $menace_renatura && $rapide) || (!$tortues_marines && !$autre_espece_marine && !$production_de_peche && !$menace_renatura && !$rapide)) {
            $observations = $em->getRepository("AppBundle:Observations")->getBetweenRenatura($dateDebut, $dateFin, $projet);
        } else {
            $param["tortues_marines"] = $tortues_marines;
            $param["autre_espece_marine"] = $autre_espece_marine;
            $param["production_de_peche"] = $production_de_peche;
            $param["menace_renatura"] = $menace_renatura;
            $param["rapide"] = $rapide;

            $observations = $em->getRepository("AppBundle:Observations")->getBetweenWithParamRenatura($dateDebut, $dateFin, $param, $projet);
        }

        $nb = count($observations);
        for($i = 0; $i < $nb; $i++){
            $observation = $observations[$i];
            $result[$i]["id"] = $observation->getId();
            $result[$i]["alpha"] = $observation->getAlpha();
            $result[$i]["date"] = $observation->getDate()->format("d/m/Y");
            $result[$i]["heure"] = $observation->getDate()->format("H:i");
            $result[$i]["mort"] = $observation->getMort();
            $result[$i]["coordX"] = $observation->getCoordX();
            $result[$i]["coordY"] = $observation->getCoordY();
            $result[$i]["collecteur"] = $observation->getCollecteur();
            $result[$i]["typeid"] = $observation->getTypeObservations()->getId();
            $result[$i]["typequestion"] = $observation->getTypeObservations()->getTypeQuestion();
            
            $result[$i]["userAdd"] =  $observation->getUtilisateurs()->getUsername();
            if($request->getLocale() == "fr") {
                $result[$i]["type"] = $observation->getTypeObservations()->getNomFr();
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["espece"] = $observation->getEspeces()->getNomFr();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomFr();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomFr();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }
            else {
                if($observation->getEspeces()!=null)
                {
                    $result[$i]["type"] = $observation->getTypeObservations()->getNomEn();
                    $result[$i]["espece"] = $observation->getEspeces()->getNomEn();
                    $result[$i]["groupe"] = $observation->getGroupes()->getNomEn();
                    $result[$i]["sousgroupe"] = $observation->getSousGroupes()->getNomEn();
                }
                else{
                    $result[$i]["espece"] = "Identification";
                    $result[$i]["groupe"] = "Identification";
                    $result[$i]["sousgroupe"] = "Identification";
                }
            }

            if($observation->getImg1() != NULL){
                $result[$i]["image"] = "/images/observations/tof1/".$observation->getImg1();
            }
        }

        return new JsonResponse($result);
    }
    
}
