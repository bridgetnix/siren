class SQLQuery():
    def __init__(self, table_name):
        self.table_name = table_name

    def add_a_row(self, cur, conn, cols, values):
        placeholders = ','.join('%s' for col in cols.split(','))
        query_str = '''INSERT INTO 
                    {}
                    {}
                    VALUES 
                      ({})'''.format(self.table_name, cols, placeholders)
        cur.execute(query_str, values)
        conn.commit()

    def delete_a_row(self, cur, conn, id):
        cur.execute(f"DELETE FROM {self.table_name} WHERE id = {id}")
        conn.commit()

    def get_all(self, cur, limit=None, offset=None):
        limit_sql = f"LIMIT {limit} " if limit else ""
        offset_sql = f"OFFSET {offset}" if offset else ""
        
        cur.execute(f"SELECT * FROM {self.table_name} {limit_sql} {offset_sql}") 
        return cur.fetchall()

    def get_by_id(self, cur, id):
        cur.execute(f"SELECT * FROM {self.table_name} WHERE id = {id}")
        return cur.fetchone() 

    def get_by_projet(self, cur, projet_id):
        cur.execute(f"SELECT * FROM {self.table_name} WHERE projet_id = {projet_id}" )
        return cur.fetchall()

    def get_by_patrol(self, cur, patrouille_id):
        cur.execute(f"SELECT * FROM {self.table_name} WHERE patrouille_id = {patrouille_id}")
        return cur.fetchall()

    def get_count(self, cur):
        cur.execute(f"SELECT COUNT (id) FROM {self.table_name}")
        return cur.fetchone()[0]

    def get_col(self, cur, col):
        cur.execute(f"SELECT {col} FROM {self.table_name}")
        return cur.fetchall()

    def get_join_table(self, cur, data):
        col, table_1_name,table_2_name, primary_key, \
        foreign_key, condition = data.values()
        cur.execute(f'''SELECT {col} 
                        FROM {table_1_name} 
                        INNER JOIN {table_2_name} 
                        ON {primary_key} = {foreign_key} 
                        WHERE {condition}''')
        return cur.fetchall()
