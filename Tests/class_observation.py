class Observation:
    def __init__(self):
        self.table_name = 'observation'
        self.type = None

    def add(self, cur, conn, data):
        raise NotImplementedError()

    def delete(self, cur, conn, id):
        raise NotImplementedError()

    def get_all(self, cur, limit=None, offset=None):
        raise NotImplementedError()

    def get_by_id(self, cur, id):
        raise NotImplementedError()

    def get_all_by_patrol(self, cur, project_id):
        raise NotImplementedError()

    def get_count(self, cur):
        raise NotImplementedError()

    def get_date(self, cur, col, offset=0):
        raise NotImplementedError()

    def get_time(self, cur, col, offset=0):
        raise NotImplementedError

    def get_join_table(self, cur, data):
        raise NotImplementedError()

    def does_exist(self, cur, id):
        raise NotImplementedError()

    def is_empty(self, cur):
        raise NotImplementedError()
   
    def is_equal(self, a, b):
        return a == b