from datetime import datetime
import os
from dateutil.parser import parse
from patrol_factory import createClassPatrol
from dotenv import load_dotenv
from class_SQLTest import SQLTest
load_dotenv()

backend = os.getenv('TURTLE_SQL_BACKEND')
patrol = createClassPatrol(backend)
test = SQLTest(patrol)

def test_backend():
    test.backend()
   
def test_add(cur, conn):
    new_patrol =  (0, 4, 1, 2, 'Test', 1, 
            parse('2017-02-10 10:30:00'), 
            None, None, None, None, None, None)
    test.add(cur, conn, new_patrol)

def test_delete(cur, conn):
    new_patrol =  (0, 4, 1, 2, 'Test', 1, 
                   parse('2017-02-10 10:30:00'), 
                   None, None, None, None, None, None)
    test.delete(cur, conn, new_patrol) 

def test_select_by_project(cur):
    data = {
        'projet_id': 51,
        'offset': 3,
        'expected_row': (362, 74, 1, 89, # id, segment_id, type_id, patrouilleur_id
                    None, 1, # nomSite, etat
                    parse('2021-03-03 12:46:04'), # dateDebut
                    parse('2021-03-03 15:11:03'), # dateFin
                    25.2464413, 54.2041426, # coordXDebut, coordYDebut
                    25.2186307, 54.2326962, 51) # coordXFin, coordYFin, projet_id
    }
    test.select_by_project(cur, data)

def test_get_first_twenty_ids(cur):
    expected_ids = [2, 3, 25, 26, 27, 28, 30, 31, 32, 33,  
                    34, 35, 36, 37, 38, 39, 40, 42, 43, 44]
    test.get_first_twenty_ids(cur, expected_ids)

def test_get_last_twenty_ids(cur):
    expected_ids = [414, 415, 416, 417, 418, 
                    419, 420, 421, 422, 423, 
                    424, 425, 426, 427, 428, 
                    429, 430, 431, 432, 433]
    test.get_last_twenty_ids(cur, expected_ids)

def test_count(cur):
    expected_count = 403
    test.count(cur, expected_count)


def test_get_date(cur):
    datetime = ['2017-10-13 15:30:00', '2017-02-10 14:17:00']
    column_name = ['dateDebut', 'dateFin']
    offset = [5, 0]
    for i in range(2):
        data = {
            'time': parse(datetime[i]).date(),
            'column_name': column_name[i],
            'offset': offset[i]
        }   
        test.get_date(cur, data)

    

def test_get_time(cur):
    datetime = ['2017-10-13 15:30:00', '2017-02-10 14:17:00']
    column_name = ['dateDebut', 'dateFin']
    offset = [5, 0]

    for i in range(2):
        data = {
            'time': parse(datetime[i]).time(),
            'column_name': column_name[i],
            'offset': offset[i]
        }
        test.get_time(cur, data)


def test_join_table_by_projet_id(cur):
    expected_ids = [75,78,79,80,81,82,85,86,91,101,184,
                    185,194,195,196,197,198,199,373,374,
                    ]
    data = {
        "col": "patrouille.id",
        "table_1_name": "patrouille",
        "table_2_name": "projet",
        "primary_key": "projet.id",
        "foreign_key": "projet_id",
        "condition": "pays_id IS NULL",
    }
    test.join_table_by_projet_id(cur, expected_ids, data)
    