**To run the tests, we need to install following libraries:**

  1. [**pytest**](https://docs.pytest.org/en/latest/getting-started.html) <br>
   PyTest is a testing framework that allows users to write test codes using Python programming language. It helps you to write simple and scalable test cases for databases, APIs, or UI. PyTest is mainly used for writing tests for APIs. It helps to write tests from simple unit tests to complex functional tests. <br> 
     
      * _Install pytest:_
         *  pytest requires: Python 3.6, 3.7, 3.8, 3.9, or PyPy3.
         *  Run the following command in your command line:<br>
            `pip install -U pytest` 
<br>

  2. [**psycopg2**](https://pypi.org/project/psycopg2/) <br>
  Psycopg2 is a DB API 2.0 compliant PostgreSQL driver that is actively developed. It is designed for multi-threaded applications and manages its own connection pool. Other interesting features of the adapter are that if you are using the PostgreSQL array data type, Psycopg will automatically convert a result using that data type to a Python list. <br>

      *  Run the following command in your command line:<br>
         `pip install psycopg2`
         <br>
      
<br>

  3. [**dotenv**](https://pypi.org/project/python-dotenv/) <br>
  Python-dotenv reads key-value pairs from a .env file and can set them as environment variables. It helps in the development of applications following the 12-factor principles. <br>

      *  Run the following command in your command line:<br>
         `pip install python-doten`
<br>

  4. [**dateutil.parser**](https://dateutil.readthedocs.io/en/stable/) <br>
  The dateutil module provides powerful extensions to the standard datetime module. <br>

      *  Run the following command in your command line:<br>
         `pip install python-dateutil`

**To set and get variables from environment:**
1.  Create a `.env` file and copy and paste the following code into it.
    * `TURTLE_SQL_BACKEND=SQL`
    * `TURTLE_CRUD_BACKEND=CRUD`
<br>

2.  Add `.env` to `.gitignore` and make sure `.env` is hidden 

**To run tests:** 

* run following command in your command line: <br>
      `pytest -v`

