import psycopg2
import pytest 

'''
This function creates and returns a cursor
The cursor will be closed after a module test 
'''
@pytest.fixture
def cur(conn, scope="module"):
    cur = conn.cursor()
    yield cur
    cur.close()

'''
  This connection function supports only postgreSQL
  You can replace dbname "siren" to any name aplies to your database.
  You can add more information (eg. password=<password>) to the connection string
  The connection will be closed after a module test
'''
@pytest.fixture
def conn(scope="module"):
    conn = psycopg2.connect("dbname = siren \
                             user = postgres")
    yield conn
    conn.close()


    



