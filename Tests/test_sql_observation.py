import os
from dateutil.parser import parse
from patrol_factory import createClassObservation
from dotenv import load_dotenv
from class_SQLTest import SQLTest

load_dotenv()

backend = os.getenv('TURTLE_SQL_BACKEND')
observation = createClassObservation(backend)
test = SQLTest(observation)

def test_backend():
    test.backend()

def test_add(cur, conn):
    new_obs =  (0, 2, 1, # id, patrouille_id, categorie_id
                3.917471, 9.738139, # coordinates
                None, parse('2017-03-28 13:33:14')) # etate, dateAdd
    test.add(cur, conn, new_obs)
    

def test_delete(cur, conn):
    new_obs =  (0, 2, 1, # id, patrouille_id, categorie_id
                3.917471, 9.738139, # coordinates
                None, parse('2017-03-28 13:33:14')) # etate, dateAdd
    test.delete(cur, conn, new_obs)

def test_select_by_patrol(cur):
    data = {
            'patrouille_id': 37,
            'offset': 1,
            'expected': (51, 37, 3, # id, patrouiile_id, categorie_id
                        3.9155284, 11.5793433, # coordX, coordY,
                        0, parse('2017-11-18 08:21:17')) # etat, dateAdd
            }
    test.select_by_patrol(cur, data)

def test_get_first_twenty_ids(cur):
    expected_ids = [1, 2, 3, 4, 5,
                    35, 36, 37, 38, 39,
                    41, 42, 43, 44, 45,
                    46, 47, 48, 49, 50]
    test.get_first_twenty_ids(cur, expected_ids)

def test_get_last_twenty_ids(cur):
    expected_ids = [303,304, 305, 306, 307,
                    308, 309, 310, 311, 312, 
                    313, 314, 315, 316, 317,
                    318, 319, 320, 321, 322, ]
    test.get_last_twenty_ids(cur, expected_ids)   

def test_count(cur):
    expected_count = 285
    test.count(cur, expected_count)

def test_date(cur):
    data = {
        'date': parse('1991-08-13 17:47:12').date(),
        'column_name': 'dateAdd',
        'offset': 6
    }
    test.get_date(cur, data)

    data = {
        'date': parse('2017-03-28 13:33:14').date(),
        'column_name': 'dateAdd',
        'offset': 0
    }
    test.get_date(cur, data)

def test_time(cur):
    data = {
        'time': parse('1991-08-13 17:47:12').time(),
        'column_name': 'dateAdd',
        'offset': 6
    }
    test.get_time(cur, data)

    data = {
        'time': parse('2017-03-28 13:33:14').time(),
        'column_name': 'dateAdd',
        'offset': 0
    }
    test.get_time(cur, data)