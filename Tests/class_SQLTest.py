class SQLTest:
    def __init__(self, table):
        self.table = table
    def __exception_if_id_exists(self, cur, id):
        if self.table.does_exist(cur, id):
            raise Exception(f"id {id} Already Exists in database!")

    def __exception_if_id_not_exists(self, cur, id):
        if not self.table.does_exist(cur, id):
            raise Exception(f"id {id} is Not in database!")

    def backend(self):
        assert self.table.backend == 'SQL'

    def add(self, cur, conn, new_row):
        id = new_row[0]
        self.__exception_if_id_exists(cur, id)
        self.table.add(cur, conn, new_row)
        assert self.table.does_exist(cur, id)

        # delete the row after the test
        self.table.delete(cur, conn, id)
        self.__exception_if_id_exists(cur, id)

    
    def delete(self, cur, conn, new_row):
        id = new_row[0]
        self.__exception_if_id_exists(cur, id) 
        # add a new_row for test purpose      
        self.table.add(cur, conn, new_row)
        self.__exception_if_id_not_exists(cur, id)
        
        self.table.delete(cur, conn, id) 
        assert not self.table.does_exist(cur, id)

    def select_by_project(self, cur, data={}):
        projet_id, offset, expected = data.values()

        rows = self.table.get_all_by_project(cur, projet_id)
        actual = rows[offset]
        assert self.table.is_equal(expected, actual) 
        assert len(rows) == 66

    def select_by_patrol(self, cur, data = {}):
        projet_id, offset, expected = data.values()

        rows = self.table.get_all_by_patrol(cur, projet_id)
        actual = rows[offset]
        assert self.table.is_equal(expected, actual) 
        assert len(rows) == 2

    def join_table_by_projet_id(self, cur, expected, data):              
        rows = self.table.get_join_table(cur, data)
        actual = [r[0] for r in rows]
        assert self.table.is_equal(actual, expected)

    def get_first_twenty_ids(self, cur, expected):
        limit, offset = 20, 0
        rows = self.table.get_all(cur, limit, offset)
        actual = [r[0] for r in rows]
        assert  self.table.is_equal(actual, expected)

    def get_last_twenty_ids(self, cur, expected):
        limit = 20
        offset = self.table.get_count(cur) - limit              
        rows = self.table.get_all(cur, limit, offset)
        actual = [r[0] for r in rows]
        assert  self.table.is_equal(actual, expected)

    def count(self, cur, expected):       
        actual = self.table.get_count(cur)
        assert  self.table.is_equal(expected, actual)


    def get_date(self, cur, data):
        expected, col, offset = data.values()
        actual =  self.table.get_date(cur, col, offset)
        assert self.table.is_equal(expected, actual)

    def get_time(self, cur, data):
        expected, col, offset = data.values()
        actual =  self.table.get_time(cur, col, offset)
        assert self.table.is_equal(expected, actual)


    