webpackJsonp([20],{

/***/ 324:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceuilPageModule", function() { return AcceuilPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__acceuil__ = __webpack_require__(481);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_navbar_navbar_module__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_translate__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var AcceuilPageModule = /** @class */ (function () {
    function AcceuilPageModule() {
    }
    AcceuilPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__acceuil__["a" /* AcceuilPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_4_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__acceuil__["a" /* AcceuilPage */]),
            ],
        })
    ], AcceuilPageModule);
    return AcceuilPageModule;
}());

//# sourceMappingURL=acceuil.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 481:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcceuilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_services_language__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the AcceuilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AcceuilPage = /** @class */ (function () {
    function AcceuilPage(navCtrl, alertCtrl, events, geolocation, loadingCtrl, navParams, mylocalstorage, services, translate, language) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.mylocalstorage = mylocalstorage;
        this.services = services;
        this.translate = translate;
        this.language = language;
        this.langue = 0;
        this.resultat = {};
        this.userconnecte = {};
        this.monprojet = {
            note: ""
        };
    }
    AcceuilPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.events.publish('user:created', {}, Date.now());
        /*this.services.currentMenusListner.subscribe(next =>{
           alert(JSON.stringify(next));
        })
    
        this.services.getAllProjetMenu().then((myresult:any) =>{
          alert(JSON.stringify(myresult));
        })*/
        /*var initialHref = window.location.href;
        navigator.splashscreen.show();
        // Reload original app url (ie your index.html file)
        window.location = initialHref;
        navigator.splashscreen.hide();*/
        //window.location.reload();
        __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = [];
        var loading2 = this.loadingCtrl.create();
        loading2.present();
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                _this.myprojet = _this.myuserconnecte.projet;
                _this.monprojet.note = _this.myprojet.note;
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    var loading_1 = _this.loadingCtrl.create();
                    loading_1.present();
                    /*this.services.projet_typeObservation(this.myprojet.id).subscribe((result) =>{
                          console.log(result);
                          this.listetypeobservations = result;
                    }, (error)=>{
                      loading.dismiss();
                    }, ()=>{
                      loading.dismiss();
                    });*/
                    /*this.services.getAllProjetTypeObsevationsByProjetId(this.myprojet.id).then((result) =>{
                          console.log(result);
                          this.listetypeobservations = result;
                          loading.dismiss();
                    });*/
                    _this.services.getAllTypeObsevations().then(function (result) {
                        loading_1.dismiss();
                        _this.listetypeobservations = result;
                        loading2.dismiss();
                    });
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        /*this.mylocalstorage.getSession().then((result) =>{
    
          console.log("========================================");
          console.log(result);
          console.log("========================================");
          
        })*/
        /*let resultat = {
          "contenu":"le resultat",
          "questions":1,
          "observations":1,
          "etat":1
        }
        this.services.createResultat(resultat).then((result) =>{
           
          
        });*/
        //var timestamp = Math.round(new Date().getTime()/1000);
        //alert(timestamp);
        /*setInterval(() => {
    
          if(navigator.onLine){
    
                  this.mylocalstorage.getSession().then((result) =>{
    
                        this.userconnecte = result;
    
                        
                        this.geolocation.getCurrentPosition().then( (resp) =>{
                        
                              var utilisateurId = this.userconnecte["id"];
                              var projetId = this.userconnecte["projet"].id;
                              this.services.savepositions(resp.coords.latitude, resp.coords.longitude, utilisateurId, projetId).subscribe((result) =>{
                                  
                                
                              
                              }, (error)=>{
                              }, ()=>{
                              });
    
                        }).catch( (error) =>{
                            console.log("error could not get position");
                        });
    
                  });
    
            }
          
        }, 120000);*/
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.langue = 1;
            }
            else {
                _this.langue = 0;
            }
        });
        if (navigator.onLine) {
            /*let loading = this.loadingCtrl.create();
            loading.present();
            this.services.acceuil().subscribe((result) =>{
                  console.log(result);
                  this.listetypeobservations = result;
            }, (error)=>{
              loading.dismiss();
            }, ()=>{
              loading.dismiss();
            });*/
            /*let loading = this.loadingCtrl.create();
            loading.present();
              this.services.getAllTypeObsevations().then((result) =>{
                    loading.dismiss();
                    this.listetypeobservations = result;
            });*/
        }
        else {
            /*let loading = this.loadingCtrl.create();
            loading.present();
              this.services.getAllTypeObsevations().then((result) =>{
                    loading.dismiss();
                    this.listetypeobservations = result;
              });*/
        }
    };
    AcceuilPage.prototype.groupe = function (observation) {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].latitude != null) {
            if (observation.type_question == "groupe") {
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].typeobservation = observation;
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].indicateur = 1;
                //this.navCtrl.push('GroupePage');
                this.navCtrl.push('PhotosPage', { idquestion: 0, type_question: "groupe" });
            }
            else {
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].indicateur = 2;
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].typeobservation = observation;
                if (navigator.onLine) {
                    //////////////////////////////////// for test ////////////////////////////////////////////
                    /* this.questionsid =  observation.questions.id;

                      let loading = this.loadingCtrl.create();
                      loading.present();

                      if(undefined === this.questionsid){

                              loading.dismiss();
                              Parcourt.indicateur = 3;
                              this.navCtrl.push('PhotosPage',{idquestion: 0, type_question: "rapide"});
        
                      }else{
                            this.services.findQuestionById(this.questionsid).subscribe((result) =>{

                                
                                  
                            if(result.type_reponse == "text"){
                            
                                //this.navCtrl.push('NombrepetitPage',{idquestion: result.id});
                                this.navCtrl.push('PhotosPage',{idquestion: result.id, type_question: "text"});
                            }
                            if(result.type_reponse == "select"){
                              
                              // this.navCtrl.push('OuestanimalPage',{idquestion: result.id});
                              this.navCtrl.push('PhotosPage',{idquestion: result.id, type_question: "select"});
                              }
                          }, (error)=>{
                            console.log(error);
                            //votre opération a  echoué!!
                            loading.dismiss();
                          }, ()=>{
                            loading.dismiss();
                          });
                      }   */
                    //////////////////////////////////////////// for server ////////////////////////////////////////
                    this.questionsid = observation.questions;
                    if (null === this.questionsid) {
                        __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].indicateur = 3;
                        this.navCtrl.push('PhotosPage', { idquestion: 0, type_question: "rapide" });
                    }
                    else {
                        var loading_2 = this.loadingCtrl.create();
                        loading_2.present();
                        this.services.getQuestionById(this.questionsid).then(function (result) {
                            loading_2.dismiss();
                            if (result[0].type_reponse == "text") {
                                //this.navCtrl.push('NombrepetitPage',{idquestion: result[0].id});
                                _this.navCtrl.push('PhotosPage', { idquestion: result[0].id, type_question: "text" });
                            }
                            if (result[0].type_reponse == "select") {
                                // this.navCtrl.push('OuestanimalPage',{idquestion: result[0].id});
                                _this.navCtrl.push('PhotosPage', { idquestion: result[0].id, type_question: "select" });
                            }
                        });
                    }
                }
                else {
                    this.questionsid = observation.questions;
                    if (null === this.questionsid) {
                        __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].indicateur = 3;
                        this.navCtrl.push('PhotosPage', { idquestion: 0, type_question: "rapide" });
                    }
                    else {
                        var loading_3 = this.loadingCtrl.create();
                        loading_3.present();
                        this.services.getQuestionById(this.questionsid).then(function (result) {
                            loading_3.dismiss();
                            if (result[0].type_reponse == "text") {
                                //this.navCtrl.push('NombrepetitPage',{idquestion: result[0].id});
                                _this.navCtrl.push('PhotosPage', { idquestion: result[0].id, type_question: "text" });
                            }
                            if (result[0].type_reponse == "select") {
                                // this.navCtrl.push('OuestanimalPage',{idquestion: result[0].id});
                                _this.navCtrl.push('PhotosPage', { idquestion: result[0].id, type_question: "select" });
                            }
                        });
                    }
                }
            }
        }
        else {
            this.translate.get(['LOCALISATIONERROR', 'MESSAGELOCALISATION']).subscribe(function (langs) {
                var alert = _this.alertCtrl.create({
                    title: langs['LOCALISATIONERROR'],
                    message: langs['MESSAGELOCALISATION'],
                    buttons: [
                        {
                            text: 'OK',
                            role: 'cancel',
                            handler: function () {
                                _this.geolocation.getCurrentPosition().then(function (resp) {
                                    __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
                                    __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
                                }).catch(function (error) {
                                    console.log("error could not get position");
                                });
                            }
                        }
                    ]
                });
                alert.present();
            });
        }
    };
    /*savePays(){
      let pays = {
  
        nom_fr: "Cameroun",
        nom_en: "Cameroon",
        code_tel: "237",
        sigle: "CMR"
      }
      this.services.createPays(pays).then((result1) =>{
          this.services.getAllPays().then((result) =>{
                this.listpays = result;
          })
      });
    }*/
    AcceuilPage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    AcceuilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-acceuil',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/acceuil/acceuil.html"*/'<ion-header>\n    <navbar title="{{ \'BIENVENUS\' | translate }}"></navbar>\n</ion-header>\n\n\n<ion-content class="backgroundcorps">\n\n    <p class="textesacceuil">{{ \'AJOUTEROBSERVATION\' | translate }}</p>\n\n    <ion-grid>\n         <ion-row >\n              <ion-col col-6  *ngFor="let item of listetypeobservations">\n                  <div class="cardpremieracceuil" (click)="groupe(item)">\n                    <img class="premier" src="{{item.image}}" />\n                    <strong><p class="textcard" *ngIf="langue == 0">{{item.nom_fr}}</p></strong>\n                    <strong><p class="textcard" *ngIf="langue == 1">{{item.nom_en}}</p></strong>\n                  </div>\n              </ion-col>\n              \n         </ion-row>\n    </ion-grid>\n\n\n   \n   \n\n    \n   \n    <!--<br>\n    <br>\n    <ion-list>\n        <ion-item *ngFor="let item of listpays">\n             <p>nom : {{item.nom_fr}}</p>\n             <p>sigle : {{item.sigle}}</p>\n        </ion-item>\n    </ion-list> -->\n   \n\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/acceuil/acceuil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_services_services__["a" /* ServicesProvider */], __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_6__providers_services_language__["a" /* LanguageService */]])
    ], AcceuilPage);
    return AcceuilPage;
}());

//# sourceMappingURL=acceuil.js.map

/***/ })

});
//# sourceMappingURL=20.js.map