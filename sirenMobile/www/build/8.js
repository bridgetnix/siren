webpackJsonp([8],{

/***/ 325:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NombrepetitPageModule", function() { return NombrepetitPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nombrepetit__ = __webpack_require__(482);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var NombrepetitPageModule = /** @class */ (function () {
    function NombrepetitPageModule() {
    }
    NombrepetitPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__nombrepetit__["a" /* NombrepetitPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__nombrepetit__["a" /* NombrepetitPage */]),
            ],
        })
    ], NombrepetitPageModule);
    return NombrepetitPageModule;
}());

//# sourceMappingURL=nombrepetit.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 482:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NombrepetitPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_services_language__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the NombrepetitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NombrepetitPage = /** @class */ (function () {
    function NombrepetitPage(navCtrl, language, mylocalstorage, geolocation, loadingCtrl, navParams, services) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.mylocalstorage = mylocalstorage;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        this.donnees = {};
        this.testvalue = 0;
        this.langue = 0;
        this.mydata = {
            questions: {
                titre_fr: "",
                titre_en: ""
            },
            titre_fr: "",
            titre_en: ""
        };
    }
    NombrepetitPage.prototype.ionViewWillLeave = function () {
        var listereponses = [];
        if (this.testvalue == 0) {
            listereponses = __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses;
            console.log(listereponses);
            listereponses.pop();
            console.log(listereponses);
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
        }
    };
    NombrepetitPage.prototype.ionViewWillEnter = function () {
        this.testvalue = 0;
    };
    NombrepetitPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log(__WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */]);
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.langue = 1;
            }
            else {
                _this.langue = 0;
            }
        });
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.idquestion = this.navParams.get("idquestion");
        this.type_question = this.navParams.get("type_question");
        if (this.idquestion != 0) {
            if (navigator.onLine) {
                ////////////////////////////////////// FOR TEST /////////////////////
                /*let loading = this.loadingCtrl.create();
                loading.present();
                this.services.findQuestionById(this.idquestion).subscribe((result) =>{
                  console.log(result);
                  this.mareponse = result;
                  if(this.langue ==0){
                    this.titrequestion = result.titre_fr;
                  }else{
                    this.titrequestion = result.titre_en;
                  }
                  
                }, (error)=>{
                  console.log(error);
                  //votre opération a  echoué!!
                  loading.dismiss();
                }, ()=>{
                  loading.dismiss();
                });*/
                //////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////// FOR SERVER /////////
                this.services.getQuestionById(this.idquestion).then(function (result) {
                    _this.mareponse = result[0];
                    if (_this.langue == 0) {
                        _this.titrequestion = result[0].titre_fr;
                    }
                    else {
                        _this.titrequestion = result[0].titre_en;
                    }
                });
                /////////////////////////////////////////////////////////////////////
            }
            else {
                this.services.getQuestionById(this.idquestion).then(function (result) {
                    _this.mareponse = result[0];
                    if (_this.langue == 0) {
                        _this.titrequestion = result[0].titre_fr;
                    }
                    else {
                        _this.titrequestion = result[0].titre_en;
                    }
                });
            }
        }
    };
    NombrepetitPage.prototype.ouestlanimal = function (donnees) {
        var _this = this;
        if (navigator.onLine) {
            ///////////////////////////////////////////////////// ONLINE /////////////////////////////
            /*if(this.langue == 0){
              this.mydata.questions = this.mareponse;
              this.mydata.titre_fr = donnees.mareponse;
            }else{
              this.mydata.questions = this.mareponse;
              this.mydata.titre_en = donnees.mareponse;
            }
            this.testvalue = 1;
              var listereponses = [];
              if(Parcourt.listesReponses != null){
                listereponses = Parcourt.listesReponses;
                listereponses.push(this.mydata);
                Parcourt.listesReponses = listereponses;
              }else{
                
                listereponses.push(this.mydata);
                Parcourt.listesReponses = listereponses;
              }
              if(this.mareponse.questions!=undefined){
                  if(this.mareponse.questions.type_reponse == "entier"){
                    this.navCtrl.push('NombrepetitPage',{idquestion: this.mareponse.questions.id, type_question: this.mareponse.questions.type_question});
                  }else if(this.mareponse.questions.type_reponse == "text"){
                    this.navCtrl.push('DescriptionPage',{question: 0, reponse:0});
                  }else if(this.mareponse.questions.type_reponse == "select"){
                    this.navCtrl.push('OuestanimalPage',{idquestion: this.mareponse.questions.id, type_question: this.mareponse.questions.type_question});
                  }else if(this.mareponse.questions.type_reponse == "rapide"){
                    this.navCtrl.push('DescriptionPage',{idquestion: this.mareponse.questions.id, type_question: this.mareponse.questions.type_question});
                  }else if(this.mareponse.questions.type_reponse == "annuler"){
                    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                  }else{
                    this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                  }
              }else{
                this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
              }*/
            ////////////////////////////////////////////////////END ONLINE //////////////////////////
            if (this.langue == 0) {
                this.mydata.questions = this.mareponse;
                this.mydata.titre_fr = donnees.mareponse;
            }
            else {
                this.mydata.questions = this.mareponse;
                this.mydata.titre_en = donnees.mareponse;
            }
            this.testvalue = 1;
            var listereponses = [];
            if (__WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                listereponses = __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses;
                listereponses.push(this.mydata);
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
            }
            else {
                listereponses.push(this.mydata);
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
            }
            if (this.mareponse.questions != undefined) {
                this.services.getQuestionById(this.mareponse.questions).then(function (result) {
                    var questions_next = result[0];
                    if (questions_next.type_reponse == "entier") {
                        _this.navCtrl.push('NombrepetitPage', { idquestion: questions_next.id, type_question: questions_next.type_question });
                    }
                    else if (questions_next.type_reponse == "text") {
                        _this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
                    }
                    else if (questions_next.type_reponse == "select") {
                        _this.navCtrl.push('OuestanimalPage', { idquestion: questions_next.id, type_question: questions_next.type_question });
                    }
                    else if (questions_next.type_reponse == "rapide") {
                        _this.navCtrl.push('DescriptionPage', { idquestion: questions_next.id, type_question: questions_next.type_question });
                    }
                    else if (questions_next.type_reponse == "annuler") {
                        _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                    }
                    else {
                        _this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
                    }
                });
            }
            else {
                this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
            }
        }
        else {
            if (this.langue == 0) {
                this.mydata.questions = this.mareponse;
                this.mydata.titre_fr = donnees.mareponse;
            }
            else {
                this.mydata.questions = this.mareponse;
                this.mydata.titre_en = donnees.mareponse;
            }
            this.testvalue = 1;
            var listereponses = [];
            if (__WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                listereponses = __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses;
                listereponses.push(this.mydata);
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
            }
            else {
                listereponses.push(this.mydata);
                __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
            }
            if (this.mareponse.questions != undefined) {
                this.services.getQuestionById(this.mareponse.questions).then(function (result) {
                    var questions_next = result[0];
                    if (questions_next.type_reponse == "entier") {
                        _this.navCtrl.push('NombrepetitPage', { idquestion: questions_next.id, type_question: questions_next.type_question });
                    }
                    else if (questions_next.type_reponse == "text") {
                        _this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
                    }
                    else if (questions_next.type_reponse == "select") {
                        _this.navCtrl.push('OuestanimalPage', { idquestion: questions_next.id, type_question: questions_next.type_question });
                    }
                    else if (questions_next.type_reponse == "rapide") {
                        _this.navCtrl.push('DescriptionPage', { idquestion: questions_next.id, type_question: questions_next.type_question });
                    }
                    else if (questions_next.type_reponse == "annuler") {
                        _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                    }
                    else {
                        _this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
                    }
                });
            }
            else {
                this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
            }
        }
    };
    NombrepetitPage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    NombrepetitPage.prototype.backFunction = function (color) {
        __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = [];
        this.navCtrl.setRoot('AcceuilPage').then(function () { });
    };
    NombrepetitPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nombrepetit',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/nombrepetit/nombrepetit.html"*/'<ion-header>\n    <navbar [title]= "titrequestion" ></navbar>\n</ion-header>\n\n\n<ion-content class="backgroundcorps">\n\n\n     <div class="blockbuttontext">\n        <input class="moninput" [(ngModel)]="donnees.mareponse" type="number"   placeholder="{{titrequestion}}">\n     </div>\n\n     <br>\n     <br>\n     \n     <div class="blockbutton">\n        <button  ion-button class="colors"  block (click)="ouestlanimal(donnees)">\n            {{ \'SUIVANT\' | translate }}\n            <ion-icon name="arrow-forward" float-right class="monicon"></ion-icon>\n        </button>\n     </div>\n\n     <ion-row class="monbutonvalide">\n        <ion-col>\n          <button  (click)="backFunction()" ion-fab left bottom  color="danger"> <ion-icon name="md-close"></ion-icon> </button>\n        </ion-col>\n    </ion-row>\n\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/nombrepetit/nombrepetit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_services_language__["a" /* LanguageService */], __WEBPACK_IMPORTED_MODULE_5__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_services_services__["a" /* ServicesProvider */]])
    ], NombrepetitPage);
    return NombrepetitPage;
}());

//# sourceMappingURL=nombrepetit.js.map

/***/ })

});
//# sourceMappingURL=8.js.map