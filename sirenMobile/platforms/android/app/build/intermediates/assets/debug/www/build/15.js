webpackJsonp([15],{

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EspecePageModule", function() { return EspecePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__espece__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var EspecePageModule = /** @class */ (function () {
    function EspecePageModule() {
    }
    EspecePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__espece__["a" /* EspecePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__espece__["a" /* EspecePage */]),
            ],
        })
    ], EspecePageModule);
    return EspecePageModule;
}());

//# sourceMappingURL=espece.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 469:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EspecePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_services_language__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the EspecePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EspecePage = /** @class */ (function () {
    function EspecePage(navCtrl, geolocation, mylocalstorage, loadingCtrl, navParams, services, translate, language) {
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.mylocalstorage = mylocalstorage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        this.translate = translate;
        this.language = language;
        this.userconnecte = {};
        this.especeelement = __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].sousgroupe;
        this.typeobservation = __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].typeobservation;
        this.questionsid = 0;
        if (navigator.onLine) {
            this.internet = 1;
        }
        else {
            this.internet = 0;
        }
    }
    EspecePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.langue = 1;
            }
            else {
                _this.langue = 0;
            }
        });
        this.especeelement = __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].sousgroupe;
        this.typeobservation = __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].typeobservation;
        if (navigator.onLine) {
            this.langue = 'fr';
            /*let loading = this.loadingCtrl.create();
            loading.present();
            this.mylocalstorage.getSession().then((result) =>{
              
              this.userconnecte = result;
                  this.services.especeBySousGroupe(this.userconnecte["projet"].id, this.especeelement.id).subscribe((result) =>{
                        this.listeespeces = result;
                  }, (error)=>{
                    console.log(error);
                    loading.dismiss();
                  }, ()=>{
                    loading.dismiss();
                  });
            });*/
            var loading_1 = this.loadingCtrl.create();
            loading_1.present();
            this.mylocalstorage.getSession().then(function (result) {
                _this.services.getAllEspecesBySousGroupeId(_this.especeelement.id).then(function (result) {
                    loading_1.dismiss();
                    _this.listeespeces = result;
                });
            });
        }
        else {
            var loading_2 = this.loadingCtrl.create();
            loading_2.present();
            this.mylocalstorage.getSession().then(function (result) {
                _this.services.getAllEspecesBySousGroupeId(_this.especeelement.id).then(function (result) {
                    loading_2.dismiss();
                    _this.listeespeces = result;
                });
            });
        }
    };
    EspecePage.prototype.nombrepetit = function (espece) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].espece = espece;
        if (this.typeobservation.nom_fr == "Animal") {
            /*if(this.internet==0){
              this.questionsid = espece.questions_animal;
            }else{
              this.questionsid = espece.questions_animal.id;
            }*/
            this.questionsid = espece.questions_animal;
        }
        if (this.typeobservation.nom_fr == "Menaces") {
            /* if(this.internet == 0){
               this.questionsid = espece.questions_menaces;
             }else{
               this.questionsid = espece.questions_menaces.id;
             }*/
            this.questionsid = espece.questions_menaces;
        }
        if (this.typeobservation.nom_fr == "Alimetation") {
            /* if(this.internet == 0){
               this.questionsid = espece.questions_alimentation;
             }else{
               this.questionsid = espece.questions_alimentation.id;
             }*/
            this.questionsid = espece.questions_alimentation;
        }
        if (this.typeobservation.nom_fr == "Signe de présence") {
            /*if(this.internet == 0){
              this.questionsid = espece.questions_signe;
            }else{
              this.questionsid = espece.questions_signe.id;
            }*/
            this.questionsid = espece.questions_signe;
        }
        if (this.questionsid != 0) {
            if (navigator.onLine) {
                var loading_3 = this.loadingCtrl.create();
                loading_3.present();
                this.services.findQuestionById(this.questionsid).subscribe(function (result) {
                    if (result.type_reponse == "text") {
                        _this.navCtrl.push('NombrepetitPage', { idquestion: result.id, type_question: "text" });
                    }
                    if (result.type_reponse == "select") {
                        _this.navCtrl.push('OuestanimalPage', { idquestion: result.id, type_question: "select" });
                    }
                }, function (error) {
                    console.log(error);
                    //votre opération a  echoué!!
                    loading_3.dismiss();
                }, function () {
                    loading_3.dismiss();
                });
            }
            else {
                var loading_4 = this.loadingCtrl.create();
                loading_4.present();
                this.services.getQuestionById(this.questionsid).then(function (result) {
                    loading_4.dismiss();
                    if (result[0].type_reponse == "text") {
                        _this.navCtrl.push('NombrepetitPage', { idquestion: result[0].id, type_question: "text" });
                    }
                    if (result[0].type_reponse == "select") {
                        _this.navCtrl.push('OuestanimalPage', { idquestion: result[0].id, type_question: "select" });
                    }
                });
            }
        }
    };
    EspecePage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    EspecePage.prototype.backFunction = function (color) {
        __WEBPACK_IMPORTED_MODULE_3__configs_configs__["b" /* Parcourt */].listesReponses = [];
        this.navCtrl.setRoot('AcceuilPage').then(function () { });
    };
    EspecePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-espece',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/espece/espece.html"*/'<ion-header>\n    <navbar title= "{{ \'SELECTIONESPECE\' | translate }}" ></navbar>\n</ion-header>\n<ion-content class="backgroundcorps" >\n    <div well>\n        <div well-header> \n          <p *ngIf="langue == 0">{{especeelement.nom_fr}}</p>\n          <p *ngIf="langue == 1">{{especeelement.nom_fr}}</p>\n        </div>\n       <div well-body>\n          <div well-body-image>\n            <img src="{{especeelement.image}}">\n          </div>  \n         <div well-body-content>\n            <p *ngIf="langue == 0" >{{especeelement.description_fr}}</p>\n            <p *ngIf="langue == 1" >{{especeelement.description_en}}</p>\n          </div>\n       </div>\n  </div>\n    <ion-grid>\n         <ion-row >\n              <ion-col col-6  *ngFor="let item of listeespeces">\n                  <div class="cardpremieracceuil" (click)="nombrepetit(item)">\n                    <img class="premier" src="{{item.image}}" />\n                    <strong><p class="textcard" *ngIf="langue == 0">{{item.nom_fr}}</p></strong>\n                    <strong><p class="textcard" *ngIf="langue == 1">{{item.nom_en}}</p></strong>\n                </div>\n              </ion-col>\n         </ion-row>\n    </ion-grid>\n\n\n    <ion-row class="monbutonvalide">\n      <ion-col>\n        <button  (click)="backFunction()" ion-fab left bottom  color="danger"> <ion-icon name="md-close"></ion-icon> </button>\n      </ion-col>\n    </ion-row>\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/espece/espece.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_services_services__["a" /* ServicesProvider */], __WEBPACK_IMPORTED_MODULE_6_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_5__providers_services_language__["a" /* LanguageService */]])
    ], EspecePage);
    return EspecePage;
}());

//# sourceMappingURL=espece.js.map

/***/ })

});
//# sourceMappingURL=15.js.map