webpackJsonp([13],{

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InscriptionPageModule", function() { return InscriptionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inscription__ = __webpack_require__(471);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var InscriptionPageModule = /** @class */ (function () {
    function InscriptionPageModule() {
    }
    InscriptionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__inscription__["a" /* InscriptionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__inscription__["a" /* InscriptionPage */]),
            ],
        })
    ], InscriptionPageModule);
    return InscriptionPageModule;
}());

//# sourceMappingURL=inscription.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 471:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InscriptionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the InscriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InscriptionPage = /** @class */ (function () {
    function InscriptionPage(translate, geolocation, navCtrl, toastCtrl, loadingCtrl, navParams, services) {
        this.translate = translate;
        this.geolocation = geolocation;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        this.temoininscription = 0;
        this.utilisateur = {};
    }
    InscriptionPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        if (navigator.onLine) {
            this.getAllPays();
            this.getAllFonctions();
        }
        else {
            this.translate.get(['INFOSCONNEXION']).subscribe(function (langs) {
                _this.presentToast(langs['INFOSCONNEXION']);
            });
        }
    };
    InscriptionPage.prototype.inscription = function () {
        var _this = this;
        if (navigator.onLine) {
            var loading_1 = this.loadingCtrl.create();
            loading_1.present();
            this.services.saveUsers(this.utilisateur).subscribe(function (result) {
                _this.temoininscription = 1;
                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                    _this.presentToast(langs['SUCCESSOPERATION']);
                });
            }, function (error) {
                console.log(error);
                loading_1.dismiss();
            }, function () {
                loading_1.dismiss();
            });
        }
        else {
            this.translate.get(['INFOSCONNEXION']).subscribe(function (langs) {
                _this.presentToast(langs['INFOSCONNEXION']);
            });
        }
    };
    InscriptionPage.prototype.connexion = function () {
        this.navCtrl.setRoot('ConnexionPage');
    };
    InscriptionPage.prototype.getAllPays = function () {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.services.getpays().subscribe(function (result) {
            _this.listepays = result;
        }, function (error) {
            console.log(error);
            loading.dismiss();
        }, function () {
            loading.dismiss();
        });
    };
    InscriptionPage.prototype.getAllFonctions = function () {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        this.services.getfonctions().subscribe(function (result) {
            _this.listefonctions = result;
        }, function (error) {
            console.log(error);
            loading.dismiss();
        }, function () {
            loading.dismiss();
        });
    };
    InscriptionPage.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    };
    InscriptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-inscription',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/inscription/inscription.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <ion-row>\n        <ion-col col-2>\n            <img style="height: 50px; width: 50px; margin-top: -6px;" src="assets/imgs/logo.png">\n        </ion-col>\n        <ion-col>\n            <ion-title class="institle">{{ \'INSCRIPTION\' | translate }}</ion-title>\n        </ion-col>\n    </ion-row>\n    \n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="backgroundcorps">\n \n    <div *ngIf="temoininscription==0">\n\n        <p class="textesacceuil">{{ \'INFOSC\' | translate }}</p>\n        <ion-input class="monimput" type="text" [(ngModel)]="utilisateur.email" placeholder=" Email" style="background: transparent;">\n        </ion-input> \n        <hr style=" background-color: black; height: 1px; margin-top: 2px; margin-left: 20px; margin-right: 20px;">\n        <ion-input class="monimput" type="password" [(ngModel)]="utilisateur.password" placeholder=" Mot de passe" style="background: transparent;">\n        </ion-input> \n          <hr style=" background-color: black; height: 1px; margin-top: 2px; margin-left: 20px; margin-right: 20px;">\n    \n       <p class="textesacceuil">{{ \'INFOSP\' | translate }}</p>\n    \n     \n      \n       <ion-item class="monitem">\n             <ion-label>{{ \'PAYS\' | translate }}</ion-label>\n             <ion-select [(ngModel)]="utilisateur.pays" class="moselect">\n                 <ion-option  value="{{item.id}}" *ngFor="let item of listepays"> {{item.nom_fr}} </ion-option>\n             </ion-select>     \n       </ion-item>\n\n\n\n       <hr style=" background-color: black; height: 1px; margin-top: 2px; margin-left: 20px; margin-right: 20px;">\n       \n       <ion-input class="monimput" type="text" [(ngModel)]="utilisateur.telephone" placeholder=" Téléphone" style="background: transparent;">\n       </ion-input> \n       <hr style=" background-color: black; height: 1px; margin-top: 2px; margin-left: 20px; margin-right: 20px;">\n    \n       <ion-input class="monimput" type="text" [(ngModel)]="utilisateur.ville" placeholder=" Ville" style="background: transparent;">\n        </ion-input> \n     \n       \n    \n        <hr style=" background-color: black; height: 1px; margin-top: 2px; margin-left: 20px; margin-right: 20px;">\n    \n       \n\n         <ion-item class="monitem">\n            <ion-label>{{ \'FONCTIONS\' | translate }}</ion-label>\n            <ion-select [(ngModel)]="utilisateur.fonction" class="moselect">\n                <ion-option value="{{item.id}}" *ngFor="let item of listefonctions"> {{item.nom_fr}} </ion-option>\n            </ion-select>     \n        </ion-item>\n    \n         <hr style=" background-color: black; height: 1px; margin-top: 2px; margin-left: 20px; margin-right: 20px;">\n    \n         <br>\n    \n         <div class="blockbutton">\n            <button  ion-button class="colors"  block (click)="inscription(donnees)">\n               \n                {{ \'INSCRIRE\' | translate }}\n            </button>\n         </div>\n\n    </div>\n\n    <div *ngIf="temoininscription==1">\n        <p class="textesacceuil"> {{ \'INFOS\' | translate }}</p>\n        <img style="height: 25%; width: 25%;  margin-left: 40%; margin-top: 10%;" src="assets/imgs/valid.png">\n   \n   \n        <div class="blockbutton">\n            <button  ion-button class="colors"  block (click)="connexion()">\n               \n                {{ \'CONNEXION\' | translate }}\n            </button>\n        </div>\n  \n    </div>\n    \n\n  </ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/inscription/inscription.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_services_services__["a" /* ServicesProvider */]])
    ], InscriptionPage);
    return InscriptionPage;
}());

//# sourceMappingURL=inscription.js.map

/***/ })

});
//# sourceMappingURL=13.js.map