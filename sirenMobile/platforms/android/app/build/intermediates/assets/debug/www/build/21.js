webpackJsonp([21],{

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnexionPageModule", function() { return ConnexionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__connexion__ = __webpack_require__(488);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConnexionPageModule = /** @class */ (function () {
    function ConnexionPageModule() {
    }
    ConnexionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__connexion__["a" /* ConnexionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__connexion__["a" /* ConnexionPage */]),
            ],
        })
    ], ConnexionPageModule);
    return ConnexionPageModule;
}());

//# sourceMappingURL=connexion.module.js.map

/***/ }),

/***/ 488:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnexionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__configs_configs__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ConnexionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConnexionPage = /** @class */ (function () {
    function ConnexionPage(translate, geolocation, alertCtrl, navCtrl, mylocalstorage, menuCtrl, toastCtrl, loadingCtrl, navParams, services) {
        this.translate = translate;
        this.geolocation = geolocation;
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.mylocalstorage = mylocalstorage;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        //public utilisateur = {};
        this.userconnecte = {};
        this.mesprojets = [];
        this.total = 0;
        this.totaleffectue = 0;
        this.utilisateur = {
            login: "",
            password: ""
        };
    }
    ConnexionPage.prototype.ionViewDidLoad = function () {
        this.total = 9;
        this.totaleffectue = 0;
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_6__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_6__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
    };
    ConnexionPage.prototype.login = function (utilisateur) {
        var _this = this;
        var loading = this.loadingCtrl.create();
        loading.present();
        if (navigator.onLine) {
            /*this.services.login(utilisateur).subscribe(result =>{
              if(result != 0){
                
                  this.mylocalstorage.storeSession(result).then(() => {});
                  loading.onDidDismiss(() => {
                    //this.navCtrl.setRoot('PhotosPage');
      
                    ////////////////////////////////////////////// NEW /////////////////////////
      
                    if(navigator.onLine){
                          this.mylocalstorage.getSession().then((result:any) =>{
                                this.userconnecte = result;
                                let loading = this.loadingCtrl.create();
                                loading.present();
                                this.services.findProjetsByUserId(this.userconnecte["id"]).subscribe((resultat) =>{
                                    this.mesprojets = resultat;
                                    console.log(this.mesprojets);
                                    this.showRadio(this.mesprojets);
                                }, (error)=>{
                                  console.log(error);
                                  //votre opération a  echoué!!
                                  loading.dismiss();
                                }, ()=>{
                                  loading.dismiss();
                                });
                          })
                      }else{
                  
                        this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
                          this.presentToast(langs['INFOSCONNEXION']);
                          this.navCtrl.setRoot('AcceuilPage');
                        });
                        
                      }
      
                      /////////////////////////////////////////////////////////////////////////////////
                    
                    //this.navCtrl.setRoot('AcceuilPage');
                    
                    this.menuCtrl.enable(true, 'sideMenu');
                  });
              }else{
                this.translate.get(['INFOSLOGINV']).subscribe((langs:Array<string>) => {
                  this.presentToast(langs['INFOSLOGINV']);
                });
              }
            }, error =>{
              loading.dismiss();
              console.log(error);
            },() =>{
              loading.dismiss();
            });*/
            this.services.getAllCheckConnexion().then(function (result) {
                if (result == 1) {
                    _this.services.login(utilisateur).subscribe(function (result) {
                        if (result != 0) {
                            _this.mylocalstorage.storeSession(result).then(function () { });
                            loading.onDidDismiss(function () {
                                _this.navCtrl.setRoot('AcceuilPage');
                                _this.menuCtrl.enable(true, 'sideMenu');
                            });
                        }
                        else {
                            _this.translate.get(['INFOSLOGINV']).subscribe(function (langs) {
                                _this.presentToast(langs['INFOSLOGINV']);
                            });
                        }
                    }, function (error) {
                        loading.dismiss();
                        console.log(error);
                    }, function () {
                        loading.dismiss();
                    });
                }
                else {
                    var connexions_1 = {
                        valeur: 0
                    };
                    connexions_1.valeur = 1;
                    _this.services.login(utilisateur).subscribe(function (result) {
                        if (result != 0) {
                            _this.mylocalstorage.storeSession(result).then(function () { });
                            _this.services.createCheckConnexion(connexions_1).then(function (result) { });
                            _this.userconnecte = result;
                            var loading_1 = _this.loadingCtrl.create();
                            loading_1.present();
                            _this.services.findProjetsByUserId(_this.userconnecte["id"]).subscribe(function (resultat) {
                                _this.mesprojets = resultat;
                                console.log(_this.mesprojets);
                                _this.showRadio(_this.mesprojets);
                            }, function (error) {
                                console.log(error);
                                //votre opération a  echoué!!
                                loading_1.dismiss();
                            }, function () {
                                loading_1.dismiss();
                            });
                            var loading2_1 = _this.loadingCtrl.create();
                            loading2_1.present();
                            setInterval(function () {
                                loading2_1.dismiss();
                            }, 15000);
                        }
                        else {
                            _this.translate.get(['INFOSLOGINV']).subscribe(function (langs) {
                                _this.presentToast(langs['INFOSLOGINV']);
                            });
                        }
                    }, function (error) {
                        loading.dismiss();
                        console.log(error);
                    }, function () {
                        loading.dismiss();
                    });
                }
            });
        }
        else {
            this.services.findUtilisateursByLoginAndPassword(this.utilisateur).then(function (result) {
                if (result == 0) {
                    _this.translate.get(['INFOSLOGIN']).subscribe(function (langs) {
                        _this.presentToast(langs['INFOSLOGIN']);
                    });
                    loading.dismiss();
                }
                else {
                    _this.services.getAllPaysById(result[0].pays).then(function (result2) {
                        result[0].pays = result2[0];
                        _this.services.getAllFonctionById(result[0].fonctions).then(function (result3) {
                            result[0].fonctions = result3[0];
                            //this.services.getAllProjetById(result[0].projet).then((result4) =>{
                            _this.services.getAllProjetById(result[0].projet).then(function (result4) {
                                result4[0].id = result4[0].rowid;
                                result[0].projet = result4[0];
                                _this.mylocalstorage.storeSession(result[0]).then(function () {
                                    loading.dismiss();
                                    _this.navCtrl.setRoot('AcceuilPage');
                                    _this.menuCtrl.enable(true, 'sideMenu');
                                });
                            });
                        });
                    });
                }
            });
        }
    };
    ConnexionPage.prototype.inscription = function () {
        this.navCtrl.setRoot('InscriptionPage');
    };
    ConnexionPage.prototype.forguetPassword = function () {
        var _this = this;
        if (navigator.onLine) {
            window.open('http://siren.ammco.org/web/fr/resetting/request', '_blank');
        }
        else {
            this.translate.get(['INFOSCONNEXION']).subscribe(function (langs) {
                _this.presentToast(langs['INFOSCONNEXION']);
            });
        }
    };
    ConnexionPage.prototype.showRadio = function (listeprojets) {
        var _this = this;
        this.translate.get(['SELECTPROJET', 'ENREGISTRER', 'SUCCESSOPERATION']).subscribe(function (langs) {
            var alert = _this.alertCtrl.create();
            alert.setTitle(langs['SELECTPROJET']);
            _this.mesprojets.forEach(function (element) {
                var monimput = {
                    type: 'radio',
                    label: element.nom,
                    value: element.id,
                };
                alert.addInput(monimput);
            });
            alert.addButton({
                text: langs['ENREGISTRER'],
                cssClass: 'ion-button full',
                handler: function (data) {
                    var loading = _this.loadingCtrl.create();
                    loading.present();
                    _this.mylocalstorage.getSession().then(function (result) {
                        _this.userconnecte = result;
                        _this.utilisateur.login = _this.userconnecte["email"];
                        _this.utilisateur.password = _this.userconnecte["password"];
                        _this.services.changerProjet(_this.userconnecte["id"], data).subscribe(function (resultat) {
                            _this.services.login(_this.utilisateur).subscribe(function (result) {
                                if (result != 0) {
                                    _this.mylocalstorage.storeSession(result).then(function () {
                                        _this.Synchronisateur();
                                        _this.navCtrl.setRoot('AcceuilPage');
                                        /*if(this.totaleffectue == this.total){
                                            loading.onDidDismiss(() => {
                                              this.navCtrl.setRoot('AcceuilPage');
                                              this.menuCtrl.enable(true, 'sideMenu');
                                            });
                                        }*/
                                    });
                                }
                                else {
                                    _this.translate.get(['INFOSLOGINV']).subscribe(function (langs) {
                                        _this.presentToast(langs['INFOSLOGINV']);
                                    });
                                }
                            }, function (error) {
                                loading.dismiss();
                                console.log(error);
                            }, function () {
                                loading.dismiss();
                            });
                        }, function (error) {
                            console.log(error);
                            //votre opération a  echoué!!
                            loading.dismiss();
                        }, function () {
                            loading.dismiss();
                        });
                    });
                }
            });
            alert.present();
        });
    };
    ConnexionPage.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    };
    /* this.services.deletteAllPays().then((result1) =>{
  
    }) */ ;
    ConnexionPage.prototype.Synchronisateur = function () {
        var _this = this;
        this.services.deletteAllPays().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.services.getpays().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    _this.services.createPays(element).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            _this.totaleffectue = _this.totaleffectue + 1;
                            loading.dismiss();
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllFonctions().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.services.getfonctions().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    _this.services.createFonction(element).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            loading.dismiss();
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllGroupes().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.services.groupes().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    _this.services.createGroupe(element).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            loading.dismiss();
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllSousGroupes().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.services.allsousgroupes().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    var sous_groupe = {
                        "id": element.id,
                        "nom_fr": element.nom_fr,
                        "nom_en": element.nom_en,
                        "image": element.image,
                        "description_fr": element.description_fr,
                        "description_en": element.description_en,
                        "groupes": element.groupes.id
                    };
                    _this.services.createSousGroupe(sous_groupe).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            loading.dismiss();
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
                alert("uen erreur au rendez-vous c quoi le pb sousgroupe");
                alert(JSON.stringify(error));
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllReponses().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.services.allreponses().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    if (undefined != element.questions_next) {
                        var reponse = {};
                        reponse = {
                            "id": element.id,
                            "titre_fr": element.titre_fr,
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id,
                            "questions_next": element["questions_next"].id,
                            "image": element.image,
                            "taille": element.taille
                        };
                        _this.services.createReponse(reponse).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                loading.dismiss();
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    }
                    else {
                        var reponse = {};
                        reponse = {
                            "id": element.id,
                            "titre_fr": element.titre_fr,
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id,
                            "image": element.image,
                            "taille": element.taille
                        };
                        _this.services.createReponse(reponse).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    }
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAlltypeObservation().then(function (result1) {
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                //this.services.acceuil().subscribe((result) =>{ 
                var loading = _this.loadingCtrl.create();
                loading.present();
                _this.services.projet_typeObservation(_this.userconnecte["projet"].id).subscribe(function (result) {
                    var i = 0;
                    result.forEach(function (element) {
                        _this.services.createTypeObservation(element).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                loading.dismiss();
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                    console.log(error);
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllProjets().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                _this.services.findProjetsByUserId(_this.userconnecte["id"]).subscribe(function (result) {
                    var i = 0;
                    result.forEach(function (element) {
                        var projet = {
                            "id": element.id,
                            "nom": element.nom,
                            "rowid": element.id,
                            "lieu": element.lieu,
                            "public": element.public,
                            "pays": element["pays"].id,
                            "utilisateurs": element['utilisateurs'].id,
                            "note": element.note,
                            "logo": element.logo,
                            "mention": element.mention
                        };
                        _this.services.createProjet(projet).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                loading.dismiss();
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllProjetMenu().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                _this.projetId = _this.userconnecte["projet"].id;
                _this.services.getprojet_typeObservation(_this.projetId).subscribe(function (result) {
                    var i = 0;
                    //this.services.setCurrentMenuProjet(result);
                    result.forEach(function (element) {
                        var projet = {
                            "id": element.id,
                            "nomFr": element.nomFr,
                            "nomEn": element.nomEn,
                            "title": element.title
                        };
                        _this.services.createProjetMenu(projet).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                loading.dismiss();
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllEspeces().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                _this.projetId = _this.userconnecte["projet"].id;
                _this.services.findEspecesByProjetId(_this.projetId).subscribe(function (result) {
                    var i = 0;
                    result.forEach(function (element) {
                        var espece = {
                            "id": element.id,
                            "nom_fr": element.nom_fr,
                            "nom_en": element.nom_en,
                            "image": element.image,
                            "description_fr": element.description_fr,
                            "description_en": element.description_en,
                            "sous_groupes": element["sous_groupes"].id,
                            "questions_animal": element["questions_animal"].id,
                            "questions_menaces": element["questions_menances"].id,
                            "questions_signe": element["questions_signe"].id,
                            "questions_alimentation": element["questions_alimentation"].id
                        };
                        _this.services.createEspece(espece).then(function (result1) {
                            i++;
                            if (i == result.length) {
                                loading.dismiss();
                                _this.totaleffectue = _this.totaleffectue + 1;
                                if (_this.totaleffectue == _this.total) {
                                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                        _this.presentToast(langs['SUCCESSOPERATION']);
                                        _this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                        });
                    });
                }, function (error) {
                    alert("une erreur au rendez-vous c quoi le pb");
                    alert(JSON.stringify(error));
                }, function () {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllUtilisateur().then(function (result) {
            _this.mylocalstorage.getSession().then(function (result2) {
                _this.userconnecte = result2;
                _this.projetId = _this.userconnecte["projet"].id;
                var utilisateurs = {
                    "id": _this.userconnecte["id"],
                    "username": _this.userconnecte["email"],
                    "email": _this.userconnecte["email"],
                    "password": _this.userconnecte["password"],
                    "ville": _this.userconnecte["ville"],
                    "telephone": _this.userconnecte["telephone"],
                    "pays": _this.userconnecte["pays"].id,
                    "fonctions": _this.userconnecte["fonctions"].id,
                    "projet": _this.userconnecte["projet"].id,
                    "monuserid": _this.userconnecte["id"]
                };
                _this.services.createUtilisateur(utilisateurs).then(function (result1) {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllInscription().then(function (result) {
            _this.mylocalstorage.getSession().then(function (result2) {
                _this.userconnecte = result2;
                _this.projetId = _this.userconnecte["projet"].id;
                var utilisateurs = {
                    "id": _this.userconnecte["id"],
                    "username": _this.userconnecte["email"],
                    "email": _this.userconnecte["email"],
                    "password": _this.userconnecte["password"],
                    "ville": _this.userconnecte["ville"],
                    "telephone": _this.userconnecte["telephone"],
                    "pays": _this.userconnecte["pays"].id,
                    "fonctions": _this.userconnecte["fonctions"].id,
                    "projet": _this.userconnecte["projet"].id,
                    "monuserid": _this.userconnecte["id"]
                };
                _this.services.createInscription(utilisateurs).then(function (result1) {
                });
            });
        });
        ////////////////////////////////////////////////////////////////
        this.services.deletteAllQuestions().then(function (result1) {
            var loading = _this.loadingCtrl.create();
            loading.present();
            _this.services.allquestions().subscribe(function (result) {
                var i = 0;
                result.forEach(function (element) {
                    var myquestionnext;
                    if (element["questions"] == undefined) {
                        myquestionnext = "null";
                    }
                    else {
                        myquestionnext = element["questions"].id;
                    }
                    var question = {
                        "id": element.id,
                        "titre_fr": element.titre_fr,
                        "titre_en": element.titre_en,
                        "type_reponse": element.type_reponse,
                        "interval": element.interval,
                        "type_debut": element.type_debut,
                        "image": element.image,
                        "isPicture": element.isPicture,
                        "questions": myquestionnext
                    };
                    _this.services.createQuestion(question).then(function (result1) {
                        i++;
                        if (i == result.length) {
                            loading.dismiss();
                            _this.totaleffectue = _this.totaleffectue + 1;
                            if (_this.totaleffectue == _this.total) {
                                _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                                    _this.presentToast(langs['SUCCESSOPERATION']);
                                    _this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                    });
                });
            }, function (error) {
            }, function () {
            });
        });
        ////////////////////////////////////////////////////////////////
    };
    ConnexionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-connexion',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/connexion/connexion.html"*/'<ion-content class="background">\n\n    <div class="logologin">\n        \n        <img class="logo" src="assets/imgs/logo.png">   \n        <p class="textelogin">Siren</p>\n   </div>\n \n  \n    <ion-input class="monimput" type="text" type="text" [(ngModel)]="utilisateur.login"  placeholder=" Email ou téléphone" style="background: transparent;  color: #fff;">\n    </ion-input> \n    <hr style=" background-color: #fff; height: 1px; margin-top: 2px; margin-left: 33px; margin-right: 38px;">\n     \n    <ion-input class="monimput" type="password" [(ngModel)]="utilisateur.password" placeholder=" Mot de passe" style="background: transparent; color: #fff;">\n      </ion-input> \n      <hr style=" background-color: #fff; height: 1px; margin-top: 2px; margin-left: 33px; margin-right: 38px;">\n     \n    \n  \n   <ion-row>\n      <ion-col>\n        <a (click)="inscription()"><p class="texte2">nouveau ? s\'incrire</p></a>\n      </ion-col>\n      <ion-col>\n        <a (click)="forguetPassword()"><p class="texte2"> mots de passe oublié? </p></a>\n      </ion-col>\n   </ion-row>\n  \n   \n\n  <div class="blockbutton">\n    <button  ion-button class="colors"  block (click)="login(utilisateur)">\n      CONNEXION\n    </button>\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/connexion/connexion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_services_services__["a" /* ServicesProvider */]])
    ], ConnexionPage);
    return ConnexionPage;
}());

//# sourceMappingURL=connexion.js.map

/***/ })

});
//# sourceMappingURL=21.js.map