webpackJsonp([22],{

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalStorageProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs_configs__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the LocalStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LocalStorageProvider = /** @class */ (function () {
    function LocalStorageProvider(storage) {
        this.storage = storage;
        this.key = 'sirensession';
    }
    LocalStorageProvider.prototype.storeSession = function (data) {
        __WEBPACK_IMPORTED_MODULE_2__configs_configs__["c" /* Session */].utilisateur = data;
        return this.storage.set(this.key, data);
    };
    LocalStorageProvider.prototype.getSession = function () {
        var _this = this;
        return new Promise(function (resolve, failed) {
            _this.storage.get(_this.key).then(function (data) {
                resolve(data);
            }).catch(function (error) {
                failed();
            });
        });
    };
    //store key
    LocalStorageProvider.prototype.setKey = function (key, value) {
        return this.storage.set(key, value);
    };
    //get the stored key
    LocalStorageProvider.prototype.getKey = function (key) {
        return this.storage.get(key);
    };
    //delete key
    LocalStorageProvider.prototype.removeKey = function (key) {
        return this.storage.remove(key);
    };
    //clear the whole local storage
    LocalStorageProvider.prototype.clearStorage = function () {
        return this.storage.clear();
    };
    LocalStorageProvider.prototype.logut = function () {
        this.storage.remove(this.key);
    };
    LocalStorageProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
    ], LocalStorageProvider);
    return LocalStorageProvider;
}());

//# sourceMappingURL=localstorage.js.map

/***/ }),

/***/ 114:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__host_host__ = __webpack_require__(291);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the ServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ServicesProvider = /** @class */ (function () {
    function ServicesProvider(http, toastCtrl, https, sqlite) {
        var _this = this;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.https = https;
        this.sqlite = sqlite;
        this.initHeaders().then(function (next) {
            _this.headers = next;
        });
        this.initHeaders2().then(function (next) {
            _this.myheaders = next;
        });
        this.host = __WEBPACK_IMPORTED_MODULE_2__host_host__["a" /* currentHost */];
    }
    ServicesProvider.prototype.initHeaders = function () {
        return new Promise(function (resolve, reject) {
            resolve({
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({})
            });
        });
    };
    ServicesProvider.prototype.initHeaders2 = function () {
        return new Promise(function (resolve, reject) {
            resolve({
                headers: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Access-Control-Allow-Origin': '*'
                })
            });
        });
    };
    /**
     * Retourne les détails sur un article
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.login = function (utilisateur) {
        return this.http.get(this.host + 'connexion/' + utilisateur.login + '/' + utilisateur.password, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.acceuil = function () {
        return this.http.get(this.host + 'typeobservations', this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.projet_typeObservation = function (projetId) {
        return this.http.get(this.host + 'typeobservations/' + projetId, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.allquestions = function () {
        return this.http.get(this.host + 'questions', this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.groupes = function () {
        return this.http.get(this.host + 'groupes', this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.sousgroupes = function (id) {
        return this.http.get(this.host + 'sousgroupesgroupes/' + id, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.allsousgroupes = function () {
        return this.http.get(this.host + 'sousgroupes', this.headers);
    };
    /*private currentMenusProjet = new BehaviorSubject<any>(null);
    public currentMenusListner = this.currentMenusProjet.asObservable();
    setCurrentMenuProjet(listemenus){
      this.currentMenusProjet.next(listemenus);
    }*/
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.allreponses = function () {
        return this.http.get(this.host + 'reponses', this.headers);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.especeBySousGroupe = function (projetId, especeId) {
        return this.http.get(this.host + 'especessousgroupes/' + projetId + '/' + especeId, this.headers);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.findQuestionById = function (questionId) {
        return this.http.get(this.host + 'questionsid/' + questionId, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.findReponseByQuestionId = function (questionId) {
        return this.http.get(this.host + 'reponsesquestions/' + questionId, this.headers);
    };
    //http://siren.ammco.org/web/fr/api/reponsesquestions/230
    /**
     * créer une obseravtion
     * @param
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.saveObservation = function (observation) {
        alert(JSON.stringify(observation));
        return this.http.post(this.host + 'observation', observation, this.myheaders);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.findProjetsByUserId = function (utilisateurid) {
        return this.http.get(this.host + 'projetutilisateur/' + utilisateurid, this.headers);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.findAllProjets = function () {
        return this.http.get(this.host + 'projetutilisateur', this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.findObervationByProjetId = function (utilisateurID, projetID) {
        return this.http.get(this.host + 'observationsuser/' + utilisateurID + '/' + projetID, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    /*saveresulatat(resultat): Observable<any> {
      return this.http.get(
        this.host + 'resultats/' + resultat.contenu + '/' + resultat.questions + '/' + resultat.observations,
        this.headers
      );
    }*/
    ServicesProvider.prototype.saveresulatat = function (resultat) {
        return this.http.post(this.host + 'resultats', resultat, this.myheaders);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.savepositions = function (latitude, longitude, utilisateurId, projetId) {
        return this.http.get(this.host + 'position/' + latitude + '/' + longitude + '/' + utilisateurId + '/' + projetId, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.findEspecesByProjetId = function (projetId) {
        return this.http.get(this.host + 'especes/' + projetId, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.changerProjet = function (utilisateurid, projetId) {
        return this.http.get(this.host + 'changerprojet/' + utilisateurid + '/' + projetId, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.getprojet_typeObservation = function (projetId) {
        console.log(this.host + 'menu_projet/' + projetId);
        return this.http.get(this.host + 'menu_projet/' + projetId, this.headers);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.findObservationByUserId = function (utilisateurid, projetId) {
        return this.http.get(this.host + 'observationsuser/' + utilisateurid + '/' + projetId, this.headers);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.findNotificationByUserId = function (utilisateurid) {
        return this.http.get(this.host + 'notificationsprojet/' + utilisateurid, this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.saveCommentaire = function (useremail, values) {
        return this.http.get(this.host + 'feedback/' + useremail + '/' + useremail + '/' + values, this.headers);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.getpays = function () {
        return this.http.get(this.host + 'pays', this.headers);
    };
    /**
    * Retourne la liste des questions
    * @returns {Observable<any>}
    */
    ServicesProvider.prototype.getfonctions = function () {
        return this.http.get(this.host + 'fonctions', this.headers);
    };
    /**
     * Retourne la liste des questions
     * @returns {Observable<any>}
     */
    ServicesProvider.prototype.saveUsers = function (utilisateur) {
        return this.http.get(this.host + 'inscription/' + utilisateur.email + '/' + utilisateur.email + '/' + utilisateur.telephone + '/' + utilisateur.password + '/' + utilisateur.ville + '/' + utilisateur.pays + '/' + utilisateur.fonction, this.headers);
    };
    ///////////////////////////////////////////////////////////////
    ///// base de données local ///////////
    //////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createPays = function (pays) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO PAYS VALUES(?,?,?,?,?)', [pays.id, pays.nom_fr, pays.nom_en, pays.code_tel, pays.sigle])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table pays donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllPays = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PAYS ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, code_tel: res.rows.item(i).code_tel, sigle: res.rows.item(i).sigle });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllPays = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM PAYS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createCheckConnexion = function (checkconnexion) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO CHECKCONNEXION VALUES(NULL,?)', [checkconnexion.valeur])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table CHECKCONNEXION donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllCheckConnexion = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM CHECKCONNEXION ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, valeur: res.rows.item(i).valeur });
                    }
                    if (expenses.length != 0) {
                        resolve(1);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllcheckconnexion = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM CHECKCONNEXION', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createFonction = function (fonction) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO FONCTIONS VALUES(?,?,?)', [fonction.id, fonction.nom_fr, fonction.nom_en])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table FONCTIONS donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllFonction = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM FONCTIONS ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllFonctions = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM FONCTIONS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createProjetMenu = function (projetmenu) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO PROJETMENU VALUES(?,?,?,?)', [projetmenu.id, projetmenu.nomFr, projetmenu.nomEn, projetmenu.title])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table PROJETMENU donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllProjetMenu = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PROJETMENU ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nomFr: res.rows.item(i).nomFr, nomEn: res.rows.item(i).nomEn, title: res.rows.item(i).title });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllProjetMenu = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM PROJETMENU', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createInscription = function (utilisateur) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO INSCRIPTION VALUES(NULL,?,?,?,?,?,?,?,?)', [utilisateur.username, utilisateur.email, utilisateur.telephone, utilisateur.password, utilisateur.ville, utilisateur.pays, utilisateur.fonction, utilisateur.monuserid])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table INSCRIPTION donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllInscription = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM INSCRIPTION ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, username: res.rows.item(i).username, email: res.rows.item(i).email, telephone: res.rows.item(i).telephone, password: res.rows.item(i).password, ville: res.rows.item(i).ville, pays: res.rows.item(i).pays, fonction: res.rows.item(i).fonction, monuserid: res.rows.item(i).monuserid });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllInscription = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM INSCRIPTION', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createGroupe = function (groupe) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO GROUPES VALUES(?,?,?,?,?,?)', [groupe.id, groupe.nom_fr, groupe.nom_en, groupe.image, groupe.description_fr, groupe.description_en])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table GROUPES donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllGroupes = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM GROUPES ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllGroupesById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM GROUPES WHERE id = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllGroupes = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM GROUPES', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createSousGroupe = function (sousgroupe) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO SOUS_GROUPES VALUES(?,?,?,?,?,?,?)', [sousgroupe.id, sousgroupe.nom_fr, sousgroupe.nom_en, sousgroupe.image, sousgroupe.description_fr, sousgroupe.description_en, sousgroupe.groupes])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table SOUS_GROUPES donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllSousGroupes = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM SOUS_GROUPES ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en, groupes: res.rows.item(i).groupes });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllSousGroupesById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM SOUS_GROUPES WHERE id = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en, groupes: res.rows.item(i).groupes });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllSousGroupes = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM SOUS_GROUPES', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createQuestion = function (question) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO QUESTIONS VALUES(?,?,?,?,?,?,?,?,?)', [question.id, question.titre_fr, question.titre_en, question.type_reponse, question.interval, question.type_debut, question.image, question.isPicture, question.questions])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table QUESTIONS donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllQuestions = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM QUESTIONS ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, titre_fr: res.rows.item(i).titre_fr, titre_en: res.rows.item(i).titre_en, type_reponse: res.rows.item(i).type_reponse, interval: res.rows.item(i).interval, type_debut: res.rows.item(i).type_debut, image: res.rows.item(i).image, isPicture: res.rows.item(i).isPicture, questions: res.rows.item(i).questions });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllQuestions = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM QUESTIONS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createReponse = function (reponse) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO REPONSES VALUES(?,?,?,?,?,?,?)', [reponse.id, reponse.titre_fr, reponse.titre_en, reponse.questions, reponse.questions_next, reponse.image, reponse.taille])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table REPONSES donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllReponses = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM REPONSES ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, titre_fr: res.rows.item(i).titre_fr, titre_en: res.rows.item(i).titre_en, question: res.rows.item(i).question, questions_next: res.rows.item(i).questions_next, image: res.rows.item(i).image, taille: res.rows.item(i).taille });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllReponses = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM REPONSES', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createProjetTypeObservation = function (projettypeobservation) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO PROJET_TYPE_OBSERVATIONS VALUES(?,?,?,?,?,?,?)', [projettypeobservation.id, projettypeobservation.nom_fr, projettypeobservation.nom_en, projettypeobservation.image, projettypeobservation.type_question, projettypeobservation.questions.id, projettypeobservation.projet.id])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table PROJET_TYPE_OBSERVATIONS donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                // this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllProjetTypeObsevations = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PROJET_TYPE_OBSERVATIONS ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, type_question: res.rows.item(i).type_question, questions: res.rows.item(i).questions, projet: res.rows.item(i).projet });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllProjetTypeObsevationsById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PROJET_TYPE_OBSERVATIONS WHERE id = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, type_question: res.rows.item(i).type_question, questions: res.rows.item(i).questions, projet: res.rows.item(i).projet });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllProjetTypeObsevationsByProjetId = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PROJET_TYPE_OBSERVATIONS WHERE projet = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, type_question: res.rows.item(i).type_question, questions: res.rows.item(i).questions, projet: res.rows.item(i).projet });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllProjettypeObservation = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM TYPE_OBSERVATIONS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                // this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.createTypeObservation = function (typeobservation) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO TYPE_OBSERVATIONS VALUES(?,?,?,?,?,?)', [typeobservation.id, typeobservation.nom_fr, typeobservation.nom_en, typeobservation.image, typeobservation.type_question, typeobservation.questions.id])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table TYPE_OBSERVATIONS donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllTypeObsevations = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM TYPE_OBSERVATIONS ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, type_question: res.rows.item(i).type_question, questions: res.rows.item(i).questions });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllTypeObsevationsById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM TYPE_OBSERVATIONS WHERE id = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, type_question: res.rows.item(i).type_question, questions: res.rows.item(i).questions });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAlltypeObservation = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM TYPE_OBSERVATIONS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createEspece = function (espece) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO ESPECES VALUES(?,?,?,?,?,?,?,?,?,?,?)', [espece.id, espece.nom_fr, espece.nom_en, espece.image, espece.description_fr, espece.description_en, espece.sous_groupes, espece.questions_animal, espece.questions_menaces, espece.questions_signe, espece.questions_alimentation])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table ESPECES donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllEspeces = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM ESPECES ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en, sous_groupes: res.rows.item(i).sous_groupes, questions_animal: res.rows.item(i).questions_animal, questions_menaces: res.rows.item(i).questions_menaces, questions_signe: res.rows.item(i).questions_signe, questions_alimentation: res.rows.item(i).questions_alimentation });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllEspecesById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM ESPECES WHERE id = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en, sous_groupes: res.rows.item(i).sous_groupes, questions_animal: res.rows.item(i).questions_animal, questions_menaces: res.rows.item(i).questions_menaces, questions_signe: res.rows.item(i).questions_signe, questions_alimentation: res.rows.item(i).questions_alimentation });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllEspeces = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM ESPECES', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createProjet = function (projet) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO PROJET VALUES(?,?,?,?,?,?,?,?,?,?)', [projet.id, projet.nom, projet.lieu, projet.public, projet.pays, projet.utilisateurs, projet.rowid, projet.logo, projet.mention, projet.note])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    //this.presentToast('table PROJET donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllProjets = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PROJET ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, rowid: res.rows.item(i).rowid, logo: res.rows.item(i).logo, note: res.rows.item(i).note, mention: res.rows.item(i).mention, nom: res.rows.item(i).nom, lieu: res.rows.item(i).lieu, public: res.rows.item(i).public, pays: res.rows.item(i).pays, utilisateurs: res.rows.item(i).utilisateurs });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllProjets = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM PROJET', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createUtilisateur = function (utilisateur) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO UTILISATEURS VALUES(NULL,?,?,?,?,?,?,?,?,?)', [utilisateur.username, utilisateur.email, utilisateur.password, utilisateur.ville, utilisateur.telephone, utilisateur.pays, utilisateur.fonctions, utilisateur.projet, utilisateur.monuserid])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    // this.presentToast('table UTILISATEURS donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                // this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllUtilisateur = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM UTILISATEURS ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, username: res.rows.item(i).username, email: res.rows.item(i).username, password: res.rows.item(i).password, ville: res.rows.item(i).password, telephone: res.rows.item(i).telephone, pays: res.rows.item(i).pays, fonctions: res.rows.item(i).fonctions, projet: res.rows.item(i).projet, monuserid: res.rows.item(i).monuserid });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllUtilisateur = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM UTILISATEURS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createObservation = function (observation) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO OBSERVATIONS VALUES(NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [observation.dateo, observation.coordX, observation.coordY, observation.img1File, observation.img2File, observation.img3File, observation.img4File, observation.note, observation.etat, observation.typeObservations, observation.utilisateur, observation.projet, observation.groupe, observation.sousgroupe, observation.espece, observation.madate])
                    .then(function (res) {
                    var result = res["insertId"];
                    resolve(result);
                })
                    .catch(function (e) {
                    //this.presentToast('table OBSERVATIONS donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(0);
            });
        });
    };
    ServicesProvider.prototype.getAllObservations = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM OBSERVATIONS ORDER BY id DESC', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, dateo: res.rows.item(i).dateo, coordX: res.rows.item(i).coordX, coordY: res.rows.item(i).coordY, img1File: res.rows.item(i).img1File, img2File: res.rows.item(i).img2File, img3File: res.rows.item(i).img3File, img4File: res.rows.item(i).img4File, note: res.rows.item(i).note, etat: res.rows.item(i).etat, typeObservations: res.rows.item(i).typeObservations, utilisateur: res.rows.item(i).utilisateur, projet: res.rows.item(i).projet, groupe: res.rows.item(i).groupe, sousgroupe: res.rows.item(i).sousgroupe, espece: res.rows.item(i).espece, date: res.rows.item(i).madate });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                ///this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllObservations = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM OBSERVATIONS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteObservationByID = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM OBSERVATIONS WHERE id = ?', [id])
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllObservationByEtat = function (etat) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM OBSERVATIONS WHERE etat = ?', [etat])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, dateo: res.rows.item(i).dateo, coordX: res.rows.item(i).coordX, coordY: res.rows.item(i).coordY, img1File: res.rows.item(i).img1File, img2File: res.rows.item(i).img2File, img3File: res.rows.item(i).img3File, img4File: res.rows.item(i).img4File, note: res.rows.item(i).note, etat: res.rows.item(i).etat, typeObservations: res.rows.item(i).typeObservations, utilisateur: res.rows.item(i).utilisateur, projet: res.rows.item(i).projet, groupe: res.rows.item(i).groupe, sousgroupe: res.rows.item(i).sousgroupe, espece: res.rows.item(i).espece, madate: res.rows.item(i).madate });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.createResultat = function (resultat) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('INSERT INTO RESULTATS VALUES(?,?,?,?,?)', [resultat.id, resultat.contenu, resultat.questions, resultat.observations, resultat.etat])
                    .then(function (res) {
                    //alert("INSERTED error " + JSON.stringify(res));
                    var result = res["insertId"];
                    resolve(result);
                })
                    .catch(function (e) {
                    //this.presentToast('table RESULTATS donst exist!');
                    resolve(e);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
                resolve(e);
            });
        });
    };
    ServicesProvider.prototype.getAllReultats = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM RESULTATS ', {})
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, contenu: res.rows.item(i).contenu, questions: res.rows.item(i).questions, observations: res.rows.item(i).observations, etat: res.rows.item(i).etat });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.getAllReultatsByObservationId = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM RESULTATS WHERE observations = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, contenu: res.rows.item(i).contenu, questions: res.rows.item(i).questions, observations: res.rows.item(i).observations, etat: res.rows.item(i).etat });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.deletteAllReultats = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('DELETE FROM RESULTATS', {})
                    .then(function (res) {
                    resolve(1);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.findresultatByObservationId = function (observations) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM RESULTATS WHERE observations = ?', [observations])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, contenu: res.rows.item(i).contenu, question: res.rows.item(i).questions, observation: res.rows.item(i).observations, etat: res.rows.item(i).etat });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database  RESULTATS donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database RESULTATS donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.findUtilisateursByLoginAndPassword = function (utilisateur) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM UTILISATEURS WHERE (email = ? or telephone = ?) and password = ?', [utilisateur.login, utilisateur.login, utilisateur.password])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, username: res.rows.item(i).username, email: res.rows.item(i).email, password: res.rows.item(i).password, ville: res.rows.item(i).ville, telephone: res.rows.item(i).telephone, pays: res.rows.item(i).pays, fonctions: res.rows.item(i).fonctions, projet: res.rows.item(i).projet, monuserid: res.rows.item(i).monuserid });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database donst exist!');
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database donst exist!');
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getAllEspecesBySousGroupeId = function (sous_groupeId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM ESPECES WHERE sous_groupes = ?', [sous_groupeId])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en, sous_groupes: res.rows.item(i).sous_groupes, questions_animal: res.rows.item(i).questions_animal, questions_menaces: res.rows.item(i).questions_menaces, questions_signe: res.rows.item(i).questions_signe, questions_alimentation: res.rows.item(i).questions_alimentation });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database  ESPECES donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database ESPECES donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getAllSousGroupesBygroupeId = function (groupeId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM SOUS_GROUPES WHERE groupes = ?', [groupeId])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en, image: res.rows.item(i).image, description_fr: res.rows.item(i).description_fr, description_en: res.rows.item(i).description_en, groupes: res.rows.item(i).groupes });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database SOUS_GROUPES donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database SOUS_GROUPES donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getQuestionById = function (questionId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM QUESTIONS WHERE id = ?', [questionId])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, titre_fr: res.rows.item(i).titre_fr, titre_en: res.rows.item(i).titre_en, type_reponse: res.rows.item(i).type_reponse, interval: res.rows.item(i).interval, type_debut: res.rows.item(i).type_debut, image: res.rows.item(i).image, isPicture: res.rows.item(i).isPicture, questions: res.rows.item(i).questions });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    // this.presentToast('database QUESTIONS donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database QUESTIONS donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getReponseByQuestionId = function (questionId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM REPONSES WHERE questions = ?', [questionId])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, titre_fr: res.rows.item(i).titre_fr, titre_en: res.rows.item(i).titre_en, questions: res.rows.item(i).questions, questions_next: res.rows.item(i).questions_next, image: res.rows.item(i).image, taille: res.rows.item(i).taille });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database QUESTIONS donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database QUESTIONS donst exist!');
                resolve(0);
            });
        });
    };
    ServicesProvider.prototype.getAllObservationByUserIdAndProjet = function (utilisateurId, projetId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM OBSERVATIONS WHERE utilisateur = ? and projet = ?', [utilisateurId, projetId])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, dateo: res.rows.item(i).dateo, coordX: res.rows.item(i).coordX, coordY: res.rows.item(i).coordY, img1File: res.rows.item(i).img1File, img2File: res.rows.item(i).img2File, img3File: res.rows.item(i).img3File, img4File: res.rows.item(i).img4File, note: res.rows.item(i).note, etat: res.rows.item(i).etat, typeObservations: res.rows.item(i).typeObservations, utilisateur: res.rows.item(i).utilisateur, projet: res.rows.item(i).projet, groupe: res.rows.item(i).groupe, sousgroupe: res.rows.item(i).sousgroupe, espece: res.rows.item(i).espece });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    alert("big big error");
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database OBSERVATIONS donst exist!');
            });
        });
    };
    ////////////////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getAllPaysById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PAYS WHERE id = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom: res.rows.item(i).nom, lieu: res.rows.item(i).lieu, public: res.rows.item(i).public, pays: res.rows.item(i).pays, utilisateurs: res.rows.item(i).utilisateurs });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database  PAYS donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database PAYS donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getAllFonctionById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM FONCTIONS WHERE id = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database  FONCTIONS donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database FONCTIONS donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getAllProjetsByUserId = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PROJET WHERE utilisateurs = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, nom_fr: res.rows.item(i).nom_fr, nom_en: res.rows.item(i).nom_en });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database  PROJET donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database PROJET donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getObervationByProjetId = function (userId, projetId) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM OBSERVATIONS WHERE utilisateur = ?', [userId])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, dateo: res.rows.item(i).dateo, coordX: res.rows.item(i).coordX, coordY: res.rows.item(i).coordY, img1File: res.rows.item(i).img1File, img2File: res.rows.item(i).img2File, img3File: res.rows.item(i).img3File, img4File: res.rows.item(i).img4File, note: res.rows.item(i).note, etat: res.rows.item(i).etat, typeObservations: res.rows.item(i).typeObservations, utilisateur: res.rows.item(i).utilisateur, projet: res.rows.item(i).projet, groupe: res.rows.item(i).groupe, sousgroupe: res.rows.item(i).sousgroupe, espece: res.rows.item(i).espece, date: res.rows.item(i).madate });
                    }
                    if (expenses.length != 0) {
                        resolve(expenses);
                    }
                    else {
                        resolve(0);
                    }
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database  OBSERVATIONS donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database OBSERVATIONS donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.updateObservation = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('UPDATE OBSERVATIONS SET etat=?  WHERE id = ?', [1, id])
                    .then(function (res) {
                    resolve(res);
                })
                    .catch(function (e) {
                    console.log(e);
                    //this.presentToast('database  PROJET donst exist!');
                    resolve(0);
                });
            }).catch(function (e) {
                console.log(e);
                //this.presentToast('database PROJET donst exist!');
                resolve(0);
            });
        });
    };
    ///////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////
    ServicesProvider.prototype.getAllProjetById = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.sqlite.create({
                name: 'sirendb.db',
                location: 'default'
            }).then(function (db) {
                db.executeSql('SELECT * FROM PROJET WHERE rowid = ?', [id])
                    .then(function (res) {
                    var expenses = [];
                    for (var i = 0; i < res.rows.length; i++) {
                        expenses.push({ id: res.rows.item(i).id, rowid: res.rows.item(i).rowid, nom: res.rows.item(i).nom, lieu: res.rows.item(i).lieu, public: res.rows.item(i).public, pays: res.rows.item(i).pays, utilisateurs: res.rows.item(i).utilisateurs });
                    }
                    resolve(expenses);
                })
                    .catch(function (e) {
                    console.log(e);
                    resolve(0);
                });
            }).catch(function (e) {
                //this.presentToast('database donst exist!');
            });
        });
    };
    ServicesProvider.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    };
    ServicesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Http */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_sqlite__["a" /* SQLite */]])
    ], ServicesProvider);
    return ServicesProvider;
}());

//# sourceMappingURL=services.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguageService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_translate_src_translate_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__configs_configs__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the LanguageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LanguageService = /** @class */ (function () {
    function LanguageService(storage, translate) {
        this.storage = storage;
        this.translate = translate;
        this.key = "Language";
        console.log('Hello LanguageProvider Provider');
    }
    LanguageService.prototype.storeLanguage = function (lan) {
        var _this = this;
        return new Promise(function (resolve) {
            try {
                _this.storage.set(_this.key, lan).then(function (val) {
                    _this.translate.setDefaultLang(lan);
                    __WEBPACK_IMPORTED_MODULE_3__configs_configs__["a" /* LANG */].value = lan;
                    resolve(val);
                }, function (error) {
                    _this.translate.setDefaultLang('fr');
                    __WEBPACK_IMPORTED_MODULE_3__configs_configs__["a" /* LANG */].value = 'fr';
                    resolve('fr');
                });
            }
            catch (error) {
                _this.translate.setDefaultLang('fr');
                __WEBPACK_IMPORTED_MODULE_3__configs_configs__["a" /* LANG */].value = 'fr';
                resolve('fr');
            }
        });
    };
    LanguageService.prototype.loadLanguage = function () {
        var _this = this;
        return new Promise(function (resolve) {
            try {
                _this.storage.get(_this.key).then(function (lan) {
                    if (lan == null || typeof lan !== 'string') {
                        lan = 'fr';
                    }
                    __WEBPACK_IMPORTED_MODULE_3__configs_configs__["a" /* LANG */].value = lan;
                    resolve(lan);
                }).catch(function (error) {
                    __WEBPACK_IMPORTED_MODULE_3__configs_configs__["a" /* LANG */].value = 'fr';
                    resolve('fr');
                });
            }
            catch (error) {
                __WEBPACK_IMPORTED_MODULE_3__configs_configs__["a" /* LANG */].value = 'fr';
                resolve('fr');
            }
        });
    };
    LanguageService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2_ng2_translate_src_translate_service__["b" /* TranslateService */]])
    ], LanguageService);
    return LanguageService;
}());

//# sourceMappingURL=language.js.map

/***/ }),

/***/ 128:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 128;

/***/ }),

/***/ 169:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/acceuil/acceuil.module": [
		325,
		20
	],
	"../pages/carte/carte.module": [
		311,
		19
	],
	"../pages/changeprojet/changeprojet.module": [
		312,
		18
	],
	"../pages/commentaire/commentaire.module": [
		313,
		17
	],
	"../pages/connexion/connexion.module": [
		331,
		21
	],
	"../pages/description/description.module": [
		328,
		16
	],
	"../pages/espece/espece.module": [
		314,
		15
	],
	"../pages/groupe/groupe.module": [
		315,
		14
	],
	"../pages/inscription/inscription.module": [
		316,
		13
	],
	"../pages/langue/langue.module": [
		317,
		12
	],
	"../pages/mentionlegal/mentionlegal.module": [
		318,
		11
	],
	"../pages/mesnotifications/mesnotifications.module": [
		319,
		10
	],
	"../pages/mesobservations/mesobservations.module": [
		320,
		0
	],
	"../pages/mesprojet/mesprojet.module": [
		321,
		9
	],
	"../pages/nombrepetit/nombrepetit.module": [
		326,
		8
	],
	"../pages/ouestanimal/ouestanimal.module": [
		332,
		7
	],
	"../pages/photos/photos.module": [
		329,
		1
	],
	"../pages/profils/profils.module": [
		324,
		6
	],
	"../pages/sirin/sirin.module": [
		327,
		5
	],
	"../pages/siteweb/siteweb.module": [
		322,
		4
	],
	"../pages/sous-espece/sous-espece.module": [
		323,
		3
	],
	"../pages/synchronisation/synchronisation.module": [
		330,
		2
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 169;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(236);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export translateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_network__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_services_language__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__angular_http__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_sqlite__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_connectivity_connectivity__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_image_picker_ngx__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













//translate





function translateLoader(http) { return new __WEBPACK_IMPORTED_MODULE_13_ng2_translate__["d" /* TranslateStaticLoader */](http, './assets/i18n', '.json'); }
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/carte/carte.module#CartePageModule', name: 'CartePage', segment: 'carte', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/changeprojet/changeprojet.module#ChangeprojetPageModule', name: 'ChangeprojetPage', segment: 'changeprojet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/commentaire/commentaire.module#CommentairePageModule', name: 'CommentairePage', segment: 'commentaire', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/espece/espece.module#EspecePageModule', name: 'EspecePage', segment: 'espece', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/groupe/groupe.module#GroupePageModule', name: 'GroupePage', segment: 'groupe', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inscription/inscription.module#InscriptionPageModule', name: 'InscriptionPage', segment: 'inscription', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/langue/langue.module#LanguePageModule', name: 'LanguePage', segment: 'langue', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mentionlegal/mentionlegal.module#MentionlegalPageModule', name: 'MentionlegalPage', segment: 'mentionlegal', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mesnotifications/mesnotifications.module#MesnotificationsPageModule', name: 'MesnotificationsPage', segment: 'mesnotifications', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mesobservations/mesobservations.module#MesobservationsPageModule', name: 'MesobservationsPage', segment: 'mesobservations', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mesprojet/mesprojet.module#MesprojetPageModule', name: 'MesprojetPage', segment: 'mesprojet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/siteweb/siteweb.module#SitewebPageModule', name: 'SitewebPage', segment: 'siteweb', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sous-espece/sous-espece.module#SousEspecePageModule', name: 'SousEspecePage', segment: 'sous-espece', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profils/profils.module#ProfilsPageModule', name: 'ProfilsPage', segment: 'profils', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/acceuil/acceuil.module#AcceuilPageModule', name: 'AcceuilPage', segment: 'acceuil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/nombrepetit/nombrepetit.module#NombrepetitPageModule', name: 'NombrepetitPage', segment: 'nombrepetit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sirin/sirin.module#SirinPageModule', name: 'SirinPage', segment: 'sirin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/description/description.module#DescriptionPageModule', name: 'DescriptionPage', segment: 'description', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/photos/photos.module#PhotosPageModule', name: 'PhotosPage', segment: 'photos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/synchronisation/synchronisation.module#SynchronisationPageModule', name: 'SynchronisationPage', segment: 'synchronisation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/connexion/connexion.module#ConnexionPageModule', name: 'ConnexionPage', segment: 'connexion', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/ouestanimal/ouestanimal.module#OuestanimalPageModule', name: 'OuestanimalPage', segment: 'ouestanimal', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_10__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_13_ng2_translate__["b" /* TranslateModule */].forRoot({
                    provide: __WEBPACK_IMPORTED_MODULE_13_ng2_translate__["a" /* TranslateLoader */],
                    useFactory: translateLoader,
                    deps: [__WEBPACK_IMPORTED_MODULE_14__angular_http__["a" /* Http */]]
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_sqlite__["a" /* SQLite */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_7__providers_services_services__["a" /* ServicesProvider */],
                __WEBPACK_IMPORTED_MODULE_11__providers_services_language__["a" /* LanguageService */],
                __WEBPACK_IMPORTED_MODULE_9__providers_localstorage_localstorage__["a" /* LocalStorageProvider */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_image_picker_ngx__["a" /* ImagePicker */],
                __WEBPACK_IMPORTED_MODULE_16__providers_connectivity_connectivity__["a" /* ConnectivityProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 291:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return currentHost; });
var online = "https://siren.ammco.org/web/fr/api/";
var currentHost = online;

//# sourceMappingURL=host.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate_src_translate_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_services_language__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_sqlite__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__ = __webpack_require__(112);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__configs_configs__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MyApp = /** @class */ (function () {
    function MyApp(platform, events, geolocation, services, network, sqlite, alertCtrl, translate, language, mylocalstorage, statusBar, splashScreen) {
        var _this = this;
        this.platform = platform;
        this.events = events;
        this.geolocation = geolocation;
        this.services = services;
        this.network = network;
        this.sqlite = sqlite;
        this.alertCtrl = alertCtrl;
        this.translate = translate;
        this.language = language;
        this.mylocalstorage = mylocalstorage;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.testimage = 0;
        this.testcolor = 0;
        this.myresult = [];
        this.myTitle = [];
        this.langue = 0;
        this.AJOUTEROBSERVATION = 0;
        this.PROFIL = 0;
        this.LANGUE = 0;
        this.MESPROJETS = 0;
        this.CHANGEPROJET = 0;
        this.MESOBSERVATION = 0;
        this.MESNOTIFICATION = 0;
        this.COMMANTAIRE = 0;
        this.ALLEZSURCARTE = 0;
        this.ALLEZSURSITE = 0;
        this.SYNCRONISATION = 0;
        this.initializeApp();
        events.subscribe('user:created', function (user, time) {
            _this.services.getAllProjetMenu().then(function (myresult) {
                myresult.forEach(function (element) {
                    _this.myresult.push(element.title);
                    if (_this.langue == 0) {
                        _this.myTitle.push(element.nomFr);
                    }
                    else {
                        _this.myTitle.push(element.nomEn);
                    }
                });
            });
        });
        //  if(window["cordova"]){
        //    alert("le mokai");
        //  } else {
        //    alert("le mokai pas de cordova");
        //  }
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'AJOUTEROBSERVATION', component: 'AcceuilPage', icon: 'home' },
            { title: 'PROFIL', component: 'ProfilsPage', icon: 'person' },
            { title: 'LANGUE', component: 'LanguePage', icon: 'chatboxes' },
            { title: 'MESPROJETS', component: 'MesprojetPage', icon: 'albums' },
            { title: 'CHANGEPROJET', component: 'ChangeprojetPage', icon: 'transgender' },
            { title: 'MESOBSERVATION', component: 'MesobservationsPage', icon: 'document' },
            { title: 'MESNOTIFICATION', component: 'MesnotificationsPage', icon: 'notifications' },
            { title: 'COMMANTAIRE', component: 'CommentairePage', icon: 'book' },
            { title: 'ALLEZSURCARTE', component: 'CartePage', icon: 'globe' },
            { title: 'ALLEZSURSITE', component: 'SitewebPage', icon: 'globe' },
            { title: 'MENTIOLEGALE', component: 'MentionlegalPage', icon: 'globe' },
            { title: 'SYNCRONISATION', component: 'SynchronisationPage', icon: 'git-network' },
            {
                title: 'DECONNEXION',
                component: 'ConnexionPage',
                count: 0,
                icon: 'log-out',
                confirmAction: function () {
                    _this.translate.get(['Cancel', 'Confirm Logout', 'OK']).subscribe(function (langs) {
                        _this.alertCtrl.create({
                            message: langs['Confirm Logout'],
                            buttons: [{
                                    text: langs['Cancel'],
                                    handler: function (data) {
                                    }
                                }, {
                                    text: langs['OK'],
                                    handler: function (data) {
                                        _this.mylocalstorage.logut();
                                        _this.nav.setRoot('ConnexionPage');
                                        _this.rootPage = 'ConnexionPage';
                                    }
                                }
                            ]
                        }).present();
                    });
                }
            },
        ];
        this.mylocalstorage.getSession().then(function (result) {
            _this.userconnecte = result;
            if (_this.userconnecte != null) {
                console.log(_this.userconnecte);
                _this.email = _this.userconnecte.email;
            }
        });
        this.language.loadLanguage().then(function (lan) {
            _this.translate.setDefaultLang(lan);
            _this.mylocalstorage.getSession().then(function (result) {
                if (result == null) {
                    //this.rootPage = 'PhotosPage';
                    _this.rootPage = 'ConnexionPage';
                    //this.rootPage = 'SynchronisationPage';
                }
                else {
                    _this.rootPage = 'AcceuilPage';
                    //this.rootPage = 'InscriptionPage';
                }
            });
        });
    }
    MyApp.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.langue = 1;
            }
            else {
                _this.langue = 0;
            }
        });
        this.mylocalstorage.getSession().then(function (result) {
            _this.userconnecte = result;
            if (_this.userconnecte != null) {
                console.log(_this.userconnecte);
                console.log("===");
                if ((!undefined == _this.userconnecte.projet) || (_this.userconnecte.projet != null)) {
                    _this.updateColor(_this.userconnecte.projet.couleur);
                    /////////////////////////////// CONNECTED MODE ////////////////////////////
                    /*this.services.currentMenusListner.subscribe(next =>{
                      next.forEach(element => {
                        this.myresult.push(element.title);
                        if(this.langue == 0){
                          this.myTitle.push(element.nomFr);
                        }else{
                          this.myTitle.push(element.nomEn);
                        }
                      });
                    })*/
                    if (navigator.onLine) {
                        _this.services.getprojet_typeObservation(_this.userconnecte.projet.id).subscribe(function (myresult) {
                            console.log("==============================");
                            console.log(myresult);
                            console.log(_this.userconnecte.projet);
                            console.log(_this.userconnecte.projet.id);
                            myresult.forEach(function (element) {
                                _this.myresult.push(element.title);
                                if (_this.langue == 0) {
                                    _this.myTitle.push(element.nomFr);
                                }
                                else {
                                    _this.myTitle.push(element.nomEn);
                                }
                            });
                            console.log("==============================");
                        }, function (error) {
                        }, function () {
                        });
                        /*this.services.getAllProjetMenu().then((myresult:any) =>{
        
                          myresult.forEach(element => {
                            this.myresult.push(element.title);
                            if(this.langue == 0){
                              this.myTitle.push(element.nomFr);
                            }else{
                              this.myTitle.push(element.nomEn);
                            }
                          });
                        })*/
                    }
                    else {
                        _this.services.getAllProjetMenu().then(function (myresult) {
                            myresult.forEach(function (element) {
                                _this.myresult.push(element.title);
                                if (_this.langue == 0) {
                                    _this.myTitle.push(element.nomFr);
                                }
                                else {
                                    _this.myTitle.push(element.nomEn);
                                }
                            });
                        });
                    }
                    //////////////////////////////////////////////////////////////////
                    if ((!undefined == _this.userconnecte.projet.couleur) || (_this.userconnecte.projet.couleur != null)) {
                        _this.testcolor = 1;
                    }
                    if ((!undefined == _this.userconnecte.projet.image) || (_this.userconnecte.projet.image != null)) {
                        _this.testimage = 1;
                    }
                }
                _this.email = _this.userconnecte.email;
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_11__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_11__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.intiliazedatabase();
        setInterval(function () {
            if (navigator.onLine) {
                _this.mylocalstorage.getSession().then(function (result) {
                    _this.userconnecte = result;
                    _this.geolocation.getCurrentPosition().then(function (resp) {
                        var utilisateurId = _this.userconnecte["id"];
                        var projetId = _this.userconnecte["projet"].id;
                        _this.services.savepositions(resp.coords.latitude, resp.coords.longitude, utilisateurId, projetId).subscribe(function (result) {
                            __WEBPACK_IMPORTED_MODULE_11__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
                            __WEBPACK_IMPORTED_MODULE_11__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
                        }, function (error) {
                        }, function () {
                        });
                    }).catch(function (error) {
                        console.log("error could not get position");
                    });
                });
            }
        }, 30000);
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.confirmAction == null) {
            this.nav.setRoot(page.component);
        }
        else {
            page.confirmAction();
        }
    };
    MyApp.prototype.isAutorise = function (title) {
        return this.myresult.indexOf(title) > -1;
    };
    MyApp.prototype.returnTitle = function (title) {
        var index = this.myresult.indexOf(title);
        return this.myTitle[index];
    };
    MyApp.prototype.intiliazedatabase = function () {
        this.sqlite.create({
            name: 'sirendb.db',
            location: 'default'
        }).then(function (db) {
            db.executeSql('CREATE TABLE IF NOT EXISTS PAYS (id integer primary key, nom_fr, nom_en, code_tel, sigle)', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS CHECKCONNEXION (id integer primary key, valeur)', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS FONCTIONS (id integer primary key, nom_fr, nom_en)', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS INSCRIPTION (id integer primary key, username, email, telephone, password, ville, pays integer, fonction integer, monuserid integer , FOREIGN KEY(pays) REFERENCES PAYS(id), FOREIGN KEY(fonction) REFERENCES PAYS(FONCTIONS))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS GROUPES (id integer primary key, nom_fr, nom_en, image, description_fr, description_en)', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS SOUS_GROUPES (id integer primary key, nom_fr, nom_en, image, description_fr, description_en, groupes integer, FOREIGN KEY(groupes) REFERENCES GROUPES(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS QUESTIONS (id integer primary key, titre_fr, titre_en, type_reponse, interval, type_debut, image, isPicture, questions)', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS REPONSES (id integer primary key, titre_fr, titre_en, questions integer, questions_next integer, image, taille, FOREIGN KEY(questions) REFERENCES QUESTIONS(id), FOREIGN KEY(questions_next) REFERENCES QUESTIONS(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS TYPE_OBSERVATIONS (id integer primary key, nom_fr, nom_en, image, type_question, questions integer, FOREIGN KEY(questions) REFERENCES QUESTIONS(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS ESPECES (id integer primary key, nom_fr, nom_en, image, description_fr, description_en, sous_groupes integer, questions_animal, questions_menaces, questions_signe, questions_alimentation, FOREIGN KEY(sous_groupes) REFERENCES SOUS_GROUPES(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS PROJET (id integer primary key, nom, lieu, public, pays integer, utilisateurs integer,rowid, logo,mention, note, FOREIGN KEY(pays) REFERENCES PAYS(id), FOREIGN KEY(utilisateurs) REFERENCES UTILISATEURS(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS PROJET_TYPE_OBSERVATIONS (id integer primary key, nom_fr, nom_en, image, type_question, questions integer,projet integer, FOREIGN KEY(questions) REFERENCES QUESTIONS(id), FOREIGN KEY(projet) REFERENCES PROJET(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS UTILISATEURS (id integer primary key, username, email, password, ville, telephone, pays integer, fonctions integer, projet integer, monuserid integer, FOREIGN KEY(pays) REFERENCES PAYS(id), FOREIGN KEY(fonctions) REFERENCES FONCTIONS(id), FOREIGN KEY(projet) REFERENCES PROJET(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS OBSERVATIONS (id integer primary key,  dateo , coordX, coordY,  img1File, img2File, img3File, img4File, note, etat, typeObservations integer, utilisateur integer, projet integer, groupe integer, sousgroupe integer, espece integer, madate, FOREIGN KEY(typeObservations) REFERENCES TYPE_OBSERVATIONS(id), FOREIGN KEY(espece) REFERENCES ESPECES(id), FOREIGN KEY(sousgroupe) REFERENCES SOUS_GROUPES(id), FOREIGN KEY(groupe) REFERENCES GROUPES(id), FOREIGN KEY(utilisateur) REFERENCES UTILISATEURS(id), FOREIGN KEY(projet) REFERENCES PROJET(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS RESULTATS (id integer primary key, contenu, questions integer, observations integer, etat, FOREIGN KEY(questions) REFERENCES QUESTIONS(id), FOREIGN KEY(observations) REFERENCES OBSERVATIONS(id))', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
            db.executeSql('CREATE TABLE IF NOT EXISTS PROJETMENU (id integer primary key, nomFr, nomEn, title)', {})
                .then(function (res) { return console.log('Executed SQL'); })
                .catch(function (e) { return console.log(e); });
        }).catch(function (e) { return console.log(e); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header >\n    <ion-toolbar>\n      <ion-title>Siren</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n\n    <div class="profil" *ngIf="testimage == 0">\n      <img class="image" src="assets/imgs/logo.png">\n        <p class="texte">{{email}}</p>\n    </div>\n\n    <div class="profil" *ngIf="testimage == 1">\n        <img class="image2" src="{{userconnecte.projet.image}}">\n          <p class="texte">{{email}}</p>\n    </div>\n\n   \n    \n    <div  *ngFor="let p of pages">\n\n        <button *ngIf="isAutorise(p.title) || p.title == \'SYNCRONISATION\' || p.title == \'DECONNEXION\'" class="lebutton" menuClose ion-item  (click)="openPage(p)">\n          <!-- <ion-icon  name="{{p.icon}}"> {{p.title | translate}} </ion-icon> -->\n          <ion-icon  name="{{p.icon}}"> {{returnTitle(p.title)}} </ion-icon>\n        </button>\n\n    </div>  \n     \n      \n    \n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/app/app.html"*/,
            queries: {
                nav: new __WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */]('content')
            }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_9__providers_services_services__["a" /* ServicesProvider */], __WEBPACK_IMPORTED_MODULE_8__ionic_native_network__["a" /* Network */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_sqlite__["a" /* SQLite */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5_ng2_translate_src_translate_service__["b" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_6__providers_services_language__["a" /* LanguageService */], __WEBPACK_IMPORTED_MODULE_4__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectivityProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(89);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(34);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ConnectivityProvider = /** @class */ (function () {
    function ConnectivityProvider(http, platform) {
        this.http = http;
        this.platform = platform;
        this.onDevice = this.platform.is('cordova');
    }
    ConnectivityProvider.prototype.isOnline = function () {
        return navigator.onLine;
    };
    ConnectivityProvider.prototype.isOffline = function () {
        return !navigator.onLine;
    };
    ConnectivityProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Platform */]])
    ], ConnectivityProvider);
    return ConnectivityProvider;
}());

//# sourceMappingURL=connectivity.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return Session; });
/* unused harmony export Languages */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LANG; });
/* unused harmony export formatNumberOfDate */
/* unused harmony export showDateAndTime_2 */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Parcourt; });
var Session = {
    utilisateur: null,
    toke: null
};
var Languages = [
    { value: "fr", name: "francais" },
    { value: "en", name: "anglais" }
];
var LANG = {
    value: "fr"
};
function formatNumberOfDate(val) {
    val = Number(val);
    return (val < 10) ? "0" + val : val;
}
function showDateAndTime_2(datetime_mysql, lang) {
    if (lang === void 0) { lang = 'fr'; }
    var d = new Date(datetime_mysql), year = d.getFullYear(), month = formatNumberOfDate("" + (d.getMonth() + 1)), day = formatNumberOfDate("" + d.getDate()), hours = formatNumberOfDate("" + d.getHours()), minutes = formatNumberOfDate("" + d.getMinutes()), seconds = formatNumberOfDate("" + d.getSeconds());
    if (lang === 'fr') {
        return day + "/" + month + "/" + year + " \u00E0 " + hours + ":" + minutes + ":" + seconds;
    }
    else if (lang === 'en') {
        return year + "-" + month + "-" + day + " at " + hours + ":" + minutes + ":" + seconds;
    }
    else {
        return year + "-" + month + "-" + day + " at " + hours + ":" + minutes + ":" + seconds;
    }
}
var Parcourt = {
    typeobservation: null,
    indicateur: null,
    latitude: null,
    longitude: null,
    groupe: null,
    sousgroupe: null,
    espece: null,
    listesReponses: [],
    observation: null,
    description: null,
    images: null,
    couleur: null
};
//# sourceMappingURL=configs.js.map

/***/ })

},[218]);
//# sourceMappingURL=main.js.map