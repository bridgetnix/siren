webpackJsonp([1],{

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotosPageModule", function() { return PhotosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__photos__ = __webpack_require__(486);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_navbar_navbar_module__ = __webpack_require__(334);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_base64__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_image_resizer__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_image_picker_ngx__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var PhotosPageModule = /** @class */ (function () {
    function PhotosPageModule() {
    }
    PhotosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__photos__["a" /* PhotosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_4_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__photos__["a" /* PhotosPage */]),
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_base64__["a" /* Base64 */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_image_resizer__["a" /* ImageResizer */],
                __WEBPACK_IMPORTED_MODULE_8__ionic_native_image_picker_ngx__["a" /* ImagePicker */]
            ]
        })
    ], PhotosPageModule);
    return PhotosPageModule;
}());

//# sourceMappingURL=photos.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export DestinationType */
/* unused harmony export EncodingType */
/* unused harmony export MediaType */
/* unused harmony export PictureSourceType */
/* unused harmony export PopoverArrowDirection */
/* unused harmony export Direction */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Camera; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(29);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DestinationType;
(function (DestinationType) {
    DestinationType[DestinationType["DATA_URL"] = 0] = "DATA_URL";
    DestinationType[DestinationType["FILE_URL"] = 1] = "FILE_URL";
    DestinationType[DestinationType["NATIVE_URI"] = 2] = "NATIVE_URI";
})(DestinationType || (DestinationType = {}));
var EncodingType;
(function (EncodingType) {
    EncodingType[EncodingType["JPEG"] = 0] = "JPEG";
    EncodingType[EncodingType["PNG"] = 1] = "PNG";
})(EncodingType || (EncodingType = {}));
var MediaType;
(function (MediaType) {
    MediaType[MediaType["PICTURE"] = 0] = "PICTURE";
    MediaType[MediaType["VIDEO"] = 1] = "VIDEO";
    MediaType[MediaType["ALLMEDIA"] = 2] = "ALLMEDIA";
})(MediaType || (MediaType = {}));
var PictureSourceType;
(function (PictureSourceType) {
    PictureSourceType[PictureSourceType["PHOTOLIBRARY"] = 0] = "PHOTOLIBRARY";
    PictureSourceType[PictureSourceType["CAMERA"] = 1] = "CAMERA";
    PictureSourceType[PictureSourceType["SAVEDPHOTOALBUM"] = 2] = "SAVEDPHOTOALBUM";
})(PictureSourceType || (PictureSourceType = {}));
var PopoverArrowDirection;
(function (PopoverArrowDirection) {
    PopoverArrowDirection[PopoverArrowDirection["ARROW_UP"] = 1] = "ARROW_UP";
    PopoverArrowDirection[PopoverArrowDirection["ARROW_DOWN"] = 2] = "ARROW_DOWN";
    PopoverArrowDirection[PopoverArrowDirection["ARROW_LEFT"] = 3] = "ARROW_LEFT";
    PopoverArrowDirection[PopoverArrowDirection["ARROW_RIGHT"] = 4] = "ARROW_RIGHT";
    PopoverArrowDirection[PopoverArrowDirection["ARROW_ANY"] = 5] = "ARROW_ANY";
})(PopoverArrowDirection || (PopoverArrowDirection = {}));
var Direction;
(function (Direction) {
    Direction[Direction["BACK"] = 0] = "BACK";
    Direction[Direction["FRONT"] = 1] = "FRONT";
})(Direction || (Direction = {}));
/**
 * @name Camera
 * @description
 * Take a photo or capture video.
 *
 * Requires the Cordova plugin: `cordova-plugin-camera`. For more info, please see the [Cordova Camera Plugin Docs](https://github.com/apache/cordova-plugin-camera).
 *
 * @usage
 * ```typescript
 * import { Camera, CameraOptions } from '@ionic-native/camera';
 *
 * constructor(private camera: Camera) { }
 *
 * ...
 *
 *
 * const options: CameraOptions = {
 *   quality: 100,
 *   destinationType: this.camera.DestinationType.FILE_URI,
 *   encodingType: this.camera.EncodingType.JPEG,
 *   mediaType: this.camera.MediaType.PICTURE
 * }
 *
 * this.camera.getPicture(options).then((imageData) => {
 *  // imageData is either a base64 encoded string or a file URI
 *  // If it's base64 (DATA_URL):
 *  let base64Image = 'data:image/jpeg;base64,' + imageData;
 * }, (err) => {
 *  // Handle error
 * });
 * ```
 * @interfaces
 * CameraOptions
 * CameraPopoverOptions
 */
var Camera = (function (_super) {
    __extends(Camera, _super);
    function Camera() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * Constant for possible destination types
         */
        _this.DestinationType = {
            /** Return base64 encoded string. DATA_URL can be very memory intensive and cause app crashes or out of memory errors. Use FILE_URI or NATIVE_URI if possible */
            DATA_URL: 0,
            /** Return file uri (content://media/external/images/media/2 for Android) */
            FILE_URI: 1,
            /** Return native uri (eg. asset-library://... for iOS) */
            NATIVE_URI: 2
        };
        /**
         * Convenience constant
         */
        _this.EncodingType = {
            /** Return JPEG encoded image */
            JPEG: 0,
            /** Return PNG encoded image */
            PNG: 1
        };
        /**
         * Convenience constant
         */
        _this.MediaType = {
            /** Allow selection of still pictures only. DEFAULT. Will return format specified via DestinationType */
            PICTURE: 0,
            /** Allow selection of video only, ONLY RETURNS URL */
            VIDEO: 1,
            /** Allow selection from all media types */
            ALLMEDIA: 2
        };
        /**
         * Convenience constant
         */
        _this.PictureSourceType = {
            /** Choose image from picture library (same as SAVEDPHOTOALBUM for Android) */
            PHOTOLIBRARY: 0,
            /** Take picture from camera */
            CAMERA: 1,
            /** Choose image from picture library (same as PHOTOLIBRARY for Android) */
            SAVEDPHOTOALBUM: 2
        };
        /**
         * Convenience constant
         */
        _this.PopoverArrowDirection = {
            ARROW_UP: 1,
            ARROW_DOWN: 2,
            ARROW_LEFT: 4,
            ARROW_RIGHT: 8,
            ARROW_ANY: 15
        };
        /**
         * Convenience constant
         */
        _this.Direction = {
            /** Use the back-facing camera */
            BACK: 0,
            /** Use the front-facing camera */
            FRONT: 1
        };
        return _this;
    }
    /**
     * Take a picture or video, or load one from the library.
     * @param {CameraOptions} [options] Options that you want to pass to the camera. Encoding type, quality, etc. Platform-specific quirks are described in the [Cordova plugin docs](https://github.com/apache/cordova-plugin-camera#cameraoptions-errata-).
     * @returns {Promise<any>} Returns a Promise that resolves with Base64 encoding of the image data, or the image file URI, depending on cameraOptions, otherwise rejects with an error.
     */
    Camera.prototype.getPicture = function (options) { return; };
    /**
     * Remove intermediate image files that are kept in temporary storage after calling camera.getPicture.
     * Applies only when the value of Camera.sourceType equals Camera.PictureSourceType.CAMERA and the Camera.destinationType equals Camera.DestinationType.FILE_URI.
     * @returns {Promise<any>}
     */
    Camera.prototype.cleanup = function () { return; };
    ;
    Camera.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */] },
    ];
    /** @nocollapse */
    Camera.ctorParameters = function () { return []; };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            callbackOrder: 'reverse'
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], Camera.prototype, "getPicture", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])({
            platforms: ['iOS']
        }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], Camera.prototype, "cleanup", null);
    Camera = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["g" /* Plugin */])({
            pluginName: 'Camera',
            plugin: 'cordova-plugin-camera',
            pluginRef: 'navigator.camera',
            repo: 'https://github.com/apache/cordova-plugin-camera',
            platforms: ['Android', 'Browser', 'iOS', 'Windows']
        })
    ], Camera);
    return Camera;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["f" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Base64; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(29);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @beta
 * @name Base64
 * @description
 * This Plugin is used to encode base64 of any file, it uses js code for iOS, but in case of android it uses native code to handle android versions lower than v.3
 *
 * @usage
 * ```typescript
 * import { Base64 } from '@ionic-native/base64';
 *
 * constructor(private base64: Base64) { }
 *
 * ...
 *
 * let filePath: string = 'file:///...';
 * this.base64.encodeFile(filePath).then((base64File: string) => {
 *   console.log(base64File);
 * }, (err) => {
 *   console.log(err);
 * });
 *
 * ```
 */
var Base64 = (function (_super) {
    __extends(Base64, _super);
    function Base64() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * This function encodes base64 of any file
     * @param {string} filePath Absolute file path
     * @return {Promise<string>} Returns a promise that resolves when the file is successfully encoded
     */
    Base64.prototype.encodeFile = function (filePath) { return; };
    Base64.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */] },
    ];
    /** @nocollapse */
    Base64.ctorParameters = function () { return []; };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", Promise)
    ], Base64.prototype, "encodeFile", null);
    Base64 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["g" /* Plugin */])({
            pluginName: 'Base64',
            plugin: 'com-badrit-base64',
            pluginRef: 'plugins.Base64',
            repo: 'https://github.com/hazemhagrass/phonegap-base64',
            platforms: ['Android', 'iOS']
        })
    ], Base64);
    return Base64;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["f" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImageResizer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(29);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * @name Image Resizer
 * @description
 * Cordova Plugin For Image Resize
 *
 * @usage
 * ```typescript
 * import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
 *
 * constructor(private imageResizer: ImageResizer) { }
 *
 * ...
 *
 * let options = {
 *  uri: uri,
 *  folderName: 'Protonet',
 *  quality: 90,
 *  width: 1280,
 *  height: 1280
 * } as ImageResizerOptions;
 *
 * this.imageResizer
 *   .resize(options)
 *   .then((filePath: string) => console.log('FilePath', filePath))
 *   .catch(e => console.log(e));
 *
 * ```
 * @interfaces
 * ImageResizerOptions
 */
var ImageResizer = (function (_super) {
    __extends(ImageResizer, _super);
    function ImageResizer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @returns {Promise<any>}
     */
    /**
       * @returns {Promise<any>}
       */
    ImageResizer.prototype.resize = /**
       * @returns {Promise<any>}
       */
    function (options) {
        return;
    };
    ImageResizer.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */] },
    ];
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["a" /* Cordova */])(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], ImageResizer.prototype, "resize", null);
    /**
     * @name Image Resizer
     * @description
     * Cordova Plugin For Image Resize
     *
     * @usage
     * ```typescript
     * import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
     *
     * constructor(private imageResizer: ImageResizer) { }
     *
     * ...
     *
     * let options = {
     *  uri: uri,
     *  folderName: 'Protonet',
     *  quality: 90,
     *  width: 1280,
     *  height: 1280
     * } as ImageResizerOptions;
     *
     * this.imageResizer
     *   .resize(options)
     *   .then((filePath: string) => console.log('FilePath', filePath))
     *   .catch(e => console.log(e));
     *
     * ```
     * @interfaces
     * ImageResizerOptions
     */
    ImageResizer = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["g" /* Plugin */])({
            pluginName: 'ImageResizer',
            plugin: 'info.protonet.imageresizer',
            pluginRef: 'ImageResizer',
            repo: 'https://github.com/JoschkaSchulz/cordova-plugin-image-resizer',
            platforms: ['Android', 'iOS', 'Windows']
        })
    ], ImageResizer);
    return ImageResizer;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["f" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 486:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhotosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_base64__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_image_resizer__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_image_picker_ngx__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_services_language__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the PhotosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PhotosPage = /** @class */ (function () {
    function PhotosPage(navCtrl, language, mylocalstorage, imagePicker, imageResizer, geolocation, base64, alertCtrl, navParams, camera) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.mylocalstorage = mylocalstorage;
        this.imagePicker = imagePicker;
        this.imageResizer = imageResizer;
        this.geolocation = geolocation;
        this.base64 = base64;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.observation = {
            image1: '0',
            image2: '0',
            image3: '0',
            image4: '0',
        };
        this.listeimages = [];
        this.listezozo = [];
        this.compteur = 1;
        this.mesttreposition = 0;
        this.testeur = 0;
        this.images = [];
        this.testvalue = 0;
        this.titrequestion = "";
    }
    PhotosPage.prototype.ionViewWillLeave = function () {
        if (this.testvalue == 0) {
        }
    };
    PhotosPage.prototype.ionViewWillEnter = function () {
        this.testvalue = 0;
        __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = [];
    };
    PhotosPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.titrequestion = "Add picture(4 pictures max)";
            }
            else {
                _this.titrequestion = "Ajouter photos(4 photos max)";
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.testeur = 0;
        // this.question = this.navParams.get("question");
        // this.reponse = this.navParams.get("reponse");
        this.idquestion = this.navParams.get("idquestion");
        this.type_question = this.navParams.get("type_question");
        this.listezozo = [1, 2, 3, 5, 8];
    };
    PhotosPage.prototype.deleteall = function () {
        var _this = this;
        this.alertCtrl.create({
            message: 'Voulez-vous vraiment suprimer ces photos ?',
            buttons: [{
                    text: 'Non',
                    handler: function (data) {
                    }
                }, {
                    text: 'Oui',
                    handler: function (data) {
                        _this.testeur = 0;
                        _this.compteur = 0;
                        _this.listeimages = [];
                    }
                }
            ]
        }).present();
    };
    PhotosPage.prototype.deleteone = function (index) {
        var _this = this;
        this.alertCtrl.create({
            message: 'Voulez-vous vraiment suprimer cette photos ?',
            buttons: [{
                    text: 'Non',
                    handler: function (data) {
                    }
                }, {
                    text: 'Oui',
                    handler: function (data) {
                        _this.listeimages.splice(index, 1);
                        _this.mesttreposition = index;
                    }
                }
            ]
        }).present();
    };
    PhotosPage.prototype.getPictures = function () {
        var _this = this;
        this.imagePicker.getPictures({
            maximumImagesCount: 5,
            outputType: 1
        }).then(function (selectedImg) {
            selectedImg.forEach(function (i) { return _this.images.push("data:image/jpeg;base64," + i); });
        });
    };
    PhotosPage.prototype.takegalerie = function () {
        var _this = this;
        var options = {
            quality: 75,
            destinationType: 0,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: 0
        };
        this.camera.getPicture(options).then(function (ImageData) {
            var base64Image = ImageData;
            if (_this.mesttreposition == 0) {
                if (_this.compteur <= 4) {
                    var element = {
                        image: "data:image/jpeg;base64," + base64Image,
                        id: _this.compteur
                    };
                    _this.listeimages.push(element);
                    if (_this.compteur == 1) {
                        _this.observation.image1 = element.image;
                        /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                          
                          this.observation.image1 = base64File;
                        }, (err) =>{
                        })*/
                    }
                    if (_this.compteur == 2) {
                        _this.observation.image2 = element.image;
                        /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                          this.observation.image2 = base64File;
                        }, (err) =>{
                        })*/
                    }
                    if (_this.compteur == 3) {
                        _this.observation.image3 = element.image;
                        /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                          this.observation.image3 = base64File;
                        }, (err) =>{
                        })*/
                    }
                    if (_this.compteur == 4) {
                        _this.observation.image4 = element.image;
                        /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                          this.observation.image4 = base64File;
                        }, (err) =>{
                        })*/
                    }
                    _this.compteur++;
                }
                else {
                    _this.testeur = 1;
                }
            }
            else {
                _this.listeimages.push(element);
                if (_this.mesttreposition == 1) {
                    _this.base64.encodeFile(base64Image).then(function (base64File) {
                        _this.observation.image1 = base64File;
                    }, function (err) {
                    });
                }
                if (_this.mesttreposition == 2) {
                    _this.base64.encodeFile(base64Image).then(function (base64File) {
                        _this.observation.image2 = base64File;
                    }, function (err) {
                    });
                }
                if (_this.mesttreposition == 3) {
                    _this.base64.encodeFile(base64Image).then(function (base64File) {
                        _this.observation.image3 = base64File;
                    }, function (err) {
                    });
                }
                if (_this.mesttreposition == 4) {
                    _this.base64.encodeFile(base64Image).then(function (base64File) {
                        _this.observation.image4 = base64File;
                    }, function (err) {
                    });
                }
                _this.mesttreposition = 0;
            }
        }, function (err) {
        });
    };
    PhotosPage.prototype.takephotos = function () {
        var _this = this;
        var options = {
            quality: 75,
            destinationType: 1,
            /*encodingType:this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: 1*/
            sourceType: 1,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: false,
            saveToPhotoAlbum: true
        };
        this.camera.getPicture(options).then(function (ImageData) {
            var base64Image = ImageData;
            var optionss = {
                uri: ImageData,
                folderName: 'Protonet',
                quality: 90,
                width: 1024,
                height: 748
            };
            if (_this.mesttreposition == 0) {
                if (_this.compteur <= 4) {
                    var element = {
                        image: base64Image,
                        id: _this.compteur
                    };
                    _this.listeimages.push(element);
                    if (_this.compteur == 1) {
                        try {
                            _this.imageResizer
                                .resize(optionss)
                                .then(function (filePath) {
                                _this.base64.encodeFile(filePath).then(function (base64File) {
                                    _this.observation.image1 = base64File;
                                    //this.observation.image1 = base64File;  
                                }, function (err) {
                                });
                            }).catch(function (e) {
                            });
                        }
                        catch (error) {
                        }
                    }
                    if (_this.compteur == 2) {
                        try {
                            _this.imageResizer
                                .resize(optionss)
                                .then(function (filePath) {
                                _this.base64.encodeFile(filePath).then(function (base64File) {
                                    _this.observation.image2 = base64File;
                                    //this.observation.image2 = base64File;  
                                }, function (err) {
                                });
                            }).catch(function (e) {
                            });
                        }
                        catch (error) {
                        }
                    }
                    if (_this.compteur == 3) {
                        try {
                            _this.imageResizer
                                .resize(optionss)
                                .then(function (filePath) {
                                _this.base64.encodeFile(filePath).then(function (base64File) {
                                    _this.observation.image3 = base64File;
                                    //this.observation.image3 = base64File;  
                                }, function (err) {
                                });
                            }).catch(function (e) {
                            });
                        }
                        catch (error) {
                        }
                    }
                    if (_this.compteur == 4) {
                        try {
                            _this.imageResizer
                                .resize(optionss)
                                .then(function (filePath) {
                                _this.base64.encodeFile(filePath).then(function (base64File) {
                                    _this.observation.image4 = base64File;
                                    //this.observation.image4 = base64File;  
                                }, function (err) {
                                });
                            }).catch(function (e) {
                            });
                        }
                        catch (error) {
                        }
                    }
                    _this.compteur++;
                }
                else {
                    _this.testeur = 1;
                }
            }
            else {
                _this.listeimages.push(element);
                if (_this.mesttreposition == 1) {
                    try {
                        _this.imageResizer
                            .resize(optionss)
                            .then(function (filePath) {
                            _this.base64.encodeFile(filePath).then(function (base64File) {
                                _this.observation.image1 = base64File;
                                //this.observation.image1 = base64File;  
                            }, function (err) {
                            });
                        }).catch(function (e) {
                        });
                    }
                    catch (error) {
                    }
                }
                if (_this.mesttreposition == 2) {
                    try {
                        _this.imageResizer
                            .resize(optionss)
                            .then(function (filePath) {
                            _this.base64.encodeFile(filePath).then(function (base64File) {
                                _this.observation.image2 = base64File;
                                //this.observation.image2 = base64File;  
                            }, function (err) {
                            });
                        }).catch(function (e) {
                        });
                    }
                    catch (error) {
                    }
                }
                if (_this.mesttreposition == 3) {
                    try {
                        _this.imageResizer
                            .resize(optionss)
                            .then(function (filePath) {
                            _this.base64.encodeFile(filePath).then(function (base64File) {
                                _this.observation.image3 = base64File;
                                //this.observation.image3 = base64File;  
                            }, function (err) {
                            });
                        }).catch(function (e) {
                        });
                    }
                    catch (error) {
                    }
                }
                if (_this.mesttreposition == 4) {
                    try {
                        _this.imageResizer
                            .resize(optionss)
                            .then(function (filePath) {
                            _this.base64.encodeFile(filePath).then(function (base64File) {
                                _this.observation.image4 = base64File;
                                //this.observation.image4 = base64File;  
                            }, function (err) {
                            });
                        }).catch(function (e) {
                        });
                    }
                    catch (error) {
                    }
                }
                _this.mesttreposition = 0;
            }
        }, function (err) {
        });
    };
    PhotosPage.prototype.backFunction = function (color) {
        __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].listesReponses = [];
        this.navCtrl.setRoot('AcceuilPage').then(function () { });
    };
    PhotosPage.prototype.suivant = function () {
        __WEBPACK_IMPORTED_MODULE_4__configs_configs__["b" /* Parcourt */].observation = this.observation;
        //$state.go('app.description',{question:$scope.titrequestion, reponse: $scope.mareponse});
        // this.navCtrl.push('DescriptionPage', {question: this.question, reponse:this.reponse});
        //this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
        this.testvalue = 1;
        console.log("============================================");
        console.log(this.type_question);
        console.log(this.idquestion);
        console.log("============================================");
        if (this.type_question == "entier") {
            this.navCtrl.push('NombrepetitPage', { idquestion: this.idquestion, type_question: this.type_question });
        }
        else if (this.type_question == "text") {
            this.navCtrl.push('DescriptionPage', { idquestion: this.idquestion, type_question: this.type_question });
        }
        else if (this.type_question == "select") {
            this.navCtrl.push('OuestanimalPage', { idquestion: this.idquestion, type_question: this.type_question });
        }
        else if (this.type_question == "rapide") {
            this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
        }
        else if (this.type_question == "annuler") {
            this.navCtrl.setRoot('AcceuilPage').then(function () { });
        }
        else {
            this.navCtrl.push('GroupePage');
        }
    };
    PhotosPage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    PhotosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-photos',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/photos/photos.html"*/'<ion-header>\n  <navbar [title]= "titrequestion" ></navbar>\n</ion-header>\n\n\n<ion-content padding class="backgroundcorps">\n\n    <div  class="mebouttons">\n      <button *ngIf="testeur==0" style="background-color: #1b9eea" ion-button icon-start  (click)="takephotos()">\n        <ion-icon  style="zoom:2.0;"  name="camera"></ion-icon>\n      </button>\n      <button *ngIf="testeur==0" style="background-color: #1b9eea" ion-button icon-start  (click)="takegalerie()">\n          <ion-icon style="zoom:2.0;" name="md-images"></ion-icon>\n      </button>\n      <button style="background-color: #cc0c0c"  ion-button icon-start  (click)="deleteall()">\n        <ion-icon style="zoom:2.0;"  name="md-trash"></ion-icon>\n      </button>\n      <!--<button style="background-color: #251f79" ion-button icon-start  (click)="takephotos()">\n        <ion-icon name="md-refresh"></ion-icon>\n      </button>-->\n    </div>\n    \n    <!--<ion-card style="height: 37%; width: 95%;">\n      <img style="height: 75%; width: 90%;" src="assets/imgs/logo.png" alt="">\n      <button class="special" style="background-color: #cc0c0c"  ion-button icon-start  (click)="deleteone(1)">\n        <ion-icon  name="md-trash"></ion-icon>\n      </button>\n  </ion-card>-->\n  \n  <ion-card *ngFor="let item of listeimages" style="height: 37%; width: 95%;">\n    <img style="height: 75%; width: 100%;" src="{{item.image}}" alt="">\n    <button class="special" style="background-color: #cc0c0c"  ion-button icon-start  (click)="deleteone(1)">\n        <ion-icon  name="md-trash"></ion-icon>\n    </button>\n    \n    \n  </ion-card>\n\n\n\n  <!-- <div class="blockbutton" *ngIf="testeur!=0"> -->\n  <div class="blockbutton">\n      <button  ion-button class="colors"  block (click)="suivant()">\n          {{ \'SUIVANT\' | translate }}\n          <ion-icon name="arrow-forward" float-right class="monicon"></ion-icon>\n      </button>\n  </div>\n\n  <ion-row class="monbutonvalide">\n    <ion-col>\n      <button  (click)="backFunction()" ion-fab left bottom  color="danger"> <ion-icon name="md-close"></ion-icon> </button>\n    </ion-col>\n  </ion-row>\n\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/photos/photos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_9__providers_services_language__["a" /* LanguageService */], __WEBPACK_IMPORTED_MODULE_8__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_image_picker_ngx__["a" /* ImagePicker */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_image_resizer__["a" /* ImageResizer */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_base64__["a" /* Base64 */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */]])
    ], PhotosPage);
    return PhotosPage;
}());

//# sourceMappingURL=photos.js.map

/***/ })

});
//# sourceMappingURL=1.js.map