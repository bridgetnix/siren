webpackJsonp([5],{

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SirinPageModule", function() { return SirinPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sirin__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var SirinPageModule = /** @class */ (function () {
    function SirinPageModule() {
    }
    SirinPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sirin__["a" /* SirinPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sirin__["a" /* SirinPage */]),
            ],
        })
    ], SirinPageModule);
    return SirinPageModule;
}());

//# sourceMappingURL=sirin.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 483:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SirinPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_services_language__ = __webpack_require__(116);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the SirinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SirinPage = /** @class */ (function () {
    function SirinPage(navCtrl, language, geolocation, translate, toastCtrl, loadingCtrl, services, mylocalstorage, navParams) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.geolocation = geolocation;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.services = services;
        this.mylocalstorage = mylocalstorage;
        this.navParams = navParams;
        this.listequestions = [];
        this.testvalue = 0;
    }
    SirinPage.prototype.ionViewWillLeave = function () {
        var listereponses = [];
        if (this.testvalue == 0) {
            listereponses = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses;
            console.log(listereponses);
            listereponses.pop();
            console.log(listereponses);
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
        }
    };
    SirinPage.prototype.ionViewWillEnter = function () {
        this.testvalue = 0;
    };
    SirinPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.langue = 1;
            }
            else {
                _this.langue = 0;
            }
        });
        console.log(__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses);
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.question = this.navParams.get("question");
        this.reponse = this.navParams.get("reponse");
        this.latitude = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].latitude;
        this.longitude = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].longitude;
        this.typeobservation = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].typeobservation.nom_fr;
        if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].indicateur == 1) {
            this.testeur = 1;
            this.groupe = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].groupe.nom_fr;
            this.sous_groupe = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].sousgroupe.nom_fr;
            this.espece = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].espece.nom_fr;
        }
        else if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].indicateur == 2) {
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].groupe = {
                id: 0
            };
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].sousgroupe = {
                id: 0
            };
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].espece = {
                id: 0
            };
        }
        else {
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].groupe = {
                id: 0
            };
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].sousgroupe = {
                id: 0
            };
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].espece = {
                id: 0
            };
            this.testeur = 3;
        }
        this.listequestions = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses;
        this.description = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].description;
        this.madate = new Date();
    };
    SirinPage.prototype.showActionsheet = function () {
        var _this = this;
        this.testvalue = 1;
        var today = new Date();
        this.mylocalstorage.getSession().then(function (result) {
            _this.userconnecte = result;
            /*if(navigator.onLine){

                 this.observation = {
                   "projet":this.userconnecte.projet.id,
                   "dateo":Math.round(new Date().getTime()/1000),
                   "utilisateur":this.userconnecte.id,
                   "coordX":Parcourt.latitude,
                   "coordY":Parcourt.longitude,
                   "typeObservations":Parcourt.typeobservation.id,
                   "groupe":Parcourt.groupe.id,
                   "sousgroupe":Parcourt.sousgroupe.id,
                   "espece": Parcourt.espece.id,
                   "img1File": Parcourt.observation.image1,
                   "img2File":Parcourt.observation.image2,
                   "img3File":Parcourt.observation.image3,
                   "img4File":Parcourt.observation.image4,
                   "note":Parcourt.description,
                   "etat":1
                 };

                 let loading = this.loadingCtrl.create();
                 loading.present();

                 this.services.saveObservation(this.observation).subscribe((result) =>{
                   
                       
                       //this.observation = {...this.observation,  "etat":1}
                       if(Parcourt.listesReponses.length >0){

                             Parcourt.listesReponses.forEach(element => {

                              
                                 let resultat = {
                                   "contenu":element.titre_fr,
                                   "questions":element.questions.id,
                                   "observations":result,
                                   "etat":1
                                 }

                                 this.services.saveresulatat(resultat).subscribe((result) =>{
                                     
                                 }, (error)=>{
                                     alert("one error was done");
                                 }, ()=>{
                                 });


                                 
                               
                           });
                        }
                        
                   

                   this.observation = {...this.observation, madate:today};

                   this.services.createObservation(this.observation).then((result) =>{

                     this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                        

                       if(Parcourt.listesReponses.length >0){

                            var i =0;

                             Parcourt.listesReponses.forEach(element => {
                                
                                 i++;
                             
                                 let resultat = {
                                   "contenu":element.titre_fr,
                                   "questions":element.questions.id,
                                   "observations":result,
                                   "etat":1
                                 }

                                 this.services.createResultat(resultat).then((result) =>{
                                   
                                   if(Parcourt.listesReponses.length == i){

                                     this.presentToast(langs['SUCCESSOPERATION']);
                                     Parcourt.listesReponses = null;
                                     this.navCtrl.setRoot('MesobservationsPage');

                                   }
                                    
                                   
                                 });
                                 

                           });
                       }else{
                         
                         Parcourt.listesReponses = null;
                         this.navCtrl.setRoot('MesobservationsPage');

                       }
                           
                       
                     });

                   });

                 }, (error)=>{
                   alert("error na fo boutokou");
                   console.log(error);
                   loading.dismiss();
                 }, ()=>{
                   loading.dismiss();
                 });


       

           }else{*/
            _this.observation = {
                "projet": _this.userconnecte["projet"].id,
                "dateo": Math.round(new Date().getTime() / 1000),
                "utilisateur": _this.userconnecte.id,
                "coordX": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].latitude,
                "coordY": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].longitude,
                "typeObservations": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].typeobservation.id,
                "groupe": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].groupe.id,
                "sousgroupe": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].sousgroupe.id,
                "espece": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].espece.id,
                "img1File": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].observation.image1,
                "img2File": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].observation.image2,
                "img3File": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].observation.image3,
                "img4File": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].observation.image4,
                "note": __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].description,
                "etat": 0
            };
            //this.observation = {...this.observation,  "etat":0}
            _this.observation = __assign({}, _this.observation, { madate: today });
            _this.services.createObservation(_this.observation).then(function (result) {
                if (result == 0) {
                    _this.presentToast('insert not working!');
                }
                else {
                    _this.translate.get(['SUCCESSOPERATION']).subscribe(function (langs) {
                        _this.presentToast(langs['SUCCESSOPERATION']);
                        if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses.length > 0) {
                            var i = 0;
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses.forEach(function (element) {
                                i++;
                                var resultat = {
                                    "contenu": element.titre_fr,
                                    "questions": element.questions.id,
                                    "observations": result,
                                    "etat": 0
                                };
                                _this.services.createResultat(resultat).then(function (result) {
                                    if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses.length == i) {
                                        __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = null;
                                        _this.navCtrl.setRoot('MesobservationsPage');
                                    }
                                });
                            });
                            _this.observation = {};
                            _this.navCtrl.setRoot('MesobservationsPage');
                        }
                        else {
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = null;
                            _this.navCtrl.setRoot('MesobservationsPage');
                        }
                    });
                }
            });
            /**/
            // }
        });
    };
    SirinPage.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    };
    SirinPage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    SirinPage.prototype.backFunction = function (color) {
        __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = [];
        this.navCtrl.setRoot('AcceuilPage').then(function () { });
    };
    SirinPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sirin',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/sirin/sirin.html"*/'<ion-header>\n  <navbar [title]= "\'Siren\'" ></navbar>\n</ion-header>\n\n\n<ion-content padding class="backgroundcorps">\n\n    <p class="titre">{{ \'RECAPITULATIF\' | translate }}</p>\n\n    <br>\n   \n\n    <ion-row>\n       <ion-col class="dateheure2" *ngIf="langue==0">\n          Date et heure :\n       </ion-col>\n       <ion-col class="dateheure2" *ngIf="langue==1">\n        Date and time :\n       </ion-col>\n       <ion-col class="dateheure3">\n         {{madate  | date: \'dd/MM/yyyy H:mm\' }}\n       </ion-col>\n    </ion-row>\n\n    <ion-row>\n        <ion-col class="dateheure2" *ngIf="langue==0">\n          Localisation :\n        </ion-col>\n        <ion-col class="dateheure2" *ngIf="langue==1">\n          Localisation :\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{latitude}}  | {{longitude}}\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col class="dateheure2" *ngIf="langue==0">\n          Type d\'observation :\n        </ion-col>\n        <ion-col class="dateheure2" *ngIf="langue==1">\n          Observation Type:\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{typeobservation}}\n        </ion-col>\n    </ion-row>\n    <ion-row *ngIf="testeur == 1">\n        <ion-col class="dateheure2">\n          Groupe :\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{groupe}}\n        </ion-col>\n    </ion-row>\n    <ion-row *ngIf="testeur == 1"> \n        <ion-col class="dateheure2">\n          Sous-groupe :\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{sous_groupe}}\n        </ion-col>\n    </ion-row>\n    <ion-row *ngIf="testeur == 1">\n        <ion-col class="dateheure2">\n          Espèce :\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{espece}}\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col class="dateheure2" *ngIf="langue==0">\n          Description :\n        </ion-col>\n        <ion-col class="dateheure2" *ngIf="langue==1">\n          Description :\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{description}}\n        </ion-col>\n    </ion-row>\n\n    <ion-row *ngIf="question!=0">\n        <ion-col class="dateheure2">\n          {{question}}\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{reponse}}\n        </ion-col>\n    </ion-row>\n\n    <ion-row *ngFor="let item of listequestions">\n        <ion-col class="dateheure2">\n          {{item.questions.titre_fr}}\n        </ion-col>\n        <ion-col class="dateheure3">\n          {{item.titre_fr}}\n        </ion-col>\n    </ion-row>\n\n    <ion-row class="monbutonvalide">\n      <ion-col>\n        <button  (click)="backFunction()" ion-fab left bottom  color="danger"> <ion-icon name="md-close"></ion-icon> </button>\n      </ion-col>\n      <ion-col >\n        <button style="margin-left: 70%;" (click)="showActionsheet()" ion-fab right bottom  color="positive"> <ion-icon name="md-checkmark"></ion-icon> </button>\n      </ion-col>\n    </ion-row>\n\n\n    \n    \n\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/sirin/sirin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_7__providers_services_language__["a" /* LanguageService */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_5_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_services_services__["a" /* ServicesProvider */], __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], SirinPage);
    return SirinPage;
}());

//# sourceMappingURL=sirin.js.map

/***/ })

});
//# sourceMappingURL=5.js.map