webpackJsonp([18],{

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChangeprojetPageModule", function() { return ChangeprojetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__changeprojet__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ChangeprojetPageModule = /** @class */ (function () {
    function ChangeprojetPageModule() {
    }
    ChangeprojetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__changeprojet__["a" /* ChangeprojetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__changeprojet__["a" /* ChangeprojetPage */]),
            ],
        })
    ], ChangeprojetPageModule);
    return ChangeprojetPageModule;
}());

//# sourceMappingURL=changeprojet.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangeprojetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__ = __webpack_require__(115);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the ChangeprojetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ChangeprojetPage = /** @class */ (function () {
    function ChangeprojetPage(navCtrl, appCtrl, geolocation, toastCtrl, translate, alertCtrl, mylocalstorage, loadingCtrl, navParams, services) {
        this.navCtrl = navCtrl;
        this.appCtrl = appCtrl;
        this.geolocation = geolocation;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.mylocalstorage = mylocalstorage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        this.mesprojets = [];
        this.userconnecte = {};
        this.showalert = 0;
        this.utilisateur = {
            login: "",
            password: ""
        };
    }
    ChangeprojetPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        if (navigator.onLine) {
            this.mylocalstorage.getSession().then(function (result) {
                _this.userconnecte = result;
                var loading = _this.loadingCtrl.create();
                loading.present();
                _this.services.findProjetsByUserId(_this.userconnecte["id"]).subscribe(function (resultat) {
                    _this.mesprojets = resultat;
                    _this.showRadio(_this.mesprojets);
                }, function (error) {
                    console.log(error);
                    //votre opération a  echoué!!
                    loading.dismiss();
                }, function () {
                    loading.dismiss();
                });
            });
        }
        else {
            this.translate.get(['INFOSCONNEXION']).subscribe(function (langs) {
                _this.presentToast(langs['INFOSCONNEXION']);
                _this.navCtrl.setRoot('AcceuilPage');
            });
        }
    };
    ChangeprojetPage.prototype.showRadio = function (listeprojets) {
        var _this = this;
        this.translate.get(['SELECTPROJET', 'ENREGISTRER', 'SUCCESSOPERATION']).subscribe(function (langs) {
            var alert = _this.alertCtrl.create();
            alert.setTitle(langs['SELECTPROJET']);
            _this.mesprojets.forEach(function (element) {
                var monimput = {
                    type: 'radio',
                    label: element.nom,
                    value: element.id,
                };
                alert.addInput(monimput);
            });
            alert.addButton({
                text: langs['ENREGISTRER'],
                cssClass: 'ion-button full',
                handler: function (data) {
                    console.log(data);
                    var loading = _this.loadingCtrl.create();
                    loading.present();
                    _this.mylocalstorage.getSession().then(function (result) {
                        _this.userconnecte = result;
                        _this.utilisateur.login = _this.userconnecte["email"];
                        _this.utilisateur.password = _this.userconnecte["password"];
                        //Parcourt.images = 
                        _this.services.changerProjet(_this.userconnecte["id"], data).subscribe(function (resultat) {
                            _this.services.login(_this.utilisateur).subscribe(function (result) {
                                if (result != 0) {
                                    _this.mylocalstorage.storeSession(result).then(function () {
                                        //this.appCtrl.getRootNav().setRoot("AcceuilPage");
                                        _this.services.deletteAlltypeObservation().then(function (result1) {
                                            //this.services.acceuil().subscribe((result) =>{ 
                                            _this.services.projet_typeObservation(_this.userconnecte["projet"].id).subscribe(function (result) {
                                                var i = 0;
                                                result.forEach(function (element) {
                                                    _this.services.createTypeObservation(element).then(function (result1) {
                                                        i++;
                                                        if (i == result.length) {
                                                            _this.navCtrl.setRoot('AcceuilPage').then(function () {
                                                                _this.showalert = 1;
                                                                window.location.reload();
                                                            });
                                                            _this.presentToast(langs['SUCCESSOPERATION']);
                                                        }
                                                    });
                                                });
                                            }, function (error) {
                                                console.log(error);
                                            }, function () {
                                            });
                                        });
                                    });
                                }
                                else {
                                    _this.translate.get(['INFOSLOGINV']).subscribe(function (langs) {
                                        _this.presentToast(langs['INFOSLOGINV']);
                                    });
                                }
                            }, function (error) {
                                loading.dismiss();
                                console.log(error);
                            }, function () {
                                loading.dismiss();
                            });
                        }, function (error) {
                            console.log(error);
                            //votre opération a  echoué!!
                            loading.dismiss();
                        }, function () {
                            loading.dismiss();
                        });
                    });
                }
            });
            if (_this.showalert == 0) {
                alert.present();
            }
        });
    };
    ChangeprojetPage.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 5000,
        });
        toast.present();
    };
    ChangeprojetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-changeprojet',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/changeprojet/changeprojet.html"*/'<ion-header>\n    <navbar title= "{{ \'BIENVENUS\' | translate }}" ></navbar>\n</ion-header>\n\n\n<ion-content class="backgroundcorps">\n      \n</ion-content>'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/changeprojet/changeprojet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ToastController */], __WEBPACK_IMPORTED_MODULE_4_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_services_services__["a" /* ServicesProvider */]])
    ], ChangeprojetPage);
    return ChangeprojetPage;
}());

//# sourceMappingURL=changeprojet.js.map

/***/ })

});
//# sourceMappingURL=18.js.map