webpackJsonp([16],{

/***/ 328:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescriptionPageModule", function() { return DescriptionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__description__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var DescriptionPageModule = /** @class */ (function () {
    function DescriptionPageModule() {
    }
    DescriptionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__description__["a" /* DescriptionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__description__["a" /* DescriptionPage */]),
            ],
        })
    ], DescriptionPageModule);
    return DescriptionPageModule;
}());

//# sourceMappingURL=description.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 485:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DescriptionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_services_language__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DescriptionPage = /** @class */ (function () {
    function DescriptionPage(navCtrl, language, services, loadingCtrl, mylocalstorage, geolocation, navParams) {
        this.navCtrl = navCtrl;
        this.language = language;
        this.services = services;
        this.loadingCtrl = loadingCtrl;
        this.mylocalstorage = mylocalstorage;
        this.geolocation = geolocation;
        this.navParams = navParams;
        this.monobservation = {
            description: ''
        };
        this.manobservation = {
            description: ''
        };
        this.printpicture = 0;
        this.noteobservation = 0;
        this.langue = 0;
        this.reponses = {
            questions: {
                titre_fr: "",
                titre_en: ""
            },
            titre_fr: "",
            titre_en: ""
        };
        this.monprojet = {
            note: ""
        };
    }
    DescriptionPage.prototype.ionViewWillLeave = function () {
        this.noteobservation = 0;
    };
    DescriptionPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.langue = 1;
            }
            else {
                _this.langue = 0;
            }
        });
        this.noteobservation = 0;
        console.log(__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses);
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    _this.myprojet = _this.myuserconnecte.projet;
                    _this.monprojet.note = _this.myprojet.note;
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.question = this.navParams.get("question");
        this.reponse = this.navParams.get("reponse");
        this.idquestion = this.navParams.get("idquestion");
        this.type_question = this.navParams.get("type_question");
        if (navigator.onLine) {
            /////////////////////////////////////////////////////////// ONLINE ////////////////////////////////
            /*console.log(this.idquestion);
            console.log(this.type_question);
      
            if((this.question!=0)||(this.question!='0')){
      
                let loading = this.loadingCtrl.create();
                loading.present();
                this.services.findQuestionById(this.idquestion).subscribe((result) =>{
                  console.log(result);
                  this.nextquestion = result;
                  
                  if(result.image!=undefined){
                    this.printpicture =1;
                    this.myimage =result.image;
                  }else{
                    //alert("le mokai");
                  }
                  
                  if(result.type_reponse != 'select'){
                    if(this.langue==0){
                      this.titrequestion = result.titre_fr;
                    }else{
                      this.titrequestion = result.titre_en;
                    }
                    
                  }else{
                    this.noteobservation = 1;
                  }
                  
                }, (error)=>{
                  console.log(error);
                  //votre opération a  echoué!!
                  loading.dismiss();
                }, ()=>{
                  loading.dismiss();
                });
            }else{
              this.noteobservation = 1;
            }*/
            ////////////////////////////////////////////////////////////////////////////////
            if ((this.question != 0) || (this.question != '0')) {
                var loading_1 = this.loadingCtrl.create();
                loading_1.present();
                this.services.getQuestionById(this.idquestion).then(function (result) {
                    loading_1.dismiss();
                    _this.nextquestion = result[0];
                    if (result[0].image != undefined) {
                        _this.printpicture = 1;
                        _this.myimage = result[0].image;
                    }
                    else {
                        //alert("le mokai");
                    }
                    if (result[0].type_reponse != 'select') {
                        if (_this.langue == 0) {
                            _this.titrequestion = result[0].titre_fr;
                        }
                        else {
                            _this.titrequestion = result[0].titre_en;
                        }
                    }
                    else {
                        _this.noteobservation = 1;
                    }
                });
            }
            else {
                this.noteobservation = 1;
            }
        }
        else {
            if ((this.question != 0) || (this.question != '0')) {
                var loading_2 = this.loadingCtrl.create();
                loading_2.present();
                this.services.getQuestionById(this.idquestion).then(function (result) {
                    loading_2.dismiss();
                    _this.nextquestion = result[0];
                    if (result[0].image != undefined) {
                        _this.printpicture = 1;
                        _this.myimage = result[0].image;
                    }
                    else {
                        //alert("le mokai");
                    }
                    if (result[0].type_reponse != 'select') {
                        if (_this.langue == 0) {
                            _this.titrequestion = result[0].titre_fr;
                        }
                        else {
                            _this.titrequestion = result[0].titre_en;
                        }
                    }
                    else {
                        _this.noteobservation = 1;
                    }
                });
            }
            else {
                this.noteobservation = 1;
            }
        }
    };
    DescriptionPage.prototype.suivant = function () {
        var _this = this;
        if ((this.question != 0) || (this.question != '0')) {
            if (navigator.onLine) {
                /*if(this.nextquestion.questions!=undefined){
                  this.type_question = this.nextquestion.questions.type_reponse;
                  this.idquestion = this.nextquestion.questions.id;
      
                  this.reponses.questions = this.nextquestion;
                  if(this.langue == 0){
                    this.reponses.titre_fr = this.monobservation.description;
                  }else{
                    this.reponses.titre_en = this.monobservation.description;
                  }
                  
      
                  var listereponses = [];
                  if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(this.reponses);
                    Parcourt.listesReponses = listereponses;
                  }else{
                    
                    listereponses.push(this.reponses);
                    Parcourt.listesReponses = listereponses;
                  }
                  
                  if(this.type_question == "entier"){
                    this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion, type_question: this.type_question});
                  }else if(this.type_question == "text"){
                    this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                  }else if(this.type_question == "select"){
                    this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion, type_question: this.type_question});
                  }else if(this.type_question == "rapide"){
                    this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                  }else if(this.type_question == "annuler"){
                    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                  }else{
                    this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                    //this.navCtrl.push('GroupePage');
                  }
      
                }else{
                  if(this.noteobservation == 0){
      
                    if(this.langue==0){
                      this.reponses.questions = this.nextquestion;
                      //this.reponses.questions.titre_fr = this.titrequestion;
                      this.reponses.titre_fr = this.monobservation.description;
                    }else{
                      this.reponses.questions = this.nextquestion;
                      //this.reponses.questions.titre_en = this.titrequestion;
                      this.reponses.titre_en = this.monobservation.description;
                    }
                      
      
                    var listereponses = [];
                    if(Parcourt.listesReponses != null){
                      listereponses = Parcourt.listesReponses;
                      listereponses.push(this.reponses);
                      Parcourt.listesReponses = listereponses;
                    }else{
                      
                      listereponses.push(this.reponses);
                      Parcourt.listesReponses = listereponses;
                    }
                    this.noteobservation = 1;
                  }else{
                    Parcourt.description = this.manobservation.description;
                    this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                  }
      
                }*/
                if (this.nextquestion.questions != "null") {
                    this.services.getQuestionById(this.nextquestion.questions).then(function (result) {
                        var questions_next = result[0];
                        _this.type_question = questions_next.type_reponse;
                        _this.idquestion = questions_next.id;
                        _this.reponses.questions = questions_next;
                        if (_this.langue == 0) {
                            _this.reponses.titre_fr = _this.monobservation.description;
                        }
                        else {
                            _this.reponses.titre_en = _this.monobservation.description;
                        }
                        var listereponses = [];
                        if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                            listereponses = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses;
                            listereponses.push(_this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        else {
                            listereponses.push(_this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        if (_this.type_question == "entier") {
                            _this.navCtrl.push('NombrepetitPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "text") {
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "select") {
                            _this.navCtrl.push('OuestanimalPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "rapide") {
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "annuler") {
                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                        }
                        else {
                            //this.navCtrl.push('GroupePage');
                            _this.navCtrl.push('SirinPage', { question: 0, reponse: 0 });
                        }
                    });
                }
                else {
                    if (this.noteobservation == 0) {
                        if (this.langue == 0) {
                            this.reponses.questions = this.nextquestion;
                            //this.reponses.questions.titre_fr = this.titrequestion;
                            this.reponses.titre_fr = this.monobservation.description;
                        }
                        else {
                            this.reponses.questions = this.nextquestion;
                            //this.reponses.questions.titre_en = this.titrequestion;
                            this.reponses.titre_en = this.monobservation.description;
                        }
                        var listereponses = [];
                        if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                            listereponses = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses;
                            listereponses.push(this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        else {
                            listereponses.push(this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        this.noteobservation = 1;
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].description = this.manobservation.description;
                        this.navCtrl.push('SirinPage', { question: 0, reponse: 0 });
                    }
                }
            }
            else {
                if (this.nextquestion.questions != "null") {
                    this.services.getQuestionById(this.nextquestion.questions).then(function (result) {
                        var questions_next = result[0];
                        _this.type_question = questions_next.type_reponse;
                        _this.idquestion = questions_next.id;
                        _this.reponses.questions = questions_next;
                        if (_this.langue == 0) {
                            _this.reponses.titre_fr = _this.monobservation.description;
                        }
                        else {
                            _this.reponses.titre_en = _this.monobservation.description;
                        }
                        var listereponses = [];
                        if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                            listereponses = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses;
                            listereponses.push(_this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        else {
                            listereponses.push(_this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        if (_this.type_question == "entier") {
                            _this.navCtrl.push('NombrepetitPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "text") {
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "select") {
                            _this.navCtrl.push('OuestanimalPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "rapide") {
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        }
                        else if (_this.type_question == "annuler") {
                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                        }
                        else {
                            //this.navCtrl.push('GroupePage');
                            _this.navCtrl.push('SirinPage', { question: 0, reponse: 0 });
                        }
                    });
                }
                else {
                    if (this.noteobservation == 0) {
                        if (this.langue == 0) {
                            this.reponses.questions = this.nextquestion;
                            //this.reponses.questions.titre_fr = this.titrequestion;
                            this.reponses.titre_fr = this.monobservation.description;
                        }
                        else {
                            this.reponses.questions = this.nextquestion;
                            //this.reponses.questions.titre_en = this.titrequestion;
                            this.reponses.titre_en = this.monobservation.description;
                        }
                        var listereponses = [];
                        if (__WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                            listereponses = __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses;
                            listereponses.push(this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        else {
                            listereponses.push(this.reponses);
                            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                        }
                        this.noteobservation = 1;
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].description = this.manobservation.description;
                        this.navCtrl.push('SirinPage', { question: 0, reponse: 0 });
                    }
                }
            }
        }
        else {
            __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].description = this.manobservation.description;
            this.navCtrl.push('SirinPage', { question: 0, reponse: 0 });
        }
        //this.navCtrl.push('SirinPage', {question: this.question, reponse:this.reponse});
        /*  if(this.type_question == "text"){
            this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion});
          }else if(this.type_question == "select"){
            this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion});
          }else{
            this.navCtrl.push('GroupePage');
          }*/
        /*if(this.type_question == "entier"){
          this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion, type_question: this.type_question});
        }else if(this.type_question == "text"){
          this.navCtrl.push('DescriptionPage',{idquestion: 0, type_question: this.type_question});
        }else if(this.type_question == "select"){
          this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion, type_question: this.type_question});
        }else if(this.type_question == "rapide"){
          this.navCtrl.push('DescriptionPage',{idquestion: 0, type_question: this.type_question});
        }else if(this.type_question == "annuler"){
          this.navCtrl.setRoot('AcceuilPage').then(()=>{});
        }else{
          this.navCtrl.push('GroupePage');
        }*/
    };
    DescriptionPage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    DescriptionPage.prototype.backFunction = function (color) {
        __WEBPACK_IMPORTED_MODULE_2__configs_configs__["b" /* Parcourt */].listesReponses = [];
        this.navCtrl.setRoot('AcceuilPage').then(function () { });
    };
    DescriptionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-description',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/description/description.html"*/'<ion-header *ngIf="noteobservation == 0">\n  <navbar [title]= "titrequestion" ></navbar>\n</ion-header>\n\n<ion-header *ngIf="noteobservation == 1">\n  <navbar [title]= "\'Observation\'" ></navbar>\n</ion-header>\n\n\n<ion-content padding class="backgroundcorps" *ngIf="noteobservation == 0">\n\n  <p style="text-align: center; font-weight: bold; font-size: 20px;">{{titrequestion}}</p>\n    \n  <img src="{{myimage}}" style="width: 100%; height: 50%;" *ngIf="printpicture==1">\n\n  <div class="formlogin" style="margin-top: 20px;">\n    <textarea style="width: 100%;" type="text" [(ngModel)]="monobservation.description" placeholder="{{titrequestion}}"></textarea>\n    <hr>\n  \n  </div>\n\n  <div class="blockbutton">\n      <button  ion-button class="colors"  block (click)="suivant()">\n          {{ \'SUIVANT\' | translate }}\n          <ion-icon name="arrow-forward" float-right class="monicon"></ion-icon>\n      </button>\n  </div>\n\n  <ion-row class="monbutonvalide">\n      <ion-col>\n        <button  (click)="backFunction()" ion-fab left bottom  color="danger"> <ion-icon name="md-close"></ion-icon> </button>\n      </ion-col>\n    </ion-row>\n    \n</ion-content>\n\n\n\n\n<ion-content padding class="backgroundcorps" *ngIf="noteobservation == 1">\n\n  <br>\n  <br>\n\n  <div class="formlogin" style="margin-top: 20px;">\n    <textarea style="width: 100%;" type="text" [(ngModel)]="manobservation.description" placeholder=" une description de l\'observation"></textarea>\n    <hr>\n  \n  </div>\n\n\n\n  <p style="text-align: center; font-weight: bold; font-size: 20px;">{{monprojet.note}}</p>\n\n  <div class="blockbutton">\n      <button  ion-button class="colors"  block (click)="suivant()">\n          {{ \'SUIVANT\' | translate }}\n          <ion-icon name="arrow-forward" float-right class="monicon"></ion-icon>\n      </button>\n  </div>\n\n  <ion-row class="monbutonvalide">\n    <ion-col>\n      <button  (click)="backFunction()" ion-fab left bottom  color="danger"> <ion-icon name="md-close"></ion-icon> </button>\n    </ion-col>\n  </ion-row>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/description/description.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_6__providers_services_language__["a" /* LanguageService */], __WEBPACK_IMPORTED_MODULE_5__providers_services_services__["a" /* ServicesProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_4__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], DescriptionPage);
    return DescriptionPage;
}());

//# sourceMappingURL=description.js.map

/***/ })

});
//# sourceMappingURL=16.js.map