webpackJsonp([7],{

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OuestanimalPageModule", function() { return OuestanimalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ouestanimal__ = __webpack_require__(489);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_translate__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var OuestanimalPageModule = /** @class */ (function () {
    function OuestanimalPageModule() {
    }
    OuestanimalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ouestanimal__["a" /* OuestanimalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__components_navbar_navbar_module__["a" /* NavbarModule */],
                __WEBPACK_IMPORTED_MODULE_3_ng2_translate__["b" /* TranslateModule */],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__ouestanimal__["a" /* OuestanimalPage */]),
            ],
        })
    ], OuestanimalPageModule);
    return OuestanimalPageModule;
}());

//# sourceMappingURL=ouestanimal.module.js.map

/***/ }),

/***/ 334:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__navbar__ = __webpack_require__(335);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NavbarModule = /** @class */ (function () {
    function NavbarModule() {
    }
    NavbarModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__navbar__["a" /* NavbarComponent */]]
        })
    ], NavbarModule);
    return NavbarModule;
}());

//# sourceMappingURL=navbar.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(112);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(network) {
        this.network = network;
        this.title = 'Siren';
        this.showMenuToggle = true;
        this.msg_network = 'msg network';
        this.showNetworkStatus = false;
        this.colorNetworkStatus = 'red';
        console.log(this.network.type);
        this.checkNetwork();
    }
    NavbarComponent.prototype.checkNetwork = function () {
        var _this = this;
        this.network.onDisconnect().subscribe(function (next) {
            _this.msg_network = 'Network was disconnected';
            _this.colorNetworkStatus = 'red';
            _this.showNetworkStatus = true;
        });
        this.network.onConnect().subscribe(function (next) {
            _this.msg_network = 'Network connected';
            _this.colorNetworkStatus = 'green';
            _this.showNetworkStatus = true;
            setTimeout(function () {
                _this.showNetworkStatus = false;
            }, 10000);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], NavbarComponent.prototype, "showMenuToggle", void 0);
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'navbar',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/'<ion-navbar>\n  <button ion-button menuToggle *ngIf="showMenuToggle">\n    <ion-icon class="iconmenu" name="menu"></ion-icon>\n  </button>\n  <ion-title class="iconmenu">{{title}}</ion-title>\n</ion-navbar>\n\n<div style="height: 13px; font-size: 11px; color: white; text-align: center;"\n     [style.background-color]="colorNetworkStatus"\n     *ngIf="showNetworkStatus">\n  {{msg_network}}\n</div>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/components/navbar/navbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], NavbarComponent);
    return NavbarComponent;
}());

//# sourceMappingURL=navbar.js.map

/***/ }),

/***/ 489:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OuestanimalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(34);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_services_services__ = __webpack_require__(114);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__configs_configs__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_services_language__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_translate__ = __webpack_require__(216);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the OuestanimalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OuestanimalPage = /** @class */ (function () {
    function OuestanimalPage(navCtrl, translate, alertCtrl, language, geolocation, mylocalstorage, loadingCtrl, navParams, services) {
        this.navCtrl = navCtrl;
        this.translate = translate;
        this.alertCtrl = alertCtrl;
        this.language = language;
        this.geolocation = geolocation;
        this.mylocalstorage = mylocalstorage;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.services = services;
        this.data = {};
        this.ispicture = 0;
        this.langue = 0;
        this.testvalue = 0;
        this.printpicture = 0;
        this.mydata = {
            questions: {
                titre_fr: "",
                titre_en: ""
            },
            titre_fr: "",
            titre_en: ""
        };
    }
    /*ionViewWillLeave() {
      var listereponses = [];
      if(this.testvalue == 0){
        listereponses = Parcourt.listesReponses;
        console.log(listereponses);
        listereponses.pop();
        console.log(listereponses);
        Parcourt.listesReponses = listereponses;
      }
    }
    ionViewWillEnter() {
      this.testvalue = 0;
    }*/
    OuestanimalPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.language.loadLanguage().then(function (result) {
            if (result == 'en') {
                _this.langue = 1;
            }
            else {
                _this.langue = 0;
            }
        });
        this.mylocalstorage.getSession().then(function (result) {
            _this.myuserconnecte = result;
            if (_this.myuserconnecte != null) {
                if ((!undefined == _this.myuserconnecte.projet) || (_this.myuserconnecte.projet != null)) {
                    if ((!undefined == _this.myuserconnecte.projet.couleur) || (_this.myuserconnecte.projet.couleur != null)) {
                        _this.updateColor(_this.myuserconnecte.projet.couleur);
                    }
                    else {
                        _this.updateColor("#1b9eea");
                    }
                }
            }
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].latitude = resp.coords.latitude;
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].longitude = resp.coords.longitude;
        }).catch(function (error) {
            console.log("error could not get position");
        });
        this.idquestion = this.navParams.get("idquestion");
        this.type_question = this.navParams.get("type_question");
        this.mylocalstorage.getSession().then(function (result) {
            _this.userconnecte = result;
        });
        this.super(this.idquestion);
    };
    OuestanimalPage.prototype.mokaiAlert = function (title) {
        var alert = this.alertCtrl.create({
            title: "detail",
            message: title,
            buttons: [
                {
                    text: 'ok',
                    role: 'cancel',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    OuestanimalPage.prototype.changeStatus1 = function (data) {
        var _this = this;
        if (this.langue == 0) {
            this.mydata.questions = this.maquestion;
            this.mydata.titre_fr = data.titre_fr;
        }
        else {
            this.mydata.questions = this.maquestion;
            this.mydata.titre_en = data.titre_fr;
        }
        this.testvalue = 1;
        var listereponses = [];
        if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
            listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
            listereponses.push(this.mydata);
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
        }
        else {
            listereponses.push(this.mydata);
            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
        }
        if (navigator.onLine) {
            /*
            var listereponses = [];
             if(Parcourt.listesReponses != null){
               listereponses = Parcourt.listesReponses;
               listereponses.push(data);
               Parcourt.listesReponses = listereponses;
             }else{
               
               listereponses.push(data);
               Parcourt.listesReponses = listereponses;
             }
 
             if(data.questions_next!=undefined){
 
                 if(data.questions_next.type_reponse == "entier"){
                   this.navCtrl.push('NombrepetitPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                 }else if(data.questions_next.type_reponse == "text"){
                   this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                 }else if(data.questions_next.type_reponse == "select"){
                   this.navCtrl.push('OuestanimalPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                 }else if(data.questions_next.type_reponse == "rapide"){
                   this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                 }else if(data.questions_next.type_reponse == "annuler"){
 
                   var mymessage = ""
 
                   if(this.langue == 1){
                     mymessage = data.questions_next.titre_fr;
                   }else{
                     mymessage = data.questions_next.titre_en;
                   }
                   
                   this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe((langs:Array<string>) => {
                     let alert = this.alertCtrl.create({
                       title: langs['NAVIGATIONERROR'],
                       message: mymessage,
                       buttons: [
                         {
                           text: langs['CANCELD'],
                           role: 'cancel',
                           handler: () => {
                             Parcourt.listesReponses = [];
                             this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                           }
                         }
                       ]
                     });
                     alert.present();
                   })
 
                   
 
 
                 }
             }else{
               this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
             }
             */
            //////////////////////////////////////////////// END ONLINE //////////////////////////
            if (data.questions_next != undefined) {
                this.services.getQuestionById(data.questions_next).then(function (result) {
                    var questions_next = result[0];
                    if (questions_next.type_reponse == "entier") {
                        _this.navCtrl.push('NombrepetitPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "text") {
                        _this.navCtrl.push('DescriptionPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "select") {
                        _this.navCtrl.push('OuestanimalPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "rapide") {
                        _this.navCtrl.push('DescriptionPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "annuler") {
                        var mymessage = "";
                        if (_this.langue == 1) {
                            mymessage = questions_next.titre_fr;
                        }
                        else {
                            mymessage = questions_next.titre_en;
                        }
                        _this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe(function (langs) {
                            var alert = _this.alertCtrl.create({
                                title: langs['NAVIGATIONERROR'],
                                message: mymessage,
                                buttons: [
                                    {
                                        text: langs['CANCELD'],
                                        role: 'cancel',
                                        handler: function () {
                                            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = [];
                                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                        }
                                    }
                                ]
                            });
                            alert.present();
                        });
                    }
                });
            }
            else {
                this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
            }
            ///////////////////////////////////////////////////////////////////////////////
        }
        else {
            if (data.questions_next != undefined) {
                this.services.getQuestionById(data.questions_next).then(function (result) {
                    var questions_next = result[0];
                    if (questions_next.type_reponse == "entier") {
                        _this.navCtrl.push('NombrepetitPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "text") {
                        _this.navCtrl.push('DescriptionPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "select") {
                        _this.navCtrl.push('OuestanimalPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "rapide") {
                        _this.navCtrl.push('DescriptionPage', { idquestion: questions_next.id, type_question: questions_next.type_reponse });
                    }
                    else if (questions_next.type_reponse == "annuler") {
                        var mymessage = "";
                        if (_this.langue == 1) {
                            mymessage = questions_next.titre_fr;
                        }
                        else {
                            mymessage = questions_next.titre_en;
                        }
                        _this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe(function (langs) {
                            var alert = _this.alertCtrl.create({
                                title: langs['NAVIGATIONERROR'],
                                message: mymessage,
                                buttons: [
                                    {
                                        text: langs['CANCELD'],
                                        role: 'cancel',
                                        handler: function () {
                                            __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = [];
                                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                        }
                                    }
                                ]
                            });
                            alert.present();
                        });
                    }
                });
            }
            else {
                this.navCtrl.push('DescriptionPage', { question: 0, reponse: 0 });
            }
        }
    };
    OuestanimalPage.prototype.changeStatus = function (data) {
        var _this = this;
        this.listesreponses.forEach(function (element) {
            if (element.id == data.id) {
                data.responses = element;
                /*console.log(data.reponses);
                console.log(this.userconnecte);
                console.log("==================================== " + typeof data.responses.questions_next + " ======================")
                if(typeof data.responses.questions_next === 'undefined'){
                  console.log("==================== ");
                  console.log("================================");
                  console.log("================================");
                }*/
                if ((undefined != data.responses["questions_next"]) && (data.responses["questions_next"] != null)) {
                    // dans le cas ou il ya la connexion internet 
                    if (navigator.onLine) {
                        ///////////////////////////////////////////////////////// ONLINE/////////////////////////////////////
                        /* if(data.responses.questions_next["type_reponse"] == "text"){
            
                           // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question
                           // et pour cella nous devons recupérer le projet en cours.
                           var listereponses = [];
                           if(Parcourt.listesReponses != null){
                            listereponses = Parcourt.listesReponses;
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                            }else{
                              
                              listereponses.push(data.responses);
                              Parcourt.listesReponses = listereponses;
                            }
                            if(undefined != this.userconnecte.projet["questions"]){
              
                               
                                if(this.userconnecte.projet.questions["type_reponse"] == "select"){
                                    this.super(this.userconnecte.projet.questions.id);
                                }
        
        
                                if(data.questions_next.type_reponse == "entier"){
                                  this.navCtrl.push('NombrepetitPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                }else if(data.questions_next.type_reponse == "text"){
                                  this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                }else if(data.questions_next.type_reponse == "select"){
                                  this.navCtrl.push('OuestanimalPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                }else if(data.questions_next.type_reponse == "rapide"){
                                  this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                }else if(data.questions_next.type_reponse == "annuler"){
                                  this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                                }
                              
                            }else{
                              var listereponses = [];
                              if(Parcourt.listesReponses != null){
                                listereponses = Parcourt.listesReponses;
                                listereponses.push(data.responses);
                                Parcourt.listesReponses = listereponses;
                              }else{
                                
                                listereponses.push(data.responses);
                                Parcourt.listesReponses = listereponses;
                              }
        
                              if(data.questions_next.type_reponse == "entier"){
                                this.navCtrl.push('NombrepetitPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                              }else if(data.questions_next.type_reponse == "text"){
                                this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                              }else if(data.questions_next.type_reponse == "select"){
                                this.navCtrl.push('OuestanimalPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                              }else if(data.questions_next.type_reponse == "rapide"){
                                this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                              }else if(data.questions_next.type_reponse == "annuler"){
                                this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                              }
                              //this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id});
                             
                           }
        
                            
                        }
                        if(data.responses.questions_next["type_reponse"] == "select"){
                          var listereponses = [];
                          if(Parcourt.listesReponses != null){
                            listereponses = Parcourt.listesReponses;
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }else{
                            
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }
                          this.super(data.responses.questions_next.id);
                        }
        
                        if(data.responses.questions_next["type_reponse"] == "entier"){
                          var listereponses = [];
                          if(Parcourt.listesReponses != null){
                            listereponses = Parcourt.listesReponses;
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }else{
                            
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }
                          this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id});
                        }*/
                        /////////////////////////////////////////////////////// FOR PROD ////////////////////////////////////////////
                        /*let loading = this.loadingCtrl.create();
                         loading.present();
              
                          this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                                loading.dismiss();
        
                                data.responses = {...data.responses, questions:result[0]};
                                var listereponses = [];
                                
                                if(Parcourt.listesReponses != null){
                                  listereponses = Parcourt.listesReponses;
                                  listereponses.push(data.responses);
                                  Parcourt.listesReponses = listereponses;
                                }else{
                                  
                                  listereponses.push(data.responses);
                                  Parcourt.listesReponses = listereponses;
                                }
        
        
        
        
                                 
                                this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{
        
                                      if(result2[0].type_reponse == "text"){
                                        // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                        
                                        this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                      }
                                      if(result2[0].type_reponse == "entier"){
                                        // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                        
                                        this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                      }
                                      if(result2[0].type_reponse == "select"){
                                        this.super(result2[0].id);
                                      }
        
                                    
                                });
        
        
                                
                          });*/
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
                        _this.services.getQuestionById(data.responses["questions"]).then(function (result) {
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            var listereponses = [];
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            _this.services.getQuestionById(data.responses["questions_next"]).then(function (result2) {
                                //alert(JSON.stringify(result2));
                                /*if(result2[0].type_reponse == "text"){
                                  // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                  
                                  this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                }
                                if(result2[0].type_reponse == "entier"){
                                  // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                  
                                  this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                }
                                if(result2[0].type_reponse == "select"){
                                  this.super(result2[0].id);
                                }*/
                                if (result2[0].type_reponse == "text") {
                                    // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question 
                                    // et pour cella nous devons recupérer le projet en cours.
                                    var listereponses = [];
                                    /*if(Parcourt.listesReponses != null){
                                     listereponses = Parcourt.listesReponses;
                                     listereponses.push(data.responses);
                                     Parcourt.listesReponses = listereponses;
                                     }else{
                                       
                                       listereponses.push(data.responses);
                                       Parcourt.listesReponses = listereponses;
                                     }*/
                                    if (undefined != _this.userconnecte.projet["questions"]) {
                                        /*if(this.userconnecte.projet.questions["type_reponse"] == "text"){
                                            this.navCtrl.push('NombrepetitPage', {idquestion: this.userconnecte.projet.questions.questions.id});
                                        }*/
                                        if (_this.userconnecte.projet.questions["type_reponse"] == "select") {
                                            _this.super(_this.userconnecte.projet.questions.id);
                                        }
                                        if (result2[0].type_reponse == "entier") {
                                            _this.navCtrl.push('NombrepetitPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "text") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "select") {
                                            _this.navCtrl.push('OuestanimalPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "rapide") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "annuler") {
                                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                        }
                                    }
                                    else {
                                        /*var listereponses = [];
                                        if(Parcourt.listesReponses != null){
                                          listereponses = Parcourt.listesReponses;
                                          listereponses.push(data.responses);
                                          Parcourt.listesReponses = listereponses;
                                        }else{
                                          
                                          listereponses.push(data.responses);
                                          Parcourt.listesReponses = listereponses;
                                        }*/
                                        if (result2[0].type_reponse == "entier") {
                                            _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "text") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "select") {
                                            _this.navCtrl.push('OuestanimalPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "rapide") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "annuler") {
                                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                        }
                                        //this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id}); 
                                    }
                                }
                                if (result2[0].type_reponse == "select") {
                                    /*var listereponses = [];
                                    if(Parcourt.listesReponses != null){
                                      listereponses = Parcourt.listesReponses;
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }else{
                                      
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }*/
                                    _this.super(result2[0].id);
                                }
                                if (result2[0].type_reponse == "entier") {
                                    /*var listereponses = [];
                                    if(Parcourt.listesReponses != null){
                                      listereponses = Parcourt.listesReponses;
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }else{
                                      
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }*/
                                    _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id });
                                }
                                if (result2[0].type_reponse == "annuler") {
                                    /*var listereponses = [];
                                    if(Parcourt.listesReponses != null){
                                      listereponses = Parcourt.listesReponses;
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }else{
                                      
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }*/
                                    var mymessage = "";
                                    if (_this.langue == 0) {
                                        mymessage = result2[0].titre_fr;
                                    }
                                    else {
                                        mymessage = result2[0].titre_en;
                                    }
                                    _this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe(function (langs) {
                                        var alert = _this.alertCtrl.create({
                                            title: langs['NAVIGATIONERROR'],
                                            message: mymessage,
                                            buttons: [
                                                {
                                                    text: langs['CANCELD'],
                                                    role: 'cancel',
                                                    handler: function () {
                                                        __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = [];
                                                        _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                                    }
                                                }
                                            ]
                                        });
                                        alert.present();
                                    });
                                }
                            });
                        });
                    }
                    else {
                        // dans le cas ou il n'ya pas de connexion internet  this.type_question = this.navParams.get("type_question");
                        _this.services.getQuestionById(data.responses["questions"]).then(function (result) {
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            var listereponses = [];
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            _this.services.getQuestionById(data.responses["questions_next"]).then(function (result2) {
                                //alert(JSON.stringify(result2));
                                /*if(result2[0].type_reponse == "text"){
                                  // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                  
                                  this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                }
                                if(result2[0].type_reponse == "entier"){
                                  // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                  
                                  this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                }
                                if(result2[0].type_reponse == "select"){
                                  this.super(result2[0].id);
                                }*/
                                if (result2[0].type_reponse == "text") {
                                    // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question 
                                    // et pour cella nous devons recupérer le projet en cours.
                                    /*data.responses = {...data.responses, questions:result2[0]};
                                    var listereponses = [];
                                    if(Parcourt.listesReponses != null){
                                     listereponses = Parcourt.listesReponses;
                                     listereponses.push(data.responses);
                                     Parcourt.listesReponses = listereponses;
                                     }else{
                                       
                                       listereponses.push(data.responses);
                                       Parcourt.listesReponses = listereponses;
                                     }*/
                                    if (undefined != _this.userconnecte.projet["questions"]) {
                                        /*if(this.userconnecte.projet.questions["type_reponse"] == "text"){
                                            this.navCtrl.push('NombrepetitPage', {idquestion: this.userconnecte.projet.questions.questions.id});
                                        }*/
                                        if (_this.userconnecte.projet.questions["type_reponse"] == "select") {
                                            _this.super(_this.userconnecte.projet.questions.id);
                                        }
                                        if (result2[0].type_reponse == "entier") {
                                            _this.navCtrl.push('NombrepetitPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "text") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "select") {
                                            _this.navCtrl.push('OuestanimalPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "rapide") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.userconnecte.projet.questions.questions.id, type_question: _this.userconnecte.projet.questions.questions_next.type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "annuler") {
                                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                        }
                                    }
                                    else {
                                        /*var listereponses = [];
                                        if(Parcourt.listesReponses != null){
                                          listereponses = Parcourt.listesReponses;
                                          listereponses.push(data.responses);
                                          Parcourt.listesReponses = listereponses;
                                        }else{
                                          
                                          listereponses.push(data.responses);
                                          Parcourt.listesReponses = listereponses;
                                        }*/
                                        if (result2[0].type_reponse == "entier") {
                                            _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "text") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "select") {
                                            _this.navCtrl.push('OuestanimalPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "rapide") {
                                            _this.navCtrl.push('DescriptionPage', { idquestion: result2[0].id, type_question: result2[0].type_reponse });
                                        }
                                        else if (result2[0].type_reponse == "annuler") {
                                            _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                        }
                                        //this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id}); 
                                    }
                                }
                                if (result2[0].type_reponse == "select") {
                                    /*var listereponses = [];
                                    if(Parcourt.listesReponses != null){
                                      listereponses = Parcourt.listesReponses;
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }else{
                                      
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }*/
                                    _this.super(result2[0].id);
                                }
                                if (result2[0].type_reponse == "entier") {
                                    /*var listereponses = [];
                                    if(Parcourt.listesReponses != null){
                                      listereponses = Parcourt.listesReponses;
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }else{
                                      
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }*/
                                    _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id });
                                }
                                if (result2[0].type_reponse == "annuler") {
                                    /*var listereponses = [];
                                    if(Parcourt.listesReponses != null){
                                      listereponses = Parcourt.listesReponses;
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }else{
                                      
                                      listereponses.push(data.responses);
                                      Parcourt.listesReponses = listereponses;
                                    }*/
                                    var mymessage = "";
                                    if (_this.langue == 0) {
                                        mymessage = result2[0].titre_fr;
                                    }
                                    else {
                                        mymessage = result2[0].titre_en;
                                    }
                                    _this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe(function (langs) {
                                        var alert = _this.alertCtrl.create({
                                            title: langs['NAVIGATIONERROR'],
                                            message: mymessage,
                                            buttons: [
                                                {
                                                    text: langs['CANCELD'],
                                                    role: 'cancel',
                                                    handler: function () {
                                                        __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = [];
                                                        _this.navCtrl.setRoot('AcceuilPage').then(function () { });
                                                    }
                                                }
                                            ]
                                        });
                                        alert.present();
                                    });
                                }
                            });
                        });
                    }
                }
                else {
                    if (navigator.onLine) {
                        ///////////////////////////////////////////////////// ONLINE /////////////////////////////////
                        /*var listereponses = [];
                       
                        if(Parcourt.listesReponses != null){
                          
                          listereponses = Parcourt.listesReponses;
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }else{
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }
          
                        this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                        */
                        /////////////////////////////////////////////////////////// FOR PROD ////////////////////////////////////
                        /*this.services.getQuestionById(element.questions).then((result) =>{
                            
                          var listereponses = [];
                          
                          //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                          data.responses = {...data.responses, questions:result[0]};
          
                        
                          if(Parcourt.listesReponses != null){
                            listereponses = Parcourt.listesReponses;
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }else{
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }
          
                          //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                          //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
          
                          this.testvalue = 1;
                          
                          this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                          //this.navCtrl.push('SirinPage', {question: 0, reponse:0});
          
                      });*/
                        /////////////////////////////////////////////////// ENONLINE /////////////////////////////////////
                        _this.services.getQuestionById(element.questions).then(function (result) {
                            var listereponses = [];
                            //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            _this.testvalue = 1;
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        });
                    }
                    else {
                        _this.services.getQuestionById(element.questions).then(function (result) {
                            var listereponses = [];
                            //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            _this.testvalue = 1;
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                        });
                    }
                }
            }
        });
    };
    OuestanimalPage.prototype.super = function (idquestion) {
        var _this = this;
        if (idquestion != 0) {
            console.log(idquestion);
            if (navigator.onLine) {
                //////////////////////////////////////////////////////////////ONLINE ///////////////////////////////////
                /*let loading = this.loadingCtrl.create();
                loading.present();
                this.services.findReponseByQuestionId(idquestion).subscribe((result) =>{
                  this.listesreponses = result;
                  console.log(idquestion);
                  console.log(result);
                  console.log(result[0].questions);
                  this.maquestion = result[0].questions;
                  if(this.maquestion.image!=undefined){
                    this.printpicture =1;
                    this.myimage =this.maquestion.image;
                  }else{
                    //alert("le mokai");
                  }
                  if(this.maquestion.isPicture){
                    this.ispicture = 1;
                  }
                  
                  console.log(this.listesreponses);
                  
                  if(this.langue == 0){
                    this.titrequestion = result[0].questions.titre_fr;
                  }else{
                    this.titrequestion = result[0].questions.titre_en;
                  }
                }, (error)=>{
                  console.log(error);
                  //votre opération a  echoué!!
                  loading.dismiss();
                }, ()=>{
                  loading.dismiss();
                });*/
                ///////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////// PROD ////////////////////////////////////
                /*let loading = this.loadingCtrl.create();
                loading.present();
                this.services.getReponseByQuestionId(idquestion).then((result) =>{
                      loading.dismiss();
                      this.listesreponses = result;
                      this.services.getQuestionById(idquestion).then((result) =>{
                            loading.dismiss();
                            this.titrequestion = result[0].titre_fr;
                      });
                     
                });*/
                /////////////////////////////////////////////////////////////////////////////////
                this.services.getReponseByQuestionId(idquestion).then(function (result) {
                    _this.listesreponses = result;
                    _this.services.getQuestionById(idquestion).then(function (result) {
                        _this.maquestion = result[0];
                        if (_this.maquestion.image != undefined && _this.maquestion.image != null && _this.maquestion.image != "null") {
                            _this.printpicture = 1;
                            _this.myimage = _this.maquestion.image;
                        }
                        else {
                            //alert("le mokai");
                        }
                        if (_this.maquestion.isPicture == "true") {
                            _this.ispicture = 1;
                        }
                        if (_this.langue == 0) {
                            _this.titrequestion = result[0].titre_fr;
                        }
                        else {
                            _this.titrequestion = result[0].titre_en;
                        }
                    });
                });
            }
            else {
                this.services.getReponseByQuestionId(idquestion).then(function (result) {
                    _this.listesreponses = result;
                    _this.services.getQuestionById(idquestion).then(function (result) {
                        _this.maquestion = result[0];
                        if (_this.maquestion.image != undefined && _this.maquestion.image != null && _this.maquestion.image != "null") {
                            _this.printpicture = 1;
                            _this.myimage = _this.maquestion.image;
                        }
                        else {
                            //alert("le mokai");
                        }
                        if (_this.maquestion.isPicture == "true") {
                            _this.ispicture = 1;
                        }
                        if (_this.langue == 0) {
                            _this.titrequestion = result[0].titre_fr;
                        }
                        else {
                            _this.titrequestion = result[0].titre_en;
                        }
                    });
                });
            }
        }
    };
    OuestanimalPage.prototype.siren = function (data) {
        var _this = this;
        this.testvalue = 1;
        this.listesreponses.forEach(function (element) {
            if (element.id == data.id) {
                data.responses = element;
                /*console.log(data.reponses);
                console.log(this.userconnecte);
                console.log("==================================== " + typeof data.responses.questions_next + " ======================")
                if(typeof data.responses.questions_next === 'undefined'){
                  console.log("==================== ");
                  console.log("================================");
                  console.log("================================");
                }*/
                if ((undefined != data.responses["questions_next"]) && (data.responses["questions_next"] != null)) {
                    // dans le cas ou il ya la connexion internet 
                    if (navigator.onLine) {
                        //////////////////////////ONLINE/////////////////////////////////////////
                        /*if(data.responses.questions_next["type_reponse"] == "text"){
           
                          // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question
                          // et pour cella nous devons recupérer le projet en cours.
                          var listereponses = [];
                          if(Parcourt.listesReponses != null){
                           listereponses = Parcourt.listesReponses;
                           listereponses.push(data.responses);
                           Parcourt.listesReponses = listereponses;
                           }else{
                             
                             listereponses.push(data.responses);
                             Parcourt.listesReponses = listereponses;
                           }
                           if(undefined != this.userconnecte.projet["questions"]){
             
                               if(this.userconnecte.projet.questions["type_reponse"] == "text"){
                                   this.navCtrl.push('NombrepetitPage', {idquestion: this.userconnecte.projet.questions.questions.id});
                                 }
                               if(this.userconnecte.projet.questions["type_reponse"] == "select"){
                                   this.super(this.userconnecte.projet.questions.id);
                               }
                             
                           }else{
                             var listereponses = [];
                             if(Parcourt.listesReponses != null){
                               listereponses = Parcourt.listesReponses;
                               listereponses.push(data.responses);
                               Parcourt.listesReponses = listereponses;
                             }else{
                               
                               listereponses.push(data.responses);
                               Parcourt.listesReponses = listereponses;
                             }
                             this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id});
                            
                          }
       
                           
                       }
                       if(data.responses.questions_next["type_reponse"] == "select"){
                         var listereponses = [];
                         if(Parcourt.listesReponses != null){
                           listereponses = Parcourt.listesReponses;
                           listereponses.push(data.responses);
                           Parcourt.listesReponses = listereponses;
                         }else{
                           
                           listereponses.push(data.responses);
                           Parcourt.listesReponses = listereponses;
                         }
                         this.super(data.responses.questions_next.id);
                       }*/
                        /* let loading = this.loadingCtrl.create();
                          loading.present();
               
                           this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                                 loading.dismiss();
         
                                 data.responses = {...data.responses, questions:result[0]};
                                 var listereponses = [];
                                 
                                 if(Parcourt.listesReponses != null){
                                   listereponses = Parcourt.listesReponses;
                                   listereponses.push(data.responses);
                                   Parcourt.listesReponses = listereponses;
                                 }else{
                                   
                                   listereponses.push(data.responses);
                                   Parcourt.listesReponses = listereponses;
                                 }
         
         
         
         
                                  
                                 this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{
         
                                     
         
                                       if(result2[0].type_reponse == "text"){
                                         // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                         
                                         this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                       }
                                       if(result2[0].type_reponse == "entier"){
                                         // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                         
                                         this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id});
                                       }
                                       if(result2[0].type_reponse == "select"){
                                         this.super(result2[0].id);
                                       }
                                 });
         
         
                                 
                           });*/
                        //////////////////////////////////////////////////END ONLINE////////////////////////////////////////////
                        ///////////////////////////////////////////////// LOCAL ///////////////////////////////////////////////
                        _this.services.getQuestionById(data.responses["questions"]).then(function (result) {
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            var listereponses = [];
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            _this.services.getQuestionById(data.responses["questions_next"]).then(function (result2) {
                                if (result2[0].type_reponse == "text") {
                                    // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                    _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id });
                                }
                                if (result2[0].type_reponse == "entier") {
                                    // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                    _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id });
                                }
                                if (result2[0].type_reponse == "select") {
                                    _this.super(result2[0].id);
                                }
                            });
                        });
                        //////////////////////////////////////////////////// END LOCAL //////////////////////////////////////////////
                    }
                    else {
                        // dans le cas ou il n'ya pas de connexion internet  this.type_question = this.navParams.get("type_question");
                        _this.services.getQuestionById(data.responses["questions"]).then(function (result) {
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            var listereponses = [];
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            _this.services.getQuestionById(data.responses["questions_next"]).then(function (result2) {
                                if (result2[0].type_reponse == "text") {
                                    // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                    _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id });
                                }
                                if (result2[0].type_reponse == "entier") {
                                    // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                    _this.navCtrl.push('NombrepetitPage', { idquestion: result2[0].id });
                                }
                                if (result2[0].type_reponse == "select") {
                                    _this.super(result2[0].id);
                                }
                            });
                        });
                    }
                }
                else {
                    if (navigator.onLine) {
                        /////////////////////////////////////////////////////////////// ONLINE ////////////////////////////////////
                        /*var listereponses = [];
                        if(Parcourt.listesReponses != null){ this.type_question = this.navParams.get("type_question");
                          listereponses = Parcourt.listesReponses;
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }else{
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }
  
                        
                      //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                        //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                        //this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                        this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                       */
                        /////////////////////////////////////////////////////////////// END ONLINE /////////////////////////////////
                        ////////////////////////////////////////////////////////////// LOCAL ///////////////////////////////////////
                        _this.services.getQuestionById(element.questions).then(function (result) {
                            var listereponses = [];
                            //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                            //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                            //this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                        });
                        //////////////////////////////////////////////////////END LOCAL /////////////////////////////////////////
                    }
                    else {
                        _this.services.getQuestionById(element.questions).then(function (result) {
                            var listereponses = [];
                            //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                            data.responses = __assign({}, data.responses, { questions: result[0] });
                            if (__WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses != null) {
                                listereponses = __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses;
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            else {
                                listereponses.push(data.responses);
                                __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = listereponses;
                            }
                            //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                            //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                            _this.navCtrl.push('DescriptionPage', { idquestion: _this.idquestion, type_question: _this.type_question });
                            //this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                        });
                    }
                }
            }
        });
    };
    OuestanimalPage.prototype.updateColor = function (color) {
        document.documentElement.style.setProperty("--color", color);
    };
    OuestanimalPage.prototype.backFunction = function (color) {
        __WEBPACK_IMPORTED_MODULE_5__configs_configs__["b" /* Parcourt */].listesReponses = [];
        this.navCtrl.setRoot('AcceuilPage').then(function () { });
    };
    OuestanimalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-ouestanimal',template:/*ion-inline-start:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/ouestanimal/ouestanimal.html"*/'<ion-header>\n    <navbar [title]= "titrequestion" ></navbar>\n</ion-header>\n\n\n<ion-content padding class="backgroundcorps">\n\n    <p style="text-align: center; font-weight: bold; font-size: 20px;">{{titrequestion}}</p>\n\n<div *ngIf="ispicture==0">\n\n    <ion-list radio-group [(ngModel)]="data.id" >\n\n        <ion-card class="moncardre" *ngFor="let item of listesreponses">\n            <ion-icon *ngIf="(langue == 0) && (item.taille == \'true\')" name="add-circle" (click)="mokaiAlert(item.titre_fr)"></ion-icon>\n            <ion-icon *ngIf="(langue == 1) && (item.taille == \'true\')" name="add-circle" (click)="mokaiAlert(item.titre_en)"></ion-icon>\n            <img *ngIf="item.is_picture" class="image" src="{{item.image}}">\n            <ion-item style="text-align: justify; height: 30%;">\n                <ion-label  *ngIf="langue == 0">{{item.titre_fr}}  </ion-label>\n                <ion-label  *ngIf="langue == 1">{{item.titre_en}} </ion-label>\n                <!--<ion-radio value="{{item.id}}" checked></ion-radio>-->\n                <ion-radio value="{{item.id}}" (ionSelect)="changeStatus(item)"></ion-radio>\n                 \n            </ion-item>\n        </ion-card>  \n      </ion-list>\n\n</div>\n  \n<div *ngIf="ispicture==1">\n\n    <img src="{{myimage}}" style="width: 100%; height: 50%;" *ngIf="printpicture==1">\n    \n    <ion-grid>\n        <ion-row >\n                <ion-col col-6  *ngFor="let item of listesreponses">\n                    <ion-icon *ngIf="(langue == 0) && (item.taille == \'true\')" name="add-circle" (click)="mokaiAlert(item.titre_fr)"></ion-icon>\n                    <ion-icon *ngIf="(langue == 1) && (item.taille == \'true\')" name="add-circle" (click)="mokaiAlert(item.titre_en)"></ion-icon>\n                    <div class="cardpremieracceuil" (click)="changeStatus1(item)">\n                    <img class="premier" src="{{item.image}}" />\n                    <strong><p class="textcard" *ngIf="langue == 0">{{item.titre_fr}}</p></strong>\n                    <strong><p class="textcard" *ngIf="langue == 1">{{item.titre_en}}</p></strong>\n                    </div>\n                </ion-col>\n                \n        </ion-row>\n    </ion-grid>\n\n    \n\n</div>\n  \n\n<ion-row class="monbutonvalide">\n    <ion-col>\n      <button  (click)="backFunction()" ion-fab left bottom  color="danger"> <ion-icon name="md-close"></ion-icon> </button>\n    </ion-col>\n</ion-row>\n\n  <!--<div class="blockbutton">\n      <button  ion-button class="colors"  block (click)="siren(data)">\n          {{ \'SUIVANT\' | translate }}\n          <ion-icon name="arrow-forward" float-right class="monicon"></ion-icon>\n      </button>\n   </div>-->\n</ion-content>\n'/*ion-inline-end:"/home/doumtsop/Bureau/CODE/IONIC/Siren/src/pages/ouestanimal/ouestanimal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_7_ng2_translate__["c" /* TranslateService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_6__providers_services_language__["a" /* LanguageService */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_3__providers_localstorage_localstorage__["a" /* LocalStorageProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_services_services__["a" /* ServicesProvider */]])
    ], OuestanimalPage);
    return OuestanimalPage;
}());

//# sourceMappingURL=ouestanimal.js.map

/***/ })

});
//# sourceMappingURL=7.js.map