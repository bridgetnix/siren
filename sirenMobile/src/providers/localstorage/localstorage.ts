import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import { Session } from '../../configs/configs';

/*
  Generated class for the LocalStorageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalStorageProvider {
  private key = 'sirensession';

  constructor(private storage: Storage) {
    
  }

  storeSession(data) {
    Session.utilisateur = data;
    return this.storage.set(this.key, data);
  }

  getSession() {
    return new Promise((resolve, failed) => {
      this.storage.get(this.key).then((data) => {
        resolve(data);
      }).catch((error) => {
        failed();
      });
    });
  }

  //store key
  setKey(key, value) {
    return this.storage.set(key, value);
  }

  //get the stored key
  getKey(key) {
    return this.storage.get(key);
  }

  //delete key
  removeKey(key) {
    return this.storage.remove(key);
  }

  //clear the whole local storage
  clearStorage() {
    return this.storage.clear();
  }

  logut(){
    this.storage.remove(this.key);
  }
}
