import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

/*
  Generated class for the ConnectivityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConnectivityProvider {

  onDevice: boolean;

  constructor(public http: HttpClient, public platform: Platform) {
    this.onDevice = this.platform.is('cordova');
  }

  isOnline(): boolean{
     return navigator.onLine;
  }

  isOffline(): boolean{
     return !navigator.onLine;
  }

}
