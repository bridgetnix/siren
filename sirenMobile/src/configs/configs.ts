
export var Session:any = {
    utilisateur:null,
    toke:null
};

export const Languages = [
    {value:"fr", name:"francais"},
    {value:"en", name:"anglais"}
];

export var LANG:any = {
    value:"fr"
};

export function formatNumberOfDate(val) {
    val = Number(val);
    return (val < 10) ? `0${val}` : val;
}

export function showDateAndTime_2(datetime_mysql: string, lang = 'fr') {
    const d = new Date(datetime_mysql),
      year = d.getFullYear(),
      month = formatNumberOfDate(`${d.getMonth() + 1}`),
      day = formatNumberOfDate(`${d.getDate()}`),
      hours = formatNumberOfDate(`${d.getHours()}`),
      minutes = formatNumberOfDate(`${d.getMinutes()}`),
      seconds = formatNumberOfDate(`${d.getSeconds()}`);
  
    if (lang === 'fr') {
      return `${day}/${month}/${year} à ${hours}:${minutes}:${seconds}`;
    } else if (lang === 'en') {
      return `${year}-${month}-${day} at ${hours}:${minutes}:${seconds}`;
    } else {
      return `${year}-${month}-${day} at ${hours}:${minutes}:${seconds}`;
    }
  }

export var Parcourt: any = {
    typeobservation:null,
    indicateur:null,
    latitude:null,
    longitude:null,
    groupe:null,
    sousgroupe:null,
    espece:null,
    listesReponses:[],
    observation: null,
    description: null,
    images:null,
    couleur: null
};
