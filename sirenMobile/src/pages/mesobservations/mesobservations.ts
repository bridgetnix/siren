import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';
import moment from 'moment';
import { LanguageService } from '../../providers/services/language';
import { TranslateService } from 'ng2-translate';

/**
 * Generated class for the MesobservationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mesobservations',
  templateUrl: 'mesobservations.html',
})
export class MesobservationsPage {

  public mesobservations:any;
  public userconnecte = {};
  public projetId:any;
  public observationchoisie = {}; 
  public montypeObservations = "";
  public monsousgroupe = "";
  public mongroupe = "";
  public monespece = "";
  public testeur = 0;
  public montesteur = 0;
  public detail = 0;
  public listequestions = [];
  public myuserconnecte: any;
  
  constructor(public translate:TranslateService, private alertCtrl: AlertController, public language:LanguageService, public navCtrl: NavController,private geolocation: Geolocation, public toastCtrl: ToastController, public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
    moment.locale('fr');
  }

  ionViewDidLoad() {

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });

   /* if(navigator.onLine){
  
        this.mylocalstorage.getSession().then((result) =>{
          this.userconnecte = result;
          this.projetId =  this.userconnecte["projet"].id;
          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.findObservationByUserId(this.userconnecte["id"], this.projetId).subscribe((resultat) =>{  
              
                this.mesobservations = resultat;
               
          
            }, (error)=>{
            console.log(error);
            //votre opération a  echoué!!
            loading.dismiss();
          }, ()=>{
            loading.dismiss();
          });
        })

    }else{*/

        this.mylocalstorage.getSession().then((result) =>{
            
            this.userconnecte = result;
            this.projetId =  this.userconnecte["projet"];
            this.services.getAllObservations().then((result: Array<any>) =>{
            //this.services.getAllObservationByUserIdAndProjet(this.userconnecte["id"], this.projetId).then((result) =>{
             
              this.mesobservations = result;
              /*result.forEach(element => {
                this.mesobservations.push({
                  ...element,
                  date: showDateAndTime_2(element.dateo)
                });
              });*/

            });


        })

        
        

  // } 

  }


  parseDate(date, format) {
    //var ladate = date.substring(0,16)+'Z'
    return moment(date).format(format);
  }



  ajouter(){
       
       this.navCtrl.setRoot('AcceuilPage');
  }


  details(element){


    this.detail=1;
    this.testeur = 4;
    
    this.observationchoisie = element;

    this.services.getAllTypeObsevationsById(element.typeObservations).then((resultat) =>{
       this.montypeObservations = resultat[0].nom_fr;
    });

    this.services.getAllSousGroupesById(element.sousgroupe).then((resultat:any) =>{
       if(resultat.length >0){
        this.monsousgroupe = resultat[0].nom_fr;
        this.montesteur = 1;
       }else{
         this.montesteur = 0;
       }
       
    });


    this.services.getAllGroupesById(element.groupe).then((resultat:any) =>{
      if(resultat.length >0){
        this.mongroupe = resultat[0].nom_fr;
        this.montesteur = 1;
      }else{
        this.montesteur = 0;
      }
        
    });

    this.services.getAllEspecesById(element.espece).then((resultat:any) =>{
      if(resultat.length >0){
        this.monespece = resultat[0].nom_fr;
        this.montesteur = 1;
      }else{
        this.montesteur = 0;
      }
    });

    
    this.services.getAllReultatsByObservationId(element.id).then((resultat:any) =>{
    
      resultat.forEach(element => {
        this.services.getQuestionById(element.questions).then((resultat) =>{
            var data = {
              contenu:element.contenu,
              questions:resultat[0].titre_fr
            }
            this.listequestions.push(data);
        });
        
      });
        
      
      
    });
}

  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

  backmyFunction(){
    this.detail =0;
  }

  deletteElement(element){
  

    this.translate.get(['DELETEOBSERVATION', 'MESSAGEDELETEOBSERVATION']).subscribe((langs:Array<string>) => {
      let alert = this.alertCtrl.create({
        title: langs['DELETEOBSERVATION'],
        message: langs['MESSAGEDELETEOBSERVATION'],
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: () => {
              this.services.deletteObservationByID(element.id).then((result: Array<any>) =>{

                this.mylocalstorage.getSession().then((result) =>{
            
                  this.userconnecte = result;
                  this.projetId =  this.userconnecte["projet"];
                  this.services.getAllObservations().then((result: Array<any>) =>{
                  //this.services.getAllObservationByUserIdAndProjet(this.userconnecte["id"], this.projetId).then((result) =>{
                   
                    this.mesobservations = result;
                    /*result.forEach(element => {
                      this.mesobservations.push({
                        ...element,
                        date: showDateAndTime_2(element.dateo)
                      });
                    });*/
      
                  });
      
      
              })


              });
            }
          }
        ]
      });
      alert.present();
    })

    
  }

}
