import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilsPage } from './profils';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { TranslateModule } from 'ng2-translate';

@NgModule({
  declarations: [
    ProfilsPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(ProfilsPage),
  ],
})
export class ProfilsPageModule {}
