import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';



/**
 * Generated class for the ProfilsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profils',
  templateUrl: 'profils.html',
})
export class ProfilsPage {

  
  public userconnecte = {};
  public myuserconnecte:any;
  testimage = 0;
  testcolor = 0;
  public pays: any;
  public nomprojet: any;
  public fonctions: any;
  public fonctionstmp: any;
  public paystmp: any;
  

  constructor(public navCtrl: NavController, private geolocation: Geolocation, public services: ServicesProvider, public mylocalstorage: LocalStorageProvider, public navParams: NavParams) {
    
  }

  ionViewDidLoad() {

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
         console.log("error could not get position");
    });


    this.mylocalstorage.getSession().then((result) =>{
        this.myuserconnecte = result;
        if(this.myuserconnecte!=null){
          console.log(this.myuserconnecte);
          console.log("===");
          
          if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
            console.log(this.myuserconnecte.projet);
            if((!undefined == this.myuserconnecte.projet.couleur)|| (this.myuserconnecte.projet.couleur!=null)){
              this.testcolor = 1;
            }
            if((!undefined == this.myuserconnecte.projet.image)|| (this.myuserconnecte.projet.image!=null)){
              this.testimage = 1;
            }
            
          }

        }
        
    })



    if(navigator.onLine){
      this.mylocalstorage.getSession().then((result) =>{
          this.userconnecte = result;
          this.pays = this.userconnecte["pays"].nom_fr;
          this.fonctions = this.userconnecte["fonctions"].nom_fr;
          this.nomprojet = this.userconnecte["projet"].nom;
      })
    
    }else{
          this.mylocalstorage.getSession().then((result) =>{
              /*this.userconnecte = result;
              this.paystmp = this.userconnecte["pays"];
              this.fonctionstmp = this.userconnecte["fonctions"];
              this.services.getAllPaysById(this.paystmp).then((result) =>{
                  this.pays = result[0].nom_fr;
              });
              this.services.getAllFonctionById(this.fonctionstmp).then((result) =>{
                  this.fonctions = result[0].nom_fr;
              });*/

              this.userconnecte = result;
              this.pays = this.userconnecte["pays"].nom_fr;
              this.fonctions = this.userconnecte["fonctions"].nom_fr;
              this.nomprojet = this.userconnecte["projet"].nom;
          })  

    }
  }

}
