import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { Parcourt } from '../../configs/configs';
import {Geolocation} from '@ionic-native/geolocation';
import { LanguageService } from '../../providers/services/language';
import { TranslateService } from 'ng2-translate';

/**
 * Generated class for the GroupePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-groupe',
  templateUrl: 'groupe.html',
})
export class GroupePage { 
  
  public langue=0;
  public listegroupes: any;

  constructor(public navCtrl: NavController, private geolocation: Geolocation, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider, public translate:TranslateService, public language:LanguageService) {
   
    this.geolocation.getCurrentPosition().then( (resp) =>{ 
            Parcourt.latitude = resp.coords.latitude;
            Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
      console.log("error could not get position");
    });
      
  
  }

  ionViewDidLoad() {


    this.language.loadLanguage().then((result) =>{
        if(result == 'en'){
          this.langue = 1;
        }else{
          this.langue = 0;
        }
    });
   
    if(navigator.onLine){
        /*let loading = this.loadingCtrl.create();
        loading.present();
        this.services.groupes().subscribe((result) =>{
              this.listegroupes = result;
        }, (error)=>{
          console.log(error);
          loading.dismiss();
        }, ()=>{
          loading.dismiss();
        });*/

        let loading = this.loadingCtrl.create();
        loading.present();
        this.services.getAllGroupes().then((result) =>{
              loading.dismiss();
              this.listegroupes = result;
        });

        
    }else{

      let loading = this.loadingCtrl.create();
      loading.present();
      this.services.getAllGroupes().then((result) =>{
            loading.dismiss();
            this.listegroupes = result;
      });

    }





  }

  sous_espece(groupe){
    
    Parcourt.groupe = groupe;
    this.navCtrl.push('SousEspecePage',{id:groupe.id});
    
  }


  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }


}
