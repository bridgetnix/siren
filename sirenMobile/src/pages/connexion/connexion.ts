import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, MenuController, AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { TranslateService } from 'ng2-translate';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';

/**
 * Generated class for the ConnexionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-connexion',
  templateUrl: 'connexion.html',
})
export class ConnexionPage {

  //public utilisateur = {};
  public userconnecte = {};
  public projetId: any;
  public mesprojets = [];
  public total = 0;
  public totaleffectue = 0;
  public utilisateur = {
    login:"",
    password:""
  };

  constructor(public translate:TranslateService,private geolocation: Geolocation, public alertCtrl: AlertController, public navCtrl: NavController, public mylocalstorage: LocalStorageProvider, public menuCtrl: MenuController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  
  }

  ionViewDidLoad() {
    this.total = 9;
    this.totaleffectue = 0;
    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });
  }

  


  login(utilisateur){

    let loading = this.loadingCtrl.create();
    loading.present();
    
    if(navigator.onLine){

      /*this.services.login(utilisateur).subscribe(result =>{
        if(result != 0){
          
            this.mylocalstorage.storeSession(result).then(() => {});
            loading.onDidDismiss(() => {
              //this.navCtrl.setRoot('PhotosPage');

              ////////////////////////////////////////////// NEW /////////////////////////

              if(navigator.onLine){
                    this.mylocalstorage.getSession().then((result:any) =>{
                          this.userconnecte = result;
                          let loading = this.loadingCtrl.create();
                          loading.present();
                          this.services.findProjetsByUserId(this.userconnecte["id"]).subscribe((resultat) =>{  
                              this.mesprojets = resultat;
                              console.log(this.mesprojets);
                              this.showRadio(this.mesprojets);
                          }, (error)=>{
                            console.log(error);
                            //votre opération a  echoué!!
                            loading.dismiss();
                          }, ()=>{
                            loading.dismiss();
                          });
                    })
                }else{
            
                  this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
                    this.presentToast(langs['INFOSCONNEXION']); 
                    this.navCtrl.setRoot('AcceuilPage');
                  });
                  
                }

                /////////////////////////////////////////////////////////////////////////////////
              
              //this.navCtrl.setRoot('AcceuilPage');
              
              this.menuCtrl.enable(true, 'sideMenu');
            });
        }else{
          this.translate.get(['INFOSLOGINV']).subscribe((langs:Array<string>) => {
            this.presentToast(langs['INFOSLOGINV']); 
          });
        }
      }, error =>{
        loading.dismiss();
        console.log(error);
      },() =>{
        loading.dismiss();
      });*/

      
      this.services.getAllCheckConnexion().then((result) =>{
      
           if( result==1){
                this.services.login(utilisateur).subscribe(result =>{
                  if(result != 0){
                      this.mylocalstorage.storeSession(result).then(() => {});
                      loading.onDidDismiss(() => {
                        this.navCtrl.setRoot('AcceuilPage');
                        this.menuCtrl.enable(true, 'sideMenu');
                      });
                  }else{
                    this.translate.get(['INFOSLOGINV']).subscribe((langs:Array<string>) => {
                      this.presentToast(langs['INFOSLOGINV']); 
                    });
                  }
                }, error =>{
                  loading.dismiss();
                  console.log(error);
                },() =>{
                  loading.dismiss();
                    
                });
           }else{

                          let connexions = {
                            valeur:0
                          };
                          connexions.valeur = 1;
               
                          
                          this.services.login(utilisateur).subscribe(result =>{
                            if(result != 0){
                                this.mylocalstorage.storeSession(result).then(() => {});

                                    this.services.createCheckConnexion(connexions).then((result) =>{});
                                        
                                        this.userconnecte = result;
                                        let loading = this.loadingCtrl.create();
                                        loading.present();
                                        this.services.findProjetsByUserId(this.userconnecte["id"]).subscribe((resultat) =>{  
                                            this.mesprojets = resultat;
                                            console.log(this.mesprojets);
                                            this.showRadio(this.mesprojets);
                                        }, (error)=>{
                                          console.log(error);
                                          //votre opération a  echoué!!
                                          loading.dismiss();
                                        }, ()=>{
                                          loading.dismiss();
                                        });
                                    

                                    let loading2 = this.loadingCtrl.create();
                                    loading2.present();  
                                    setInterval(()=>{
                                        loading2.dismiss();
                                    }, 15000);


                            }else{
                            
                              this.translate.get(['INFOSLOGINV']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['INFOSLOGINV']); 
                              });
                            }
                          }, error =>{
                            loading.dismiss();
                            console.log(error);
                          },() =>{
                            loading.dismiss();
                          });
                

           }
       })


       

          
            

    }else{


        this.services.findUtilisateursByLoginAndPassword(this.utilisateur).then((result) =>{
           if(result == 0){

            this.translate.get(['INFOSLOGIN']).subscribe((langs:Array<string>) => {
              this.presentToast(langs['INFOSLOGIN']); 
            });
            
            loading.dismiss();
          }else{

            this.services.getAllPaysById(result[0].pays).then((result2) =>{
                result[0].pays = result2[0];

                this.services.getAllFonctionById(result[0].fonctions).then((result3) =>{
                    result[0].fonctions = result3[0];
                    //this.services.getAllProjetById(result[0].projet).then((result4) =>{
                    this.services.getAllProjetById(result[0].projet).then((result4) =>{
                      result4[0].id = result4[0].rowid;
                      result[0].projet = result4[0];
                      this.mylocalstorage.storeSession(result[0]).then(() => {
                          loading.dismiss();
                          this.navCtrl.setRoot('AcceuilPage');
                          this.menuCtrl.enable(true, 'sideMenu');
                        });

                    });

                });
            });
            
           }
              

      })


    }
                
  }

  inscription(){
    this.navCtrl.setRoot('InscriptionPage');
  }

  forguetPassword(){

    if(navigator.onLine){

      window.open('http://siren.ammco.org/web/fr/resetting/request', '_blank');

      }else{

          this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
            this.presentToast(langs['INFOSCONNEXION']); 
          });
          
      }
  }


  showRadio(listeprojets){ 
    
    this.translate.get(['SELECTPROJET', 'ENREGISTRER', 'SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
          let alert = this.alertCtrl.create();

          alert.setTitle(langs['SELECTPROJET']);

          this.mesprojets.forEach(element => {
            let monimput = {
              type: 'radio',
              label: element.nom,
              value: element.id,
            }
            alert.addInput(monimput);
          });
          
          alert.addButton({
            text:langs['ENREGISTRER'],
            cssClass:'ion-button full',
            handler:data => {

              let loading = this.loadingCtrl.create();
              loading.present();
              
                
                this.mylocalstorage.getSession().then((result) =>{
                  this.userconnecte = result;
                 
                  this.utilisateur.login = this.userconnecte["email"];
                  this.utilisateur.password = this.userconnecte["password"];

                     this.services.changerProjet(this.userconnecte["id"], data).subscribe((resultat) =>{  
               
                              this.services.login(this.utilisateur).subscribe(result =>{
                                if(result != 0){

                                    this.mylocalstorage.storeSession(result).then(() => {
                                      
                                        this.Synchronisateur();
                                        this.navCtrl.setRoot('AcceuilPage');
                                        /*if(this.totaleffectue == this.total){
                                            loading.onDidDismiss(() => {
                                              this.navCtrl.setRoot('AcceuilPage');
                                              this.menuCtrl.enable(true, 'sideMenu');
                                            });
                                        }*/ 

                                    });
                                

                                }else{
                                  this.translate.get(['INFOSLOGINV']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['INFOSLOGINV']); 
                                  });
                                }
                              }, error =>{
                                loading.dismiss();
                                console.log(error);
                              },() =>{
                                loading.dismiss();
                                  
                              });


                    }, (error)=>{
                      console.log(error);
                      //votre opération a  echoué!!
                      loading.dismiss();
                  }, ()=>{
                    loading.dismiss();
                  });

                })



              
              
            }
          });

         alert.present();

      });

  }


  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  /* this.services.deletteAllPays().then((result1) =>{

  }) */;


 
  Synchronisateur(){




    this.services.deletteAllPays().then((result1) =>{
      let loading = this.loadingCtrl.create();
      loading.present();
        this.services.getpays().subscribe((result) =>{ 
              var i = 0;  
              result.forEach(element => {
                  this.services.createPays(element).then((result1) =>{
                      i++;
                      if(i==result.length){
                          this.totaleffectue = this.totaleffectue + 1;
                          loading.dismiss();
                          if(this.totaleffectue == this.total){
                            this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                            });
                          }
                      }
                  });
              });
        }, (error)=>{
          
         
        }, ()=>{
          
        });
    });
      

      ////////////////////////////////////////////////////////////////

      this.services.deletteAllFonctions().then((result1) =>{
            let loading = this.loadingCtrl.create();
            loading.present();
            this.services.getfonctions().subscribe((result) =>{   
                  var i=0;
                  result.forEach(element => {
                      this.services.createFonction(element).then((result1) =>{

                        i++;
                        if(i==result.length){
                            loading.dismiss();
                            this.totaleffectue = this.totaleffectue + 1;
                            if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                          
                      });
                  });
            }, (error)=>{
             
              
            }, ()=>{
             
            });
      })
      
      
      

      ////////////////////////////////////////////////////////////////
      this.services.deletteAllGroupes().then((result1) =>{
          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.groupes().subscribe((result) =>{  
                var i = 0; 
                result.forEach(element => {
                    this.services.createGroupe(element).then((result1) =>{
                      i++;
                      if(i==result.length){
                          loading.dismiss(); 
                          this.totaleffectue = this.totaleffectue + 1;
                          if(this.totaleffectue == this.total){
                            this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                            });
                          }
                      }  
                    });
                });
          }, (error)=>{
            
           
          }, ()=>{
            
          });
      });
     

      ////////////////////////////////////////////////////////////////

      this.services.deletteAllSousGroupes().then((result1) =>{
            let loading = this.loadingCtrl.create();
            loading.present();
            this.services.allsousgroupes().subscribe((result) =>{  
                  var i=0; 
                  result.forEach(element => {
                      let sous_groupe = {
                        "id": element.id, 
                        "nom_fr": element.nom_fr, 
                        "nom_en": element.nom_en, 
                        "image": element.image, 
                        "description_fr": element.description_fr, 
                        "description_en": element.description_en, 
                        "groupes": element.groupes.id
                      }
                      this.services.createSousGroupe(sous_groupe).then((result1) =>{
                        i++;
                        if(i==result.length){
                            loading.dismiss();
                            this.totaleffectue = this.totaleffectue + 1;
                            if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['SUCCESSOPERATION']); 
                                    this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                      });
                  });
            }, (error)=>{

                alert("uen erreur au rendez-vous c quoi le pb sousgroupe");
                alert(JSON.stringify(error));
              
            }, ()=>{
              
            });
      });
      ////////////////////////////////////////////////////////////////

      this.services.deletteAllReponses().then((result1) =>{
            let loading = this.loadingCtrl.create();
            loading.present();
            this.services.allreponses().subscribe((result) =>{ 
                  var i=0;  
                  result.forEach(element => {
                      
                      if(undefined!= element.questions_next){
                          let reponse = {};
                         
                          reponse = {
                            "id": element.id, 
                            "titre_fr": element.titre_fr, 
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id, 
                            "questions_next": element["questions_next"].id,
                            "image":element.image,
                            "taille":element.taille
                          }
                          this.services.createReponse(reponse).then((result1) =>{

                            i++;
                            if(i==result.length){
                                loading.dismiss();
                                this.totaleffectue = this.totaleffectue + 1;
                                if(this.totaleffectue == this.total){
                                    this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                        this.presentToast(langs['SUCCESSOPERATION']); 
                                        this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                          
                          });
                      }else{
                          let reponse = {};
                          reponse = {
                            "id": element.id, 
                            "titre_fr": element.titre_fr, 
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id,
                            "image":element.image,
                            "taille":element.taille
                      
                          }

                          this.services.createReponse(reponse).then((result1) =>{
                                i++;
                                if(i==result.length){
                                    
                                    this.totaleffectue = this.totaleffectue + 1;
                                    if(this.totaleffectue == this.total){
                                        this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                            this.presentToast(langs['SUCCESSOPERATION']); 
                                            this.navCtrl.setRoot('AcceuilPage');
                                        });
                                    }
                                }
                          });
                     }
                      
                  });
            }, (error)=>{
            }, ()=>{
             
            });
        });
      ////////////////////////////////////////////////////////////////

      this.services.deletteAlltypeObservation().then((result1) =>{
        this.mylocalstorage.getSession().then((result) =>{
            this.userconnecte = result;
          //this.services.acceuil().subscribe((result) =>{ 
            let loading = this.loadingCtrl.create();
            loading.present();
            this.services.projet_typeObservation(this.userconnecte["projet"].id).subscribe((result) =>{  
                var i=0;
                result.forEach(element => {
                    this.services.createTypeObservation(element).then((result1) =>{

                      i++;
                     
                      if(i==result.length){
                          loading.dismiss();
                          this.totaleffectue = this.totaleffectue + 1;
                          if(this.totaleffectue == this.total){
                            this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                            });
                          }
                      }
                        
                    });
                });
            }, (error)=>{
                console.log(error);
                
            }, ()=>{
                
            });
        })
      });
      ////////////////////////////////////////////////////////////////


      this.services.deletteAllProjets().then((result1) =>{
        let loading = this.loadingCtrl.create();
        loading.present();
         
          this.mylocalstorage.getSession().then((result) =>{
              
              this.userconnecte = result;
              
              this.services.findProjetsByUserId(this.userconnecte["id"]).subscribe((result) =>{   
                      var i=0;
                      result.forEach(element => {
                        var projet = {
                          "id": element.id, 
                          "nom": element.nom, 
                          "rowid":element.id,
                          "lieu": element.lieu, 
                          "public": element.public, 
                          "pays": element["pays"].id, 
                          "utilisateurs": element['utilisateurs'].id,
                          "note":element.note,
                          "logo":element.logo,
                          "mention":element.mention
                        }
                        
                        this.services.createProjet(projet).then((result1) =>{

                          i++;
                          if(i==result.length){
                              loading.dismiss();
                              this.totaleffectue = this.totaleffectue + 1;
                              if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['SUCCESSOPERATION']); 
                                    this.navCtrl.setRoot('AcceuilPage');
                                });
                              }
                          }
                            
                        });
                    });
              }, (error)=>{
                
              }, ()=>{
                
              });
          })

      })      

      ////////////////////////////////////////////////////////////////


      ////////////////////////////////////////////////////////////////


      this.services.deletteAllProjetMenu().then((result1) =>{

        let loading = this.loadingCtrl.create();
        loading.present();
         
        this.mylocalstorage.getSession().then((result) =>{
            
            this.userconnecte = result;
            this.projetId =  this.userconnecte["projet"].id;
            
            this.services.getprojet_typeObservation(this.projetId).subscribe((result) =>{   
                    var i=0;
                    //this.services.setCurrentMenuProjet(result);
                    result.forEach(element => {
                      var projet = {
                        "id": element.id, 
                        "nomFr": element.nomFr, 
                        "nomEn":element.nomEn,
                        "title": element.title
                      }
                      
                      this.services.createProjetMenu(projet).then((result1) =>{

                        i++;
                        if(i==result.length){
                            loading.dismiss();
                            this.totaleffectue = this.totaleffectue + 1;
                            if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                        
                          
                      });
                  });
            }, (error)=>{
              
            }, ()=>{
              
            });
        })

    })      

    ////////////////////////////////////////////////////////////////



      this.services.deletteAllEspeces().then((result1) =>{
            
        let loading = this.loadingCtrl.create();
        loading.present();
            this.mylocalstorage.getSession().then((result) =>{
              
              this.userconnecte = result;

              
              this.projetId =  this.userconnecte["projet"].id;

             
              
              this.services.findEspecesByProjetId(this.projetId).subscribe((result) =>{  
                    var i=0; 
                   
                    result.forEach(element => {

                      var espece = {
                        "id": element.id, 
                        "nom_fr":element.nom_fr, 
                        "nom_en":element.nom_en,
                        "image": element.image, 
                        "description_fr": element.description_fr, 
                        "description_en": element.description_en, 
                        "sous_groupes": element["sous_groupes"].id, 
                        "questions_animal": element["questions_animal"].id, 
                        "questions_menaces": element["questions_menances"].id, 
                        "questions_signe": element["questions_signe"].id, 
                        "questions_alimentation": element["questions_alimentation"].id
                      }
                        
                        this.services.createEspece(espece).then((result1) =>{
                             
                            i++;
                            if(i==result.length){
                                loading.dismiss();
                                this.totaleffectue = this.totaleffectue + 1;
                                if(this.totaleffectue == this.total){
                                    this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                        this.presentToast(langs['SUCCESSOPERATION']); 
                                        this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                            
                        });
                    });
              }, (error)=>{

                  alert("une erreur au rendez-vous c quoi le pb");
                  alert(JSON.stringify(error));
                
              }, ()=>{
                
              });
          })
      })
          

    ////////////////////////////////////////////////////////////////

      
         
            this.services.deletteAllUtilisateur().then((result) =>{   
              this.mylocalstorage.getSession().then((result2) =>{
              
                this.userconnecte = result2;
                this.projetId =  this.userconnecte["projet"].id;

                

                    var utilisateurs = {
                      "id":this.userconnecte["id"],
                      "username":this.userconnecte["email"], 
                      "email":this.userconnecte["email"], 
                      "password": this.userconnecte["password"], 
                      "ville": this.userconnecte["ville"], 
                      "telephone": this.userconnecte["telephone"], 
                      "pays": this.userconnecte["pays"].id, 
                      "fonctions": this.userconnecte["fonctions"].id, 
                      "projet":this.userconnecte["projet"].id,
                      "monuserid":this.userconnecte["id"]
                    }
                    this.services.createUtilisateur(utilisateurs).then((result1) =>{
                        
                    });
              }); 
            }); 

    ////////////////////////////////////////////////////////////////


  
      this.services.deletteAllInscription().then((result) =>{   
        this.mylocalstorage.getSession().then((result2) =>{
         
          this.userconnecte = result2;
          this.projetId =  this.userconnecte["projet"].id;

          
              var utilisateurs = {
                "id":this.userconnecte["id"],
                "username":this.userconnecte["email"], 
                "email":this.userconnecte["email"], 
                "password": this.userconnecte["password"], 
                "ville": this.userconnecte["ville"], 
                "telephone": this.userconnecte["telephone"], 
                "pays": this.userconnecte["pays"].id, 
                "fonctions": this.userconnecte["fonctions"].id, 
                "projet":this.userconnecte["projet"].id,
                "monuserid":this.userconnecte["id"]
              }
              this.services.createInscription(utilisateurs).then((result1) =>{
                  
              });
         }); 
      }); 

    ////////////////////////////////////////////////////////////////

    

    this.services.deletteAllQuestions().then((result1) =>{

      let loading = this.loadingCtrl.create();
      loading.present();
            
              this.services.allquestions().subscribe((result) =>{   
                    var i=0;
                    result.forEach(element => {
                        var myquestionnext;
                        if(element["questions"]==undefined){
                          myquestionnext = "null";
                        }else{
                          myquestionnext = element["questions"].id;
                        }
                        var question = {
                          "id": element.id, 
                          "titre_fr": element.titre_fr, 
                          "titre_en":element.titre_en, 
                          "type_reponse":element.type_reponse, 
                          "interval":element.interval, 
                          "type_debut":element.type_debut,
                          "image":element.image,
                          "isPicture":element.isPicture,
                          "questions":myquestionnext
                        }
                        this.services.createQuestion(question).then((result1) =>{

                          i++;
                          if(i==result.length){
                              loading.dismiss();
                              this.totaleffectue = this.totaleffectue + 1;
                              if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['SUCCESSOPERATION']); 
                                    this.navCtrl.setRoot('AcceuilPage');
                                });
                              }
                          }
                            
                        });
                    });
              }, (error)=>{
               
              }, ()=>{
               
              });
      });
      ////////////////////////////////////////////////////////////////
     

  }


}
