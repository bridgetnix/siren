import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';


/**
 * Generated class for the MesprojetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mesprojet',
  templateUrl: 'mesprojet.html',
})
export class MesprojetPage {
  public mesprojets: any;
  public mesobservations:any;
  public userconnecte = {};
  public observationchoisie = {}; 
  public montypeObservations = "";
  public monsousgroupe = "";
  public mongroupe = "";
  public monespece = "";
  public testeur = 0;
  public montesteur = 0;
  public detail = 0;
  public listequestions = [];
  public myuserconnecte: any;

  constructor(public navCtrl: NavController,private geolocation: Geolocation, public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  }

  ionViewDidLoad() {

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });

    this.testeur = 0;
    this.detail = 0;
  
    this.mylocalstorage.getSession().then((result) =>{
     
      if(navigator.onLine){

          this.userconnecte = result;
          let loading = this.loadingCtrl.create();
          loading.present();
          this.services.findProjetsByUserId(this.userconnecte["id"]).subscribe((resultat) =>{  
              this.mesprojets = resultat;
          }, (error)=>{
            console.log(error);
            //votre opération a  echoué!!
            loading.dismiss();
          }, ()=>{
            loading.dismiss();
          });
          
      }else{

          //this.userconnecte = result;
          //this.services.getAllProjetsByUserId(this.userconnecte["id"]).then((resultat) =>{
          this.services.getAllProjets().then((resultat) =>{
            this.mesprojets = resultat;
          });
      } 
    })
  }



  findObservationByProjet(id){

    
    this.mylocalstorage.getSession().then((result) =>{
     
      if(navigator.onLine){
          this.testeur = 1;
        
          this.userconnecte = result;
          
          this.services.getAllObservationByUserIdAndProjet(this.userconnecte["id"], id).then((resultat) =>{
            this.mesobservations = resultat;
          });
          
      }else{
          this.testeur = 2;
          this.userconnecte = result;
         
          this.services.getAllObservationByUserIdAndProjet(this.userconnecte["id"], id).then((resultat) =>{
            this.mesobservations = resultat;
          });
      } 
    })


  }

  

  retours(){
    this.testeur = 0;
    this.detail = 0;
    this.observationchoisie = {}; 
    this.montypeObservations = "";
    this.monsousgroupe = "";
    this.mongroupe = "";
    this.monespece = "";
    this.testeur = 0;
    this.detail = 0;
    this.listequestions = [];
  }


  details(element){


      this.detail=1;
      this.testeur = 4;
      
      this.observationchoisie = element;

      this.services.getAllTypeObsevationsById(element.typeObservations).then((resultat) =>{
         this.montypeObservations = resultat[0].nom_fr;
      });


     

      this.services.getAllSousGroupesById(element.sousgroupe).then((resultat:any) =>{
        if(resultat.length >0){
         this.monsousgroupe = resultat[0].nom_fr;
         this.montesteur = 1;
        }else{
          this.montesteur = 0;
        }
        
     });
 
 
     this.services.getAllGroupesById(element.groupe).then((resultat:any) =>{
       if(resultat.length >0){
         this.mongroupe = resultat[0].nom_fr;
         this.montesteur = 1;
       }else{
         this.montesteur = 0;
       }
         
     });
 
     this.services.getAllEspecesById(element.espece).then((resultat:any) =>{
       if(resultat.length >0){
         this.monespece = resultat[0].nom_fr;
         this.montesteur = 1;
       }else{
         this.montesteur = 0;
       }
     });
 




      
      this.services.getAllReultatsByObservationId(element.id).then((resultat:any) =>{
      
        resultat.forEach(element => {
          this.services.getQuestionById(element.questions).then((resultat) =>{
              var data = {
                contenu:element.contenu,
                questions:resultat[0].titre_fr
              }
              this.listequestions.push(data);
          });
          
        });
          
        
        
      });
  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

}
