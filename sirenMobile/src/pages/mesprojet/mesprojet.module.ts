import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MesprojetPage } from './mesprojet';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    MesprojetPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(MesprojetPage),
  ],
})
export class MesprojetPageModule {}
