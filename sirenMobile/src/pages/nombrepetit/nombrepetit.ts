import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { LanguageService } from '../../providers/services/language';


/**
 * Generated class for the NombrepetitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nombrepetit',
  templateUrl: 'nombrepetit.html',
})
export class NombrepetitPage {

  public idquestion: any;
  public titrequestion: any;
  public type_question: any;
  public donnees = {};
  public myuserconnecte: any;
  public testvalue = 0;
  public mareponse:any;
  public langue =0;
  public mydata = {
    questions:{
      titre_fr:"",
      titre_en:""
    },
      titre_fr:"",
      titre_en:""
  }
  constructor(public navCtrl: NavController, public language:LanguageService, public mylocalstorage: LocalStorageProvider, private geolocation: Geolocation, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  }

  ionViewWillLeave() {
    var listereponses = [];
    if(this.testvalue == 0){
      listereponses = Parcourt.listesReponses;
      console.log(listereponses);
      listereponses.pop();
      console.log(listereponses);
      Parcourt.listesReponses = listereponses;
    }
  }
  ionViewWillEnter() {
    this.testvalue = 0;
  } 

  

  ionViewDidLoad() {
    console.log(Parcourt);

    this.language.loadLanguage().then((result) =>{
      
        if(result == 'en'){
          this.langue = 1;
        }else{
          this.langue = 0;
        }
    });

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });


    this.idquestion = this.navParams.get("idquestion");
    this.type_question = this.navParams.get("type_question");
    
    if(this.idquestion!=0){

      if(navigator.onLine){
           ////////////////////////////////////// FOR TEST /////////////////////
            /*let loading = this.loadingCtrl.create();
            loading.present();
            this.services.findQuestionById(this.idquestion).subscribe((result) =>{  
              console.log(result);
              this.mareponse = result;
              if(this.langue ==0){
                this.titrequestion = result.titre_fr;
              }else{
                this.titrequestion = result.titre_en;
              }
              
            }, (error)=>{
              console.log(error);
              //votre opération a  echoué!!
              loading.dismiss();
            }, ()=>{
              loading.dismiss();
            });*/
            //////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////// FOR SERVER /////////
            
            this.services.getQuestionById(this.idquestion).then((result) =>{
                  this.mareponse = result[0];
                  if(this.langue == 0){
                    this.titrequestion = result[0].titre_fr;
                  }else{
                    this.titrequestion = result[0].titre_en;
                  }  
            });
            /////////////////////////////////////////////////////////////////////

      }else{

        
        this.services.getQuestionById(this.idquestion).then((result) =>{
              this.mareponse = result[0];
              if(this.langue == 0){
                this.titrequestion = result[0].titre_fr;
              }else{
                this.titrequestion = result[0].titre_en;
              }  
        });


      }

                   
    }


  }

  ouestlanimal(donnees){
  

    if(navigator.onLine){

          ///////////////////////////////////////////////////// ONLINE /////////////////////////////
    
            /*if(this.langue == 0){
              this.mydata.questions = this.mareponse;
              this.mydata.titre_fr = donnees.mareponse;
            }else{
              this.mydata.questions = this.mareponse;
              this.mydata.titre_en = donnees.mareponse;
            }
            this.testvalue = 1;
              var listereponses = [];
              if(Parcourt.listesReponses != null){
                listereponses = Parcourt.listesReponses;
                listereponses.push(this.mydata);
                Parcourt.listesReponses = listereponses;
              }else{
                
                listereponses.push(this.mydata);
                Parcourt.listesReponses = listereponses;
              }
              if(this.mareponse.questions!=undefined){
                  if(this.mareponse.questions.type_reponse == "entier"){
                    this.navCtrl.push('NombrepetitPage',{idquestion: this.mareponse.questions.id, type_question: this.mareponse.questions.type_question});
                  }else if(this.mareponse.questions.type_reponse == "text"){
                    this.navCtrl.push('DescriptionPage',{question: 0, reponse:0});  
                  }else if(this.mareponse.questions.type_reponse == "select"){
                    this.navCtrl.push('OuestanimalPage',{idquestion: this.mareponse.questions.id, type_question: this.mareponse.questions.type_question});
                  }else if(this.mareponse.questions.type_reponse == "rapide"){
                    this.navCtrl.push('DescriptionPage',{idquestion: this.mareponse.questions.id, type_question: this.mareponse.questions.type_question});  
                  }else if(this.mareponse.questions.type_reponse == "annuler"){
                    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                  }else{
                    this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                  }
              }else{
                this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
              }*/

            ////////////////////////////////////////////////////END ONLINE //////////////////////////

            
              if(this.langue == 0){
                this.mydata.questions = this.mareponse;
                this.mydata.titre_fr = donnees.mareponse;
              }else{
                this.mydata.questions = this.mareponse;
                this.mydata.titre_en = donnees.mareponse;
              }
              this.testvalue = 1;
                var listereponses = [];
                if(Parcourt.listesReponses != null){
                  listereponses = Parcourt.listesReponses;
                  listereponses.push(this.mydata);
                  Parcourt.listesReponses = listereponses;
                }else{
                  
                  listereponses.push(this.mydata);
                  Parcourt.listesReponses = listereponses;
                }
                if(this.mareponse.questions!=undefined){
    
                  this.services.getQuestionById(this.mareponse.questions).then((result) =>{
                    var questions_next = result[0];
    
    
                    
                        if(questions_next.type_reponse == "entier"){
                          this.navCtrl.push('NombrepetitPage',{idquestion: questions_next.id, type_question: questions_next.type_question});
                        }else if(questions_next.type_reponse == "text"){
                          this.navCtrl.push('DescriptionPage',{question: 0, reponse:0});  
                        }else if(questions_next.type_reponse == "select"){
                          this.navCtrl.push('OuestanimalPage',{idquestion: questions_next.id, type_question: questions_next.type_question});
                        }else if(questions_next.type_reponse == "rapide"){
                          this.navCtrl.push('DescriptionPage',{idquestion: questions_next.id, type_question: questions_next.type_question});  
                        }else if(questions_next.type_reponse == "annuler"){
                          this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                        }else{
                          this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                        }
    
                  });
    
                }else{
                  this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                }

        }else{


          if(this.langue == 0){
            this.mydata.questions = this.mareponse;
            this.mydata.titre_fr = donnees.mareponse;
          }else{
            this.mydata.questions = this.mareponse;
            this.mydata.titre_en = donnees.mareponse;
          }
          this.testvalue = 1;
            var listereponses = [];
            if(Parcourt.listesReponses != null){
              listereponses = Parcourt.listesReponses;
              listereponses.push(this.mydata);
              Parcourt.listesReponses = listereponses;
            }else{
              
              listereponses.push(this.mydata);
              Parcourt.listesReponses = listereponses;
            }
            if(this.mareponse.questions!=undefined){

              this.services.getQuestionById(this.mareponse.questions).then((result) =>{
                var questions_next = result[0];


                
                    if(questions_next.type_reponse == "entier"){
                      this.navCtrl.push('NombrepetitPage',{idquestion: questions_next.id, type_question: questions_next.type_question});
                    }else if(questions_next.type_reponse == "text"){
                      this.navCtrl.push('DescriptionPage',{question: 0, reponse:0});  
                    }else if(questions_next.type_reponse == "select"){
                      this.navCtrl.push('OuestanimalPage',{idquestion: questions_next.id, type_question: questions_next.type_question});
                    }else if(questions_next.type_reponse == "rapide"){
                      this.navCtrl.push('DescriptionPage',{idquestion: questions_next.id, type_question: questions_next.type_question});  
                    }else if(questions_next.type_reponse == "annuler"){
                      this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                    }else{
                      this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                    }

              });

            }else{
              this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
            }



        }
      
  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }

}
