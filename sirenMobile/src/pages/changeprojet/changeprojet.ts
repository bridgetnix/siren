import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from 'ng2-translate';
import { Parcourt } from '../../configs/configs';
import {Geolocation} from '@ionic-native/geolocation';
import { App } from 'ionic-angular';

/**
 * Generated class for the ChangeprojetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changeprojet',
  templateUrl: 'changeprojet.html',
})
export class ChangeprojetPage {

  public mesprojets = [];
  public userconnecte = {};
  public showalert = 0;
  public utilisateur = {
    login:"",
    password:""
  };

  constructor(public navCtrl: NavController, public appCtrl: App,private geolocation: Geolocation, public toastCtrl: ToastController, public translate:TranslateService, public alertCtrl: AlertController, public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  }

  ionViewDidLoad() {

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });
    
    if(navigator.onLine){
        this.mylocalstorage.getSession().then((result:any) =>{
              this.userconnecte = result;
              let loading = this.loadingCtrl.create();
              loading.present();
              this.services.findProjetsByUserId(this.userconnecte["id"]).subscribe((resultat) =>{  
                  this.mesprojets = resultat;
                  this.showRadio(this.mesprojets);
              }, (error)=>{
                console.log(error);
                //votre opération a  echoué!!
                loading.dismiss();
              }, ()=>{
                loading.dismiss();
              });
        })
    }else{

      this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
        this.presentToast(langs['INFOSCONNEXION']); 
        this.navCtrl.setRoot('AcceuilPage');
      });
      
    }

  }


  showRadio(listeprojets){ 

    
    this.translate.get(['SELECTPROJET', 'ENREGISTRER', 'SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
          let alert = this.alertCtrl.create();

          alert.setTitle(langs['SELECTPROJET']);

          this.mesprojets.forEach(element => {
            let monimput = {
              type: 'radio',
              label: element.nom,
              value: element.id,
            }
            alert.addInput(monimput);
          });
          
          alert.addButton({
            text:langs['ENREGISTRER'],
            cssClass:'ion-button full',
            handler:data => {

              console.log(data);

              let loading = this.loadingCtrl.create();
              loading.present();
              
                
                this.mylocalstorage.getSession().then((result) =>{
                  this.userconnecte = result;
                 
                  this.utilisateur.login = this.userconnecte["email"];
                  this.utilisateur.password = this.userconnecte["password"];

                  

                  //Parcourt.images = 

                     this.services.changerProjet(this.userconnecte["id"], data).subscribe((resultat) =>{  
               
                              this.services.login(this.utilisateur).subscribe(result =>{
                                if(result != 0){
                                    
                                    this.mylocalstorage.storeSession(result).then(() => {
                                        //this.appCtrl.getRootNav().setRoot("AcceuilPage");

                                        this.services.deletteAlltypeObservation().then((result1) =>{
        
                                          //this.services.acceuil().subscribe((result) =>{ 
                                            this.services.projet_typeObservation(this.userconnecte["projet"].id).subscribe((result) =>{  
                                                var i=0;
                                                result.forEach(element => {
                                                    
                                                    this.services.createTypeObservation(element).then((result1) =>{
                                
                                                         i++;
                                                         if(i==result.length){

                                                              this.navCtrl.setRoot('AcceuilPage').then(()=>{
                                                                this.showalert = 1;
                                                                window.location.reload();
                                                              });
                                                              this.presentToast(langs['SUCCESSOPERATION']); 

                                                         }
                                                          
                                                        
                                                    });
                                                });
                                          }, (error)=>{
                                            console.log(error);
                                            
                                          }, ()=>{
                                            
                                          });
                                      }); 

                                    });
                                

                                }else{
                                  this.translate.get(['INFOSLOGINV']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['INFOSLOGINV']); 
                                  });
                                }
                              }, error =>{
                                loading.dismiss();
                                console.log(error);
                              },() =>{
                                loading.dismiss();
                                  
                              });


                    }, (error)=>{
                      console.log(error);
                      //votre opération a  echoué!!
                      loading.dismiss();
                  }, ()=>{
                    loading.dismiss();
                  });

                })



              
              
            }
          });
         if(this.showalert ==0){
          alert.present();
         }
         

      });

  }


  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
    });
    toast.present();
  }

}
