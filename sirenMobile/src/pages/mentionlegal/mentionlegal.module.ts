import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MentionlegalPage } from './mentionlegal';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { TranslateModule } from 'ng2-translate';

@NgModule({
  declarations: [
    MentionlegalPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(MentionlegalPage),
  ],
})
export class MentionlegalPageModule {}
