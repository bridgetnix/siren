import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';

/**
 * Generated class for the MentionlegalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mentionlegal',
  templateUrl: 'mentionlegal.html',
})
export class MentionlegalPage {

  public reponse: any; 
  public idquestion: any;
  public type_question: any;
  public myuserconnecte: any;
  public titrequestion:any;
  public nextquestion:any;
  public myimage:any;
  public printpicture =0;
  public noteobservation = 0;
  public myprojet:any;


  public monprojet={
    note:"",
    mention:""
  };

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public mylocalstorage: LocalStorageProvider) {
   
  }

  ionViewDidLoad() {
    
    
     
    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          this.myprojet = this.myuserconnecte.projet;
          this.monprojet.note = this.myprojet.note;
          this.monprojet.mention = this.myprojet.mention;
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);

          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })
  

  }



  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

}

