import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

/**
 * Generated class for the SitewebPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-siteweb',
  templateUrl: 'siteweb.html',
})
export class SitewebPage {

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, public translate:TranslateService, public navParams: NavParams) {
  }

  

  ionViewDidLoad() {
    if(navigator.onLine){

        window.open('http://www.ammco.org', '_blank');

    }else{

        
        this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
          this.presentToast(langs['INFOSCONNEXION']); 
          this.navCtrl.setRoot('AcceuilPage');
        });
        
    }
      
  }


  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
    });
    toast.present();
  }

}
