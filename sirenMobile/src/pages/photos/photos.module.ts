import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhotosPage } from './photos';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { TranslateModule } from 'ng2-translate';
import { Camera } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import { ImageResizer } from '@ionic-native/image-resizer';
import {ImagePicker} from '@ionic-native/image-picker/ngx';

@NgModule({
  declarations: [
    PhotosPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(PhotosPage),
  ],
  providers:[
    Camera,
    Base64,
    ImageResizer,
    ImagePicker
  ]
})
export class PhotosPageModule {}
