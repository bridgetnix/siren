import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';
import {Base64} from '@ionic-native/base64';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { LanguageService } from '../../providers/services/language';


/**
 * Generated class for the PhotosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-photos',
  templateUrl: 'photos.html',
})
export class PhotosPage {

  public question: any;
  public reponse: any;
  public photos1: any;
  public idquestion: any;
  public type_question: any;
  public observation = {
    image1:'0',
    image2:'0',
    image3:'0',
    image4:'0',

  };
  public listeimages = [];
  public listezozo = [];
  public compteur = 1;
  public mesttreposition = 0;
  public testeur = 0;
  public images = [];
  public myuserconnecte: any;
  public testvalue =0;
  public titrequestion="";

  

  constructor(public navCtrl: NavController, public language:LanguageService, public mylocalstorage: LocalStorageProvider, public imagePicker: ImagePicker, private imageResizer: ImageResizer,private geolocation: Geolocation, private base64: Base64, public alertCtrl: AlertController, public navParams: NavParams, private camera: Camera) {
  }

  
  ionViewWillLeave() {
    if(this.testvalue == 0){
      
    }
  }
  ionViewWillEnter() {
    this.testvalue = 0;
    Parcourt.listesReponses = [];
  }

  ionViewDidLoad() {

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.language.loadLanguage().then((result) =>{
      
          if(result == 'en'){
            this.titrequestion = "Add picture(4 pictures max)";
          }else{
            this.titrequestion = "Ajouter photos(4 photos max)";
          }
    });

    

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });


    this.testeur = 0;
   // this.question = this.navParams.get("question");
   // this.reponse = this.navParams.get("reponse");
     this.idquestion = this.navParams.get("idquestion");
     this.type_question = this.navParams.get("type_question");
     this.listezozo = [1,2,3,5,8];
  }

  deleteall(){

    this.alertCtrl.create({
      message: 'Voulez-vous vraiment suprimer ces photos ?',
      buttons: [{
          text:'Non',
          handler: data => {
          }
        },{
          text:'Oui',
          handler: data => {
            this.testeur = 0;
            this.compteur = 0;
            this.listeimages = [];
          }
        }
        ]
    }).present();
        
  }


  deleteone(index){

    this.alertCtrl.create({
      message: 'Voulez-vous vraiment suprimer cette photos ?',
      buttons: [{
          text:'Non',
          handler: data => {
          }
        },{
          text:'Oui',
          handler: data => {
            this.listeimages.splice(index, 1);
            this.mesttreposition = index;
          }
        }
        ]
    }).present();

  }


  getPictures() {
    this.imagePicker.getPictures({
      maximumImagesCount: 5,
      outputType: 1
    }).then(selectedImg => {
      selectedImg.forEach(i => this.images.push("data:image/jpeg;base64," + i));
    })
  }


  takegalerie(){

    const options: CameraOptions = {
      quality:75,
      destinationType:0,
      encodingType:this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 0

    }

    this.camera.getPicture(options).then((ImageData) => {
        let base64Image = ImageData;
  

          if(this.mesttreposition==0){

                if(this.compteur <= 4){
                  var element = {
                    image: "data:image/jpeg;base64," + base64Image,
                    id:this.compteur
                  }
                  this.listeimages.push(element);
                  if(this.compteur==1){

                    this.observation.image1 = element.image;
                    /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                      
                      this.observation.image1 = base64File;
                    }, (err) =>{  
                    })*/
                  }
                  if(this.compteur==2){
                    this.observation.image2 = element.image;
                    /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                      this.observation.image2 = base64File;
                    }, (err) =>{  
                    })*/
                  }
                  if(this.compteur==3){
                    this.observation.image3 = element.image;
                    /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                      this.observation.image3 = base64File;
                    }, (err) =>{  
                    })*/
                  }
                  if(this.compteur==4){
                    this.observation.image4 = element.image;
                    /*this.base64.encodeFile(base64Image).then((base64File: string)=>{
                      this.observation.image4 = base64File;
                    }, (err) =>{  
                    })*/
                  }
                  this.compteur ++;
              }else{
                  this.testeur = 1;
              }

        }else{

              this.listeimages.push(element);
              if(this.mesttreposition==1){
                this.base64.encodeFile(base64Image).then((base64File: string)=>{
                  this.observation.image1 = base64File;
                }, (err) =>{  
                })
              }

              if(this.mesttreposition==2){
                this.base64.encodeFile(base64Image).then((base64File: string)=>{
                  this.observation.image2 = base64File;
                }, (err) =>{  
                })
              }

              if(this.mesttreposition==3){
                this.base64.encodeFile(base64Image).then((base64File: string)=>{
                  this.observation.image3 = base64File;
                }, (err) =>{  
                })
              }

              if(this.mesttreposition==4){
                this.base64.encodeFile(base64Image).then((base64File: string)=>{
                  this.observation.image4 = base64File;
                }, (err) =>{  
                })
              }

              this.mesttreposition = 0;


        }
                
            
    }, (err) =>{

    })
}



  

  takephotos(){

        const options: CameraOptions = {
          quality:75,
          destinationType:1,
          /*encodingType:this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          sourceType: 1*/
          sourceType: 1,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          allowEdit: false,
          saveToPhotoAlbum:true
        }

        this.camera.getPicture(options).then((ImageData) => {
            let base64Image = ImageData;


            let optionss = {
              uri: ImageData,
              folderName: 'Protonet',
              quality: 90,
              width: 1024,
              height: 748
            } as ImageResizerOptions;


              if(this.mesttreposition==0){

                    if(this.compteur <= 4){
                      var element = {
                        image: base64Image,
                        id:this.compteur
                      }
                      this.listeimages.push(element);
                      if(this.compteur==1){      
                 
                          try {

                            this.imageResizer
                            .resize(optionss)
                              .then((filePath: string) => {
                                 
                                    this.base64.encodeFile(filePath).then((base64File: string)=>{
                                      this.observation.image1 = base64File;
                                      //this.observation.image1 = base64File;  
                                    }, (err) =>{ 
                                        
                                    })

                            }).catch(e => {

                            });
                            
                          } catch (error) {
                          }       
                      }
                      if(this.compteur==2){
                        try {

                            this.imageResizer
                            .resize(optionss)
                              .then((filePath: string) => {
                                 
                                    this.base64.encodeFile(filePath).then((base64File: string)=>{
                                      this.observation.image2 = base64File;
                                      //this.observation.image2 = base64File;  
                                    }, (err) =>{ 
                                        
                                    })
                                    
                            }).catch(e => {

                            });
                            
                          } catch (error) {
                          } 
                      }
                      if(this.compteur==3){
                       
                        try {

                          this.imageResizer
                          .resize(optionss)
                            .then((filePath: string) => {
                               
                                  this.base64.encodeFile(filePath).then((base64File: string)=>{
                                    this.observation.image3 = base64File;
                                    //this.observation.image3 = base64File;  
                                  }, (err) =>{ 
                                      
                                  })
                                  
                          }).catch(e => {

                          });
                          
                        } catch (error) {
                        } 
                        
                      }
                      if(this.compteur==4){
                        
                        try {

                          this.imageResizer
                          .resize(optionss)
                            .then((filePath: string) => {
                               
                                  this.base64.encodeFile(filePath).then((base64File: string)=>{
                                    this.observation.image4 = base64File;
                                    //this.observation.image4 = base64File;  
                                  }, (err) =>{ 
                                      
                                  })
                                  
                          }).catch(e => {

                          });
                          
                        } catch (error) {
                        } 


                      }
                      this.compteur ++;
                  }else{
                      this.testeur = 1;
                  }

            }else{

                  this.listeimages.push(element);
                  if(this.mesttreposition==1){
                    try {

                      this.imageResizer
                      .resize(optionss)
                        .then((filePath: string) => {
                           
                              this.base64.encodeFile(filePath).then((base64File: string)=>{
                                this.observation.image1 = base64File;
                                //this.observation.image1 = base64File;  
                              }, (err) =>{ 
                                  
                              })
                              
                      }).catch(e => {

                      });
                      
                    } catch (error) {
                    }
                  }

                  if(this.mesttreposition==2){
                    
                    try {

                      this.imageResizer
                      .resize(optionss)
                        .then((filePath: string) => {
                           
                              this.base64.encodeFile(filePath).then((base64File: string)=>{
                                this.observation.image2 = base64File;
                                //this.observation.image2 = base64File;  
                              }, (err) =>{ 
                                  
                              })
                              
                      }).catch(e => {

                      });
                      
                    } catch (error) {
                    }


                  }

                  if(this.mesttreposition==3){
                    
                    try {

                      this.imageResizer
                      .resize(optionss)
                        .then((filePath: string) => {
                           
                              this.base64.encodeFile(filePath).then((base64File: string)=>{
                                this.observation.image3 = base64File;
                                //this.observation.image3 = base64File;  
                              }, (err) =>{ 
                                  
                              })
                              
                      }).catch(e => {

                      });
                      
                    } catch (error) {
                    }
                  }

                  if(this.mesttreposition==4){
                    
                    try {

                      this.imageResizer
                      .resize(optionss)
                        .then((filePath: string) => {
                           
                              this.base64.encodeFile(filePath).then((base64File: string)=>{
                                this.observation.image4 = base64File;
                                //this.observation.image4 = base64File;  
                              }, (err) =>{ 
                                  
                              })
                              
                      }).catch(e => {

                      });
                      
                    } catch (error) {
                    }

                  }

                  this.mesttreposition = 0;


            }
                    
                
        }, (err) =>{

        })
  }


  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }


  suivant(){
    Parcourt.observation = this.observation;
   
    //$state.go('app.description',{question:$scope.titrequestion, reponse: $scope.mareponse});

   // this.navCtrl.push('DescriptionPage', {question: this.question, reponse:this.reponse});
  //this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
  this.testvalue =1;
  console.log("============================================");
  console.log(this.type_question);
  console.log(this.idquestion);
  console.log("============================================");

    if(this.type_question == "entier"){
      this.navCtrl.push('NombrepetitPage',{idquestion: this.idquestion, type_question: this.type_question});
    }else if(this.type_question == "text"){
      this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});  
    }else if(this.type_question == "select"){
      this.navCtrl.push('OuestanimalPage',{idquestion: this.idquestion, type_question: this.type_question});
    }else if(this.type_question == "rapide"){
      this.navCtrl.push('DescriptionPage',{question: 0, reponse: 0});  
    }else if(this.type_question == "annuler"){
      this.navCtrl.setRoot('AcceuilPage').then(()=>{});
    }else{
      this.navCtrl.push('GroupePage');
    }

  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

}
