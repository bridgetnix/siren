import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LanguePage } from './langue';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { TranslateModule } from 'ng2-translate';

@NgModule({
  declarations: [
    LanguePage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(LanguePage),
  ],
})
export class LanguePageModule {}
