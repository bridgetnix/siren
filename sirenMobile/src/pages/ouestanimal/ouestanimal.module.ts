import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OuestanimalPage } from './ouestanimal';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    OuestanimalPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(OuestanimalPage),
  ],
})
export class OuestanimalPageModule {}
