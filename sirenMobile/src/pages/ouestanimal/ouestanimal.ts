import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';
import { LanguageService } from '../../providers/services/language';
import { TranslateService } from 'ng2-translate';


/**
 * Generated class for the OuestanimalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ouestanimal',
  templateUrl: 'ouestanimal.html',
})
export class OuestanimalPage {
  public idquestion: any;
  public titrequestion: any;
  public listesreponses: any;
  public data = {};
  public userconnecte: any;
  public type_question: any;
  public myuserconnecte: any;
  public ispicture = 0;
  public maquestion : any;
  public langue = 0;
  public testvalue = 0;
  public myimage:any;
  public printpicture = 0;

  public mydata = {
    questions:{
      titre_fr:"",
      titre_en:""
    },
      titre_fr:"",
      titre_en:""
  }

  constructor(public navCtrl: NavController, public translate:TranslateService, private alertCtrl: AlertController, public language:LanguageService,private geolocation: Geolocation, public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  }

  /*ionViewWillLeave() {
    var listereponses = [];
    if(this.testvalue == 0){
      listereponses = Parcourt.listesReponses;
      console.log(listereponses);
      listereponses.pop();
      console.log(listereponses);
      Parcourt.listesReponses = listereponses;
    }
  }
  ionViewWillEnter() {
    this.testvalue = 0;
  }*/

  ionViewDidLoad() {


    this.language.loadLanguage().then((result) =>{
      
          if(result == 'en'){
            this.langue = 1;
          }else{
            this.langue = 0;
          }
    });

    


    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });
    
    this.idquestion = this.navParams.get("idquestion");
    this.type_question = this.navParams.get("type_question");


    this.mylocalstorage.getSession().then((result) =>{
        this.userconnecte = result;
    })
    this.super(this.idquestion);

  }

  mokaiAlert(title){
    let alert = this.alertCtrl.create({
      title: "detail",
      message: title,
      buttons: [
        {
          text: 'ok',
          role: 'cancel',
          handler: () => {
           
          }
        }
      ]
    });
    alert.present();
  }


  changeStatus1(data){

    
    if(this.langue == 0){
      this.mydata.questions = this.maquestion;
      this.mydata.titre_fr = data.titre_fr;
    }else{
      this.mydata.questions = this.maquestion;
      this.mydata.titre_en = data.titre_fr;
    }
    this.testvalue = 1;
      var listereponses = [];
      if(Parcourt.listesReponses != null){
        listereponses = Parcourt.listesReponses;
        listereponses.push(this.mydata);
        Parcourt.listesReponses = listereponses;
      }else{
        
        listereponses.push(this.mydata);
        Parcourt.listesReponses = listereponses;
      }

      if(navigator.onLine){
            
           /* 
           var listereponses = [];
            if(Parcourt.listesReponses != null){
              listereponses = Parcourt.listesReponses;
              listereponses.push(data);
              Parcourt.listesReponses = listereponses;
            }else{
              
              listereponses.push(data);
              Parcourt.listesReponses = listereponses;
            }

            if(data.questions_next!=undefined){

                if(data.questions_next.type_reponse == "entier"){
                  this.navCtrl.push('NombrepetitPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                }else if(data.questions_next.type_reponse == "text"){
                  this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});  
                }else if(data.questions_next.type_reponse == "select"){
                  this.navCtrl.push('OuestanimalPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                }else if(data.questions_next.type_reponse == "rapide"){
                  this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});  
                }else if(data.questions_next.type_reponse == "annuler"){

                  var mymessage = ""

                  if(this.langue == 1){
                    mymessage = data.questions_next.titre_fr;
                  }else{
                    mymessage = data.questions_next.titre_en;
                  }
                  
                  this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe((langs:Array<string>) => {
                    let alert = this.alertCtrl.create({
                      title: langs['NAVIGATIONERROR'],
                      message: mymessage,
                      buttons: [
                        {
                          text: langs['CANCELD'],
                          role: 'cancel',
                          handler: () => {
                            Parcourt.listesReponses = [];
                            this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                          }
                        }
                      ]
                    });
                    alert.present();
                  })

                  


                }
            }else{
              this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
            }
            */
           //////////////////////////////////////////////// END ONLINE //////////////////////////

           if(data.questions_next!=undefined){
            this.services.getQuestionById(data.questions_next).then((result) =>{
              
              
              var questions_next = result[0];
              if(questions_next.type_reponse == "entier"){
                this.navCtrl.push('NombrepetitPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});
              }else if(questions_next.type_reponse == "text"){
                this.navCtrl.push('DescriptionPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});  
              }else if(questions_next.type_reponse == "select"){
                this.navCtrl.push('OuestanimalPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});
              }else if(questions_next.type_reponse == "rapide"){
                this.navCtrl.push('DescriptionPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});  
              }else if(questions_next.type_reponse == "annuler"){
  
                var mymessage = ""
  
                if(this.langue == 1){
                  mymessage = questions_next.titre_fr;
                }else{
                  mymessage = questions_next.titre_en;
                }
                
                this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe((langs:Array<string>) => {
                  let alert = this.alertCtrl.create({
                    title: langs['NAVIGATIONERROR'],
                    message: mymessage,
                    buttons: [
                      {
                        text: langs['CANCELD'],
                        role: 'cancel',
                        handler: () => {
                          Parcourt.listesReponses = [];
                          this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                        }
                      }
                    ]
                  });
                  alert.present();
                })
              }
            })
  
          }else{
            this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
          }
          ///////////////////////////////////////////////////////////////////////////////

      }else{

        
        if(data.questions_next!=undefined){
          this.services.getQuestionById(data.questions_next).then((result) =>{
            
           
            var questions_next = result[0];
            if(questions_next.type_reponse == "entier"){
              this.navCtrl.push('NombrepetitPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});
            }else if(questions_next.type_reponse == "text"){
              this.navCtrl.push('DescriptionPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});  
            }else if(questions_next.type_reponse == "select"){
              this.navCtrl.push('OuestanimalPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});
            }else if(questions_next.type_reponse == "rapide"){
              this.navCtrl.push('DescriptionPage',{idquestion: questions_next.id, type_question: questions_next.type_reponse});  
            }else if(questions_next.type_reponse == "annuler"){

              var mymessage = ""

              if(this.langue == 1){
                mymessage = questions_next.titre_fr;
              }else{
                mymessage = questions_next.titre_en;
              }
              
              this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe((langs:Array<string>) => {
                let alert = this.alertCtrl.create({
                  title: langs['NAVIGATIONERROR'],
                  message: mymessage,
                  buttons: [
                    {
                      text: langs['CANCELD'],
                      role: 'cancel',
                      handler: () => {
                        Parcourt.listesReponses = [];
                        this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                      }
                    }
                  ]
                });
                alert.present();
              })
            }
          })

        }else{
          this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
        }

      }
  }



  changeStatus(data){


    this.listesreponses.forEach(element => {
      
        if(element.id == data.id){

          data.responses = element;
           
          
           
            /*console.log(data.reponses);
            console.log(this.userconnecte);
            console.log("==================================== " + typeof data.responses.questions_next + " ======================")
            if(typeof data.responses.questions_next === 'undefined'){
              console.log("==================== ");
              console.log("================================");
              console.log("================================");
            }*/
            

          if((undefined != data.responses["questions_next"])&&(data.responses["questions_next"]!=null)){
             
                 // dans le cas ou il ya la connexion internet 
            if(navigator.onLine){

              ///////////////////////////////////////////////////////// ONLINE/////////////////////////////////////
                /* if(data.responses.questions_next["type_reponse"] == "text"){
    
                   // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question 
                   // et pour cella nous devons recupérer le projet en cours.
                   var listereponses = [];
                   if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                    }else{
                      
                      listereponses.push(data.responses);
                      Parcourt.listesReponses = listereponses;
                    }
                    if(undefined != this.userconnecte.projet["questions"]){
      
                       
                        if(this.userconnecte.projet.questions["type_reponse"] == "select"){
                            this.super(this.userconnecte.projet.questions.id);
                        }


                        if(data.questions_next.type_reponse == "entier"){
                          this.navCtrl.push('NombrepetitPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                        }else if(data.questions_next.type_reponse == "text"){
                          this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});  
                        }else if(data.questions_next.type_reponse == "select"){
                          this.navCtrl.push('OuestanimalPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                        }else if(data.questions_next.type_reponse == "rapide"){
                          this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});  
                        }else if(data.questions_next.type_reponse == "annuler"){
                          this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                        }
                      
                    }else{
                      var listereponses = [];
                      if(Parcourt.listesReponses != null){
                        listereponses = Parcourt.listesReponses;
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }else{
                        
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }

                      if(data.questions_next.type_reponse == "entier"){
                        this.navCtrl.push('NombrepetitPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                      }else if(data.questions_next.type_reponse == "text"){
                        this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});  
                      }else if(data.questions_next.type_reponse == "select"){
                        this.navCtrl.push('OuestanimalPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});
                      }else if(data.questions_next.type_reponse == "rapide"){
                        this.navCtrl.push('DescriptionPage',{idquestion: data.questions_next.id, type_question: data.questions_next.type_reponse});  
                      }else if(data.questions_next.type_reponse == "annuler"){
                        this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                      }
                      //this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id}); 
                     
                   }

                    
                }
                if(data.responses.questions_next["type_reponse"] == "select"){
                  var listereponses = [];
                  if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }else{
                    
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }
                  this.super(data.responses.questions_next.id);
                }

                if(data.responses.questions_next["type_reponse"] == "entier"){
                  var listereponses = [];
                  if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }else{
                    
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }
                  this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id}); 
                }*/

                

                /////////////////////////////////////////////////////// FOR PROD ////////////////////////////////////////////

                /*let loading = this.loadingCtrl.create();
                 loading.present();
      
                  this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                        loading.dismiss();

                        data.responses = {...data.responses, questions:result[0]};
                        var listereponses = [];
                        
                        if(Parcourt.listesReponses != null){
                          listereponses = Parcourt.listesReponses;
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }else{
                          
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }




                         
                        this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{

                              if(result2[0].type_reponse == "text"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "entier"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "select"){
                                this.super(result2[0].id);
                              }

                            
                        });


                        
                  });*/

                  ///////////////////////////////////////////////////////////////////////////////////////////////////////////



                
      
                  this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                        

                    data.responses = {...data.responses, questions:result[0]};
                    var listereponses = [];
                    
                    if(Parcourt.listesReponses != null){
                      listereponses = Parcourt.listesReponses;
                      listereponses.push(data.responses);
                      Parcourt.listesReponses = listereponses;
                    }else{
                      
                      listereponses.push(data.responses);
                      Parcourt.listesReponses = listereponses;
                    }

                     
                    this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{
                           //alert(JSON.stringify(result2));

                          /*if(result2[0].type_reponse == "text"){
                            // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                            
                            this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                          }
                          if(result2[0].type_reponse == "entier"){
                            // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                            
                            this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                          }
                          if(result2[0].type_reponse == "select"){
                            this.super(result2[0].id);
                          }*/


                          if(result2[0].type_reponse == "text"){

                            // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question 
                            // et pour cella nous devons recupérer le projet en cours.
                            var listereponses = [];
                            /*if(Parcourt.listesReponses != null){
                             listereponses = Parcourt.listesReponses;
                             listereponses.push(data.responses);
                             Parcourt.listesReponses = listereponses;
                             }else{
                               
                               listereponses.push(data.responses);
                               Parcourt.listesReponses = listereponses;
                             }*/
                             if(undefined != this.userconnecte.projet["questions"]){
               
                                 /*if(this.userconnecte.projet.questions["type_reponse"] == "text"){
                                     this.navCtrl.push('NombrepetitPage', {idquestion: this.userconnecte.projet.questions.questions.id});
                                 }*/
                                 if(this.userconnecte.projet.questions["type_reponse"] == "select"){
                                     this.super(this.userconnecte.projet.questions.id);
                                 }
         
         
                                 if(result2[0].type_reponse == "entier"){
                                   this.navCtrl.push('NombrepetitPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                 }else if(result2[0].type_reponse == "text"){
                                   this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});  
                                 }else if(result2[0].type_reponse == "select"){
                                   this.navCtrl.push('OuestanimalPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                 }else if(result2[0].type_reponse == "rapide"){
                                   this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});  
                                 }else if(result2[0].type_reponse == "annuler"){
                                   this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                                 }
                               
                             }else{
                               /*var listereponses = [];
                               if(Parcourt.listesReponses != null){
                                 listereponses = Parcourt.listesReponses;
                                 listereponses.push(data.responses);
                                 Parcourt.listesReponses = listereponses;
                               }else{
                                 
                                 listereponses.push(data.responses);
                                 Parcourt.listesReponses = listereponses;
                               }*/
         
                               if(result2[0].type_reponse == "entier"){
                                 this.navCtrl.push('NombrepetitPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});
                               }else if(result2[0].type_reponse == "text"){
                                 this.navCtrl.push('DescriptionPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});  
                               }else if(result2[0].type_reponse == "select"){
                                 this.navCtrl.push('OuestanimalPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});
                               }else if(result2[0].type_reponse == "rapide"){
                                 this.navCtrl.push('DescriptionPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});  
                               }else if(result2[0].type_reponse == "annuler"){
                                 this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                               }
                               //this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id}); 
                              
                            }
         
                             
                         }
                         if(result2[0].type_reponse == "select"){
                           /*var listereponses = [];
                           if(Parcourt.listesReponses != null){
                             listereponses = Parcourt.listesReponses;
                             listereponses.push(data.responses);
                             Parcourt.listesReponses = listereponses;
                           }else{
                             
                             listereponses.push(data.responses);
                             Parcourt.listesReponses = listereponses;
                           }*/
                           this.super(result2[0].id);
                         }
         
                         if(result2[0].type_reponse == "entier"){
                           /*var listereponses = [];
                           if(Parcourt.listesReponses != null){
                             listereponses = Parcourt.listesReponses;
                             listereponses.push(data.responses);
                             Parcourt.listesReponses = listereponses;
                           }else{
                             
                             listereponses.push(data.responses);
                             Parcourt.listesReponses = listereponses;
                           }*/
                           this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                         }

                         if(result2[0].type_reponse == "annuler"){
                          /*var listereponses = [];
                          if(Parcourt.listesReponses != null){
                            listereponses = Parcourt.listesReponses;
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }else{
                            
                            listereponses.push(data.responses);
                            Parcourt.listesReponses = listereponses;
                          }*/


                          
                          var mymessage = ""

                          if(this.langue == 0){
                            mymessage = result2[0].titre_fr;
                          }else{
                            mymessage = result2[0].titre_en;
                          }
                          
                          this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe((langs:Array<string>) => {
                            let alert = this.alertCtrl.create({
                              title: langs['NAVIGATIONERROR'],
                              message: mymessage,
                              buttons: [
                                {
                                  text: langs['CANCELD'],
                                  role: 'cancel',
                                  handler: () => {
                                    Parcourt.listesReponses = [];
                                    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                                  }
                                }
                              ]
                            });
                            alert.present();
                          })

                       

                      
                      
                        }
                    
            
            
            
                  });


                    
              });


          }else{
               // dans le cas ou il n'ya pas de connexion internet  this.type_question = this.navParams.get("type_question");

                
      
                  this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                        

                        data.responses = {...data.responses, questions:result[0]};
                        var listereponses = [];
                        
                        if(Parcourt.listesReponses != null){
                          listereponses = Parcourt.listesReponses;
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }else{
                          
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }
                        
                         
                        this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{
                               //alert(JSON.stringify(result2));

                              /*if(result2[0].type_reponse == "text"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "entier"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "select"){
                                this.super(result2[0].id);
                              }*/


                              if(result2[0].type_reponse == "text"){
    
                                // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question 
                                // et pour cella nous devons recupérer le projet en cours.
                                
                                /*data.responses = {...data.responses, questions:result2[0]};
                                var listereponses = [];
                                if(Parcourt.listesReponses != null){
                                 listereponses = Parcourt.listesReponses;
                                 listereponses.push(data.responses);
                                 Parcourt.listesReponses = listereponses;
                                 }else{
                                   
                                   listereponses.push(data.responses);
                                   Parcourt.listesReponses = listereponses;
                                 }*/
                                 if(undefined != this.userconnecte.projet["questions"]){
                   
                                     /*if(this.userconnecte.projet.questions["type_reponse"] == "text"){
                                         this.navCtrl.push('NombrepetitPage', {idquestion: this.userconnecte.projet.questions.questions.id});
                                     }*/
                                     if(this.userconnecte.projet.questions["type_reponse"] == "select"){
                                         this.super(this.userconnecte.projet.questions.id);
                                     }
             
             
                                     if(result2[0].type_reponse == "entier"){
                                       this.navCtrl.push('NombrepetitPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                     }else if(result2[0].type_reponse == "text"){
                                       this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});  
                                     }else if(result2[0].type_reponse == "select"){
                                       this.navCtrl.push('OuestanimalPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});
                                     }else if(result2[0].type_reponse == "rapide"){
                                       this.navCtrl.push('DescriptionPage',{idquestion: this.userconnecte.projet.questions.questions.id, type_question: this.userconnecte.projet.questions.questions_next.type_reponse});  
                                     }else if(result2[0].type_reponse == "annuler"){
                                       this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                                     }
                                   
                                 }else{
                                   /*var listereponses = [];
                                   if(Parcourt.listesReponses != null){
                                     listereponses = Parcourt.listesReponses;
                                     listereponses.push(data.responses);
                                     Parcourt.listesReponses = listereponses;
                                   }else{
                                     
                                     listereponses.push(data.responses);
                                     Parcourt.listesReponses = listereponses;
                                   }*/
             
                                   if(result2[0].type_reponse == "entier"){
                                     this.navCtrl.push('NombrepetitPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});
                                   }else if(result2[0].type_reponse == "text"){
                                     this.navCtrl.push('DescriptionPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});  
                                   }else if(result2[0].type_reponse == "select"){
                                     this.navCtrl.push('OuestanimalPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});
                                   }else if(result2[0].type_reponse == "rapide"){
                                     this.navCtrl.push('DescriptionPage',{idquestion: result2[0].id, type_question: result2[0].type_reponse});  
                                   }else if(result2[0].type_reponse == "annuler"){
                                     this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                                   }
                                   //this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id}); 
                                  
                                }
             
                                 
                             }
                             if(result2[0].type_reponse == "select"){
                               /*var listereponses = [];
                               if(Parcourt.listesReponses != null){
                                 listereponses = Parcourt.listesReponses;
                                 listereponses.push(data.responses);
                                 Parcourt.listesReponses = listereponses;
                               }else{
                                 
                                 listereponses.push(data.responses);
                                 Parcourt.listesReponses = listereponses;
                               }*/
                               this.super(result2[0].id);
                             }
             
                             if(result2[0].type_reponse == "entier"){
                               /*var listereponses = [];
                               if(Parcourt.listesReponses != null){
                                 listereponses = Parcourt.listesReponses;
                                 listereponses.push(data.responses);
                                 Parcourt.listesReponses = listereponses;
                               }else{
                                 
                                 listereponses.push(data.responses);
                                 Parcourt.listesReponses = listereponses;
                               }*/
                               this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                             }

                             if(result2[0].type_reponse == "annuler"){
                              /*var listereponses = [];
                              if(Parcourt.listesReponses != null){
                                listereponses = Parcourt.listesReponses;
                                listereponses.push(data.responses);
                                Parcourt.listesReponses = listereponses;
                              }else{
                                
                                listereponses.push(data.responses);
                                Parcourt.listesReponses = listereponses;
                              }*/


                              
                              var mymessage = ""

                              if(this.langue == 0){
                                mymessage = result2[0].titre_fr;
                              }else{
                                mymessage = result2[0].titre_en;
                              }
                              
                              this.translate.get(['NAVIGATIONERROR', 'CANCELD']).subscribe((langs:Array<string>) => {
                                let alert = this.alertCtrl.create({
                                  title: langs['NAVIGATIONERROR'],
                                  message: mymessage,
                                  buttons: [
                                    {
                                      text: langs['CANCELD'],
                                      role: 'cancel',
                                      handler: () => {
                                        Parcourt.listesReponses = [];
                                        this.navCtrl.setRoot('AcceuilPage').then(()=>{});
                                      }
                                    }
                                  ]
                                });
                                alert.present();
                              })

                           

                          
                          
                            }
                        
                
                
                
                      });


                        
                  });

          }

    
    
    
         }else{

          if(navigator.onLine){


              ///////////////////////////////////////////////////// ONLINE /////////////////////////////////
              /*var listereponses = [];
             
              if(Parcourt.listesReponses != null){ 
                
                listereponses = Parcourt.listesReponses;
                listereponses.push(data.responses);
                Parcourt.listesReponses = listereponses;
              }else{
                listereponses.push(data.responses);
                Parcourt.listesReponses = listereponses;
              }

              this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
              */

               /////////////////////////////////////////////////////////// FOR PROD ////////////////////////////////////

              /*this.services.getQuestionById(element.questions).then((result) =>{
                  
                var listereponses = [];
                
                //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                data.responses = {...data.responses, questions:result[0]};

              
                if(Parcourt.listesReponses != null){
                  listereponses = Parcourt.listesReponses;
                  listereponses.push(data.responses);
                  Parcourt.listesReponses = listereponses;
                }else{
                  listereponses.push(data.responses);
                  Parcourt.listesReponses = listereponses;
                }

                //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});

                this.testvalue = 1;
                
                this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                //this.navCtrl.push('SirinPage', {question: 0, reponse:0});

            });*/

            /////////////////////////////////////////////////// ENONLINE /////////////////////////////////////
         
                this.services.getQuestionById(element.questions).then((result) =>{
                  
                  var listereponses = [];
                  
                  //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                  data.responses = {...data.responses, questions:result[0]};

                
                  if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }else{
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }
                  this.testvalue = 1;
                  this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                
              });
         
            }else{
            
            this.services.getQuestionById(element.questions).then((result) =>{
              
                var listereponses = [];
                
                //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                data.responses = {...data.responses, questions:result[0]};

               
                if(Parcourt.listesReponses != null){
                  listereponses = Parcourt.listesReponses;
                  listereponses.push(data.responses);
                  Parcourt.listesReponses = listereponses;
                }else{
                  listereponses.push(data.responses);
                  Parcourt.listesReponses = listereponses;
                }
                this.testvalue = 1;
                this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
               
            });

          }
                 
            
         }

       }

    });
  }

 

  super(idquestion){
   
    if(idquestion!=0){
      console.log(idquestion);

      if(navigator.onLine){
          //////////////////////////////////////////////////////////////ONLINE ///////////////////////////////////
          /*let loading = this.loadingCtrl.create();
          loading.present();
          this.services.findReponseByQuestionId(idquestion).subscribe((result) =>{  
            this.listesreponses = result; 
            console.log(idquestion);
            console.log(result);
            console.log(result[0].questions);
            this.maquestion = result[0].questions;
            if(this.maquestion.image!=undefined){
              this.printpicture =1;
              this.myimage =this.maquestion.image;
            }else{
              //alert("le mokai");
            }
            if(this.maquestion.isPicture){
              this.ispicture = 1;
            }
            
            console.log(this.listesreponses);
            
            if(this.langue == 0){
              this.titrequestion = result[0].questions.titre_fr;
            }else{
              this.titrequestion = result[0].questions.titre_en;
            }
          }, (error)=>{
            console.log(error);
            //votre opération a  echoué!!
            loading.dismiss();
          }, ()=>{
            loading.dismiss();
          });*/
          ///////////////////////////////////////////////////////////////////////////////////

          ///////////////////////////////////////// PROD ////////////////////////////////////
          /*let loading = this.loadingCtrl.create();
          loading.present();
          this.services.getReponseByQuestionId(idquestion).then((result) =>{
                loading.dismiss();
                this.listesreponses = result; 
                this.services.getQuestionById(idquestion).then((result) =>{
                      loading.dismiss();
                      this.titrequestion = result[0].titre_fr;
                });
               
          });*/
          /////////////////////////////////////////////////////////////////////////////////

          this.services.getReponseByQuestionId(idquestion).then((result) =>{
               
            this.listesreponses = result;
            
            this.services.getQuestionById(idquestion).then((result) =>{
              
                  this.maquestion = result[0];
                  if(this.maquestion.image!=undefined && this.maquestion.image!=null && this.maquestion.image!="null"){
                   
                    this.printpicture =1;
                    this.myimage =this.maquestion.image;
                  }else{
                    //alert("le mokai");
                  }
                  if(this.maquestion.isPicture == "true"){
                    this.ispicture = 1;
                  }

                  if(this.langue == 0){
                    this.titrequestion = result[0].titre_fr;
                  }else{
                    this.titrequestion = result[0].titre_en;
                  }
                  
            });
           
      });


        }else{

          
          this.services.getReponseByQuestionId(idquestion).then((result) =>{
               
                this.listesreponses = result;
                
                this.services.getQuestionById(idquestion).then((result) =>{
                  
                      
                      this.maquestion = result[0];
                      if(this.maquestion.image!=undefined && this.maquestion.image!=null && this.maquestion.image!="null"){
                       
                        this.printpicture =1;
                        this.myimage =this.maquestion.image;
                      }else{
                        //alert("le mokai");
                      }
                      if(this.maquestion.isPicture == "true"){
                        this.ispicture = 1;
                      }

                      if(this.langue == 0){
                        this.titrequestion = result[0].titre_fr;
                      }else{
                        this.titrequestion = result[0].titre_en;
                      }
                      
                });
               
          });
  
  
        }
                   
    }

   

  }



  siren(data){

    this.testvalue = 1;

    this.listesreponses.forEach(element => {
      
        if(element.id == data.id){

          
         
            
          data.responses = element;
           
          
           
            /*console.log(data.reponses);
            console.log(this.userconnecte);
            console.log("==================================== " + typeof data.responses.questions_next + " ======================")
            if(typeof data.responses.questions_next === 'undefined'){
              console.log("==================== ");
              console.log("================================");
              console.log("================================");
            }*/
            

          if((undefined != data.responses["questions_next"])&&(data.responses["questions_next"]!=null)){
             
                 // dans le cas ou il ya la connexion internet 
            if(navigator.onLine){
                 //////////////////////////ONLINE/////////////////////////////////////////
                 /*if(data.responses.questions_next["type_reponse"] == "text"){
    
                   // avant d'y aller on se rassure d'abord que le projet n'a pas aussi une question 
                   // et pour cella nous devons recupérer le projet en cours.
                   var listereponses = [];
                   if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                    }else{
                      
                      listereponses.push(data.responses);
                      Parcourt.listesReponses = listereponses;
                    }
                    if(undefined != this.userconnecte.projet["questions"]){
      
                        if(this.userconnecte.projet.questions["type_reponse"] == "text"){
                            this.navCtrl.push('NombrepetitPage', {idquestion: this.userconnecte.projet.questions.questions.id});
                          }
                        if(this.userconnecte.projet.questions["type_reponse"] == "select"){
                            this.super(this.userconnecte.projet.questions.id);
                        }
                      
                    }else{
                      var listereponses = [];
                      if(Parcourt.listesReponses != null){
                        listereponses = Parcourt.listesReponses;
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }else{
                        
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }
                      this.navCtrl.push('NombrepetitPage', {idquestion: data.responses.questions_next.id}); 
                     
                   }

                    
                }
                if(data.responses.questions_next["type_reponse"] == "select"){
                  var listereponses = [];
                  if(Parcourt.listesReponses != null){
                    listereponses = Parcourt.listesReponses;
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }else{
                    
                    listereponses.push(data.responses);
                    Parcourt.listesReponses = listereponses;
                  }
                  this.super(data.responses.questions_next.id);
                }*/

               /* let loading = this.loadingCtrl.create();
                 loading.present();
      
                  this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                        loading.dismiss();

                        data.responses = {...data.responses, questions:result[0]};
                        var listereponses = [];
                        
                        if(Parcourt.listesReponses != null){
                          listereponses = Parcourt.listesReponses;
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }else{
                          
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }




                         
                        this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{

                            

                              if(result2[0].type_reponse == "text"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "entier"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "select"){
                                this.super(result2[0].id);
                              }
                        });


                        
                  });*/

                  //////////////////////////////////////////////////END ONLINE////////////////////////////////////////////
                  ///////////////////////////////////////////////// LOCAL ///////////////////////////////////////////////

                  this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                      

                    data.responses = {...data.responses, questions:result[0]};
                    var listereponses = [];
                    
                    if(Parcourt.listesReponses != null){
                      listereponses = Parcourt.listesReponses;
                      listereponses.push(data.responses);
                      Parcourt.listesReponses = listereponses;
                    }else{
                      
                      listereponses.push(data.responses);
                      Parcourt.listesReponses = listereponses;
                    }

                     
                    this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{
                           
                         

                                if(result2[0].type_reponse == "text"){
                                  // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                  
                                  this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                                }
                                if(result2[0].type_reponse == "entier"){
                                  // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                  
                                  this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                                }
                                if(result2[0].type_reponse == "select"){
                                  this.super(result2[0].id);
                                }

                          });


                          
                    });

                    //////////////////////////////////////////////////// END LOCAL //////////////////////////////////////////////


          }else{
               // dans le cas ou il n'ya pas de connexion internet  this.type_question = this.navParams.get("type_question");

                
      
                  this.services.getQuestionById(data.responses["questions"]).then((result) =>{
                      

                        data.responses = {...data.responses, questions:result[0]};
                        var listereponses = [];
                        
                        if(Parcourt.listesReponses != null){
                          listereponses = Parcourt.listesReponses;
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }else{
                          
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }

                         
                        this.services.getQuestionById(data.responses["questions_next"]).then((result2) =>{
                               
                             

                              if(result2[0].type_reponse == "text"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "entier"){
                                // cas ou l'objet qui est dans utilisateur a aussi une question à gerer
                                
                                this.navCtrl.push('NombrepetitPage', {idquestion: result2[0].id}); 
                              }
                              if(result2[0].type_reponse == "select"){
                                this.super(result2[0].id);
                              }

                        });


                        
                  });

          }

    
    
    
         }else{

              if(navigator.onLine){

                     /////////////////////////////////////////////////////////////// ONLINE ////////////////////////////////////
                      /*var listereponses = [];
                      if(Parcourt.listesReponses != null){ this.type_question = this.navParams.get("type_question");
                        listereponses = Parcourt.listesReponses;
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }else{
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }

                      
                    //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});
                      //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                      //this.navCtrl.push('SirinPage', {question: 0, reponse:0});
                      this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                     */
                    /////////////////////////////////////////////////////////////// END ONLINE /////////////////////////////////
                    ////////////////////////////////////////////////////////////// LOCAL ///////////////////////////////////////

                      this.services.getQuestionById(element.questions).then((result) =>{
                          
                        var listereponses = [];
                        
                        //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                        data.responses = {...data.responses, questions:result[0]};

                      
                        if(Parcourt.listesReponses != null){
                          listereponses = Parcourt.listesReponses;
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }else{
                          listereponses.push(data.responses);
                          Parcourt.listesReponses = listereponses;
                        }

                        //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                        //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});

                        
                        this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                        //this.navCtrl.push('SirinPage', {question: 0, reponse:0});

                    });
                    //////////////////////////////////////////////////////END LOCAL /////////////////////////////////////////

         
         
            }else{
            
                  this.services.getQuestionById(element.questions).then((result) =>{
                    
                      var listereponses = [];
                      
                      //data.responses["questions"] = result[0]; personne = {...personne, age:valeur}
                      data.responses = {...data.responses, questions:result[0]};

                    
                      if(Parcourt.listesReponses != null){
                        listereponses = Parcourt.listesReponses;
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }else{
                        listereponses.push(data.responses);
                        Parcourt.listesReponses = listereponses;
                      }

                      //this.navCtrl.push('PhotosPage', {question: 0, reponse:0});
                      //this.navCtrl.push('DescriptionPage', {question: 0, reponse:0});

                      
                      this.navCtrl.push('DescriptionPage',{idquestion: this.idquestion, type_question: this.type_question});
                      //this.navCtrl.push('SirinPage', {question: 0, reponse:0});

                    });

             }
                 
            
         }

       }

    });
  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }

  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }

}
