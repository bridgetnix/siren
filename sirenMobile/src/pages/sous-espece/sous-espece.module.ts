import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SousEspecePage } from './sous-espece';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { TranslateModule } from 'ng2-translate';

@NgModule({
  declarations: [
    SousEspecePage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(SousEspecePage),
  ],
})
export class SousEspecePageModule {}
