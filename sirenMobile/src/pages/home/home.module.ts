import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { HomePage } from './home';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    NavbarModule,
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule {}
