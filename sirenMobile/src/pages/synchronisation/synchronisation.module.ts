import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SynchronisationPage } from './synchronisation';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    SynchronisationPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(SynchronisationPage),
  ],
})
export class SynchronisationPageModule {}
