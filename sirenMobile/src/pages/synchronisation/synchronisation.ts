import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from 'ng2-translate';
import { Parcourt } from '../../configs/configs';
import {Geolocation} from '@ionic-native/geolocation';


/**
 * Generated class for the SynchronisationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-synchronisation',
  templateUrl: 'synchronisation.html',
})
export class SynchronisationPage {
 
  public listedesobservations: any;
   

  public userconnecte = {};
  public projetId: any;
  public total = 0;
  public totaleffectue = 0;

  constructor(public translate:TranslateService, private geolocation: Geolocation, public navCtrl: NavController, public toastCtrl: ToastController, public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  }
  
  ionViewDidLoad() {

        this.geolocation.getCurrentPosition().then( (resp) =>{ 
                Parcourt.latitude = resp.coords.latitude;
                Parcourt.longitude = resp.coords.longitude;
        }).catch( (error) =>{   
        console.log("error could not get position");
        });
    
      this.total = 9;
      this.totaleffectue = 0;


      if(navigator.onLine){
        
        let etat = 0;
        this.services.getAllObservationByEtat(etat).then((resultat) =>{
         
               this.listedesobservations = resultat;
               var compteur = 0;
               

               if(this.listedesobservations.length != 0){  
               
                 for (let index = 0; index < this.listedesobservations.length; index++) {

                    
               
                   
                  this.services.saveObservation(this.listedesobservations[index]).subscribe((result) =>{  

                        
                            
                        this.services.updateObservation(this.listedesobservations[index].id).then((resultat) =>{  
                            
                            compteur++;
                            
                            this.services.findresultatByObservationId(this.listedesobservations[index].id).then((resultat2) =>{

                                      ////////////////////// logiquement ici je vais itérer sur la liste des reponses

                                     

                                      if(resultat2!=0){

                                                  var listeresultats: any;
                                                  listeresultats = resultat2;
                                                  var k=0;
                                                          listeresultats.forEach(element => {
                                                          element.observation = result;
                                                          this.services.saveresulatat(element).subscribe((result) =>{ 
                                                              
                                                              
                                                                k++;   
                                                              
                                                                if(k==listeresultats.length){
                                                                
                                                                  if(compteur == this.listedesobservations.length){
                                                                      
                                                                      this.Synchronisateur();
                                                                      
                                                                  }
                                                                }
                                                                
                                                              

                                                          }, (error)=>{
                                                              alert("one error was done " + JSON.stringify(error));
                                                          }, ()=>{
                                                          });
                                                  });



                                      }else{

                                          if(compteur == this.listedesobservations.length){
                                             
                                              this.Synchronisateur();
                                              
                                          }

                                      }
                                      
                                        
                              });

                                          


                                  ///////////////////////////////////////////////////////////////////
                            });
                        
                      }, (error)=>{

                           alert("erreur de sauvegarde de observation " + JSON.stringify(error));
                       
                      }, ()=>{
                      });
                 }

                }else{

                    this.Synchronisateur();

                }
          });

         


      }else{

        this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
          this.presentToast(langs['INFOSCONNEXION']); 
          this.navCtrl.setRoot('AcceuilPage');
        });
        
      }
  }

  


  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
    });
    toast.present();
  }



  Synchronisateur(){




    this.services.deletteAllPays().then((result1) =>{
       
        this.services.getpays().subscribe((result) =>{ 
              var i = 0;  
              result.forEach(element => {
                  this.services.createPays(element).then((result1) =>{
                      i++;
                      if(i==result.length){
                          this.totaleffectue = this.totaleffectue + 1;
                          
                          if(this.totaleffectue == this.total){
                            this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                            });
                          }
                      }
                  });
              });
        }, (error)=>{
          
         
        }, ()=>{
          
        });
    });
      

      ////////////////////////////////////////////////////////////////

      this.services.deletteAllFonctions().then((result1) =>{
            
            this.services.getfonctions().subscribe((result) =>{   
                  var i=0;
                  result.forEach(element => {
                      this.services.createFonction(element).then((result1) =>{

                        i++;
                        if(i==result.length){
                            
                            this.totaleffectue = this.totaleffectue + 1;
                            if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                          
                      });
                  });
            }, (error)=>{
             
              
            }, ()=>{
             
            });
      })
      
      
      

      ////////////////////////////////////////////////////////////////
      this.services.deletteAllGroupes().then((result1) =>{
         
          this.services.groupes().subscribe((result) =>{  
                var i = 0; 
                result.forEach(element => {
                    this.services.createGroupe(element).then((result1) =>{
                      i++;
                      if(i==result.length){
                         
                          this.totaleffectue = this.totaleffectue + 1;
                          if(this.totaleffectue == this.total){
                            this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                            });
                          }
                      }  
                    });
                });
          }, (error)=>{
            
           
          }, ()=>{
            
          });
      });
     

      ////////////////////////////////////////////////////////////////

      this.services.deletteAllSousGroupes().then((result1) =>{
           
            this.services.allsousgroupes().subscribe((result) =>{  
                  var i=0; 
                  result.forEach(element => {
                      let sous_groupe = {
                        "id": element.id, 
                        "nom_fr": element.nom_fr, 
                        "nom_en": element.nom_en, 
                        "image": element.image, 
                        "description_fr": element.description_fr, 
                        "description_en": element.description_en, 
                        "groupes": element.groupes.id
                      }
                      this.services.createSousGroupe(sous_groupe).then((result1) =>{
                        i++;
                        if(i==result.length){
                            
                            this.totaleffectue = this.totaleffectue + 1;
                            if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['SUCCESSOPERATION']); 
                                    this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                      });
                  });
            }, (error)=>{

                alert("uen erreur au rendez-vous c quoi le pb sousgroupe");
                alert(JSON.stringify(error));
              
            }, ()=>{
              
            });
      });
      ////////////////////////////////////////////////////////////////

      this.services.deletteAllReponses().then((result1) =>{
            
            this.services.allreponses().subscribe((result) =>{ 
                  var i=0;  
                  result.forEach(element => {
                      
                      if(undefined!= element.questions_next){
                          let reponse = {};
                         
                          reponse = {
                            "id": element.id, 
                            "titre_fr": element.titre_fr, 
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id, 
                            "questions_next": element["questions_next"].id,
                            "image":element.image,
                            "taille":element.taille
                          }
                          this.services.createReponse(reponse).then((result1) =>{

                            i++;
                            if(i==result.length){
                                
                                this.totaleffectue = this.totaleffectue + 1;
                                if(this.totaleffectue == this.total){
                                    this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                        this.presentToast(langs['SUCCESSOPERATION']); 
                                        this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                          
                          });
                      }else{
                          let reponse = {};
                          reponse = {
                            "id": element.id, 
                            "titre_fr": element.titre_fr, 
                            "titre_en": element.titre_en,
                            "questions": element["questions"].id,
                            "image":element.image,
                            "taille":element.taille
                      
                          }

                          this.services.createReponse(reponse).then((result1) =>{
                                i++;
                                if(i==result.length){
                                    
                                    this.totaleffectue = this.totaleffectue + 1;
                                    if(this.totaleffectue == this.total){
                                        this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                            this.presentToast(langs['SUCCESSOPERATION']); 
                                            this.navCtrl.setRoot('AcceuilPage');
                                        });
                                    }
                                }
                          });
                     }
                      
                  });
            }, (error)=>{
            }, ()=>{
             
            });
        });
      ////////////////////////////////////////////////////////////////

      this.services.deletteAlltypeObservation().then((result1) =>{
        
        this.mylocalstorage.getSession().then((result) =>{
            this.userconnecte = result;
          //this.services.acceuil().subscribe((result) =>{ 
            this.services.projet_typeObservation(this.userconnecte["projet"].id).subscribe((result) =>{  
                var i=0;
                result.forEach(element => {
                    
                    this.services.createTypeObservation(element).then((result1) =>{

                      i++;
                     
                      if(i==result.length){
                       
                          this.totaleffectue = this.totaleffectue + 1;
                          if(this.totaleffectue == this.total){
                            this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                            });
                          }
                      }
                        
                    });
                });
            }, (error)=>{
                console.log(error);
                
            }, ()=>{
                
            });
        })
      });
      ////////////////////////////////////////////////////////////////


      this.services.deletteAllProjets().then((result1) =>{
         
          this.mylocalstorage.getSession().then((result) =>{
              
              this.userconnecte = result;
              
              this.services.findProjetsByUserId(this.userconnecte["id"]).subscribe((result) =>{   
                      var i=0;
                      result.forEach(element => {
                        var projet = {
                          "id": element.id, 
                          "nom": element.nom, 
                          "rowid":element.id,
                          "lieu": element.lieu, 
                          "public": element.public, 
                          "pays": element["pays"].id, 
                          "utilisateurs": element['utilisateurs'].id,
                          "note":element.note,
                          "logo":element.logo,
                          "mention":element.mention
                        }
                        
                        this.services.createProjet(projet).then((result1) =>{

                          i++;
                          if(i==result.length){
                              
                              this.totaleffectue = this.totaleffectue + 1;
                              if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['SUCCESSOPERATION']); 
                                    this.navCtrl.setRoot('AcceuilPage');
                                });
                              }
                          }
                            
                        });
                    });
              }, (error)=>{
                
              }, ()=>{
                
              });
          })

      })      

      ////////////////////////////////////////////////////////////////


      ////////////////////////////////////////////////////////////////


      this.services.deletteAllProjetMenu().then((result1) =>{
         
        this.mylocalstorage.getSession().then((result) =>{
            
            this.userconnecte = result;
            this.projetId =  this.userconnecte["projet"].id;
            
            this.services.getprojet_typeObservation(this.projetId).subscribe((result) =>{   
                    var i=0;
                    //this.services.setCurrentMenuProjet(result);
                    result.forEach(element => {
                      var projet = {
                        "id": element.id, 
                        "nomFr": element.nomFr, 
                        "nomEn":element.nomEn,
                        "title": element.title
                      }
                      
                      this.services.createProjetMenu(projet).then((result1) =>{

                        i++;
                        if(i==result.length){
                            
                            this.totaleffectue = this.totaleffectue + 1;
                            if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                this.presentToast(langs['SUCCESSOPERATION']); 
                                this.navCtrl.setRoot('AcceuilPage');
                                });
                            }
                        }
                        
                          
                      });
                  });
            }, (error)=>{
              
            }, ()=>{
              
            });
        })

    })      

    ////////////////////////////////////////////////////////////////



      this.services.deletteAllEspeces().then((result1) =>{
            
            this.mylocalstorage.getSession().then((result) =>{
              
              this.userconnecte = result;

              
              this.projetId =  this.userconnecte["projet"].id;

             
              
              this.services.findEspecesByProjetId(this.projetId).subscribe((result) =>{  
                    var i=0; 
                   
                    result.forEach(element => {

                      var espece = {
                        "id": element.id, 
                        "nom_fr":element.nom_fr, 
                        "nom_en":element.nom_en,
                        "image": element.image, 
                        "description_fr": element.description_fr, 
                        "description_en": element.description_en, 
                        "sous_groupes": element["sous_groupes"].id, 
                        "questions_animal": element["questions_animal"].id, 
                        "questions_menaces": element["questions_menances"].id, 
                        "questions_signe": element["questions_signe"].id, 
                        "questions_alimentation": element["questions_alimentation"].id
                      }
                        
                        this.services.createEspece(espece).then((result1) =>{
                             
                            i++;
                            if(i==result.length){
                                
                                this.totaleffectue = this.totaleffectue + 1;
                                if(this.totaleffectue == this.total){
                                    this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                        this.presentToast(langs['SUCCESSOPERATION']); 
                                        this.navCtrl.setRoot('AcceuilPage');
                                    });
                                }
                            }
                            
                        });
                    });
              }, (error)=>{

                  alert("une erreur au rendez-vous c quoi le pb");
                  alert(JSON.stringify(error));
                
              }, ()=>{
                
              });
          })
      })
          

    ////////////////////////////////////////////////////////////////

      
         
            this.services.deletteAllUtilisateur().then((result) =>{   
              this.mylocalstorage.getSession().then((result2) =>{
              
                this.userconnecte = result2;
                this.projetId =  this.userconnecte["projet"].id;

                

                    var utilisateurs = {
                      "id":this.userconnecte["id"],
                      "username":this.userconnecte["email"], 
                      "email":this.userconnecte["email"], 
                      "password": this.userconnecte["password"], 
                      "ville": this.userconnecte["ville"], 
                      "telephone": this.userconnecte["telephone"], 
                      "pays": this.userconnecte["pays"].id, 
                      "fonctions": this.userconnecte["fonctions"].id, 
                      "projet":this.userconnecte["projet"].id,
                      "monuserid":this.userconnecte["id"]
                    }
                    this.services.createUtilisateur(utilisateurs).then((result1) =>{
                        
                    });
              }); 
            }); 

    ////////////////////////////////////////////////////////////////


  
      this.services.deletteAllInscription().then((result) =>{   
        this.mylocalstorage.getSession().then((result2) =>{
         
          this.userconnecte = result2;
          this.projetId =  this.userconnecte["projet"].id;

          
              var utilisateurs = {
                "id":this.userconnecte["id"],
                "username":this.userconnecte["email"], 
                "email":this.userconnecte["email"], 
                "password": this.userconnecte["password"], 
                "ville": this.userconnecte["ville"], 
                "telephone": this.userconnecte["telephone"], 
                "pays": this.userconnecte["pays"].id, 
                "fonctions": this.userconnecte["fonctions"].id, 
                "projet":this.userconnecte["projet"].id,
                "monuserid":this.userconnecte["id"]
              }
              this.services.createInscription(utilisateurs).then((result1) =>{
                  
              });
         }); 
      }); 

    ////////////////////////////////////////////////////////////////

    

    this.services.deletteAllQuestions().then((result1) =>{
            
              this.services.allquestions().subscribe((result) =>{   
                    var i=0;

                    result.forEach(element => {
                        var myquestionnext;
                        if(element["questions"]==undefined){
                          myquestionnext = "null";
                        }else{
                          myquestionnext = element["questions"].id;
                        }
                        var question = {
                          "id": element.id, 
                          "titre_fr": element.titre_fr, 
                          "titre_en":element.titre_en, 
                          "type_reponse":element.type_reponse, 
                          "interval":element.interval, 
                          "type_debut":element.type_debut,
                          "image":element.image,
                          "isPicture":element.isPicture,
                          "questions":myquestionnext
                        }
                        this.services.createQuestion(question).then((result1) =>{

                          i++;
                          if(i==result.length){
                            
                              this.totaleffectue = this.totaleffectue + 1;
                              if(this.totaleffectue == this.total){
                                this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                                    this.presentToast(langs['SUCCESSOPERATION']); 
                                    this.navCtrl.setRoot('AcceuilPage');
                                });
                              }
                          }
                            
                        });
                    });
              }, (error)=>{
               
              }, ()=>{
               
              });
      });
      ////////////////////////////////////////////////////////////////
     

  }




}
