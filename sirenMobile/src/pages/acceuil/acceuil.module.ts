import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcceuilPage } from './acceuil';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { TranslateModule } from 'ng2-translate';

@NgModule({
  declarations: [
    AcceuilPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(AcceuilPage),
  ],
})
export class AcceuilPageModule {}
