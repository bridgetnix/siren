import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from 'ng2-translate';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../../configs/configs';

/**
 * Generated class for the MesnotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mesnotifications',
  templateUrl: 'mesnotifications.html',
})
export class MesnotificationsPage {

  public mesnotifications = [];
  public userconnecte = {};
  public projetId:any;
  constructor(public translate:TranslateService,private geolocation: Geolocation, public navCtrl: NavController, public toastCtrl: ToastController, public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  }

  ionViewDidLoad() {

        this.geolocation.getCurrentPosition().then( (resp) =>{ 
              Parcourt.latitude = resp.coords.latitude;
              Parcourt.longitude = resp.coords.longitude;
        }).catch( (error) =>{   
        console.log("error could not get position");
        });
  
       if(navigator.onLine){
          this.mylocalstorage.getSession().then((result) =>{
          
            this.userconnecte = result;
            this.projetId =  this.userconnecte["projet"].id;
            let loading = this.loadingCtrl.create();
            loading.present();
            this.services.findNotificationByUserId(this.projetId).subscribe((resultat) =>{  
                
                  this.mesnotifications = resultat;
            
              }, (error)=>{
              console.log(error);
              //votre opération a  echoué!!
              loading.dismiss();
            }, ()=>{
              loading.dismiss();
            });


          })

        }else{

          this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
            this.presentToast(langs['INFOSCONNEXION']); 
            this.navCtrl.setRoot('AcceuilPage');
          });
          
          
        }

      

  }



  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
    });
    toast.present();
  }

}
