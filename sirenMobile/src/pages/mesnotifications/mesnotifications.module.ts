import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MesnotificationsPage } from './mesnotifications';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    MesnotificationsPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(MesnotificationsPage),
  ],
})
export class MesnotificationsPageModule {}
