import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';

/**
 * Generated class for the CommentairePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commentaire',
  templateUrl: 'commentaire.html',
})
export class CommentairePage {

  public userconnecte = {};
  public projetId:any;
  constructor(public translate:TranslateService, public navCtrl: NavController, public alertCtrl: AlertController, public toastCtrl: ToastController, public mylocalstorage: LocalStorageProvider, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  }


  ionViewDidLoad() {

    if(navigator.onLine){
      this.showRadio();

    }else{


      this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
        this.presentToast(langs['INFOSCONNEXION']); 
        this.navCtrl.setRoot('AcceuilPage');
      });
    }
  }


  showRadio(){ 
    
    this.translate.get(['LEAVECOMMENT', 'ENREGISTRER', 'SUCCESSOPERATION']).subscribe((langs:Array<string>) => {

            let alert = this.alertCtrl.create();
            alert.setTitle(langs['LEAVECOMMENT']);
            alert.addInput({
              name: 'commentaire',
              type: 'textarea'
            });
            
            alert.addButton({
              text:langs['ENREGISTRER'],
              cssClass:'alertDanger',
              handler:data => {
                console.log();
                

                this.mylocalstorage.getSession().then((result) =>{
            
                  this.userconnecte = result;
                  this.projetId =  this.userconnecte["projet"].id;
                  let loading = this.loadingCtrl.create();
                  loading.present();
                  this.services.saveCommentaire(this.userconnecte["email"], data.commentaire).subscribe((resultat) =>{  
                        this.presentToast(langs['SUCCESSOPERATION']); 
                        this.navCtrl.setRoot('AcceuilPage');
                  
                    }, (error)=>{
                    console.log(error);
                    //votre opération a  echoué!!
                    loading.dismiss();
                  }, ()=>{
                    loading.dismiss();
                  });
                })
              }
            });

            alert.present();


    });

  }


  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
    });
    toast.present();
  }

}
