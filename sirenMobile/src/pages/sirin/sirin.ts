import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Parcourt } from '../../configs/configs';
import { LocalStorageProvider } from '../../providers/localstorage/localstorage';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from 'ng2-translate';
import {Geolocation} from '@ionic-native/geolocation';
import { LanguageService } from '../../providers/services/language';


/**
 * Generated class for the SirinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sirin',
  templateUrl: 'sirin.html',
})
export class SirinPage {


  public question: any;
  public reponse: any;
  public observation: any;
  public userconnecte: any;

  public latitude: any;
  public longitude: any;
  public typeobservation: any;
  public groupe: any;
  public sous_groupe: any;
  public espece: any;
  public listequestions = [];
  public description: any;
  public langue: any; 
  public testeur: any; 
  public madate: any; 
  public myuserconnecte: any;
  public testvalue=0;

  constructor(public navCtrl: NavController, public language:LanguageService,private geolocation: Geolocation, public translate:TranslateService, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public services: ServicesProvider, public mylocalstorage: LocalStorageProvider, public navParams: NavParams) {
 
  }


  ionViewWillLeave() {

    


    var listereponses = [];
    if(this.testvalue == 0){
      listereponses = Parcourt.listesReponses;
      console.log(listereponses);
      listereponses.pop();
      console.log(listereponses);
      Parcourt.listesReponses = listereponses;
    }
  }
  ionViewWillEnter() {
    this.testvalue = 0;
  }

  ionViewDidLoad() {

    this.language.loadLanguage().then((result) =>{
        if(result == 'en'){
          this.langue = 1;
        }else{
          this.langue = 0;
        }
    });

    console.log(Parcourt.listesReponses);

    this.mylocalstorage.getSession().then((result) =>{
      this.myuserconnecte = result;
      if(this.myuserconnecte!=null){
        if((!undefined == this.myuserconnecte.projet)||(this.myuserconnecte.projet!=null)){
          if((!undefined == this.myuserconnecte.projet.couleur)||(this.myuserconnecte.projet.couleur!=null)){
            this.updateColor(this.myuserconnecte.projet.couleur);
          }else{
            this.updateColor("#1b9eea");
          }
        }
      }
    })

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });


    this.question = this.navParams.get("question");
    this.reponse = this.navParams.get("reponse");
    this.latitude = Parcourt.latitude;
    this.longitude = Parcourt.longitude;
    this.typeobservation = Parcourt.typeobservation.nom_fr;

    if( Parcourt.indicateur==1){
      this.testeur = 1;
      this.groupe = Parcourt.groupe.nom_fr;
      this.sous_groupe = Parcourt.sousgroupe.nom_fr;
      this.espece = Parcourt.espece.nom_fr;
    }else  if( Parcourt.indicateur==2){

      Parcourt.groupe = {
        id:0
      }
      Parcourt.sousgroupe = {
        id:0
      }
      Parcourt.espece = {
        id:0
      }
    

    }else{

      Parcourt.groupe = {
        id:0
      }
      Parcourt.sousgroupe = {
        id:0
      }
      Parcourt.espece = {
        id:0
      }

      this.testeur = 3;

    }

    

    this.listequestions = Parcourt.listesReponses;
    
    this.description = Parcourt.description;
    this.madate = new Date();


  }


  showActionsheet(){

    this.testvalue =1;

    var today = new Date();
    this.mylocalstorage.getSession().then((result) =>{

             this.userconnecte = result;  
             
             
             /*if(navigator.onLine){

                  this.observation = {
                    "projet":this.userconnecte.projet.id,
                    "dateo":Math.round(new Date().getTime()/1000),
                    "utilisateur":this.userconnecte.id,
                    "coordX":Parcourt.latitude,
                    "coordY":Parcourt.longitude,
                    "typeObservations":Parcourt.typeobservation.id,
                    "groupe":Parcourt.groupe.id,
                    "sousgroupe":Parcourt.sousgroupe.id,
                    "espece": Parcourt.espece.id,
                    "img1File": Parcourt.observation.image1,
                    "img2File":Parcourt.observation.image2,
                    "img3File":Parcourt.observation.image3,
                    "img4File":Parcourt.observation.image4,
                    "note":Parcourt.description,
                    "etat":1
                  };

                  let loading = this.loadingCtrl.create();
                  loading.present();

                  this.services.saveObservation(this.observation).subscribe((result) =>{  
                    
                        
                        //this.observation = {...this.observation,  "etat":1}
                        if(Parcourt.listesReponses.length >0){

                              Parcourt.listesReponses.forEach(element => {

                               
                                  let resultat = {
                                    "contenu":element.titre_fr, 
                                    "questions":element.questions.id, 
                                    "observations":result,
                                    "etat":1
                                  }

                                  this.services.saveresulatat(resultat).subscribe((result) =>{ 
                                      
                                  }, (error)=>{
                                      alert("one error was done");
                                  }, ()=>{
                                  });


                                  
                                
                            });
                         }
                         
                    

                    this.observation = {...this.observation, madate:today};

                    this.services.createObservation(this.observation).then((result) =>{

                      this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                         

                        if(Parcourt.listesReponses.length >0){

                             var i =0;

                              Parcourt.listesReponses.forEach(element => {
                                 
                                  i++;
                              
                                  let resultat = {
                                    "contenu":element.titre_fr, 
                                    "questions":element.questions.id, 
                                    "observations":result,
                                    "etat":1
                                  }

                                  this.services.createResultat(resultat).then((result) =>{
                                    
                                    if(Parcourt.listesReponses.length == i){

                                      this.presentToast(langs['SUCCESSOPERATION']);
                                      Parcourt.listesReponses = null;     
                                      this.navCtrl.setRoot('MesobservationsPage');

                                    }
                                     
                                    
                                  });
                                  

                            });
                        }else{
                          
                          Parcourt.listesReponses = null;     
                          this.navCtrl.setRoot('MesobservationsPage');

                        }
                            
                        
                      });

                    });

                  }, (error)=>{
                    alert("error na fo boutokou");
                    console.log(error);
                    loading.dismiss();
                  }, ()=>{
                    loading.dismiss();
                  });


        

            }else{*/

                this.observation = {
                    "projet":this.userconnecte["projet"].id,
                    "dateo":Math.round(new Date().getTime()/1000),
                    "utilisateur":this.userconnecte.id,
                    "coordX":Parcourt.latitude,
                    "coordY":Parcourt.longitude,
                    "typeObservations":Parcourt.typeobservation.id,
                    "groupe":Parcourt.groupe.id,
                    "sousgroupe":Parcourt.sousgroupe.id,
                    "espece": Parcourt.espece.id,
                    "img1File": Parcourt.observation.image1,
                    "img2File":Parcourt.observation.image2,
                    "img3File":Parcourt.observation.image3,
                    "img4File":Parcourt.observation.image4,
                    "note":Parcourt.description,
                    "etat":0
                  };

                  //this.observation = {...this.observation,  "etat":0}

                  this.observation = {...this.observation, madate:today};
                  
                  this.services.createObservation(this.observation).then((result) =>{
                    if(result == 0){
                      this.presentToast('insert not working!');
                    }else{
                      this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
   
                          this.presentToast(langs['SUCCESSOPERATION']);

                         
  
                          if(Parcourt.listesReponses.length > 0){

                               var i =0;

                                Parcourt.listesReponses.forEach(element => {

                                    i++;

                                    let resultat = {
                                      "contenu":element.titre_fr, 
                                      "questions":element.questions.id, 
                                      "observations":result,
                                      "etat":0
                                    }

                                    this.services.createResultat(resultat).then((result) =>{

                                         if(Parcourt.listesReponses.length==i){
                                          Parcourt.listesReponses = null;
                                          this.navCtrl.setRoot('MesobservationsPage');
                                         }
                                        
                                    });
                                  
                              });

                              this.observation = {};
                              this.navCtrl.setRoot('MesobservationsPage');


                          }else{
                         
                            Parcourt.listesReponses = null;
                            this.navCtrl.setRoot('MesobservationsPage');

                          }
                         
                          
                      });
                    }
                    
                  });


                  /**/

                  


           // }





  }) 


 


  }


  presentToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }


  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }


  backFunction(color) {
    Parcourt.listesReponses = [];
    this.navCtrl.setRoot('AcceuilPage').then(()=>{});
  }
  

  
                                     

}
