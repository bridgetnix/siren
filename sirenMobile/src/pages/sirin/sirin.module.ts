import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SirinPage } from './sirin';
import { TranslateModule } from 'ng2-translate';
import { NavbarModule } from '../../components/navbar/navbar.module';

@NgModule({
  declarations: [
    SirinPage,
  ],
  imports: [
    NavbarModule,
    TranslateModule,
    IonicPageModule.forChild(SirinPage),
  ],
})
export class SirinPageModule {}
