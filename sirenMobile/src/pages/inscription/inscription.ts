import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ServicesProvider } from '../../providers/services/services';
import { TranslateService } from 'ng2-translate';
import { Parcourt } from '../../configs/configs';
import {Geolocation} from '@ionic-native/geolocation';

/**
 * Generated class for the InscriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inscription',
  templateUrl: 'inscription.html',
})
export class InscriptionPage {

  public temoininscription = 0;
  public utilisateur = {};
  public listepays: any;
  public listefonctions: any;
  public langue: any;
  constructor(public translate:TranslateService, private geolocation: Geolocation, public navCtrl: NavController, public toastCtrl: ToastController, public loadingCtrl: LoadingController, public navParams: NavParams, public services: ServicesProvider) {
  
  }

  ionViewDidLoad() {

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
          Parcourt.latitude = resp.coords.latitude;
          Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
    console.log("error could not get position");
    });

    if(navigator.onLine){
      this.getAllPays();
      this.getAllFonctions();
    }else{
      this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
        this.presentToast(langs['INFOSCONNEXION']);  
      });
    }
  }

  inscription(){
    if(navigator.onLine){

          let loading = this.loadingCtrl.create();
          loading.present();
          

          this.services.saveUsers(this.utilisateur).subscribe((result) =>{      
              this.temoininscription = 1;
              this.translate.get(['SUCCESSOPERATION']).subscribe((langs:Array<string>) => {
                  this.presentToast(langs['SUCCESSOPERATION']);
              });
          }, (error)=>{
            console.log(error);
            loading.dismiss();
          }, ()=>{
            loading.dismiss();
          });
     
      }else{
          this.translate.get(['INFOSCONNEXION']).subscribe((langs:Array<string>) => {
            this.presentToast(langs['INFOSCONNEXION']);  
          });
     }
  }

  connexion(){
    this.navCtrl.setRoot('ConnexionPage');
  }

  getAllPays(){

      let loading = this.loadingCtrl.create();
      loading.present();
      this.services.getpays().subscribe((result) =>{
            this.listepays = result;
      }, (error)=>{
        console.log(error);
        loading.dismiss();
      }, ()=>{
        loading.dismiss();
      });

  }

  getAllFonctions(){

    let loading = this.loadingCtrl.create();
    loading.present();

    this.services.getfonctions().subscribe((result) =>{
          
          this.listefonctions = result;

    }, (error)=>{
      console.log(error);
      loading.dismiss();
    }, ()=>{
      loading.dismiss();

      
    });

}


presentToast(message: any) {
  let toast = this.toastCtrl.create({
    message: message,
    duration: 3000
  });
  toast.present();
}



}
