import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalStorageProvider } from '../providers/localstorage/localstorage';
import { TranslateService } from 'ng2-translate/src/translate.service';
import { LanguageService } from '../providers/services/language';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import {Network} from "@ionic-native/network";
import { ServicesProvider } from '../providers/services/services';
import {Geolocation} from '@ionic-native/geolocation';
import { Parcourt } from '../configs/configs';
import { Events } from 'ionic-angular';

@Component({
  templateUrl: 'app.html',
  queries: {
    nav: new ViewChild('content')
  }
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;


  
  rootPage: any;
  public userconnecte: any;
  public email: any;
  pages: any;
  testimage = 0;
  testcolor = 0;
  public myresult=[];
  public myTitle=[];
  public langue = 0;

  public AJOUTEROBSERVATION=0;
  public PROFIL=0;
  public LANGUE=0;
  public MESPROJETS=0;
  public CHANGEPROJET=0;
  public MESOBSERVATION=0;
  public MESNOTIFICATION=0;
  public COMMANTAIRE=0;
  public ALLEZSURCARTE=0;
  public ALLEZSURSITE=0;
  public SYNCRONISATION=0;

  

  constructor(public platform: Platform, public events: Events, private geolocation: Geolocation, public services: ServicesProvider, public network: Network, private sqlite: SQLite, public alertCtrl: AlertController, public translate:TranslateService, public language:LanguageService, public mylocalstorage: LocalStorageProvider, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    events.subscribe('user:created', (user, time) => {
      this.services.getAllProjetMenu().then((myresult:any) =>{

        myresult.forEach(element => {
          this.myresult.push(element.title);
          if(this.langue == 0){
            this.myTitle.push(element.nomFr);
          }else{
            this.myTitle.push(element.nomEn);
          }  
        });
    })
      


    });

    //  if(window["cordova"]){
    //    alert("le mokai");
    //  } else {
    //    alert("le mokai pas de cordova");
    //  }

    // used for an example of ngFor and navigation
    this.pages = [
       { title: 'AJOUTEROBSERVATION', component: 'AcceuilPage' , icon: 'home'},
       { title: 'PROFIL', component: 'ProfilsPage' , icon: 'person'},
       { title: 'LANGUE', component: 'LanguePage' , icon: 'chatboxes'},
       { title: 'MESPROJETS', component: 'MesprojetPage' , icon: 'albums'},
       { title: 'CHANGEPROJET', component: 'ChangeprojetPage' , icon: 'transgender'},
       { title: 'MESOBSERVATION', component: 'MesobservationsPage' , icon: 'document'},
       { title: 'MESNOTIFICATION', component: 'MesnotificationsPage' , icon: 'notifications'},
       { title: 'COMMANTAIRE', component: 'CommentairePage' , icon: 'book'},
       { title: 'ALLEZSURCARTE', component: 'CartePage' , icon: 'globe'},
       { title: 'ALLEZSURSITE', component: 'SitewebPage' , icon: 'globe'},
       { title: 'MENTIOLEGALE', component: 'MentionlegalPage' , icon: 'globe'},
       { title: 'SYNCRONISATION', component: 'SynchronisationPage' , icon: 'git-network'},
       { 
         title: 'DECONNEXION', 
         component: 'ConnexionPage',
         count: 0, 
         icon: 'log-out',
         confirmAction:()=>{
          this.translate.get(['Cancel','Confirm Logout', 'OK']).subscribe((langs:Array<string>) => {
            this.alertCtrl.create({
              message: langs['Confirm Logout'],
              buttons: [{
                  text:langs['Cancel'],
                  handler: data => {
                  }
                },{
                  text:langs['OK'],
                  handler: data => {
                    this.mylocalstorage.logut();
                    this.nav.setRoot('ConnexionPage');
                    this.rootPage = 'ConnexionPage';
                  }
                }
                ]
            }).present();
                });
        }
        
      },
    ];

    
    this.mylocalstorage.getSession().then((result) =>{
        this.userconnecte = result;
        if(this.userconnecte!=null){
          console.log(this.userconnecte);
          this.email = this.userconnecte.email;
        }
        
    })
    

    this.language.loadLanguage().then((lan:string)=>{
      this.translate.setDefaultLang(lan);
      this.mylocalstorage.getSession().then((result)=>{
            if(result==null){
               //this.rootPage = 'PhotosPage';
               this.rootPage = 'ConnexionPage';
               
              //this.rootPage = 'SynchronisationPage';
              
            }else{
              this.rootPage = 'AcceuilPage';
              //this.rootPage = 'InscriptionPage';
            }
      });
    });

  }

  updateColor(color) {
    document.documentElement.style.setProperty(`--color`, color);
  }


  
 

  

  initializeApp() {


    this.language.loadLanguage().then((result) =>{
        if(result == 'en'){
          this.langue = 1;
        }else{
          this.langue = 0;
        }
    });

    

   

    this.mylocalstorage.getSession().then((result) =>{
        this.userconnecte = result;
        if(this.userconnecte!=null){
          console.log(this.userconnecte);
          console.log("===");
          
          if((!undefined == this.userconnecte.projet)||(this.userconnecte.projet!=null)){
            
            this.updateColor(this.userconnecte.projet.couleur)
            /////////////////////////////// CONNECTED MODE ////////////////////////////
           
            
            /*this.services.currentMenusListner.subscribe(next =>{
              next.forEach(element => {
                this.myresult.push(element.title);
                if(this.langue == 0){
                  this.myTitle.push(element.nomFr);
                }else{
                  this.myTitle.push(element.nomEn);
                }  
              });
            })*/

            if(navigator.onLine){
                this.services.getprojet_typeObservation(this.userconnecte.projet.id).subscribe((myresult) =>{
                    console.log("==============================");
                    console.log(myresult);  
                    console.log(this.userconnecte.projet);
                    console.log(this.userconnecte.projet.id);
                    myresult.forEach(element => {
                      this.myresult.push(element.title);
                      if(this.langue == 0){
                        this.myTitle.push(element.nomFr);
                      }else{
                        this.myTitle.push(element.nomEn);
                      }
                      
                    });
                    console.log("==============================");
                }, (error)=>{ 
                }, ()=>{
                });

                /*this.services.getAllProjetMenu().then((myresult:any) =>{

                  myresult.forEach(element => {
                    this.myresult.push(element.title);
                    if(this.langue == 0){
                      this.myTitle.push(element.nomFr);
                    }else{
                      this.myTitle.push(element.nomEn);
                    }  
                  });
                })*/

            }else{

              this.services.getAllProjetMenu().then((myresult:any) =>{

                    myresult.forEach(element => {
                      this.myresult.push(element.title);
                      if(this.langue == 0){
                        this.myTitle.push(element.nomFr);
                      }else{
                        this.myTitle.push(element.nomEn);
                      }  
                    });
                

              })

            }
            //////////////////////////////////////////////////////////////////
            
            

            if((!undefined == this.userconnecte.projet.couleur)|| (this.userconnecte.projet.couleur!=null)){
              this.testcolor = 1;
            }
            if((!undefined == this.userconnecte.projet.image)|| (this.userconnecte.projet.image!=null)){
              this.testimage = 1;
            }
            
          }
          this.email = this.userconnecte.email;

        }
        
    })

    this.geolocation.getCurrentPosition().then( (resp) =>{ 
              Parcourt.latitude = resp.coords.latitude;
              Parcourt.longitude = resp.coords.longitude;
    }).catch( (error) =>{   
        console.log("error could not get position");
    });

    this.intiliazedatabase();

    setInterval(() => { 

            if(navigator.onLine){

                    this.mylocalstorage.getSession().then((result) =>{
                          this.userconnecte = result;
                          this.geolocation.getCurrentPosition().then( (resp) =>{ 
                              
                              
                                var utilisateurId = this.userconnecte["id"];
                                var projetId = this.userconnecte["projet"].id;
                                this.services.savepositions(resp.coords.latitude, resp.coords.longitude, utilisateurId, projetId).subscribe((result) =>{
                                      
                                    Parcourt.latitude = resp.coords.latitude;
                                    Parcourt.longitude = resp.coords.longitude;
                                
                                }, (error)=>{
                                }, ()=>{
                                });
                          }).catch( (error) =>{   
                              console.log("error could not get position");
                          });
                    });
            }
    }, 30000);

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });


  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
  
    if(page.confirmAction == null){
      this.nav.setRoot(page.component);
      
    }else{
      page.confirmAction();
    }
  }


  
  isAutorise(title){
    return this.myresult.indexOf(title) > -1;
  }

  returnTitle(title){
    var index = this.myresult.indexOf(title);
    return this.myTitle[index];
  }


  intiliazedatabase() {

    this.sqlite.create({
      name: 'sirendb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      

      db.executeSql('CREATE TABLE IF NOT EXISTS PAYS (id integer primary key, nom_fr, nom_en, code_tel, sigle)', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS CHECKCONNEXION (id integer primary key, valeur)', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS FONCTIONS (id integer primary key, nom_fr, nom_en)', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS INSCRIPTION (id integer primary key, username, email, telephone, password, ville, pays integer, fonction integer, monuserid integer , FOREIGN KEY(pays) REFERENCES PAYS(id), FOREIGN KEY(fonction) REFERENCES PAYS(FONCTIONS))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS GROUPES (id integer primary key, nom_fr, nom_en, image, description_fr, description_en)', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS SOUS_GROUPES (id integer primary key, nom_fr, nom_en, image, description_fr, description_en, groupes integer, FOREIGN KEY(groupes) REFERENCES GROUPES(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS QUESTIONS (id integer primary key, titre_fr, titre_en, type_reponse, interval, type_debut, image, isPicture, questions)', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS REPONSES (id integer primary key, titre_fr, titre_en, questions integer, questions_next integer, image, taille, FOREIGN KEY(questions) REFERENCES QUESTIONS(id), FOREIGN KEY(questions_next) REFERENCES QUESTIONS(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS TYPE_OBSERVATIONS (id integer primary key, nom_fr, nom_en, image, type_question, questions integer, FOREIGN KEY(questions) REFERENCES QUESTIONS(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS ESPECES (id integer primary key, nom_fr, nom_en, image, description_fr, description_en, sous_groupes integer, questions_animal, questions_menaces, questions_signe, questions_alimentation, FOREIGN KEY(sous_groupes) REFERENCES SOUS_GROUPES(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS PROJET (id integer primary key, nom, lieu, public, pays integer, utilisateurs integer,rowid, logo,mention, note, FOREIGN KEY(pays) REFERENCES PAYS(id), FOREIGN KEY(utilisateurs) REFERENCES UTILISATEURS(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS PROJET_TYPE_OBSERVATIONS (id integer primary key, nom_fr, nom_en, image, type_question, questions integer,projet integer, FOREIGN KEY(questions) REFERENCES QUESTIONS(id), FOREIGN KEY(projet) REFERENCES PROJET(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));

      
      db.executeSql('CREATE TABLE IF NOT EXISTS UTILISATEURS (id integer primary key, username, email, password, ville, telephone, pays integer, fonctions integer, projet integer, monuserid integer, FOREIGN KEY(pays) REFERENCES PAYS(id), FOREIGN KEY(fonctions) REFERENCES FONCTIONS(id), FOREIGN KEY(projet) REFERENCES PROJET(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS OBSERVATIONS (id integer primary key,  dateo , coordX, coordY,  img1File, img2File, img3File, img4File, note, etat, typeObservations integer, utilisateur integer, projet integer, groupe integer, sousgroupe integer, espece integer, madate, FOREIGN KEY(typeObservations) REFERENCES TYPE_OBSERVATIONS(id), FOREIGN KEY(espece) REFERENCES ESPECES(id), FOREIGN KEY(sousgroupe) REFERENCES SOUS_GROUPES(id), FOREIGN KEY(groupe) REFERENCES GROUPES(id), FOREIGN KEY(utilisateur) REFERENCES UTILISATEURS(id), FOREIGN KEY(projet) REFERENCES PROJET(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));


      db.executeSql('CREATE TABLE IF NOT EXISTS RESULTATS (id integer primary key, contenu, questions integer, observations integer, etat, FOREIGN KEY(questions) REFERENCES QUESTIONS(id), FOREIGN KEY(observations) REFERENCES OBSERVATIONS(id))', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));

      db.executeSql('CREATE TABLE IF NOT EXISTS PROJETMENU (id integer primary key, nomFr, nomEn, title)', {})
      .then(res => console.log('Executed SQL'))
      .catch(e => console.log(e));
      

    }).catch(e => console.log(e));
  }


}
