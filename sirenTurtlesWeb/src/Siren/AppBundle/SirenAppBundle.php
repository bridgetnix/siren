<?php

namespace Siren\AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SirenAppBundle extends Bundle {

    public function getParent() {
        return 'FOSUserBundle';
    }

}
