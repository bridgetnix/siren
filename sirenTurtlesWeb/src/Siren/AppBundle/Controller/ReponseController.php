<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Siren\AppBundle\Entity\Question;
use Siren\AppBundle\Form\QuestionType;
use Siren\AppBundle\Form\ReponseType;
use Siren\AppBundle\Entity\Reponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Siren\AppBundle\Entity\Saut;
use Siren\AppBundle\Entity\SautQuestion;
use Symfony\Component\HttpFoundation\Request;
use Siren\AppBundle\Entity\Suggestion;

/**
 * Description of ReponseController
 *
 * @author Dorsin
 */
class ReponseController extends Controller {

    public function newAction() {
        $em = $this->getDoctrine()->getManager();

        $question = new Question;
        $reponse = new Reponse();

        $request = $this->get('request');
        $form = $this->createForm(new QuestionType($request->getLocale()), $question);
        $formRep = $this->createForm(new ReponseType($request->getLocale()), $reponse);

        $session = $this->get("session");
        $idProjet = $session->get("idProjet");
        $isProjet = $request->query->get("projet");
        $isProjet = ($isProjet==NULL)?false:(($isProjet==1 && $idProjet!=NULL)?true:false);

        $result = false;
        if ($request->getMethod() == 'POST') {

            $formRep->bind($request);

            if ($formRep->isValid()) {
                $sautTest = $request->request->get("sautCheck");
                if ($sautTest != null) {
                    $nbSauts = $request->request->get("nbSauts");

                    if ($nbSauts > 0) {
                        $saut = new Saut();
                        $saut->setDateAdd(new \DateTime());

                        for ($i = 1; $i <= $nbSauts; $i++) {
                            $position = $request->request->get("position" . $i);
                            $idQuest = $request->request->get("quesSaut" . $i);

                            $questSaut = $em->getRepository("SirenAppBundle:Question")->find($idQuest);


                            $sautQuestion = new SautQuestion();
                            $sautQuestion->setPosition($position);
                            $sautQuestion->setQuestion($questSaut);
                            $sautQuestion->setSaut($saut);

                            $saut->addSautQuestion($sautQuestion);
                        }

                        $reponse->setSaut($saut);
                    }
                }


                // On l'enregistre notre objet $article dans la base de données
                $reponse->setDateAdd(new \DateTime());
                //var_dump($reponse);
                $em = $this->getDoctrine()->getManager();
                $em->persist($reponse);

                $suggTest = $request->request->get("suggCheck");
                if ($suggTest != null) {
                    $nbSuggs = $request->request->get("nbSuggs");
                    if ($nbSuggs > 0) {
                        $suggestion = new Suggestion();
                        $suggestion->setParent($reponse);

                        $repsSug = array();
                        //var_dump($request->request);
                        for ($i = 1; $i <= $nbSuggs; $i++) {
                            $idRep = $request->request->get("respSugg" . $i);
                            $repSugg = $em->getRepository("SirenAppBundle:Reponse")->find($idRep);

                            $suggestion->addReponse($repSugg);
                            //$repSugg->setSuggestion($suggestion);

                            array_push($repsSug, $repSugg);
                        }
                        $suggestion->setQuestion($reponse->getQuestion());
                        $suggestion->setCategorie($reponse->getQuestion()->getCategorie());
                        $em->persist($suggestion);

                        foreach ($repsSug as $rep) {
                            $rep->setSuggestion($suggestion);
                            $em->persist($rep);
                        }
                    }
                }

                $em->flush();
                $result = true;
            }
        }

        $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();
        if($isProjet){
            $projet = $em->getRepository("SirenAppBundle:Projet")->find($idProjet);
            $user = $this->get('security.context')->getToken()->getUser();
            $questions = $projet->getQuestionsOrderByCode();

            $isAuthor = false;
            if($user->getId() == $projet->getAuteur()->getId())
                $isAuthor = true;

            return $this->render('SirenAppBundle:User/Projet:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'projet' => $projet,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'enreg' => $result,
                'auteur' => $isAuthor
            ));
        }
        else {
            $questions = $em->getRepository('SirenAppBundle:Question')->getOrderByCode();
            return $this->render('SirenAppBundle:Admin/Patrouille:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'enreg' => $result
            ));
        }
    }

    public function getReponseAction(Reponse $reponse) {
        $result["result"] = false;
        if ($reponse != null) {
            $result["id"] = $reponse->getId();
            $result["valeur_fr"] = $reponse->getValeurFr();
            $result["valeur_en"] = $reponse->getValeurEn();
            if ($reponse->getQuestionSuivante() != null)
                $result["questSuiv"] = $reponse->getQuestionSuivante()->getId();

            $result["questRep"] = $reponse->getQuestion()->getId();

            $request = $this->get('request');

            $i = 0;
            if ($reponse->getSaut() != null) {
                $result["sautCheck"] = true;
                $result["saut"]["nb"] = count($reponse->getSaut()->getSautQuestions());
                foreach ($reponse->getSaut()->getSautQuestions() as $sautQuest) {
                    $result["saut"][$i]["id"] = $sautQuest->getQuestion()->getId();
                    $result["saut"][$i++]["position"] = $sautQuest->getPosition();
                }
            }
            else
                $result["sautCheck"] = false;
            $i = 0;
            if ($reponse->getFils() != null) {
                $result["suggCheck"] = true;
                $result["sugg"]["nb"] = count($reponse->getFils()->getReponses());
                foreach ($reponse->getFils()->getReponses() as $reponse) {
                    $result["sugg"][$i]["reponse"] = $reponse->getId();
                    $result["sugg"][$i++]["question"] = $reponse->getQuestion()->getId();
                }
            }
            else
                $result["suggCheck"] = false;
            $result["result"] = true;
        }

        return new JsonResponse($result);
    }

    public function getReponseOfQuestionAction(Question $question) {
        $result["nbReponses"] = count($question->getReponses());
        $i = 0;
        $request = $this->get("request");
        foreach ($question->getReponses() as $reponse) {
            $result["reponse"][$i]["id"] = $reponse->getId();
            $result["reponse"][$i++]["valeur"] = $reponse->getValeur($request->getLocale());
        }

        return new JsonResponse($result);
    }

    public function modifAction(Reponse $reponse) {
        $em = $this->getDoctrine()->getManager();
        $result = false;

        $question = new Question;
        $request = $this->get('request');
        $form = $this->createForm(new QuestionType($request->getLocale()), $question);
        $formRep = $this->createForm(new ReponseType($request->getLocale()), $reponse);

        $session = $this->get("session");
        $idProjet = $session->get("idProjet");
        $isProjet = $request->query->get("projet");
        $isProjet = ($isProjet==NULL)?false:(($isProjet==1 && $idProjet!=NULL)?true:false);

        if ($request->getMethod() == 'POST') {

            $formRep->bind($request);

            if ($formRep->isValid()) {
                $oldSaut  = $reponse->getSaut();
                if($oldSaut!=null){
                    $reponse->setSaut(null);
                    $em->remove($oldSaut);
                }
                //var_dump($oldSaut);
                $sautTest = $request->request->get("sautCheck");
                //var_dump($sautTest);
                if ($sautTest != null) {
                    $nbSauts = $request->request->get("nbSauts");

                    if ($nbSauts > 0) {
                        $reponse->setQuestionSuivante(null);
                        $saut = new Saut();
                        $saut->setDateAdd(new \DateTime());

                        for ($i = 1; $i <= $nbSauts; $i++) {
                            $position = $request->request->get("position" . $i);
                            $idQuest = $request->request->get("quesSaut" . $i);

                            $questSaut = $em->getRepository("SirenAppBundle:Question")->find($idQuest);


                            $sautQuestion = new SautQuestion();
                            $sautQuestion->setPosition($position);
                            $sautQuestion->setQuestion($questSaut);
                            $sautQuestion->setSaut($saut);

                            $saut->addSautQuestion($sautQuestion);
                        }
                        
                        $reponse->setSaut($saut);
                    }
                }
                
                $em->persist($reponse);

                $suggTest = $request->request->get("suggCheck");
                
                $oldFils = $reponse->getFils();
                if($oldFils != null){
                    foreach($oldFils->getReponses() as $rep){
                        $rep->setSuggestion(null);
                        $em->persist($rep);
                    }
                    $em->remove($oldFils);
                }
                $em->flush();
                if ($suggTest != null) {
                    $nbSuggs = $request->request->get("nbSuggs");
                    if ($nbSuggs > 0) {
                        $suggestion = new Suggestion();
                        $suggestion->setParent($reponse);

                        $repsSug = array();
                        //var_dump($request->request);
                        for ($i = 1; $i <= $nbSuggs; $i++) {
                            $idRep = $request->request->get("respSugg" . $i);
                            $repSugg = $em->getRepository("SirenAppBundle:Reponse")->find($idRep);

                            $suggestion->addReponse($repSugg);
                            //$repSugg->setSuggestion($suggestion);

                            array_push($repsSug, $repSugg);
                        }
                        $suggestion->setQuestion($reponse->getQuestion());
                        $suggestion->setCategorie($reponse->getQuestion()->getCategorie());
                        $em->persist($suggestion);
                        
                        foreach ($repsSug as $rep) {
                            $rep->setSuggestion($suggestion);
                            $em->persist($rep);
                        }
                    }
                }
                $em->persist($reponse);
                $em->flush();
                $result = true;
            }
        }

        $categories = $em->getRepository('SirenAppBundle:Categorie')->findAll();
        if($isProjet){
            $projet = $em->getRepository("SirenAppBundle:Projet")->find($idProjet);
            $user = $this->get('security.context')->getToken()->getUser();
            $questions = $projet->getQuestionsOrderByCode();

            $isAuthor = false;
            if($user->getId() == $projet->getAuteur()->getId())
                $isAuthor = true;

            return $this->render('SirenAppBundle:User/Projet:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'projet' => $projet,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'enreg' => $result,
                'auteur' => $isAuthor
            ));
        }
        else {
            $questions = $em->getRepository('SirenAppBundle:Question')->getOrderByCode();
            return $this->render('SirenAppBundle:Admin/Patrouille:question.html.twig', array(
                'liste_questions' => $questions,
                'liste_categories' => $categories,
                'form' => $form->createView(),
                'formRep' => $formRep->createView(),
                'enreg' => $result
            ));
        }
    }

    public function deleteAction(Reponse $reponse) {

        $result = false;
        if ($reponse != null) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($reponse);
            $em->flush();
            $result = true;
        }
        $json["result"] = $result;
        return new JsonResponse($json);
    }
    
    public function isIdOfAction($id, $exp){
        $em = $this->getDoctrine()->getManager();
        $json["test"] = $em->getRepository("SirenAppBundle:Reponse")->isIdOfExp($id, $exp);
        $json["result"] = true;
        return new JsonResponse($json);
    }

}
