<?php

namespace Siren\AppBundle\Controller;

use Siren\AppBundle\Entity\Projet;
use Siren\AppBundle\Entity\Segment;
use Siren\AppBundle\Entity\Site;
use Siren\AppBundle\Form\SegmentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class SegmentController extends Controller
{
    public function getForPatrouilleAction(Projet $projet, $nomSite)
    {
        $result = false;
        $json = array();
        $json["data"] = array();
        $em = $this->getDoctrine()->getManager();
        $site = $em->getRepository("SirenAppBundle:Site")->findOneBy(array("projet" => $projet, "nom" => $nomSite));
        if($site != NULL){
            $segments = $site->getSegments();
            foreach ($segments as $segment){
                $json["data"][$segment->getNom()] = NULL;
            }
            $result = true;
        }

        $json["result"] = $result;
        return new JsonResponse($json);
    }

    public function getFromSiteAction(Site $site){
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->get("session");
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($session->get("idProjet"));

        $isAuthor = false;

        $segment = new Segment;
        $form = $this->createForm(new SegmentType, $segment);
        $request = $this->get('request');

        if($user->getId() == $projet->getAuteur()->getId())
            $isAuthor = true;

        $result = false;
        $nom = false;
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet dans la base de données
                $exist = $em->getRepository("SirenAppBundle:Segment")->findOneBy(array("nom" => $segment->getNom(), "site" => $site));
                if($exist != NULL) {
                    $nom = true;
                    $result = false;
                }else {

                    $testDebut = $request->request->get("testDebut");
                    $testPic = $request->request->get("testPic");
                    $testFin = $request->request->get("testFin");

                    if ($testDebut != NULL) {
                        $periode = $request->request->get("periodeDebut");
                        $mois = $request->request->get("moisDebut");
                        $segment->setPeriodeDebut($periode);
                        $segment->setMoisDebut($mois);
                    }

                    if ($testPic != NULL) {
                        $periode = $request->request->get("periodePic");
                        $mois = $request->request->get("moisPic");
                        $segment->setPeriodePic($periode);
                        $segment->setMoisPic($mois);
                    }

                    if ($testFin != NULL) {
                        $periode = $request->request->get("periodeFin");
                        $mois = $request->request->get("moisFin");
                        $segment->setPeriodeFin($periode);
                        $segment->setMoisFin($mois);
                    }

                    $segment->setSite($site);
                    $em->persist($segment);
                    $em->flush();
                    $result = true;
                }
            }
        }
        //var_dump($nom);
        return $this->render('SirenAppBundle:User/Projet:segment.html.twig', array(
            'liste_segments' => $site->getSegments(),
            'projet' => $projet,
            'form' => $form->createView(),
            'auteur' => $isAuthor,
            'site' => $site,
            'enreg' => $result,
            'nom' => $nom
        ));
    }

    public function getSegmentAction(Segment $segment){
        $result["result"] = false;
        if ($segment != null) {
            $result["id"] = $segment->getId();
            $result["nom"] = $segment->getNom();
            $result["ville"] = $segment->getVille();
            $result["region"] = $segment->getRegion();

            if ($segment->getPeriodeDebut() != null) {
                $result["testDebut"] = true;
                $result["periodeDebut"] = $segment->getPeriodeDebut();
                $result["moisDebut"] = $segment->getMoisDebut();
            }
            else
                $result["testDebut"] = false;

            if ($segment->getPeriodePic() != null) {
                $result["testPic"] = true;
                $result["periodePic"] = $segment->getPeriodePic();
                $result["moisPic"] = $segment->getMoisPic();
            }
            else
                $result["testPic"] = false;

            if ($segment->getPeriodeFin() != null) {
                $result["testFin"] = true;
                $result["periodeFin"] = $segment->getPeriodeFin();
                $result["moisFin"] = $segment->getMoisFin();
            }
            else
                $result["testFin"] = false;


            $result["result"] = true;
        }

        return new JsonResponse($result);
    }

    public function modifAction(Segment $segment){
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->get("session");
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($session->get("idProjet"));

        $isAuthor = false;

        $form = $this->createForm(new SegmentType, $segment);
        $request = $this->get('request');

        if($user->getId() == $projet->getAuteur()->getId())
            $isAuthor = true;

        $result = false;
        $nom = false;
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet dans la base de données
                $exist = $em->getRepository("SirenAppBundle:Segment")->findOneBy(array("nom" => $segment->getNom(), "site" => $segment->getSite()));
                if($exist != NULL) {
                    $nom = true;
                    $result = false;
                }else {
                    $testDebut = $request->request->get("testDebut");
                    $testPic = $request->request->get("testPic");
                    $testFin = $request->request->get("testFin");

                    if ($testDebut != NULL) {
                        $periode = $request->request->get("periodeDebut");
                        $mois = $request->request->get("moisDebut");
                        $segment->setPeriodeDebut($periode);
                        $segment->setMoisDebut($mois);
                    }

                    if ($testPic != NULL) {
                        $periode = $request->request->get("periodePic");
                        $mois = $request->request->get("moisPic");
                        $segment->setPeriodePic($periode);
                        $segment->setMoisPic($mois);
                    }

                    if ($testFin != NULL) {
                        $periode = $request->request->get("periodeFin");
                        $mois = $request->request->get("moisFin");
                        $segment->setPeriodeFin($periode);
                        $segment->setMoisFin($mois);
                    }

                    $em->persist($segment);
                    $em->flush();
                    $result = true;
                }
            }
        }

        return $this->render('SirenAppBundle:User/Projet:segment.html.twig', array(
            'liste_segments' => $segment->getSite()->getSegments(),
            'projet' => $projet,
            'form' => $form->createView(),
            'auteur' => $isAuthor,
            'site' => $segment->getSite(),
            'enreg' => $result,
            'nom' => $nom
        ));
    }

    public function deleteAction(Segment $segment){
        $result = false;
        if ($segment != null) {
            try {
                $em = $this->getDoctrine()->getManager();
                $em->remove($segment);
                $em->flush();
                $result = true;
            }catch (\Exception $e){
                $result = false;
                $json["error"] = $e->getMessage();
            }
        }
        $json["result"] = $result;
        return new JsonResponse($json);
    }
}
