<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Siren\AppBundle\Entity\Type;
use Siren\AppBundle\Form\TypeType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Description of TypeController
 *
 * @author Dorian
 */
class TypeController extends Controller {

    //put your code here

    public function newAction() {
        $em = $this->getDoctrine()->getManager();

        $type = new Type;
        $form = $this->createForm(new TypeType, $type);
        $request = $this->get('request');

        $result = false;
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet $article dans la base de données
                $type->setDateAdd(new \DateTime());
                $em = $this->getDoctrine()->getManager();
                $em->persist($type);
                $em->flush();
                $result = true;
            }
        }

        $types = $em->getRepository('SirenAppBundle:Type')->findAll();
        return $this->render('SirenAppBundle:Admin/Patrouille:type.html.twig', array(
                    'liste_types' => $types,
                    'form' => $form->createView(),
                    'enreg' => $result
        ));
    }

    public function getTypeAction(Type $type) {
        $result["result"] = false;
        if ($type != null) {
            $result["id"] = $type->getId();
            $result["nomFr"] = $type->getNomFr();
            $result["nomEn"] = $type->getNomEn();
            $result["valeur"] = $type->getValeur();

            $result["result"] = true;
        }

        return new JsonResponse($result);
    }

    public function modifAction(Type $type) {
        //var_dump($type);

        $type->setValeur($type->getValeur() == "1");
        $result = false;
        $form = $this->createForm(new TypeType, $type);
        $request = $this->get('request');


        if ($request->getMethod() == 'POST') {

            $form->bind($request);

            if ($form->isValid()) {
                // On l'enregistre notre objet $article dans la base de données

                $em = $this->getDoctrine()->getManager();
                $em->persist($type);
                $em->flush();
                $result = true;
            }
        }

        $types = $em->getRepository('SirenAppBundle:Type')->findAll();
        return $this->render('SirenAppBundle:Admin/Patrouille:type.html.twig', array(
                    'liste_types' => $types,
                    'form' => $form->createView(),
                    'enreg' => $result
        ));
    }

    public function deleteAction(Type $type) {

        $result = false;
        if ($type != null) {

            $em = $this->getDoctrine()->getManager();
            $em->remove($type);
            $em->flush();
            $result = true;
        }
        $json["result"] = $result;
        return new JsonResponse($json);
    }

}
