<?php

namespace Siren\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class ExportationController extends Controller
{
    public function indexAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->get("session");
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($session->get("idProjet"));

        if($projet != NULL){

            $isAuthor = false;
            if($user->getId() == $projet->getAuteur()->getId())
                $isAuthor = true;

            return $this->render('SirenAppBundle:User/Projet:exportation.html.twig', array(
                'projet' => $projet,
                'auteur' => $isAuthor
            ));
        }
    }
    
    public function get_distance_m($lat1, $lng1, $lat2, $lng2) {
      $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
      $rlo1 = deg2rad($lng1);
      $rla1 = deg2rad($lat1);
      $rlo2 = deg2rad($lng2);
      $rla2 = deg2rad($lat2);
      $dlo = ($rlo2 - $rlo1) / 2;
      $dla = ($rla2 - $rla1) / 2;
      $a = (sin($dla) * sin($dla)) + cos($rla1) * cos($rla2) * (sin($dlo) * sin(
$dlo));
      $d = 2 * atan2(sqrt($a), sqrt(1 - $a));
      return ($earth_radius * $d);
    }
public function dateDiff($date1, $date2){
    $diff = abs($date1 - $date2); // abs pour avoir la valeur absolute, ainsi éviter d'avoir une différence négative
    $retour = array();
 
    $tmp = $diff;
    $retour['second'] = $tmp % 60;
 
    $tmp = floor( ($tmp - $retour['second']) /60 );
    $retour['minute'] = $tmp % 60;
 
    $tmp = floor( ($tmp - $retour['minute'])/60 );
    $retour['hour'] = $tmp % 24;
 
    $tmp = floor( ($tmp - $retour['hour'])  /24 );
    $retour['day'] = $tmp;
 
    return $retour;
}
    public function getFileAction(){
        $user = $this->get('security.context')->getToken()->getUser();

        $session = $this->get("session");
        $request = $this->get("request");
        $em = $this->getDoctrine()->getManager();
        $projet = $em->getRepository("SirenAppBundle:Projet")->find($session->get("idProjet"));

        if($projet != NULL){

            $type = $request->request->get("type");
            $dateDebut = $request->request->get("dateDebut");
            $dateFin = $request->request->get("dateFin");
            //$public = $request->request->get("public");
            $public = true;
            
            $dateDebut = ($dateDebut == "") ? NULL : $dateDebut;
            $dateFin = ($dateFin == "") ? NULL : $dateFin;

            if($dateDebut != NULL){
                $debut = $dateDebut;
                $dateDebut = new \DateTime();
                $dateDebut->setDate(substr($debut, strrpos($debut, "/")+1), substr($debut, strpos($debut, "/")+1, (strrpos($debut, "/")-strpos($debut, "/")-1)), substr($debut, 0, strpos($debut, "/")));
            }

            if($dateFin != NULL){
                $debut = $dateFin;
                $dateFin = new \DateTime();
                $dateFin->setDate(substr($debut, strrpos($debut, "/")+1), substr($debut, strpos($debut, "/")+1, (strrpos($debut, "/")-strpos($debut, "/")-1)), substr($debut, 0, strpos($debut, "/")));
            }
            // ask the service for a Excel5
            $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

            // On récupère le service translator
            $translator = $this->get('translator');
            // Pour traduire dans la locale de l'utilisateur :
            $titre = $translator->trans('Exportation de données du');
            $description = $translator->trans("Ce fichier Excel 2007 a été généré par l'application Siren Turtles et contient des données qui y ont été sauvegardées");

            $dateExport = date("d-m-Y H:i:s");
            $phpExcelObject->getProperties()->setCreator($user->getUsername())
                ->setLastModifiedBy("Siren Turtles")
                ->setTitle($titre.' '.$dateExport)
                ->setSubject($titre)
                ->setDescription($description);

            $idsPatrouilles = array();
            if($type == "patrouille") {
                if ($public) {
                    $patrouilles = $em->getRepository("SirenAppBundle:Patrouille")->getForExportation($projet, $dateDebut, $dateFin);
                    $observations = $em->getRepository("SirenAppBundle:Observation")->getForExportation($projet, $dateDebut, $dateFin);
                }
                else {
                    $patrouilles = $em->getRepository("SirenAppBundle:Patrouille")->getForExportation(null, $dateDebut, $dateFin);
                    $observations = $em->getRepository("SirenAppBundle:Observation")->getForExportation(null, $dateDebut, $dateFin);
                }

                $texte = $translator->trans('Patrouille');
                $phpExcelObject->getActiveSheet()->setTitle($texte);
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('A1', $texte);
                $texte = $translator->trans('Projet');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('B1', $texte);
                $texte = $translator->trans('Longitude (x)');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('C1', $texte);
                $texte = $translator->trans('Latitude (y)');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('D1', $texte);
                $texte = $translator->trans('date de Début');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('E1', $texte);
                $texte = $translator->trans('date de Fin');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('F1', $texte);
                $texte = $translator->trans('Segment');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('G1', $texte);
                $texte = $translator->trans('Site');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('H1', $texte);
                $texte = $translator->trans('Temps de patrouille');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('I1', $texte);
                $texte = $translator->trans('Patrouilleur');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('J1', $texte);
                $texte = $translator->trans('Projet public');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('K1', $texte);
                $texte = $translator->trans('nbTraces');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('L1', $texte);
                $texte = $translator->trans('nbNids');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('M1', $texte);
                $texte = $translator->trans('nbCarcasses');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('N1', $texte);
                $texte = $translator->trans('nbVivants');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('O1', $texte);
                $texte = $translator->trans('nbMorts');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('P1', $texte);
                $texte = $translator->trans('Distance parcouru (km)');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('Q1', $texte);
                $texte = $translator->trans('Durée');
                $phpExcelObject->setActiveSheetIndex(0)->setCellValue('R1', $texte);

                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                $sheet = $phpExcelObject->getActiveSheet();
                $i = 2;
                foreach ($patrouilles as $patrouille) {
                    $nbTraces = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Trace");
                    $nbNids = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Nid");
                    $nbCarcasses = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfCat($patrouille, "Carcasse");
                    $nbVivants = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Vivant");
                    $nbMorts = $em->getRepository("SirenAppBundle:Observation")->getNbObservationOfInd($patrouille, "Mort");
                    $public = ($patrouille->getEtat()==1?"Oui":"Non");
                    
                    $sheet->setCellValue('A' . $i, $patrouille->getId());
                    $sheet->setCellValue('B' . $i, $patrouille->getProjet()->getNom());
                    $sheet->setCellValue('C' . $i, $patrouille->getCoordXDebut());
                    $sheet->setCellValue('D' . $i, $patrouille->getCoordYDebut());
                    $sheet->setCellValue('E' . $i, $patrouille->getDateDebut()->format("d/m/Y H:i:s"));
                    $sheet->setCellValue('F' . $i, $patrouille->getDateFin()->format("d/m/Y H:i:s"));
                    $sheet->setCellValue('G' . $i, $patrouille->getSegment()->getNom());
                    $sheet->setCellValue('H' . $i, $patrouille->getNomSite());
                    $sheet->setCellValue('I' . $i, $patrouille->getType()->getNom($request->getLocale($request->getLocale())));
                    $sheet->setCellValue('J' . $i, $patrouille->getPatrouilleur()->getNom());
                    $sheet->setCellValue('K' . $i, $public);
                    $sheet->setCellValue('L' . $i, $nbTraces);
                    $sheet->setCellValue('M' . $i, $nbNids);
                    $sheet->setCellValue('N' . $i, $nbCarcasses);
                    $sheet->setCellValue('O' . $i, $nbVivants);
                    $sheet->setCellValue('P' . $i, $nbMorts);
                    $sheet->setCellValue('Q' . $i, round($this->get_distance_m($patrouille->getCoordXDebut(), $patrouille->getCoordYDebut(), $patrouille->getCoordXFin(), $patrouille->getCoordYFin()) / 1000,
 3)." m");
 $dd1 = strtotime($patrouille->getDateFin()->format("Y-m-d H:i:s"));
 $dd2 = strtotime($patrouille->getDateDebut()->format("Y-m-d H:i:s"));
 $sheet->setCellValue('R' . $i++, date('H:i:s', $dd1-$dd2));
                    $idsPatrouilles [] = $patrouille->getId();
                }

                $sheet = $phpExcelObject->createSheet(1);
                $sheet->setTitle("Observation");
                $texte = $translator->trans('Observation');
                $sheet->setCellValue('A1', $texte);
                $texte = $translator->trans('Patrouille');
                $sheet->setCellValue('B1', $texte);
                $texte = $translator->trans('Longitude (x)');
                $sheet->setCellValue('C1', $texte);
                $texte = $translator->trans('Latitude (y)');
                $sheet->setCellValue('D1', $texte);
                $texte = $translator->trans('Catégorie');
                $sheet->setCellValue('E1', $texte);
                $texte = $translator->trans('Segment');
                $sheet->setCellValue('F1', $texte);
                $texte = $translator->trans('date d\'ajout');
                $sheet->setCellValue('G1', $texte);

                $questions = $em->getRepository("SirenAppBundle:Question")->findBy(array("projet" => null));
                $questionsProjet = $em->getRepository("SirenAppBundle:Question")->findBy(array("projet" => $projet));
                $car = "H";
                foreach ($questions as $question) {
                    $sheet->setCellValue($car++ . '1', $question->getTitre($request->getLocale()));
                }
                
                foreach ($questionsProjet as $question) {
                    $sheet->setCellValue($car++ . '1', $question->getTitre($request->getLocale()));
                }
                $sheet->setCellValue($car . '1', "Images");

                $i = 2;
                foreach ($observations as $observation){
                    $sheet->setCellValue('A'.$i, $observation->getId());
                    $sheet->setCellValue('B'.$i, array_search($observation->getPatrouille()->getId(), $idsPatrouilles)+1);
                    $sheet->setCellValue('C'.$i, $observation->getCoordX());
                    $sheet->setCellValue('D'.$i, $observation->getCoordY());
                    $sheet->setCellValue('E'.$i, $observation->getCategorie()->getNom($request->getLocale($request->getLocale())));
                    $sheet->setCellValue('F'.$i, $observation->getPatrouille()->getSegment()->getNom());
                    $sheet->setCellValue('G'.$i, $observation->getDateAdd()->format("d/m/Y H:i:s"));

                    $car = "GH";
                    foreach ($questions as $question) {
                        $repondre = $em->getRepository("SirenAppBundle:Repondre")->findOneBy(array("observation" => $observation, "question" => $question));
                        if($repondre != NULL){
                            $reponse = $em->getRepository("SirenAppBundle:Reponse")->getReponseExport($repondre);

                            if($question->getType() == "qcm" &&  $reponse != NULL) {
                                $sheet->setCellValue($car . $i, $reponse->getValeur($request->getLocale()));
                            }
                            else{
                                $sheet->setCellValue($car . $i, $repondre->getReponseText());
                            }
                        }
                        $car++;
                    }
                    foreach ($questionsProjet as $question) {
                        $repondre = $em->getRepository("SirenAppBundle:Repondre")->findOneBy(array("observation" => $observation, "question" => $question));
                        if($repondre != NULL){
                            $reponse = $em->getRepository("SirenAppBundle:Reponse")->getReponseExport($repondre);

                            if($question->getType() == "qcm" &&  $reponse != NULL) {
                                $sheet->setCellValue($car . $i, $reponse->getValeur($request->getLocale()));
                            }
                            else{
                                $sheet->setCellValue($car . $i, $repondre->getReponseText());
                            }
                        }
                        $car++;
                    }
                    $images = array();
                    foreach ($observation->getImages() as $picture){
                        $images [] = $request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/". $picture->getWebPath();
                    }
                    $sheet->setCellValue($car . $i, implode(";", $images));
                    $i++;
                }

            }
            else {
                if($public)
                    $observations = $em->getRepository("SirenAppBundle:Observation")->getForExportation($projet, $dateDebut, $dateFin);
                else
                    $observations = $em->getRepository("SirenAppBundle:Observation")->getForExportation(null, $dateDebut, $dateFin);

                $phpExcelObject->setActiveSheetIndex(0);

                $sheet = $phpExcelObject->getActiveSheet();
                $sheet->setTitle("Observation");
                $texte = $translator->trans('Observation');
                $sheet->setCellValue('A1', $texte);
                $texte = $translator->trans('Patrouille');
                $sheet->setCellValue('B1', $texte);
                $texte = $translator->trans('Longitude (x)');
                $sheet->setCellValue('C1', $texte);
                $texte = $translator->trans('Latitude (y)');
                $sheet->setCellValue('D1', $texte);
                $texte = $translator->trans('Catégorie');
                $sheet->setCellValue('E1', $texte);
                $texte = $translator->trans('Segment');
                $sheet->setCellValue('F1', $texte);
                $texte = $translator->trans('date d\'ajout');
                $sheet->setCellValue('G1', $texte);

                $questions = $em->getRepository("SirenAppBundle:Question")->findBy(array("projet" => null));
                $questionsProjet = $em->getRepository("SirenAppBundle:Question")->findBy(array("projet" => $projet));
                
                $car = "H";
                foreach ($questions as $question) {
                    $sheet->setCellValue($car++ . '1', $question->getTitre($request->getLocale()));
                }
                
                foreach ($questionsProjet as $question) {
                    $sheet->setCellValue($car++ . '1', $question->getTitre($request->getLocale()));
                }
                $sheet->setCellValue($car . '1', "Images");

                $i = 2;
                foreach ($observations as $observation){
                    $sheet->setCellValue('A'.$i, ($i - 1));
                    $sheet->setCellValue('B'.$i, $observation->getPatrouille()->getId());
                    $sheet->setCellValue('C'.$i, $observation->getCoordX());
                    $sheet->setCellValue('D'.$i, $observation->getCoordY());
                    $sheet->setCellValue('E'.$i, $observation->getCategorie()->getNom($request->getLocale($request->getLocale())));
                    $sheet->setCellValue('F'.$i, $observation->getPatrouille()->getSegment()->getNom());
                    $sheet->setCellValue('G'.$i, $observation->getDateAdd()->format("d/m/Y H:i:s"));

                    $car = "H";
                    foreach ($questions as $question) {
                        $repondre = $em->getRepository("SirenAppBundle:Repondre")->findOneBy(array("observation" => $observation, "question" => $question));
                        if($repondre != NULL){
                            $reponse = $em->getRepository("SirenAppBundle:Reponse")->getReponseExport($repondre);

                            if($question->getType() == "qcm" &&  $reponse != NULL) {
                                $sheet->setCellValue($car . $i, $reponse->getValeur($request->getLocale()));
                            }
                            else{
                                $sheet->setCellValue($car . $i, $repondre->getReponseText());
                            }
                        }
                        $car++;
                    }
                    
                    foreach ($questionsProjet as $question) {
                        $repondre = $em->getRepository("SirenAppBundle:Repondre")->findOneBy(array("observation" => $observation, "question" => $question));
                        if($repondre != NULL){
                            $reponse = $em->getRepository("SirenAppBundle:Reponse")->getReponseExport($repondre);

                            if($question->getType() == "qcm" &&  $reponse != NULL) {
                                $sheet->setCellValue($car . $i, $reponse->getValeur($request->getLocale()));
                            }
                            else{
                                $sheet->setCellValue($car . $i, $repondre->getReponseText());
                            }
                        }
                        $car++;
                    }
                    $images = array();
                    foreach ($observation->getImages() as $picture){
                        $images [] = $request->getHost().$this->container->get('router')->getContext()->getBaseUrl(). "/". $picture->getWebPath();
                    }
                    $sheet->setCellValue($car . $i, implode(";", $images));
                    $i++;
                }
            }

            // create the writer
            $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel2007');
            // create the response
            $response = $this->get('phpexcel')->createStreamedResponse($writer);
            // adding headers

            $dispositionHeader = $response->headers->makeDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'export '.$dateExport.'.xlsx'
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);

            return $response;
        }
    }
}
