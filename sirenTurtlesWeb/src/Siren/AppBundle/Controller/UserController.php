<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Siren\AppBundle\Entity\Projet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Description of UserController
 *
 * @author Dorian
 */
class UserController  extends Controller {

    public function dashboardAction() {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('siren_useradmin_dashboard'));
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $mesProjets = array();

        $projets = $em->getRepository("SirenAppBundle:Projet")->findBy(array("auteur" => $user));

        $mesProjets = $projets;
        if(count($projets) == 0){
            $projet = new Projet();
            $projet->setDateAdd(new \DateTime());
            // On récupère le service translator
            $translator = $this->get('translator');
            // Pour traduire dans la locale de l'utilisateur :
            $texteTraduit = $translator->trans('Mon premier projet');
            $projet->setNom($texteTraduit);
            $projet->setAuteur($user);
            $projet->setDateDebut(new \DateTime());
            $projet->setEtat(1);

            $em->persist($projet);
            $em->flush();
            $mesProjets [] = $projet;
        }

        $nbPats = count($user->getPatrouilles());
        $nbObs = 0;
        foreach ($user->getPatrouilles() as $patrouille)
            $nbObs += count($patrouille->getObservations());
        $nSegments = 0;
        foreach ($user->getProjetsInities() as $projet) {
            if (count($projet->getSites()) > 0) {
                foreach ($projet->getSites() as $site) {
                    $nSegments += count($site->getSegments());
                }
            }
        }

        $mesProjets = array_merge($mesProjets, $user->getProjetsParticipes()->toArray());
        return $this->render('SirenAppBundle:User:dashboard.html.twig', array(
            "nbPats" => $nbPats,
            "nbObs" => $nbObs,
            "nbSegments" => $nSegments,
            "projets" => $mesProjets
        ));
    }

    public function getFromProjetAction(Projet $projet){
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository("SirenAppBundle:Utilisateur")->findAll();

        $goodUsers = array();
        $result = false;
        $json = array();
        $json["data"] = array();

        foreach ($users as $aUser){
            if(!in_array("ROLE_ADMIN", $aUser->getRoles()) && $user->getId() != $aUser->getId() && !$projet->isPatrouilleur($aUser)){
                array_push($goodUsers, $aUser);
                $json["data"][$aUser->getUsername() . "-" . $aUser->getNom()] = NULL;
                $result = true;
            }
        }

        $json["result"] = $result;

        return new JsonResponse($json);
    }

    public function dashAction() {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('siren_userPlus_dashboard'));
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $nbPats = count($user->getPatrouilles());
        $nbObs = 0;
        foreach ($user->getPatrouilles() as $patrouille)
            $nbObs += count($patrouille->getObservations());
        $nSegments = 0;
        foreach ($user->getProjetsInities() as $projet)
            foreach ($projet->getSites() as $site)
                $nSegments += count($site->getSegments());

        $mesProjets = array();
        $mesProjets = array_merge($mesProjets, $user->getProjetsParticipes()->toArray());
        $mesProjets = array_merge($mesProjets, $user->getProjetsInities()->toArray());
        return $this->render('SirenAppBundle:User:dash.html.twig', array(
            "nbPats" => $nbPats,
            "nbObs" => $nbObs,
            "nbSegments" => $nSegments,
            "projets" => $mesProjets
        ));
    }
    public function dashboardplusAction() {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('siren_statplus_dashboard'));
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $nbPats = count($user->getPatrouilles());
        $nbObs = 0;
        foreach ($user->getPatrouilles() as $patrouille)
            $nbObs += count($patrouille->getObservations());
        $nSegments = 0;
        foreach ($user->getProjetsInities() as $projet)
            foreach ($projet->getSites() as $site)
                $nSegments += count($site->getSegments());

        return $this->render('SirenAppBundle:User:dashboardplus.html.twig', array(
            "nbPats" => $nbPats,
            "nbObs" => $nbObs,
            "nbSegments" => $nSegments
        ));
    }
    public function dashboardTempsAction() {
        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('siren_userTemps_dashboard'));
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $mesProjets = array();
        $mesProjets = array_merge($mesProjets, $user->getProjetsParticipes()->toArray());
        $mesProjets = array_merge($mesProjets, $user->getProjetsInities()->toArray());

        return $this->render('SirenAppBundle:User:dashboardTemps.html.twig', array(
            "projets" => $mesProjets));
    }


}
