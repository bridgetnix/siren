<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Description of AdminController
 *
 * @author Dorian
 */
class AdminController extends Controller {
    //put your code here
     public function dashboardAction() {
         $em = $this->getDoctrine()->getManager();
        $utilisateur = $em->getRepository("SirenAppBundle:Utilisateur")->findAll();
        $type = $em->getRepository("SirenAppBundle:Type")->findAll();
        $categorie = $em->getRepository("SirenAppBundle:Categorie")->findAll();
        $question = $em->getRepository("SirenAppBundle:Question")->findAll();

        return $this->render('SirenAppBundle:Admin:dashboard.html.twig', array(
                    'utilisateur' => count($utilisateur),
                    'type' => count($type),
                    'categorie' => count($categorie),
                    'question' => count($question),
        ));
    }
    
    public function homeAction() {
        return $this->redirect($this->generateUrl('siren_useradmin_dashboard'));
    }
    
    public function patrouilleAction() {
        return $this->render('SirenAppBundle:Admin/Patrouille:patrouille.html.twig');
    }
    public function utilisateurAction() {
            
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository("SirenAppBundle:Utilisateur")->findAll();

        return $this->render('SirenAppBundle:Admin/utilisateur:utilisateur.html.twig', array(
                    'users' => $users,
        ));
    }
    public function ajutilisateurAction() {
            
        return $this->render('SirenAppBundle:Admin/utilisateur/Registration:register.html.twig');
    }
}
