<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Participer
 *
 * @ORM\Table(name="participer")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\ParticiperRepository")
 */
class Participer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Utilisateur", inversedBy="projetsParticipes")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $patrouilleur;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Projet", inversedBy="patrouilleurs")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $projet;

    /**
     * Participer constructor.
     * @param $patrouilleur
     * @param $projet
     */
    public function __construct($patrouilleur, $projet)
    {
        $this->patrouilleur = $patrouilleur;
        $this->projet = $projet;
        $this->dateAdd = new \DateTime();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Participer
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Participer
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set projet
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     * @return Participer
     */
    public function setProjet(\Siren\AppBundle\Entity\Projet $projet = null)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \Siren\AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set patrouilleur
     *
     * @param \Siren\AppBundle\Entity\Utilisateur $patrouilleur
     * @return Participer
     */
    public function setPatrouilleur(\Siren\AppBundle\Entity\Utilisateur $patrouilleur = null)
    {
        $this->patrouilleur = $patrouilleur;

        return $this;
    }

    /**
     * Get patrouilleur
     *
     * @return \Siren\AppBundle\Entity\Utilisateur
     */
    public function getPatrouilleur()
    {
        return $this->patrouilleur;
    }
}
