<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Saut
 *
 * @ORM\Table(name="saut")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\SautRepository")
 */
class Saut
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\SautQuestion", mappedBy="saut", cascade={"persist", "remove"})
     */
    private $sautQuestions;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Saut
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sautQuestions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sautQuestions
     *
     * @param \Siren\AppBundle\Entity\SautQuestion $sautQuestions
     * @return Saut
     */
    public function addSautQuestion(\Siren\AppBundle\Entity\SautQuestion $sautQuestions)
    {
        $this->sautQuestions[] = $sautQuestions;

        return $this;
    }

    /**
     * Remove sautQuestions
     *
     * @param \Siren\AppBundle\Entity\SautQuestion $sautQuestions
     */
    public function removeSautQuestion(\Siren\AppBundle\Entity\SautQuestion $sautQuestions)
    {
        $this->sautQuestions->removeElement($sautQuestions);
    }

    /**
     * Get sautQuestions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSautQuestions()
    {
        return $this->sautQuestions;
    }
    
    /**
     * Retourne un SautQuestion se situant à une position particuliere
     * 
     * @param int $pos
     */
    
    public function getQuestionAt($pos){
        foreach($this->sautQuestions as $sQuestion){
            if($sQuestion->getPosition() == $pos)
                return $sQuestion;
        }
        return null;
    }
}
