<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\UtilisateurRepository")
 */
class Utilisateur extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;
    
    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="etat", type="string", length=255, nullable=true)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="integer", nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=255, nullable=true)
     */
    private $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="gmail", type="string", length=255, nullable=true)
     */
    private $gmail;

    /**
     * @var string
     *
     * @ORM\Column(name="nomProjet", type="string", length=255, nullable=true)
     */
    private $nomProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="organisation", type="string", length=255, nullable=true)
     */
    private $organisation;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Pays")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $pays;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Patrouille", mappedBy="patrouilleur")
     */
    private $patrouilles;
    
    /**
     * @ORM\OneToOne(targetEntity="Siren\AppBundle\Entity\Image", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Projet", mappedBy="auteur")
     */
    private $projetsInities;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Participer", mappedBy="patrouilleur")
     */
    private $projetsParticipes;


    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->patrouilles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projetsParticipes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projetsInities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->roles = array('ROLE_USER');
       
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Utilisateur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set etat
     *
     * @param string $etat
     * @return Utilisateur
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string 
     */
    public function getEtat()
    {
        return $this->etat;
    }



    /**
     * Set facebook
     *
     * @param string $facebook
     * @return Utilisateur
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook
     *
     * @return string 
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     * @return Utilisateur
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string 
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set gmail
     *
     * @param string $gmail
     * @return Utilisateur
     */
    public function setGmail($gmail)
    {
        $this->gmail = $gmail;

        return $this;
    }

    /**
     * Get gmail
     *
     * @return string 
     */
    public function getGmail()
    {
        return $this->gmail;
    }

    /**
     * Set nomProjet
     *
     * @param string $nomProjet
     * @return Utilisateur
     */
    public function setNomProjet($nomProjet)
    {
        $this->nomProjet = $nomProjet;

        return $this;
    }

    /**
     * Get nomProjet
     *
     * @return string 
     */
    public function getNomProjet()
    {
        return $this->nomProjet;
    }

    /**
     * Set organisation
     *
     * @param string $organisation
     * @return Utilisateur
     */
    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Get organisation
     *
     * @return string 
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Utilisateur
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Utilisateur
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @param string $niveau
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;
    }

    /**
     * Set pays
     *
     * @param \Siren\AppBundle\Entity\Pays $pays
     * @return Utilisateur
     */
    public function setPays(\Siren\AppBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \Siren\AppBundle\Entity\Pays 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Add patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     * @return Utilisateur
     */
    public function addPatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles[] = $patrouilles;

        return $this;
    }

    /**
     * Remove patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     */
    public function removePatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles->removeElement($patrouilles);
    }

    /**
     * Get patrouilles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPatrouilles()
    {
        return $this->patrouilles;
    }
    

    /**
     * Set image
     *
     * @param \Siren\AppBundle\Entity\Image $image
     * @return Utilisateur
     */
    public function setImage(\Siren\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Siren\AppBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add projetsInities
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     * @return Utilisateur
     */
    public function addProjetInitie(\Siren\AppBundle\Entity\Projet $projet)
    {
        $this->projetsInities[] = $projet;

        return $this;
    }

    /**
     * Remove projetsInities
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     */
    public function removeProjetIntitie(\Siren\AppBundle\Entity\Projet $projet)
    {
        $this->projetsInities->removeElement($projet);
    }

    /**
     * Get projetsInities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjetsInities()
    {
        return $this->projetsInities;
    }

    /**
     * Add projetsParticipes
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     * @return Utilisateur
     */
    public function addProjetParticipe(\Siren\AppBundle\Entity\Projet $projet)
    {
        $this->projetsParticipes[] = $projet;

        return $this;
    }

    /**
     * Remove projetsParticipes
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     */
    public function removeProjetParticipe(\Siren\AppBundle\Entity\Projet $projet)
    {
        $this->projetsParticipes->removeElement($projet);
    }

    /**
     * Get projetsParticipes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjetsParticipes()
    {
        return $this->projetsParticipes;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Utilisateur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Add projetsInities
     *
     * @param \Siren\AppBundle\Entity\Projet $projetsInities
     * @return Utilisateur
     */
    public function addProjetsInity(\Siren\AppBundle\Entity\Projet $projetsInities)
    {
        $this->projetsInities[] = $projetsInities;

        return $this;
    }

    /**
     * Remove projetsInities
     *
     * @param \Siren\AppBundle\Entity\Projet $projetsInities
     */
    public function removeProjetsInity(\Siren\AppBundle\Entity\Projet $projetsInities)
    {
        $this->projetsInities->removeElement($projetsInities);
    }

    /**
     * Add projetsParticipes
     *
     * @param \Siren\AppBundle\Entity\Participer $projetsParticipes
     * @return Utilisateur
     */
    public function addProjetsParticipe(\Siren\AppBundle\Entity\Participer $projetsParticipes)
    {
        $this->projetsParticipes[] = $projetsParticipes;

        return $this;
    }

    /**
     * Remove projetsParticipes
     *
     * @param \Siren\AppBundle\Entity\Participer $projetsParticipes
     */
    public function removeProjetsParticipe(\Siren\AppBundle\Entity\Participer $projetsParticipes)
    {
        $this->projetsParticipes->removeElement($projetsParticipes);
    }



    /**
     * Set telephone
     *
     * @param integer $telephone
     * @return Utilisateur
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return integer 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    public function getLastProjet(){
        $projet = NULL;
        if(count($this->getProjetsInities()) > 0)
            $projet = $this->getProjetsInities()[0];
        elseif (count($this->getProjetsParticipes()) > 0)
            $projet = $this->getProjetsParticipes()[0]->getProjet();
        else
            return NULL;

        foreach ($this->getProjetsInities() as $aProjet){
            if($aProjet->getDateAdd() > $projet->getDateAdd())
                $projet = $aProjet;
        }

        foreach ($this->getProjetsParticipes() as $aProjet){
            if($aProjet->getDateAdd() > $projet->getDateAdd())
                $projet = $aProjet->getProjet();
        }

        return $projet;
    }
}
