<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Observation
 *
 * @ORM\Table(name="observation")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\ObservationRepository")
 */
class Observation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="coordX", type="float", nullable=true)
     */
    private $coordX;

    /**
     * @var string
     *
     * @ORM\Column(name="coordY", type="float", nullable=true)
     */
    private $coordY;

    /**
     * @var int
     *
     * @ORM\Column(name="etat", type="integer", nullable=true)
     */
    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Patrouille", inversedBy="observations")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $patrouille;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Categorie", inversedBy="observations")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $categorie;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Image", mappedBy="observation")
     */
    private $images;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Repondre", mappedBy="observation")
     */
    private $reponses;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set coordX
     *
     * @param string $coordX
     * @return Observation
     */
    public function setCoordX($coordX)
    {
        $this->coordX = $coordX;

        return $this;
    }

    /**
     * Get coordX
     *
     * @return string 
     */
    public function getCoordX()
    {
        return $this->coordX;
    }

    /**
     * Set coordY
     *
     * @param string $coordY
     * @return Observation
     */
    public function setCoordY($coordY)
    {
        $this->coordY = $coordY;

        return $this;
    }

    /**
     * Get coordY
     *
     * @return string 
     */
    public function getCoordY()
    {
        return $this->coordY;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return Observation
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Observation
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        if($this->getPatrouille()->getProjet()->getId() == 51) {
            $this->dateAdd->setTimezone(new \DateTimeZone("Asia/Dubai"));
        }
        return $this->dateAdd;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reponses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set patrouille
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouille
     * @return Observation
     */
    public function setPatrouille(\Siren\AppBundle\Entity\Patrouille $patrouille)
    {
        $this->patrouille = $patrouille;

        return $this;
    }

    /**
     * Get patrouille
     *
     * @return \Siren\AppBundle\Entity\Patrouille 
     */
    public function getPatrouille()
    {
        return $this->patrouille;
    }

    /**
     * Set categorie
     *
     * @param \Siren\AppBundle\Entity\Categorie $categorie
     * @return Observation
     */
    public function setCategorie(\Siren\AppBundle\Entity\Categorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Siren\AppBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Add images
     *
     * @param \Siren\AppBundle\Entity\Image $images
     * @return Observation
     */
    public function addImage(\Siren\AppBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \Siren\AppBundle\Entity\Image $images
     */
    public function removeImage(\Siren\AppBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add reponses
     *
     * @param \Siren\AppBundle\Entity\Repondre $reponses
     * @return Observation
     */
    public function addReponse(\Siren\AppBundle\Entity\Repondre $reponses)
    {
        $this->reponses[] = $reponses;

        return $this;
    }

    /**
     * Remove reponses
     *
     * @param \Siren\AppBundle\Entity\Repondre $reponses
     */
    public function removeReponse(\Siren\AppBundle\Entity\Repondre $reponses)
    {
        $this->reponses->removeElement($reponses);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    public function isMort(){
        if($this->getCategorie()->getNom() != "Individu")
            return false;

        foreach ($this->reponses as $repondre){
            if($repondre->getReponseText() != NULL)
                return false;
            //var_dump($repondre);
            if(stripos("Mort", $repondre->getReponse()->getValeur()) === true)
                return true;
        }
        return false;
    }
}
