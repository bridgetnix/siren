<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Repondre
 *
 * @ORM\Table(name="repondre")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\RepondreRepository")
 */
class Repondre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reponseText", type="string", length=255, nullable=true)
     */
    private $reponseText;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Observation", inversedBy="reponses")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $observation;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Question")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $question;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Reponse", inversedBy="mesRepondre")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $reponse;

    /**
     * Repondre constructor.
     */
    public function __construct()
    {
        $this->dateAdd = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reponseText
     *
     * @param string $reponseText
     * @return Repondre
     */
    public function setReponseText($reponseText)
    {
        $this->reponseText = $reponseText;

        return $this;
    }

    /**
     * Get reponseText
     *
     * @return string 
     */
    public function getReponseText()
    {
        return $this->reponseText;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Repondre
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set observation
     *
     * @param \Siren\AppBundle\Entity\Observation $observation
     * @return Repondre
     */
    public function setObservation(\Siren\AppBundle\Entity\Observation $observation = null)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return \Siren\AppBundle\Entity\Observation 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set question
     *
     * @param \Siren\AppBundle\Entity\Question $question
     * @return Repondre
     */
    public function setQuestion(\Siren\AppBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Siren\AppBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set reponse
     *
     * @param \Siren\AppBundle\Entity\Reponse $reponse
     * @return Repondre
     */
    public function setReponse(\Siren\AppBundle\Entity\Reponse $reponse = null)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return \Siren\AppBundle\Entity\Reponse 
     */
    public function getReponse()
    {
        return $this->reponse;
    }
}
