<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 *
 * @ORM\Table(name="categorie")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\CategorieRepository")
 */
class Categorie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_fr", type="string", length=255)
     */
    private $nom_fr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nom_en", type="string", length=255, nullable=true)
     */
    private $nom_en;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Observation", mappedBy="categorie")
     */
    private $observations;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Question", mappedBy="categorie")
     */
    private $questions;
    
    /**
     * @ORM\ManyToMany(targetEntity="Siren\AppBundle\Entity\Type")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $types;
    
    /**
     * @ORM\OneToOne(targetEntity="Siren\AppBundle\Entity\Image", cascade={"persist", "remove"})
     */
    private $image;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Suggestion", mappedBy="categorie")
     */
    private $suggestions;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom($locale = "fr")
    {
        if($locale == "en"){
            return $this->nom_en;
        }
        return $this->nom_fr;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Categorie
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->observations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->types = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add observations
     *
     * @param \Siren\AppBundle\Entity\Observation $observations
     * @return Categorie
     */
    public function addObservation(\Siren\AppBundle\Entity\Observation $observations)
    {
        $this->observations[] = $observations;

        return $this;
    }

    /**
     * Remove observations
     *
     * @param \Siren\AppBundle\Entity\Observation $observations
     */
    public function removeObservation(\Siren\AppBundle\Entity\Observation $observations)
    {
        $this->observations->removeElement($observations);
    }

    /**
     * Get observations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getObservations()
    {
        return $this->observations;
    }

    /**
     * Add questions
     *
     * @param \Siren\AppBundle\Entity\Question $questions
     * @return Categorie
     */
    public function addQuestion(\Siren\AppBundle\Entity\Question $questions)
    {
        $this->questions[] = $questions;

        return $this;
    }

    /**
     * Remove questions
     *
     * @param \Siren\AppBundle\Entity\Question $questions
     */
    public function removeQuestion(\Siren\AppBundle\Entity\Question $questions)
    {
        $this->questions->removeElement($questions);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set image
     *
     * @param \Siren\AppBundle\Entity\Image $image
     * @return Categorie
     */
    public function setImage(\Siren\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Siren\AppBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add types
     *
     * @param \Siren\AppBundle\Entity\Type $types
     * @return Categorie
     */
    public function addType(\Siren\AppBundle\Entity\Type $types)
    {
        $this->types[] = $types;

        return $this;
    }

    /**
     * Remove types
     *
     * @param \Siren\AppBundle\Entity\Type $types
     */
    public function removeType(\Siren\AppBundle\Entity\Type $types)
    {
        $this->types->removeElement($types);
    }

    /**
     * Get types
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * Set nom_fr
     *
     * @param string $nomFr
     * @return Categorie
     */
    public function setNomFr($nomFr)
    {
        $this->nom_fr = $nomFr;

        return $this;
    }

    /**
     * Get nom_fr
     *
     * @return string 
     */
    public function getNomFr()
    {
        return $this->nom_fr;
    }


    /**
     * Set nom_en
     *
     * @param string $nomEn
     * @return Categorie
     */
    public function setNomEn($nomEn)
    {
        $this->nom_en = $nomEn;

        return $this;
    }

    /**
     * Get nom_en
     *
     * @return string 
     */
    public function getNomEn()
    {
        return $this->nom_en;
    }

    /**
     * Add suggestions
     *
     * @param \Siren\AppBundle\Entity\Suggestion $suggestions
     * @return Categorie
     */
    public function addSuggestion(\Siren\AppBundle\Entity\Suggestion $suggestions)
    {
        $this->suggestions[] = $suggestions;

        return $this;
    }

    /**
     * Remove suggestions
     *
     * @param \Siren\AppBundle\Entity\Suggestion $suggestions
     */
    public function removeSuggestion(\Siren\AppBundle\Entity\Suggestion $suggestions)
    {
        $this->suggestions->removeElement($suggestions);
    }

    /**
     * Get suggestions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSuggestions()
    {
        return $this->suggestions;
    }
}
