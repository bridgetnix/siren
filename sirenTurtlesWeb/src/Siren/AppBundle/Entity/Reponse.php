<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponse
 *
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\ReponseRepository")
 */
class Reponse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="valeur_fr", type="string", length=255)
     */
    protected $valeur_fr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="valeur_en", type="string", length=255, nullable=true)
     */
    protected $valeur_en;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    protected $dateAdd;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Question", inversedBy="reponses")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected $question;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Question")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $questionSuivante;
    
     /**
     * @ORM\OneToOne(targetEntity="Siren\AppBundle\Entity\Image", cascade={"persist", "remove"})
     */
    protected $image;
    
    /**
     * @ORM\OneToOne(targetEntity="Siren\AppBundle\Entity\Saut", cascade={"persist", "remove"})
     */
    protected $saut;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Suggestion", inversedBy="reponses")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $suggestion;

    /**
     * @ORM\OneToOne(targetEntity="Siren\AppBundle\Entity\Suggestion", mappedBy="parent", cascade={"remove"})
     */
    protected $fils;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Repondre", mappedBy="reponse")
     */
    private $mesRepondre;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mesRepondre = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get valeur
     *
     * @return string 
     */
    public function getValeur($locale = "fr")
    {
        if($locale == "en")
            return $this->valeur_en;
        return $this->valeur_fr;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Reponse
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set question
     *
     * @param \Siren\AppBundle\Entity\Question $question
     * @return Reponse
     */
    public function setQuestion(\Siren\AppBundle\Entity\Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \Siren\AppBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set questionSuivante
     *
     * @param \Siren\AppBundle\Entity\Question $questionSuivante
     * @return Reponse
     */
    public function setQuestionSuivante(\Siren\AppBundle\Entity\Question $questionSuivante = null)
    {
        $this->questionSuivante = $questionSuivante;

        return $this;
    }

    /**
     * Get questionSuivante
     *
     * @return \Siren\AppBundle\Entity\Question 
     */
    public function getQuestionSuivante()
    {
        return $this->questionSuivante;
    }

    /**
     * Set image
     *
     * @param \Siren\AppBundle\Entity\Image $image
     * @return Reponse
     */
    public function setImage(\Siren\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Siren\AppBundle\Entity\Image 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set valeur_fr
     *
     * @param string $valeurFr
     * @return Reponse
     */
    public function setValeurFr($valeurFr)
    {
        $this->valeur_fr = $valeurFr;

        return $this;
    }

    /**
     * Get valeur_fr
     *
     * @return string 
     */
    public function getValeurFr()
    {
        return $this->valeur_fr;
    }

    /**
     * Set valeur_en
     *
     * @param string $valeurEn
     * @return Reponse
     */
    public function setValeurEn($valeurEn)
    {
        $this->valeur_en = $valeurEn;

        return $this;
    }

    /**
     * Get valeur_en
     *
     * @return string 
     */
    public function getValeurEn()
    {
        return $this->valeur_en;
    }


    /**
     * Set saut
     *
     * @param \Siren\AppBundle\Entity\Saut $saut
     * @return Reponse
     */
    public function setSaut(\Siren\AppBundle\Entity\Saut $saut = null)
    {
        $this->saut = $saut;

        return $this;
    }

    /**
     * Get saut
     *
     * @return \Siren\AppBundle\Entity\Saut 
     */
    public function getSaut()
    {
        return $this->saut;
    }

    /**
     * Set suggestion
     *
     * @param \Siren\AppBundle\Entity\Suggestion $suggestion
     * @return Reponse
     */
    public function setSuggestion(\Siren\AppBundle\Entity\Suggestion $suggestion = null)
    {
        $this->suggestion = $suggestion;

        return $this;
    }

    /**
     * Get suggestion
     *
     * @return \Siren\AppBundle\Entity\Suggestion 
     */
    public function getSuggestion()
    {
        return $this->suggestion;
    }

    /**
     * Set fils
     *
     * @param \Siren\AppBundle\Entity\Suggestion $fils
     * @return Reponse
     */
    public function setFils(\Siren\AppBundle\Entity\Suggestion $fils = null)
    {
        $this->fils = $fils;

        return $this;
    }

    /**
     * Get fils
     *
     * @return \Siren\AppBundle\Entity\Suggestion 
     */
    public function getFils()
    {
        return $this->fils;
    }

    /**
     * Add reponses
     *
     * @param \Siren\AppBundle\Entity\Repondre $reponses
     * @return Reponse
     */
    public function addRepondre(\Siren\AppBundle\Entity\Repondre $reponses)
    {
        $this->mesRepondre[] = $reponses;

        return $this;
    }

    /**
     * Remove reponses
     *
     * @param \Siren\AppBundle\Entity\Repondre $reponses
     */
    public function removeRepondre(\Siren\AppBundle\Entity\Repondre $reponses)
    {
        $this->mesRepondre->removeElement($reponses);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMesRepondre()
    {
        return $this->mesRepondre;
    }

}
