<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\QuestionRepository")
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_fr", type="string", length=255)
     */
    private $titre_fr;
    
    /**
     * @var string
     *
     * @ORM\Column(name="titre_en", type="string", length=255, nullable=true)
     */
    private $titre_en;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="niveau", type="string", length=255, nullable=true)
     */
    private $niveau;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Categorie", inversedBy="questions")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Reponse", mappedBy="question")
     */
    private $reponses;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\SautQuestion", mappedBy="question")
     */
    private $sautQuestions;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Suggestion", mappedBy="question")
     */
    private $suggestions;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Projet", inversedBy="questions")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $projet;

    /**
     * @ORM\OneToOne(targetEntity="Siren\AppBundle\Entity\Image", cascade={"persist", "remove"})
     */
    private $image;

    /**
     * Set image
     *
     * @param \Siren\AppBundle\Entity\Image $image
     * @return Question
     */
    public function setImage(\Siren\AppBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Siren\AppBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre($locale = "fr")
    {
        if($locale == "en")
            return $this->titre_en;
        return $this->titre_fr;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Question
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Question
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dateAdd = new \DateTime();
        $this->reponses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set categorie
     *
     * @param \Siren\AppBundle\Entity\Categorie $categorie
     * @return Question
     */
    public function setCategorie(\Siren\AppBundle\Entity\Categorie $categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \Siren\AppBundle\Entity\Categorie 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Add reponses
     *
     * @param \Siren\AppBundle\Entity\Reponse $reponses
     * @return Question
     */
    public function addReponse(\Siren\AppBundle\Entity\Reponse $reponses)
    {
        $this->reponses[] = $reponses;

        return $this;
    }

    /**
     * Remove reponses
     *
     * @param \Siren\AppBundle\Entity\Reponse $reponses
     */
    public function removeReponse(\Siren\AppBundle\Entity\Reponse $reponses)
    {
        $this->reponses->removeElement($reponses);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    /**
     * Set titre_fr
     *
     * @param string $titreFr
     * @return Question
     */
    public function setTitreFr($titreFr)
    {
        $this->titre_fr = $titreFr;

        return $this;
    }

    /**
     * Get titre_fr
     *
     * @return string 
     */
    public function getTitreFr()
    {
        return $this->titre_fr;
    }

    /**
     * Set titre_en
     *
     * @param string $titreEn
     * @return Question
     */
    public function setTitreEn($titreEn)
    {
        $this->titre_en = $titreEn;

        return $this;
    }

    /**
     * Get titre_en
     *
     * @return string 
     */
    public function getTitreEn()
    {
        return $this->titre_en;
    }
    
    public function getLabelfr(){
        return $this->code."-".$this->titre_fr;
    }
    
    public function getLabelen(){
        return $this->code."-".$this->titre_en;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Question
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getNiveau()
    {
        return $this->niveau;
    }

    /**
     * @param string $niveau
     */
    public function setNiveau($niveau)
    {
        $this->niveau = $niveau;
    }

    /**
     * Add sautQuestions
     *
     * @param \Siren\AppBundle\Entity\SautQuestion $sautQuestions
     * @return Question
     */
    public function addSautQuestion(\Siren\AppBundle\Entity\SautQuestion $sautQuestions)
    {
        $this->sautQuestions[] = $sautQuestions;

        return $this;
    }

    /**
     * Remove sautQuestions
     *
     * @param \Siren\AppBundle\Entity\SautQuestion $sautQuestions
     */
    public function removeSautQuestion(\Siren\AppBundle\Entity\SautQuestion $sautQuestions)
    {
        $this->sautQuestions->removeElement($sautQuestions);
    }

    /**
     * Get sautQuestions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSautQuestions()
    {
        return $this->sautQuestions;
    }

    /**
     * Add suggestions
     *
     * @param \Siren\AppBundle\Entity\Suggestion $suggestions
     * @return Question
     */
    public function addSuggestion(\Siren\AppBundle\Entity\Suggestion $suggestions)
    {
        $this->suggestions[] = $suggestions;

        return $this;
    }

    /**
     * Remove suggestions
     *
     * @param \Siren\AppBundle\Entity\Suggestion $suggestions
     */
    public function removeSuggestion(\Siren\AppBundle\Entity\Suggestion $suggestions)
    {
        $this->suggestions->removeElement($suggestions);
    }

    /**
     * Get suggestions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSuggestions()
    {
        return $this->suggestions;
    }

    /**
     * Set projet
     *
     * @param \Siren\AppBundle\Entity\Projet $projet
     * @return Question
     */
    public function setProjet(\Siren\AppBundle\Entity\Projet $projet = null)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return \Siren\AppBundle\Entity\Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }
}
