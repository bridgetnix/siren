<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="chemin", type="string", length=255)
     */
    private $chemin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;
    
    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Observation", inversedBy="images")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private $observation;

    private $file;
    private $tempFilename;
    
    public function __construct() {
        $this->dateAdd = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chemin
     *
     * @param string $chemin
     * @return Image
     */
    public function setChemin($chemin)
    {
        $this->chemin = $chemin;

        return $this;
    }

    /**
     * Get chemin
     *
     * @return string 
     */
    public function getChemin()
    {
        return $this->chemin;
    }

    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Image
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    /**
     * Set observation
     *
     * @param \Siren\AppBundle\Entity\Observation $observation
     * @return Image
     */
    public function setObservation(\Siren\AppBundle\Entity\Observation $observation = null)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return \Siren\AppBundle\Entity\Observation 
     */
    public function getObservation()
    {
        return $this->observation;
    }
    
    // On modifie le setter de File, pour prendre en compte l'upload d'un fichier lorsqu'il en existe déjà un autre
    public function setFile(UploadedFile $file) {
        $this->file = $file;
// On vérifie si on avait déjà un fichier pour cette entité
        if (null !== $this->chemin) {
// On sauvegarde l'extension du fichier pour le supprimer plus tard
            $this->tempFilename = $this->chemin;
// On réinitialise les valeurs des attributs url et alt
            $this->chemin = null;
        }
    }

    /**
     * @ORM\PrePersist()
     */
    public function preUpload() {
        // Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
        
        $this->chemin = sha1(uniqid(mt_rand(), true)) . '.' .$this->file->guessClientExtension();

    }
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUploadUpdate() {
        // Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
        
        $this->chemin = sha1(uniqid(mt_rand(), true)) . '.' .$this->file->guessClientExtension();
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload() {
// Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
// Si on avait un ancien fichier, on le supprime
        if (null !== $this->tempFilename) {
            $oldFile = $this->getUploadRootDir() . '/' . $this->tempFilename;
            if (file_exists($oldFile)) {
                unlink($oldFile);
            }
        }
// On déplace le fichier envoyé dans le répertoire de notre choix
        
        $this->file->move($this->getUploadRootDir(), $this->chemin);
    }

    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload() {
// On sauvegarde temporairement le nom du fichier, car il dépend de l'id
        $this->tempFilename = $this->getUploadRootDir() . '/' . $this->chemin;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload() {
// En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
        if (file_exists($this->tempFilename)) {
// On supprime le fichier
            unlink($this->tempFilename);
        }
    }

    public function getUploadDir() {
// On retourne le chemin relatif vers l'image pour un navigateur
        return 'uploads/images';
    }

    protected function getUploadRootDir() {
// On retourne le chemin relatif vers l'image pour notre code PHP
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getWebPath() {
        return $this->getUploadDir() . '/' . $this->getChemin();
    }
    
    public function getFile(){
        return $this->file;
    }
}
