<?php

namespace Siren\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Segment
 *
 * @ORM\Table(name="segment")
 * @ORM\Entity(repositoryClass="Siren\AppBundle\Repository\SegmentRepository")
 */
class Segment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var int
     *
     * @ORM\Column(name="periodeDebut", type="integer", nullable=true)
     */
    private $periodeDebut;

    /**
     * @var int
     *
     * @ORM\Column(name="moisDebut", type="integer", nullable=true)
     */
    private $moisDebut;

    /**
     * @var int
     *
     * @ORM\Column(name="periodePic", type="integer", nullable=true)
     */
    private $periodePic;

    /**
     * @var int
     *
     * @ORM\Column(name="moisPic", type="integer", nullable=true)
     */
    private $moisPic;

    /**
     * @var int
     *
     * @ORM\Column(name="periodeFin", type="integer", nullable=true)
     */
    private $periodeFin;

    /**
     * @var int
     *
     * @ORM\Column(name="moisFin", type="integer", nullable=true)
     */
    private $moisFin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdd", type="datetime")
     */
    private $dateAdd;
    
    /**
     * @ORM\OneToMany(targetEntity="Siren\AppBundle\Entity\Patrouille", mappedBy="segment")
     */
    private $patrouilles;

    /**
     * @ORM\ManyToOne(targetEntity="Siren\AppBundle\Entity\Site", inversedBy="segments")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $site;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Segment
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Segment
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Segment
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return int
     */
    public function getPeriodeDebut()
    {
        return $this->periodeDebut;
    }

    /**
     * @param int $periodeDebut
     */
    public function setPeriodeDebut($periodeDebut)
    {
        $this->periodeDebut = $periodeDebut;
    }

    /**
     * @return int
     */
    public function getMoisDebut()
    {
        return $this->moisDebut;
    }

    /**
     * @param int $moisDebut
     */
    public function setMoisDebut($moisDebut)
    {
        $this->moisDebut = $moisDebut;
    }

    /**
     * @return int
     */
    public function getPeriodePic()
    {
        return $this->periodePic;
    }

    /**
     * @param int $periodePic
     */
    public function setPeriodePic($periodePic)
    {
        $this->periodePic = $periodePic;
    }

    /**
     * @return int
     */
    public function getMoisPic()
    {
        return $this->moisPic;
    }

    /**
     * @param int $moisPic
     */
    public function setMoisPic($moisPic)
    {
        $this->moisPic = $moisPic;
    }

    /**
     * @return int
     */
    public function getPeriodeFin()
    {
        return $this->periodeFin;
    }

    /**
     * @param int $periodeFin
     */
    public function setPeriodeFin($periodeFin)
    {
        $this->periodeFin = $periodeFin;
    }

    /**
     * @return int
     */
    public function getMoisFin()
    {
        return $this->moisFin;
    }

    /**
     * @param int $moisFin
     */
    public function setMoisFin($moisFin)
    {
        $this->moisFin = $moisFin;
    }



    /**
     * Set dateAdd
     *
     * @param \DateTime $dateAdd
     * @return Segment
     */
    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;

        return $this;
    }

    /**
     * Get dateAdd
     *
     * @return \DateTime 
     */
    public function getDateAdd()
    {
        return $this->dateAdd;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->patrouilles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateAdd = new \DateTime();
    }

    /**
     * Add patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     * @return Segment
     */
    public function addPatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles[] = $patrouilles;

        return $this;
    }

    /**
     * Remove patrouilles
     *
     * @param \Siren\AppBundle\Entity\Patrouille $patrouilles
     */
    public function removePatrouille(\Siren\AppBundle\Entity\Patrouille $patrouilles)
    {
        $this->patrouilles->removeElement($patrouilles);
    }

    /**
     * Get patrouilles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPatrouilles()
    {
        return $this->patrouilles;
    }


    /**
     * Get site
     *
     * @return \Siren\AppBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set site
     *
     * @param \Siren\AppBundle\Entity\Site $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }


}
