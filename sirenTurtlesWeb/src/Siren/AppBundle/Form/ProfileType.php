<?php

namespace Siren\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('prenom', 'text', array("required" => false));
        $builder->add('nom', 'text', array("required" => true));
        $builder->add('telephone', 'number', array("required" => false));
        $builder->add('organisation', 'text', array("required" => false));
//        $builder->add('pays');
        $builder->add('ville', 'text', array("required" => false))
            ->add('pays')
            ->add('image', new ImageType(), array("required" => false));
    }

    public function getParent()
    {
        return 'fos_user_profile';
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getName()
    {
        return 'app_user_profile';
    }

    public function getBlockPrefix()
    {
        return 'siren_app_bundle_profile_type';
    }
}
