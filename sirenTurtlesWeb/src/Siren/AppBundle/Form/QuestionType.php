<?php

namespace Siren\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionType extends AbstractType
{
    private $locale;
    
    public function __construct($locale = "fr") {
        $this->locale = $locale;
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre_fr', 'text', array("required" => false))
                ->add('titre_en', 'text', array("required" => false))
                ->add('code', 'text', array("required" => false))
                ->add('type', 'choice', array(
                                "choices" => array(
                                        'qcm' => 'Question à choix multiples',
                                        'qro' => 'Question à réponse ouverte'
                                        )))
                ->add('categorie', 'entity', array(
                                'class' => 'SirenAppBundle:Categorie',
                                'property' => 'nom_'.$this->locale,
                                'multiple' => false, 
                                'required' => false))
                ->add('image', new ImageType(), array("required" => false));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Siren\AppBundle\Entity\Question'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'siren_appbundle_question';
    }


}
