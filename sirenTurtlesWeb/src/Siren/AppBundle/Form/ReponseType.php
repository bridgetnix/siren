<?php

namespace Siren\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReponseType extends AbstractType
{
    private $locale;
    
    public function __construct($locale = "fr") {
        $this->locale = $locale;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('valeur_fr', 'text', array("required" => false))
                ->add('valeur_en', 'text', array("required" => false))
                ->add('question', 'entity', array(
                                'class' => 'SirenAppBundle:Question',
                                'property' => 'getLabel'.$this->locale,
                                'multiple' => false, 
                                'required' => false))
                ->add('questionSuivante', 'entity', array(
                                'class' => 'SirenAppBundle:Question',
                                'property' => 'getLabel'.$this->locale,
                                'multiple' => false, 
                                'required' => false))
                ->add('image', new ImageType(), array("required" => false));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Siren\AppBundle\Entity\Reponse'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'siren_appbundle_reponse';
    }


}
