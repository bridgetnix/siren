<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
/**
 * Description of RegistrationType
 *
 * @author georges
 */
class RegistrationType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('prenom', 'text', array("required" => false));
        $builder->add('nom', 'text', array("required" => true));
        $builder->add('telephone', 'number', array("required" => false));
        $builder->add('organisation', 'text', array("required" => false));
//        $builder->add('pays');
        $builder->add('ville', 'text', array("required" => false))
            ->add('pays', null ,array("required" => false))
            ->add('image', new ImageType(), array("required" => false));
    }
        public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}
