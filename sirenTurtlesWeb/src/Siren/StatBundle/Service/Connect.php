<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\StatBundle\Service;
use PDO;


/**
 * Description of connect
 *
 * @author georges
 */
class Connect {
    public function connection($host,$dbname,$login,$pwd){
        try
					{ 
         $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
         $bdd = new PDO('mysql:host='.$host.';dbname='.$dbname.'',$login,$pwd,$pdo_options);	
         return $bdd;
        }
				catch(Exception $e)
					{
						die('Erreur : '.$e->getMessage());
					}
    }
}
