<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Siren\StatBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use src\Siren\AppBundle\Entity;
use DateTime;
use src\Siren\StatBundle\Service;

/**
 * Description of statUserTempsController
 *
 * @author georges
 */
class statUserTempsController extends Controller{
    
    public function statTempsAction() {
        // récupération des données de la vue
        $request = Request::createFromGlobals();
        $request = $this->get("request");
        $name=$request->query->get("name");
        $type=$request->query->get("type");
        $identifiant=$request->query->get("identifiant");
        $categorie=$request->query->get("categorie");
//print_r($categorie);
    //charger les tableaux avec les données de la vue
     $distinct= array();
     $attribut= array('c.nom_fr');

     if($type=="patrouille"){
         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
         $counDistinct= 'p';
         $agregation= array();

     }elseif ($type=="observation") {
         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
            $agregation= array(array('COUNT','o'));
            $counDistinct= array();

     }
     $egalite=array();
     for($i=0; $i< (count($categorie)); $i++){
        $egal=array();
        $egal =array('c.nom_fr', $categorie[$i]);
        array_push($egalite, $egal);
     }
     $difference=array();
     $contraire= array();
     $inferieur=array();
     $superieur=array();
     $superieurEgale=array();
     $inferieurEgale=array();
     $id= 'pa.id';
     $in= array();
     $notBetween= array();
     $like=array();
     $isNull=array();
     $isNotNull=array();
     if(count($categorie) == 1 ){
        $groupBy= array();
     } else{
         $groupBy= array('c.nom_fr');
     }
     $orderBy= array();
     
//     if ($type=="patrouille"){
//        $between[0]='p.dateDebut';
//    }
//    if($type=="observation"){
            $between[0]='o.dateAdd'; 
//    }
     //découpe des dates dans between à partir de 3 
    
    //découpage par année
    if($name == "Uneannee"){
        $ref1 = date("Y-m-d H:i:s");
        $ref2 = date("Y-m-d H:i:s");
        
        $mois= array();
        for($i=11; $i>=0; $i--){
            $ref1 = new DateTime($ref1.' first day of last month');
            $ref2 = new DateTime($ref2.' last day of last month');
            $mois[$i]= $ref2->format("m-Y");
            $ref2 = $ref2->format("Y-m-d 23:59:59"); 
            $ref1 = $ref1->format("Y-m-d 00:00:00");
            $bet[$i]['debut'] = $ref1;
            $bet[$i]['fin'] = $ref2;
        }
//        print_r($bet);
      
//        print_r($mois);           
   //charger le service de construction des statistiques
    $req= $this->container->get('StatPlus.temps');
    $SELECT=$req->partieSelect($distinct,$agregation,$attribut,$counDistinct);
    $FROM =$req->partieFrom($from);
    $JOIN=$req->partieJoin($from);
    $WHERE=$req->partieWhere($egalite,$difference,$contraire,$inferieur,$superieur,$superieurEgale,$inferieurEgale,$in,$between,$notBetween
 ,$like,$isNull,$isNotNull,$id);
    $GROUPBY=$req->partieGroupBy($groupBy);
    $ORDERBY=$req->partieOrderBy($orderBy);
                       
// mettre le tout dans une variable représentant notre requete
    $resultat = $SELECT.$FROM.$JOIN.$WHERE.$GROUPBY.$ORDERBY;                    
// echo $resultat;
                     
for($i=0; $i<12; $i++){
        $query = $this->getDoctrine()
                      ->getManager()
                      ->createQuery($resultat);
    //charger les paramètres nécessaires à l'execution de la requete
                    if(isset($egalite) AND !empty($egalite)){
                        if(count($egalite)>1){

                            $param= array();
                        for($i=0; $i< count($egalite); $i++){
                            $param['egalite'.$i] = $egalite[$i][1];  
                        }
                            $param['id'] = $identifiant;
                            $query->setParameters($param);
                        }  else {
                            $param= array();
                            $param['egalite'] = $egalite[count($egalite)-1][1];
                            $param['id'] = $identifiant;
                            $query->setParameters(array($param));

                        }

                    }else{
                        $query->setParameter('id', $identifiant);
                    }
                    if(isset($difference) AND !empty($difference)){
                        if(count($difference)>1){

                            $param= array();
                        for($i=0; $i< count($difference); $i++){
                            $param['difference'.$i] = $difference[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['difference'] = $difference[count($difference)-1][1];
                        $query->setParameters(array($param));

                        }

                    }
                    if(isset($contraire) AND !empty($contraire)){
                        if(count($contraire)>1){

                            $param= array();
                        for($i=0; $i< count($contraire); $i++){
                            $param['contraire'.$i] = $contraire[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['contraire'] = $contraire[count($contraire)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($inferieur) AND !empty($inferieur)){
                        if(count($inferieur)>1){

                            $param= array();
                        for($i=0; $i< count($inferieur); $i++){
                            $param['inferieur'.$i] = $inferieur[$i][1];  
                         }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieur'] = $inferieur[count($inferieur)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($superieur) AND !empty($superieur)){
                        if(count($superieur)>1){

                            $param= array();
                        for($i=0; $i< count($superieur); $i++){
                            $param['superieur'.$i] = $superieur[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieur'] = $superieur[count($superieur)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     if(isset($superieurEgale) AND !empty($superieurEgale)){
                        if(count($superieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($superieurEgale); $i++){
                            $param['superieurEgale'.$i] = $superieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieurEgale'] = $superieurEgale[count($superieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                   }
                     if(isset($inferieurEgale) AND !empty($inferieurEgale)){
                        if(count($inferieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($inferieurEgale); $i++){
                            $param['inferieurEgale'.$i] = $inferieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieurEgale'] = $inferieurEgale[count($inferieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     
                     if (isset($between) AND !empty($between)){
                        $query->setParameter('between1', $bet[$i]['debut']);
                        $query->setParameter('between2', $bet[$i]['fin']);
                     }

                     if (isset($notBetween) AND !empty($notBetween)){
                        $query->setParameter('notbetween1', $notBetween[1]);
                        $query->setParameter('notbetween2', $notBetween[2]);
                     }
    
    $res[$i] = $query->getArrayResult();  
    

}
//print_r($res); 

    // construction de données pour la vue
    
    for($i=0; $i< count($mois); $i++){
        
        $nouveau[$i]['moisAnnee']=$mois[$i];
        $nouveau[$i]['Nid']= 0;
        $nouveau[$i]['Carcasse']=0;
        $nouveau[$i]['Individu']= 0;
        $nouveau[$i]['Trace']= 0;
        
        if(!empty($res[$i])){
         
            for($j=0; $j< count($res[$i]); $j++){

                if($res[$i][$j]['attribut']== 'Nid'){
                    $nouveau[$i]['Nid']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Carcasse'){
                    $nouveau[$i]['Carcasse']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Individu'){
                    $nouveau[$i]['Individu']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Trace'){
                    $nouveau[$i]['Trace']= $res[$i][$j]['nombre'];
                }

            }
        }
    }
//    print_r($nouveau);
                 
//print_r($nouveau);
         if (count($nouveau)== 0){
                        $template= $this->render('SirenStatBundle:StatUser:messageError.html.twig')->getContent();
                        $response = array();
                        $response['message']=$template;
                        return new Response(json_encode($response)); 
                    }else{           
        $template=$this->render('SirenStatBundle:statUserTemps:affichageTemps.html.twig', array( 'nouveaux' => $nouveau))->getContent();
        $response = array();
        $response['message']=$template;
        return new Response(json_encode($response));
                    }
    }elseif($name=="Troismois"){
        
//        $ref1 = date("Y-m-d H:i:s");
        $ref2 = date("Y-m-d H:i:s");
        $semaine= array();
        
         for($i=11; $i>=0; $i--){
                    
            $ref2 = new DateTime($ref2.' last sunday');
            $ref2 = $ref2->format("Y-m-d 23:59:59");
            $ref1 = new DateTime($ref2.'-6 day');
            $semaine[$i]= $ref1->format("d-m-Y");
            $ref1 = $ref1->format("Y-m-d 00:00:00");
            $bet[$i]['debut'] = $ref1;
            $bet[$i]['fin'] = $ref2;
         } 
//         print_r($bet);             
//charger le service de construction des statistiques
        $req= $this->container->get('StatPlus.temps');
        $SELECT=$req->partieSelect($distinct,$agregation,$attribut,$counDistinct);
        $FROM =$req->partieFrom($from);
        $JOIN=$req->partieJoin($from);
        $WHERE=$req->partieWhere($egalite,$difference,$contraire,$inferieur,$superieur,$superieurEgale,$inferieurEgale,$in,$between,$notBetween
     ,$like,$isNull,$isNotNull,$id);
        $GROUPBY=$req->partieGroupBy($groupBy);
        $ORDERBY=$req->partieOrderBy($orderBy);
                       
// mettre le tout dans une variable représentant notre requete
        $resultat = $SELECT.$FROM.$JOIN.$WHERE.$GROUPBY.$ORDERBY;                    
 
                     
        for($i=0; $i<12; $i++){
            
        $query = $this->getDoctrine()
                      ->getManager()
                      ->createQuery($resultat);
    //charger les paramètres nécessaires à l'execution de la requete
                   if(isset($egalite) AND !empty($egalite)){
                        if(count($egalite)>1){

                            $param= array();
                        for($i=0; $i< count($egalite); $i++){
                            $param['egalite'.$i] = $egalite[$i][1];  
                        }
                            $param['id'] = $identifiant;
                            $query->setParameters($param);
                        }  else {
                            $param= array();
                            $param['egalite'] = $egalite[count($egalite)-1][1];
                            $param['id'] = $identifiant;
                            $query->setParameters(array($param));

                        }

                    }else{
                        $query->setParameter('id', $identifiant);
                    }
                    if(isset($difference) AND !empty($difference)){
                        if(count($difference)>1){

                            $param= array();
                        for($i=0; $i< count($difference); $i++){
                            $param['difference'.$i] = $difference[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['difference'] = $difference[count($difference)-1][1];
                        $query->setParameters(array($param));

                        }

                    }
                    if(isset($contraire) AND !empty($contraire)){
                        if(count($contraire)>1){

                            $param= array();
                        for($i=0; $i< count($contraire); $i++){
                            $param['contraire'.$i] = $contraire[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['contraire'] = $contraire[count($contraire)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($inferieur) AND !empty($inferieur)){
                        if(count($inferieur)>1){

                            $param= array();
                        for($i=0; $i< count($inferieur); $i++){
                            $param['inferieur'.$i] = $inferieur[$i][1];  
                         }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieur'] = $inferieur[count($inferieur)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($superieur) AND !empty($superieur)){
                        if(count($superieur)>1){

                            $param= array();
                        for($i=0; $i< count($superieur); $i++){
                            $param['superieur'.$i] = $superieur[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieur'] = $superieur[count($superieur)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     if(isset($superieurEgale) AND !empty($superieurEgale)){
                        if(count($superieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($superieurEgale); $i++){
                            $param['superieurEgale'.$i] = $superieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieurEgale'] = $superieurEgale[count($superieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                   }
                     if(isset($inferieurEgale) AND !empty($inferieurEgale)){
                        if(count($inferieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($inferieurEgale); $i++){
                            $param['inferieurEgale'.$i] = $inferieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieurEgale'] = $inferieurEgale[count($inferieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     
                     if (isset($between) AND !empty($between)){
                        $query->setParameter('between1', $bet[$i]['debut']);
                        $query->setParameter('between2', $bet[$i]['fin']);
                     }

                     if (isset($notBetween) AND !empty($notBetween)){
                        $query->setParameter('notbetween1', $notBetween[1]);
                        $query->setParameter('notbetween2', $notBetween[2]);
                     }
    
    $res[$i] = $query->getArrayResult();  
    

}

//print_r($res); 
    
    for($i=0; $i< count($semaine); $i++){
        
        $nouveau[$i]['moisAnnee']=$semaine[$i];
        $nouveau[$i]['Nid']= 0;
        $nouveau[$i]['Carcasse']=0;
        $nouveau[$i]['Individu']= 0;
        $nouveau[$i]['Trace']= 0;
        
        if(!empty($res[$i])){
         
            for($j=0; $j< count($res[$i]); $j++){

                if($res[$i][$j]['attribut']== 'Nid'){
                    $nouveau[$i]['Nid']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Carcasse'){
                    $nouveau[$i]['Carcasse']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Individu'){
                    $nouveau[$i]['Individu']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Trace'){
                    $nouveau[$i]['Trace']= $res[$i][$j]['nombre'];
                }

            }
        }
    }
    
   
//    print_r($nouveau);
         
    if (count($nouveau)== 0){
                        $template= $this->render('SirenStatBundle:StatUser:messageError.html.twig')->getContent();
                        $response = array();
                        $response['message']=$template;
                        return new Response(json_encode($response)); 
                    }else{
    $template=$this->render('SirenStatBundle:statUserTemps:affichageTemps.html.twig', array( 'nouveaux' => $nouveau))->getContent();
    $response = array();
    $response['message']=$template;
    return new Response(json_encode($response));
                    }
    }elseif($name=="Unmois"){
//        $ref1 = date("Y-m-d H:i:s");
        $ref2 = date("Y-m-d H:i:s");
        
        for($i=3; $i>=0; $i--){
            $ref2 = new DateTime($ref2.' last sunday');
            $ref2 = $ref2->format("Y-m-d 23:59:59");
            $ref1 = new DateTime($ref2.'-6 day');
            $semaine[$i]= $ref1->format("d-m-Y");
            $ref1 = $ref1->format("Y-m-d 00:00:00");
            $bet[$i]['debut'] = $ref1;
            $bet[$i]['fin'] = $ref2;
         } 
//         print_r($semaine);             
//charger le service de construction des statistiques
        $req= $this->container->get('StatPlus.temps');
        $SELECT=$req->partieSelect($distinct,$agregation,$attribut,$counDistinct);
        $FROM =$req->partieFrom($from);
        $JOIN=$req->partieJoin($from);
        $WHERE=$req->partieWhere($egalite,$difference,$contraire,$inferieur,$superieur,$superieurEgale,$inferieurEgale,$in,$between,$notBetween
     ,$like,$isNull,$isNotNull,$id);
        $GROUPBY=$req->partieGroupBy($groupBy);
        $ORDERBY=$req->partieOrderBy($orderBy);
                       
// mettre le tout dans une variable représentant notre requete
        $resultat = $SELECT.$FROM.$JOIN.$WHERE.$GROUPBY.$ORDERBY;                    
 
                     
        for($i=0; $i<4; $i++){
        $query = $this->getDoctrine()
                      ->getManager()
                      ->createQuery($resultat);
    //charger les paramètres nécessaires à l'execution de la requete
                   if(isset($egalite) AND !empty($egalite)){
                        if(count($egalite)>1){

                            $param= array();
                        for($i=0; $i< count($egalite); $i++){
                            $param['egalite'.$i] = $egalite[$i][1];  
                        }
                            $param['id'] = $identifiant;
                            $query->setParameters($param);
                        }  else {
                            $param= array();
                            $param['egalite'] = $egalite[count($egalite)-1][1];
                            $param['id'] = $identifiant;
                            $query->setParameters(array($param));

                        }

                    }else{
                        $query->setParameter('id', $identifiant);
                    }
                    if(isset($difference) AND !empty($difference)){
                        if(count($difference)>1){

                            $param= array();
                        for($i=0; $i< count($difference); $i++){
                            $param['difference'.$i] = $difference[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['difference'] = $difference[count($difference)-1][1];
                        $query->setParameters(array($param));

                        }

                    }
                    if(isset($contraire) AND !empty($contraire)){
                        if(count($contraire)>1){

                            $param= array();
                        for($i=0; $i< count($contraire); $i++){
                            $param['contraire'.$i] = $contraire[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['contraire'] = $contraire[count($contraire)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($inferieur) AND !empty($inferieur)){
                        if(count($inferieur)>1){

                            $param= array();
                        for($i=0; $i< count($inferieur); $i++){
                            $param['inferieur'.$i] = $inferieur[$i][1];  
                         }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieur'] = $inferieur[count($inferieur)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($superieur) AND !empty($superieur)){
                        if(count($superieur)>1){

                            $param= array();
                        for($i=0; $i< count($superieur); $i++){
                            $param['superieur'.$i] = $superieur[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieur'] = $superieur[count($superieur)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     if(isset($superieurEgale) AND !empty($superieurEgale)){
                        if(count($superieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($superieurEgale); $i++){
                            $param['superieurEgale'.$i] = $superieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieurEgale'] = $superieurEgale[count($superieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                   }
                     if(isset($inferieurEgale) AND !empty($inferieurEgale)){
                        if(count($inferieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($inferieurEgale); $i++){
                            $param['inferieurEgale'.$i] = $inferieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieurEgale'] = $inferieurEgale[count($inferieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     
                     if (isset($between) AND !empty($between)){
                        $query->setParameter('between1', $bet[$i]['debut']);
                        $query->setParameter('between2', $bet[$i]['fin']);
                     }

                     if (isset($notBetween) AND !empty($notBetween)){
                        $query->setParameter('notbetween1', $notBetween[1]);
                        $query->setParameter('notbetween2', $notBetween[2]);
                     }
    
    $res[$i] = $query->getArrayResult();  
    

}

//print_r($res); 
    
    for($i=0; $i< count($semaine); $i++){
        
        $nouveau[$i]['moisAnnee']=$semaine[$i];
        $nouveau[$i]['Nid']= 0;
        $nouveau[$i]['Carcasse']=0;
        $nouveau[$i]['Individu']= 0;
        $nouveau[$i]['Trace']= 0;
        
        if(!empty($res[$i])){
         
            for($j=0; $j< count($res[$i]); $j++){

                if($res[$i][$j]['attribut']== 'Nid'){
                    $nouveau[$i]['Nid']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Carcasse'){
                    $nouveau[$i]['Carcasse']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Individu'){
                    $nouveau[$i]['Individu']= $res[$i][$j]['nombre'];
                }
                if($res[$i][$j]['attribut']== 'Trace'){
                    $nouveau[$i]['Trace']= $res[$i][$j]['nombre'];
                }

            }
        }
    }
    
    
     if (count($nouveau)== 0){
                        $template= $this->render('SirenStatBundle:StatUser:messageError.html.twig')->getContent();
                        $response = array();
                        $response['message']=$template;
                        return new Response(json_encode($response)); 
                    }else{
    $template=$this->render('SirenStatBundle:statUserTemps:affichageTemps.html.twig', array( 'nouveaux' => $nouveau))->getContent();
    $response = array();
    $response['message']=$template;
    return new Response(json_encode($response));
                    }
    }
    }
}
