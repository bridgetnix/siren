<?php
namespace Siren\StatBundle\Controller;


            use Symfony\Bundle\FrameworkBundle\Controller\Controller;
            use Symfony\Component\HttpFoundation\JsonResponse;
            use Symfony\Component\HttpFoundation\Response;
            use Symfony\Component\HttpFoundation\Request;
            use src\Siren\AppBundle\Entity;
            use DateTime;
            use src\Siren\StatBundle\Service;


    
            class StatController extends Controller
            {
                public function statAction(){
                   
// récupération des données de la vue
                    $request = Request::createFromGlobals();
                    $request = $this->get("request");
                    $type=$request->query->get("type");
                    $name=$request->query->get("name");
                    $categorie=$request->query->get("categorie");
                    $debut=$request->query->get("debut");
                    $fin=$request->query->get("fin");
                    
//transformer la date
                    $dateDebut = new DateTime();
                    $dateFin = new DateTime();
                    
                    if ($debut != "") {
                        $dateDebut->setDate(substr($debut, strrpos($debut, "/") + 1), substr($debut, strpos($debut, "/") + 1, (strrpos($debut, "/") - strpos($debut, "/") - 1)), substr($debut, 0, strpos($debut, "/")));
                       
                    } else {
                        $dateDebut = NULL;
                    }

                    if ($fin != "") {
                        $dateFin->setDate(substr($fin, strrpos($fin, "/") + 1), substr($fin, strpos($fin, "/") + 1, (strrpos($fin, "/") - strpos($fin, "/") - 1)), substr($fin, 0, strpos($fin, "/")));
                    } else {
                        $dateFin = NULL;
                    }
                    
//charger les tableaux avec les données de la vue
                     $distinct= array();
                     $attribut= array('c.nom_fr');
                     
                     if($type=="patrouille"){
                         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
                         
                         $counDistinct= 'p';
                          $agregation= array();
                         
                     }elseif ($type=="observation") {
                         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
                         $agregation= array(array('COUNT','o'));
                          $counDistinct= array();
                         
                     }
                    
                     
                     $egalite=array();
                     for($i=0; $i< (count($categorie)); $i++){
                        $egal=array();
                        $egal =array('c.nom_fr', $categorie[$i]);
                        array_push($egalite, $egal);
                     }
                     $difference=array();
                     $contraire= array();
                     $inferieur=array();
                     $superieur=array();
                     $superieurEgale=array();
                     $inferieurEgale=array();
                     $in= array();
                     if($dateDebut != null AND $dateFin != null){
                         if ($type=="patrouille"){
                       $between=array('p.dateDebut',$dateDebut,$dateFin);
                         }
                         if($type=="observation"){
                            $between=array('o.dateAdd',$dateDebut,$dateFin); 
                         }
                     }elseif($dateDebut != null AND $dateFin == null){
                         $dateFin = date("Y-m-d H:i:s");
                          if ($type=="patrouille"){
                       $between=array('p.dateDebut',$dateDebut,$dateFin);
                         }
                         if($type=="observation"){
                            $between=array('o.dateAdd',$dateDebut,$dateFin); 
                         }  
                     }else{
                         $between= array();
                     }
                     $notBetween= array();
                     $like=array();
                     $isNull=array();
                     $isNotNull=array();
                     if(count($categorie) == 1 ){
                        $groupBy= array();
                     } else{
                         $groupBy= array('c.nom_fr');
                     }
                     $orderBy= array();
                     
//charger le service de construction des statistiques
                        $req= $this->container->get('Stat.select');
                        $SELECT=$req->partieSelect($distinct,$agregation,$attribut,$counDistinct);
                        $FROM =$req->partieFrom($from);
                        $JOIN=$req->partieJoin($from);
                        $WHERE=$req->partieWhere($egalite,$difference,$contraire,$inferieur,$superieur,$superieurEgale,$inferieurEgale,$in,$between,$notBetween
                     ,$like,$isNull,$isNotNull);
                        $GROUPBY=$req->partieGroupBy($groupBy);
                        $ORDERBY=$req->partieOrderBy($orderBy);
                       
// mettre le tout dans une variable représentant notre requete
                    $resultat = $SELECT.$FROM.$JOIN.$WHERE.$GROUPBY.$ORDERBY;
//                    echo $resultat;
//                    echo $date = date('Y-m-d H:i:s');
//                    echo $date->format('Y-m-d H:i:s');
// faire appel au service de la couche metier de symfony                    
                    $query = $this->getDoctrine()
                                  ->getManager()
                                  ->createQuery($resultat);
                    
//charger les paramètres nécessaires à l'execution de la requete
                    if(isset($egalite) AND !empty($egalite)){
                        if(count($egalite)>1){

                            $param= array();
                        for($i=0; $i< count($egalite); $i++){
                            $param['egalite'.$i] = $egalite[$i][1];  
                        }
                            $query->setParameters($param);
                        }  else {
                            $param= array();
                            $param['egalite'] = $egalite[count($egalite)-1][1];
                            $query->setParameters(array($param));

                        }

                    }
                    if(isset($difference) AND !empty($difference)){
                        if(count($difference)>1){

                            $param= array();
                        for($i=0; $i< count($difference); $i++){
                            $param['difference'.$i] = $difference[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['difference'] = $difference[count($difference)-1][1];
                        $query->setParameters(array($param));

                        }

                    }
                    if(isset($contraire) AND !empty($contraire)){
                        if(count($contraire)>1){

                            $param= array();
                        for($i=0; $i< count($contraire); $i++){
                            $param['contraire'.$i] = $contraire[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['contraire'] = $contraire[count($contraire)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($inferieur) AND !empty($inferieur)){
                        if(count($inferieur)>1){

                            $param= array();
                        for($i=0; $i< count($inferieur); $i++){
                            $param['inferieur'.$i] = $inferieur[$i][1];  
                         }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieur'] = $inferieur[count($inferieur)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($superieur) AND !empty($superieur)){
                        if(count($superieur)>1){

                            $param= array();
                        for($i=0; $i< count($superieur); $i++){
                            $param['superieur'.$i] = $superieur[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieur'] = $superieur[count($superieur)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     if(isset($superieurEgale) AND !empty($superieurEgale)){
                        if(count($superieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($superieurEgale); $i++){
                            $param['superieurEgale'.$i] = $superieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieurEgale'] = $superieurEgale[count($superieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                   }
                     if(isset($inferieurEgale) AND !empty($inferieurEgale)){
                        if(count($inferieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($inferieurEgale); $i++){
                            $param['inferieurEgale'.$i] = $inferieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieurEgale'] = $inferieurEgale[count($inferieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     
                     if (isset($between) AND !empty($between)){
                        $query->setParameter('between1', $between[1]);
                        $query->setParameter('between2', $between[2]);
                     }

                     if (isset($notBetween) AND !empty($notBetween)){
                        $query->setParameter('notbetween1', $notBetween[1]);
                        $query->setParameter('notbetween2', $notBetween[2]);
                     }
                     $couleur = array(
                         array("color"=>"#f56954"),
                         array("color"=>"#00a65a"),
                         array("color"=>"#f39c12"),
                         array("color"=>"#00c0ef"),
                         array("color"=>"#d2d6de")
                     );
                    
                    // ajout des couleur au tableau retourné par doctrine 
                    $resultats = $query->getArrayResult();
                    
//                    print_r($resultats);
//                    echo count($resultats);
                    $somme =0;
                    //fait la somme pour pourcentage
                    for($i=0; $i< count($resultats); $i++){
                        $somme += $resultats[$i]['nombre'];
                    }
                    for($i=0; $i< count($resultats); $i++){
                        $pourcentage[$i]= ($resultats[$i]['nombre']/$somme)*100;
                    }
                    
                     for($i=0;$i< count($resultats); $i++){
                        $resultats[$i]["color"]= $couleur[$i]["color"];
                        $resultats[$i]['pourcentage']= $pourcentage[$i];
                     }
                   
                    if (count($resultats)== 0){
                        $template= $this->render('SirenStatBundle:Stat:messageError.html.twig')->getContent();
                        $response = array();
                        $response['message']=$template;
                        return new Response(json_encode($response)); 
                    }else{
                    $template= $this->render('SirenStatBundle:Stat:affichage.html.twig', array( 'resultats' => $resultats))->getContent();
                    $response = array();
                    $response['message']=$template;
                   return new Response(json_encode($response));
                    }
                 
                }
                
                
                
                
                
                
                
                
                
     public function statplusAction() {
                    // récupération des données de la vue
                    $request = Request::createFromGlobals();
                    $request = $this->get("request");
                    $name=$request->query->get("name");
                    $type=$request->query->get("type");
                    $classe=$request->query->get("classe");
                    $categorie=$request->query->get("categorie");

//charger les tableaux avec les données de la vue
                     $distinct= array();
                     $attribut= array('c.nom_fr');
                     
                     if($type=="patrouille"){
                         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
                         $agregation= array(array('COUNT','p'));
                         
                     }elseif ($type=="observation") {
                         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
                         $agregation= array(array('COUNT','o'));
                         
                     }
                     $counDistinct= array();
                     $egalite=array();
                     for($i=0; $i< (count($categorie)); $i++){
                        $egal=array();
                        $egal =array('c.nom_fr', $categorie[$i]);
                        array_push($egalite, $egal);
                     }
                     $difference=array();
                     $contraire= array();
                     $inferieur=array();
                     $superieur=array();
                     $superieurEgale=array();
                     $inferieurEgale=array();
                     $in= array();
                     if ($type=="patrouille"){
                        $between[0]='p.dateDebut';
                    }
                    if($type=="observation"){
                            $between[0]='o.dateAdd'; 
                    }
                     //découpe des dates dans between à partir de 3 
                    $bet[0] = date("Y-m-d H:i:s");
                if($name == "Uneannee"){
                    $yearrrr= array();
                   for($i=0; $i<=12; $i++){
                        $j =$i+1;
                        $between[$j]= new DateTime($bet[$i].' last day of last month');
                        $bet[$j] = $between[$j]->format("Y-m-d H:i:s");
                    } 
                      
                                       
                     $notBetween= array();
                     $like=array();
                     $isNull=array();
                     $isNotNull=array();
                     if(count($categorie) == 1 ){
                        $groupBy= array();
                     } else{
                         $groupBy= array('c.nom_fr');
                     }
                     $orderBy= array();
                     
//charger le service de construction des statistiques
                        $req= $this->container->get('Stat.select');
                        $SELECT=$req->partieSelect($distinct,$agregation,$attribut,$counDistinct);
                        $FROM =$req->partieFrom($from);
                        $JOIN=$req->partieJoin($from);
                        $WHERE=$req->partieWhere($egalite,$difference,$contraire,$inferieur,$superieur,$superieurEgale,$inferieurEgale,$in,$between,$notBetween
                     ,$like,$isNull,$isNotNull);
                        $GROUPBY=$req->partieGroupBy($groupBy);
                        $ORDERBY=$req->partieOrderBy($orderBy);
                       
// mettre le tout dans une variable représentant notre requete
                    $resultat = $SELECT.$FROM.$JOIN.$WHERE.$GROUPBY.$ORDERBY;                    
 
                     
for($i=1; $i<=12; $i++){
        $j=$i+1;
        $query = $this->getDoctrine()
                      ->getManager()
                      ->createQuery($resultat);
    //charger les paramètres nécessaires à l'execution de la requete
                    if(isset($egalite) AND !empty($egalite)){
                        if(count($egalite)>1){

                            $param= array();
                        for($i=0; $i< count($egalite); $i++){
                            $param['egalite'.$i] = $egalite[$i][1];  
                        }
                            $query->setParameters($param);
                        }  else {
                            $param= array();
                            $param['egalite'] = $egalite[count($egalite)-1][1];
                            $query->setParameters(array($param));

                        }

                    }
                    if(isset($difference) AND !empty($difference)){
                        if(count($difference)>1){

                            $param= array();
                        for($i=0; $i< count($difference); $i++){
                            $param['difference'.$i] = $difference[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['difference'] = $difference[count($difference)-1][1];
                        $query->setParameters(array($param));

                        }

                    }
                    if(isset($contraire) AND !empty($contraire)){
                        if(count($contraire)>1){

                            $param= array();
                        for($i=0; $i< count($contraire); $i++){
                            $param['contraire'.$i] = $contraire[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['contraire'] = $contraire[count($contraire)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($inferieur) AND !empty($inferieur)){
                        if(count($inferieur)>1){

                            $param= array();
                        for($i=0; $i< count($inferieur); $i++){
                            $param['inferieur'.$i] = $inferieur[$i][1];  
                         }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieur'] = $inferieur[count($inferieur)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($superieur) AND !empty($superieur)){
                        if(count($superieur)>1){

                            $param= array();
                        for($i=0; $i< count($superieur); $i++){
                            $param['superieur'.$i] = $superieur[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieur'] = $superieur[count($superieur)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     if(isset($superieurEgale) AND !empty($superieurEgale)){
                        if(count($superieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($superieurEgale); $i++){
                            $param['superieurEgale'.$i] = $superieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieurEgale'] = $superieurEgale[count($superieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                   }
                     if(isset($inferieurEgale) AND !empty($inferieurEgale)){
                        if(count($inferieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($inferieurEgale); $i++){
                            $param['inferieurEgale'.$i] = $inferieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieurEgale'] = $inferieurEgale[count($inferieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     
                     if (isset($between) AND !empty($between)){
                        $query->setParameter('between1', $between[$j]);
                        $query->setParameter('between2', $between[$i]);
                     }

                     if (isset($notBetween) AND !empty($notBetween)){
                        $query->setParameter('notbetween1', $notBetween[1]);
                        $query->setParameter('notbetween2', $notBetween[2]);
                     }
    
    $res[$i] = $query->getArrayResult();  
    

}
$moi = array("janvier", "fevrier", "mars", "avril", "mai", "juin", "juillet", "aout", "septembre", "novembre", "octobre", "decembre");
//print_r($res); echo ' je suis l\'entrée';
// appel au service StatPrevision
                    $pre= $this->container->get('Stat.prevision');
                   
                        $preAnnee= $pre->moindreCareAnnee($classe,$res);
//                        print_r($preAnnee);
                    for($i = 0; $i< count($preAnnee); $i++){
                        $preAnnee[$i]['prevision']=round($preAnnee[$i]['prevision']);
                        $preAnnee[$i]['moi']= $moi[$i]; 
                    }
                    $template=$this->render('SirenStatBundle:Stat:prevision.html.twig', array( 'resultats' => $preAnnee))->getContent();
                    $response = array();
                    $response['message']=$template;
                    return new Response(json_encode($response));
        }elseif($name=="Troismois"){
                        
                     for($i=0; $i<=12; $i++){
                        $j =$i+1;
                        $between[$j]= new DateTime($bet[$i].' -7 day');
                        $bet[$j] = $between[$j]->format("Y-m-d H:i:s");
                    } 
                    
//                    print_r($between);                  
                     $notBetween= array();
                     $like=array();
                     $isNull=array();
                     $isNotNull=array();
                     if(count($categorie) == 1 ){
                        $groupBy= array();
                     } else{
                         $groupBy= array('c.nom_fr');
                     }
                     $orderBy= array();
                     
//charger le service de construction des statistiques
                        $req= $this->container->get('Stat.select');
                        $SELECT=$req->partieSelect($distinct,$agregation,$attribut,$counDistinct);
                        $FROM =$req->partieFrom($from);
                        $JOIN=$req->partieJoin($from);
                        $WHERE=$req->partieWhere($egalite,$difference,$contraire,$inferieur,$superieur,$superieurEgale,$inferieurEgale,$in,$between,$notBetween
                     ,$like,$isNull,$isNotNull);
                        $GROUPBY=$req->partieGroupBy($groupBy);
                        $ORDERBY=$req->partieOrderBy($orderBy);
                       
// mettre le tout dans une variable représentant notre requete
                    $resultat = $SELECT.$FROM.$JOIN.$WHERE.$GROUPBY.$ORDERBY;                    
 
                     
for($i=1; $i<=12; $i++){
        $j=$i+1;
        $query = $this->getDoctrine()
                      ->getManager()
                      ->createQuery($resultat);
    //charger les paramètres nécessaires à l'execution de la requete
                    if(isset($egalite) AND !empty($egalite)){
                        if(count($egalite)>1){

                            $param= array();
                        for($i=0; $i< count($egalite); $i++){
                            $param['egalite'.$i] = $egalite[$i][1];  
                        }
                            $query->setParameters($param);
                        }  else {
                            $param= array();
                            $param['egalite'] = $egalite[count($egalite)-1][1];
                            $query->setParameters(array($param));

                        }

                    }
                    if(isset($difference) AND !empty($difference)){
                        if(count($difference)>1){

                            $param= array();
                        for($i=0; $i< count($difference); $i++){
                            $param['difference'.$i] = $difference[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['difference'] = $difference[count($difference)-1][1];
                        $query->setParameters(array($param));

                        }

                    }
                    if(isset($contraire) AND !empty($contraire)){
                        if(count($contraire)>1){

                            $param= array();
                        for($i=0; $i< count($contraire); $i++){
                            $param['contraire'.$i] = $contraire[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['contraire'] = $contraire[count($contraire)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($inferieur) AND !empty($inferieur)){
                        if(count($inferieur)>1){

                            $param= array();
                        for($i=0; $i< count($inferieur); $i++){
                            $param['inferieur'.$i] = $inferieur[$i][1];  
                         }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieur'] = $inferieur[count($inferieur)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($superieur) AND !empty($superieur)){
                        if(count($superieur)>1){

                            $param= array();
                        for($i=0; $i< count($superieur); $i++){
                            $param['superieur'.$i] = $superieur[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieur'] = $superieur[count($superieur)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     if(isset($superieurEgale) AND !empty($superieurEgale)){
                        if(count($superieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($superieurEgale); $i++){
                            $param['superieurEgale'.$i] = $superieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieurEgale'] = $superieurEgale[count($superieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                   }
                     if(isset($inferieurEgale) AND !empty($inferieurEgale)){
                        if(count($inferieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($inferieurEgale); $i++){
                            $param['inferieurEgale'.$i] = $inferieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieurEgale'] = $inferieurEgale[count($inferieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     
                     if (isset($between) AND !empty($between)){
                        $query->setParameter('between1', $between[$j]);
                        $query->setParameter('between2', $between[$i]);
                     }

                     if (isset($notBetween) AND !empty($notBetween)){
                        $query->setParameter('notbetween1', $notBetween[1]);
                        $query->setParameter('notbetween2', $notBetween[2]);
                     }
    
    $res[$i] = $query->getArrayResult();  
    

}
$moi = array("semaine1", "semaine 2", "semaine 3", "semaine 4", "semaine 5", "semaine 6", "semaine 7", "semaine 8", "semaine 9", "semaine 10", "semaine 11", "semaine 12");
//print_r($res); echo ' je suis l\'entrée';
// appel au service StatPrevision
                    $pre= $this->container->get('Stat.prevision');
                    
                        $preAnnee= $pre->moindreCareAnnee($classe,$res);
                    
                    for($i = 0; $i< count($preAnnee); $i++){
                        $preAnnee[$i]['prevision']=round($preAnnee[$i]['prevision']);
                        $preAnnee[$i]['moi']= $moi[$i]; 
                    }
//                    print_r($preAnnee);
                    $template=$this->render('SirenStatBundle:Stat:prevision.html.twig', array( 'resultats' => $preAnnee))->getContent();
                    $response = array();
                    $response['message']=$template;
                    return new Response(json_encode($response));
                }

                    
                }
                
               
                
                
                
                
            public function statcompaAction() {
                // récupération des données de la vue
                    $request = Request::createFromGlobals();
                    $request = $this->get("request");
                    $type=$request->query->get("type");
                    $categorie=$request->query->get("categorie");
                    $debut1=$request->query->get("debut1");
                    $fin1=$request->query->get("fin1");
                    $debut2=$request->query->get("debut2");
                    $fin2=$request->query->get("fin2");
                    
                    
//transformer la date
                    $dateDebut1 = new DateTime();
                    $dateFin1 = new DateTime();
                    $dateDebut2 = new DateTime();
                    $dateFin2 = new DateTime();
                    
                    
                    if ($debut1 != "") {
                        $dateDebut1->setDate(substr($debut1, strrpos($debut1, "/") + 1), substr($debut1, strpos($debut1, "/") + 1, (strrpos($debut1, "/") - strpos($debut1, "/") - 1)), substr($debut1, 0, strpos($debut1, "/")));
                       
                    } else {
                        $bet = date("Y-m-d H:i:s");
                        $dateDebut1 = new DateTime($bet.' last day of last month');
                        $bet = $dateDebut1->format("Y-m-d H:i:s");
                    }

                    if ($fin1 != "") {
                        $dateFin1->setDate(substr($fin1, strrpos($fin1, "/") + 1), substr($fin1, strpos($fin1, "/") + 1, (strrpos($fin1, "/") - strpos($fin1, "/") - 1)), substr($fin1, 0, strpos($fin1, "/")));
                    } else {
                        $dateFin1 = new DateTime($bet.' last day of last month');
                        $bet = $dateFin1->format("Y-m-d H:i:s");
                    }

                    if ($debut2 != "") {
                        $dateDebut2->setDate(substr($debut2, strrpos($debut2, "/") + 1), substr($debut2, strpos($debut2, "/") + 1, (strrpos($debut2, "/") - strpos($debut2, "/") - 1)), substr($debut2, 0, strpos($debut2, "/")));
                       
                    } else {
                        $dateDebut2 = new DateTime($bet.' last day of last month');
                        $bet = $dateDebut2->format("Y-m-d H:i:s");
                    }

                    if ($fin2 != "") {
                        $dateFin2->setDate(substr($fin2, strrpos($fin2, "/") + 1), substr($fin2, strpos($fin2, "/") + 1, (strrpos($fin2, "/") - strpos($fin2, "/") - 1)), substr($fin2, 0, strpos($fin2, "/")));
                    } else {
                        $dateFin2 = new DateTime($bet.' last day of last month');
                    }

//charger les tableaux avec les données de la vue
                     $distinct= array();
                     $attribut= array('c.nom_fr');
                     
                     if($type=="patrouille"){
                         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
                         $agregation= array(array('COUNT','p'));
                         
                     }elseif ($type=="observation") {
                         $from = array(array('SirenAppBundle:Observation', 'o'),array('categorie', 'c'),array('patrouille', 'p'));
                         $agregation= array(array('COUNT','o'));
                         
                     }
                     $counDistinct= array();
                     $egalite=array();
                     for($i=0; $i< (count($categorie)); $i++){
                        $egal=array();
                        $egal =array('c.nom_fr', $categorie[$i]);
                        array_push($egalite, $egal);
                     }
                     $difference=array();
                     $contraire= array();
                     $inferieur=array();
                     $superieur=array();
                     $superieurEgale=array();
                     $inferieurEgale=array();
                     $in= array();
                     if ($type=="patrouille"){
                        $between =array('p.dateDebut', $dateDebut1, $dateFin1, $dateDebut2, $dateFin2);
                    }
                    if($type=="observation"){
                        $between =array('o.dateAdd', $dateDebut1, $dateFin1, $dateDebut2, $dateFin2);
                        
                    }
//                    print_r($between);
                     $notBetween= array();
                     $like=array();
                     $isNull=array();
                     $isNotNull=array();
                     if(count($categorie) == 1 ){
                        $groupBy= array();
                     } else{
                         $groupBy= array('c.nom_fr');
                     }
                     $orderBy= array();
                     
//charger le service de construction des statistiques
                        $req= $this->container->get('Stat.select');
                        $SELECT=$req->partieSelect($distinct,$agregation,$attribut,$counDistinct);
                        $FROM =$req->partieFrom($from);
                        $JOIN=$req->partieJoin($from);
                        $WHERE=$req->partieWhere($egalite,$difference,$contraire,$inferieur,$superieur,$superieurEgale,$inferieurEgale,$in,$between,$notBetween
                     ,$like,$isNull,$isNotNull);
                        $GROUPBY=$req->partieGroupBy($groupBy);
                        $ORDERBY=$req->partieOrderBy($orderBy);
                       
// mettre le tout dans une variable représentant notre requete
                  $resultat = $SELECT.$FROM.$JOIN.$WHERE.$GROUPBY.$ORDERBY; 
//                    echo $resultat;
 
                     
for($i=0; $i< 2; $i++){
    $j=2;
    $k=1;
        $query = $this->getDoctrine()
                      ->getManager()
                      ->createQuery($resultat);
    //charger les paramètres nécessaires à l'execution de la requete
                    if(isset($egalite) AND !empty($egalite)){
                        if(count($egalite)>1){

                            $param= array();
                        for($i=0; $i< count($egalite); $i++){
                            $param['egalite'.$i] = $egalite[$i][1];  
                        }
                            $query->setParameters($param);
                        }  else {
                            $param= array();
                            $param['egalite'] = $egalite[count($egalite)-1][1];
                            $query->setParameters(array($param));

                        }

                    }
                    if(isset($difference) AND !empty($difference)){
                        if(count($difference)>1){

                            $param= array();
                        for($i=0; $i< count($difference); $i++){
                            $param['difference'.$i] = $difference[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['difference'] = $difference[count($difference)-1][1];
                        $query->setParameters(array($param));

                        }

                    }
                    if(isset($contraire) AND !empty($contraire)){
                        if(count($contraire)>1){

                            $param= array();
                        for($i=0; $i< count($contraire); $i++){
                            $param['contraire'.$i] = $contraire[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['contraire'] = $contraire[count($contraire)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($inferieur) AND !empty($inferieur)){
                        if(count($inferieur)>1){

                            $param= array();
                        for($i=0; $i< count($inferieur); $i++){
                            $param['inferieur'.$i] = $inferieur[$i][1];  
                         }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieur'] = $inferieur[count($inferieur)-1][1];
                        $query->setParameters(array($param));

                        }
                     }
                    if(isset($superieur) AND !empty($superieur)){
                        if(count($superieur)>1){

                            $param= array();
                        for($i=0; $i< count($superieur); $i++){
                            $param['superieur'.$i] = $superieur[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieur'] = $superieur[count($superieur)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     if(isset($superieurEgale) AND !empty($superieurEgale)){
                        if(count($superieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($superieurEgale); $i++){
                            $param['superieurEgale'.$i] = $superieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['superieurEgale'] = $superieurEgale[count($superieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                   }
                     if(isset($inferieurEgale) AND !empty($inferieurEgale)){
                        if(count($inferieurEgale)>1){

                            $param= array();
                        for($i=0; $i< count($inferieurEgale); $i++){
                            $param['inferieurEgale'.$i] = $inferieurEgale[$i][1];  
                        }
                        $query->setParameters($param);
                        }  else {
                        $param= array();
                        $param['inferieurEgale'] = $inferieurEgale[count($inferieurEgale)-1][1];
                        $query->setParameters(array($param));

                     }
                    }
                     
                     if (isset($between) AND !empty($between)){
                        $query->setParameter('between1', $between[$j]);
                        $query->setParameter('between2', $between[$k]);
                     }

                     if (isset($notBetween) AND !empty($notBetween)){
                        $query->setParameter('notbetween1', $notBetween[1]);
                        $query->setParameter('notbetween2', $notBetween[2]);
                     }
    
    $res[$i] = $query->getArrayResult();  
//    echo 'je suis dans la boucle';
     $j+=2;
    $k+=2;
   
}
    for($i=0; $i<count($res[0]); $i++){
        $res[0][$i]['nombre1']=$res[0][$i]['nombre'];
        
    }
        for($i=0; $i<count($res[1]); $i++){
        $res[1][$i]['nombre2']=$res[1][$i]['nombre'];
        
    }
    array_merge($res[0], $res[1]);
//    print_r($res[0]);

                    $template=$this->render('SirenStatBundle:Stat:compare.html.twig')->getContent();
                    
                    $response = array();
                    $response['message']=$template;
                    return new Response(json_encode($response));
                    
                }
                
            
                
  }