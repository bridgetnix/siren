
$(document).ready(function () {
   
    $('#form').submit(function(e){
        e.preventDefault();
        var categorie= [];
        var type = $("input[name='type']:checked").val();
        $('#stat_nids').each(function(){
            if($(this).is(":checked")){
                categorie.push($(this).val());
            }
        });
        $('#stat_traces').each(function(){
            if($(this).is(":checked")){
                categorie.push($(this).val());
            }
        });
        $('#stat_vivants').each(function(){
            if($(this).is(":checked")){
                categorie.push($(this).val());
            }
        });
        $('#stat_caracasses').each(function(){
            if($(this).is(":checked")){
                categorie.push($(this).val());
            }
        });
        var debut = $("#datedebut").val();
        var fin = $("#datefin").val();
        var data = {
                type: type,
                categorie: categorie,
                debut: debut,
                fin: fin
            };
            
            
            console.log(data);
            $.ajax({
                
                type: "GET",
                dataType: 'json',
                data: data,  
                url: $('#url').val(),
                success: function(donnee){
                    $('#afficher').html(donnee.message);  
                }
             });

             
            });
          
        });
