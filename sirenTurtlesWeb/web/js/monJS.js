function estDansTableau(valeur, tableau){
	var c = 0;
	for(var i = 0; i < tableau.length; i++){
		if(valeur.match(tableau[i])){
			c++;
		}
	}
	if(c == 0)
		return false;
	else return true;
}

function veriForm(selecteur){
	for(var i = 0; i < selecteur.length ; i++){
		if(selecteur[i].val()== "")
			alert("Veuillez renseigner tout les champs en rouge!");
	}
}

function lastClass(classes){
	var classesNew = [];
	classes = classes.split(' ');
	for (var i = 0, c = classes.length; i < c; i++) {
		if (classes[i]) {
			classesNew.push(classes[i]);
		}
	}
	return classesNew[classes.length-1];
}